package de.nilgmbh.energyhyperheuristic.strategyfactory;

import de.nilgmbh.energyhyperheuristic.util.Neighborhood;


/**
 * Generates the Strategies that can be used by the HH and determines their neighborhood-relations
 * 
 * Must ensure that a starting Strategy exists, which will generate a feasible solution under any 
 * circumstances
 *
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public abstract class StrategyFactory {

	public abstract Neighborhood createNeighbourhood(double maxEnergyLevel);
	
}

package de.nilgmbh.energyhyperheuristic.strategyfactory.constraints;

import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.nilgmbh.energyhyperheuristic.dispatching.Constraint;
import de.nilgmbh.energyhyperheuristic.dispatching.PointInTime;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class IsStockAvailable extends Constraint {

	public IsStockAvailable(String name) {
		super(name, true);
	}

	@Override
	public double calculateValue(ProductionPlanEntry entry) {
		double result = 0.0;
		int i = 0;
		for (Product inputProduct : entry.getOperation().getInputProducts()){
			i++;
			double demand = entry.getOperationCount() * entry.getOperation().getInputPiecesPerOperation(inputProduct);
			
			if (demand > PointInTime.getMinStocks().get(inputProduct)){
				if (demand > PointInTime.getFinalStocks().get(inputProduct)){
					result += 9999.0;
				} else {
					//TODO: Adding might be possible, further investigations necessary
					result += 9999.0;
				}
			} else {	
				result += 1 - demand / (PointInTime.getMinStocks().get(inputProduct));
			}
		}
		// if no inputs are required, 0.0 is returned, otherwise the average result over all inputs is calculated
		return i==0? 0.0 : result/i;
	}
		
}

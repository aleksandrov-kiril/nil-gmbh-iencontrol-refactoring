package de.nilgmbh.energyhyperheuristic.strategyfactory.constraints;

import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.nilgmbh.energyhyperheuristic.dispatching.Constraint;
/**
 * raises score of producing several lots at once if possible
 *
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class LongerDuration extends Constraint {

	public LongerDuration(String name) {
		super(name, false);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculateValue(ProductionPlanEntry entry) {
		int lotsize = entry.getOperation().getLotSize();
		double lots = entry.getOperationCount()/lotsize;
		return lots == 0.0 ? 1.0 : 1.0/Math.ceil(lots);
	}

}

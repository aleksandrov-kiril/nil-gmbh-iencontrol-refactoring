package de.nilgmbh.energyhyperheuristic.strategyfactory.constraints;

import java.util.Collections;
import java.util.NavigableMap;
import java.util.SortedMap;

import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.nilgmbh.energyhyperheuristic.dispatching.Constraint;
import de.nilgmbh.energyhyperheuristic.dispatching.PointInTime;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class MaxEnergyExceeded extends Constraint {

	private double maxEnergyLevel;
	
	public MaxEnergyExceeded(String name, double maxEnergyLevel) {
		super(name, false);
		this.maxEnergyLevel = maxEnergyLevel;
	}

	@Override
	public double calculateValue(ProductionPlanEntry entry) {
		boolean result = false;
		double entryMax = entry.getEntryEnergyProfile().getMaximum();
		//Check if the overall Maximum is exceeded
		if(PointInTime.getMaxEnergyLevel() + entryMax > maxEnergyLevel){
			//Check if Maximum is exceeded during the Entry's duration only
			NavigableMap<Double, Double> profile = PointInTime.getEnergyLevel();
			if (profile.size() > 0){
				double start = profile.floorKey(PointInTime.getTime().getT());
				Double end = profile.ceilingKey(PointInTime.getTime().getT() + entry.getDuration());
				if (end == null){
					//entry is longer then the current profile
					end = profile.lastKey();
				} else {
					SortedMap<Double, Double> criticalMap = profile.subMap(start, end);
					if (Collections.max(criticalMap.values()) + entryMax <= maxEnergyLevel){
						result = true;
					} else if(Collections.min(criticalMap.values()) + entryMax > maxEnergyLevel) {
						result = false;
					} else {
						//Entry might fit blockwise
						//TODO: Play Tetris here!
						result = false;
					}
				}
			} else {
				result = true;
			}
		} else {
			result = true;
		}
		
		if (result == true){
			return 0.0;
		} else {
			return 999.0;
		}
	}

}

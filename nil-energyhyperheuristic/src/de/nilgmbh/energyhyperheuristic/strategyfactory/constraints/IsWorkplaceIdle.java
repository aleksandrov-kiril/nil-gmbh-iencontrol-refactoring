package de.nilgmbh.energyhyperheuristic.strategyfactory.constraints;

import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.nilgmbh.energyhyperheuristic.dispatching.Constraint;
import de.nilgmbh.energyhyperheuristic.dispatching.PointInTime;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class IsWorkplaceIdle extends Constraint {

	public IsWorkplaceIdle(String name) {
		super(name, true);
	}

	@Override
	public double calculateValue(ProductionPlanEntry entry) {
		double result;
		double idleTime = PointInTime.getIdleTimes().get(entry.getWorkplace());
		result = (idleTime == 0.0) ? 999.0 : (entry.getDuration()/idleTime);
		return result;
	}

}

package de.nilgmbh.energyhyperheuristic.strategyfactory.constraints;

import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.Time;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.nilgmbh.energyhyperheuristic.dispatching.Constraint;
import de.nilgmbh.energyhyperheuristic.dispatching.PointInTime;


/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class ShiftRespected extends Constraint {

	private ShiftSchedule schedule;
	
	public ShiftRespected(String name, ShiftSchedule schedule) {
		super(name, true);
		this.schedule = schedule;
	}

	@Override
	public double calculateValue(ProductionPlanEntry entry) {
		double result;
		Time time = PointInTime.getTime();
		double remainingTime = schedule.getOffPeriods().getRemainingWorkingTime(time);
		double duration = entry.getDuration();
		result =  duration/remainingTime;
		return result;
	}

}

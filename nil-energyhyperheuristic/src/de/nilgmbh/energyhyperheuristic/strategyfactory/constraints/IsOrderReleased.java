package de.nilgmbh.energyhyperheuristic.strategyfactory.constraints;

import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.nilgmbh.energyhyperheuristic.dispatching.Constraint;
import de.nilgmbh.energyhyperheuristic.dispatching.PointInTime;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class IsOrderReleased extends Constraint {

	public IsOrderReleased(String name) {
		super(name, true);
	}

	@Override
	public double calculateValue(ProductionPlanEntry entry) {
		double result = Double.MAX_VALUE;
		if (entry.getOrder() != null && 
				entry.getOrder().getStartDate().before(PointInTime.getTime().getNow())){
			result = 0.0;
		}
		return result;
	}

}

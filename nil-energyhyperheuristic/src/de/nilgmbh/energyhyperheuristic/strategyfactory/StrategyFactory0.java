package de.nilgmbh.energyhyperheuristic.strategyfactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.nilgmbh.energyhyperheuristic.dispatching.Constraint;
import de.nilgmbh.energyhyperheuristic.dispatching.Strategy;
import de.nilgmbh.energyhyperheuristic.strategyfactory.constraints.IsOrderReleased;
import de.nilgmbh.energyhyperheuristic.strategyfactory.constraints.IsStockAvailable;
import de.nilgmbh.energyhyperheuristic.strategyfactory.constraints.IsWorkplaceIdle;
import de.nilgmbh.energyhyperheuristic.strategyfactory.constraints.LongerDuration;
import de.nilgmbh.energyhyperheuristic.strategyfactory.constraints.MaxEnergyExceeded;
import de.nilgmbh.energyhyperheuristic.strategyfactory.constraints.ShiftRespected;
import de.nilgmbh.energyhyperheuristic.util.Neighborhood;

/**
 * 
 * 			all Strategies
 * 			24-7		24-5		13-5
 *			DE	DEF	-----	2	-----	4
 *				|			|			|
 *			EN	1	-----	3	-----	5
 * 
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class StrategyFactory0 extends StrategyFactory {

	// 			24-7		24-5		13-5
	// 		DE	DEF			2			4
	//		EN	1			3			5
	
	@Override
	public Neighborhood createNeighbourhood(double maxEnergyLevel) {
		Neighborhood result;
		Map<Constraint, Double> constraints;
		
		Factory factory = Project.getCurrentProject().getOriginalFactory();
		
		// CONSTRAINTS
		Constraint stock  = new IsStockAvailable("stock");
		Constraint release = new IsOrderReleased("release");
		Constraint idle = new IsWorkplaceIdle("idle");
		Constraint longer = new LongerDuration("longer");
		Constraint energy = new MaxEnergyExceeded("energy", maxEnergyLevel);
		
		List<Constraint> shiftConstraints = new ArrayList<Constraint>(factory.getShiftplans().size());
		for(ShiftSchedule schedule : factory.getShiftplans()){
			shiftConstraints.add(new ShiftRespected("Shift_" + schedule.getName(), schedule));
		}
		
		//Create default Strategy - must guarantee for the dispatching heuristic to terminate
		constraints = new HashMap<Constraint, Double>();
		constraints.put(stock, 50.0);
		constraints.put(release, 50.0);		
		constraints.put(idle, 50.0);
		constraints.put(longer, 500.0);
		constraints.put(shiftConstraints.get(0), 50.0);
		Strategy defaultStrategy = new Strategy("DE " + shiftConstraints.get(0).toString(), constraints);
		result = new Neighborhood(defaultStrategy);
		
		constraints = new HashMap<Constraint, Double>();
		constraints.put(stock, 50.0);
		constraints.put(release, 50.0);		
		constraints.put(idle, 50.0);
		constraints.put(longer, 500.0);
		constraints.put(energy, 50.0);
		constraints.put(shiftConstraints.get(0), 50.0);
		Strategy strategy1 = new Strategy("EN " + shiftConstraints.get(0).toString(), constraints);
		result.add(strategy1);
		
		constraints = new HashMap<Constraint, Double>();
		constraints.put(stock, 50.0);
		constraints.put(release, 50.0);		
		constraints.put(idle, 50.0);
		constraints.put(longer, 500.0);
		constraints.put(shiftConstraints.get(1), 50.0);
		Strategy strategy2 = new Strategy("DE " + shiftConstraints.get(1).toString(), constraints);
		result.add(strategy2);
		
		constraints = new HashMap<Constraint, Double>();
		constraints.put(stock, 50.0);
		constraints.put(release, 50.0);		
		constraints.put(idle, 50.0);
		constraints.put(longer, 500.0);
		constraints.put(energy, 50.0);
		constraints.put(shiftConstraints.get(1), 50.0);
		Strategy strategy3 = new Strategy("EN " + shiftConstraints.get(1).toString(), constraints);
		result.add(strategy3);

		constraints = new HashMap<Constraint, Double>();
		constraints.put(stock, 50.0);
		constraints.put(release, 50.0);		
		constraints.put(idle, 50.0);
		constraints.put(longer, 500.0);
		constraints.put(shiftConstraints.get(2), 50.0);
		Strategy strategy4 = new Strategy("DE " + shiftConstraints.get(2).toString(), constraints);
		result.add(strategy4);
		
		constraints = new HashMap<Constraint, Double>();
		constraints.put(stock, 50.0);
		constraints.put(release, 50.0);		
		constraints.put(idle, 50.0);
		constraints.put(longer, 500.0);
		constraints.put(energy, 50.0);
		constraints.put(shiftConstraints.get(2), 50.0);
		Strategy strategy5 = new Strategy("EN " + shiftConstraints.get(2).toString(), constraints);
		result.add(strategy5);
		
		
		// 			24-7		24-5		13-5
		// 		DE	DEF	-----	2	-----	4
		//			|			|			|
		//		EN	1	-----	3	-----	5
		
		Set<Strategy> neighbors = new HashSet<Strategy>();
		neighbors.add(strategy2);
		neighbors.add(strategy1);
		result.setNeighbors(strategy3, neighbors);
		result.setNeighbors(defaultStrategy, neighbors);
		
		neighbors = new HashSet<Strategy>();
		neighbors.add(strategy3);
		neighbors.add(strategy4);
		result.setNeighbors(strategy2, neighbors);
		result.setNeighbors(strategy5, neighbors);
		
		return result;
	}
}

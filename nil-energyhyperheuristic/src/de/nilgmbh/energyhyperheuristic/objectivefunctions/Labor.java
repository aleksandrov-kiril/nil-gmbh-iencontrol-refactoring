package de.nilgmbh.energyhyperheuristic.objectivefunctions;

import java.util.Calendar;

import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Time;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;


/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class Labor {
	private static final double STANDARD_COST_PER_HOUR = 45;
	private static final double COST_NIGHTSHIFT = 45 * 1.25;
	private static final double COST_NON_WORKING = 45;
	private static final double COST_NON_WORKING_DAY = 10000;
	
	private static final double BEGIN_NIGHTSHIFT = 22 * 3600;
	private static final double END_NIGHTSHIFT = 07 * 3600;
	
	private static final double BEGIN_NON_WORKING = 14 * 3600;
	private static final double END_NON_WORKING = 15 * 3600;
	
	private static final int[] AVOIDED_WEEKDAYS = {Calendar.SATURDAY, Calendar.SUNDAY};
	
	public static double calculateLaborCost(ProductionPlanEntry	entry, Time time){
		double result = 0;
		InternalOperation io = (InternalOperation) entry.getOperation();
		double mmf = io.getMmf();
		
		
		Time workingTime = new Time(time);
		double duration = entry.getDuration();
		
		while (duration > 0){
			if (isInNonWorkingDay(workingTime)){
				double delta = workingTime.getLastMidnight() + 24*3600 - workingTime.getT();
				delta = (delta >= duration)? duration : delta; 
				result += delta / 3600 * COST_NON_WORKING_DAY * mmf;
				workingTime.moveT(delta);
				duration -= delta;
			} else {
				if (isInNightshift(workingTime.getDaytime())){
					double delta = END_NIGHTSHIFT - workingTime.getDaytime();
					delta = (delta < 0)? 3600*24 - delta : delta; //Tagessprung
					delta = (delta >= duration)? duration : delta; 
					result += delta / 3600 * COST_NIGHTSHIFT * mmf;
					workingTime.moveT(delta);
					duration -= delta;
				} else if(isInNonWorking(workingTime.getDaytime())){
					double delta = END_NON_WORKING - workingTime.getDaytime();
					delta = (delta >= duration)? duration : delta; 
					result += delta / 3600 * COST_NON_WORKING * mmf;
					workingTime.moveT(delta);
					duration -= delta;
				} else {
					double delta1 = BEGIN_NON_WORKING - workingTime.getDaytime();
					double delta2 = BEGIN_NIGHTSHIFT - workingTime.getDaytime();
					double delta = (delta1 > 0 && delta1 < delta2)? delta1 : delta2;
					delta = (delta >= duration)? duration : delta; 
					result += delta / 3600 * STANDARD_COST_PER_HOUR * mmf;
					workingTime.moveT(delta);
					duration -= delta;
				}
			}
		}
		
		return result;
	}
	
	private static boolean isInNightshift(double daytime){
		return (daytime >= BEGIN_NIGHTSHIFT) || (daytime < END_NIGHTSHIFT);
	}
	
	private static boolean isInNonWorking(double daytime){
		return (daytime >= BEGIN_NON_WORKING) && (daytime < END_NON_WORKING);
	}
	
	private static boolean isInNonWorkingDay(Time time){
		boolean result = false;
		for(int d : AVOIDED_WEEKDAYS){
			result = result ? result : time.getNow().get(Calendar.DAY_OF_WEEK) == d;
		}
		return result;
	}
}

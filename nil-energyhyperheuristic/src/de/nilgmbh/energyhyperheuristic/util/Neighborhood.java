package de.nilgmbh.energyhyperheuristic.util;

import java.util.HashSet;
import java.util.Set;

import de.nilgmbh.energyhyperheuristic.dispatching.Strategy;

public class Neighborhood {

	private Strategy start;
	private Set<Strategy> nodes;
	
	public Neighborhood(Strategy defaultStrategy){
		this.nodes = new HashSet<Strategy>();
		this.start = defaultStrategy;
		nodes.add(start);
	}

	public Strategy getStart() {			
		return start;
	}
	
	public void add(Strategy strategy){
		nodes.add(strategy);
	}
	
	/**
	 * Set neighbors as Neighbors of node and node as neighbor of neighbors.
	 * Ignores nodes that are not part of this Neighborhood.
	 * 
	 * Does not overwrite preexisting neighbors.
	 * @param node Node containing a Strategy, for which adjacent nodes shall be defined.
	 * @param neighbors Set of nodes that should be adjacent to node
	 */
	/**
	 * @param node
	 * @param neighbors
	 */
	public void setNeighbors(Strategy node, Set<Strategy> neighbors){
		if(nodes.contains(node)){
			for(Strategy neighbor : neighbors){
				if(nodes.contains(neighbor)){
					node.getNeighbors().add(neighbor);
					neighbor.getNeighbors().add(node);
				}
			}
		}
	}

	
}

package de.nilgmbh.energyhyperheuristic.hyper;

import java.util.HashMap;
import java.util.Map;

import de.fzi.wenpro.core.model.Cost;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.Time;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.core.model.productionplan.Schedule;
import de.nilgmbh.energyhyperheuristic.dismantling.SchedulableTask;
import de.nilgmbh.energyhyperheuristic.dismantling.TaskSet;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class ObjectiveFunction {
	
	private final double punishingLateness = 10;
	private final double punishingEnergy = 1000;
	private double maxEnergyLevel = Double.MAX_VALUE;
	private Map<FactoryNode, Map<ShiftSchedule, Cost>> costs;
	
	public ObjectiveFunction(Factory factory, double maxEnergyLevel) {
		this.maxEnergyLevel = maxEnergyLevel;
		costs = new HashMap<FactoryNode, Map<ShiftSchedule,Cost>>();
		for(FactoryNode factoryNode : factory.getSubNodes()){
			Map<ShiftSchedule, Cost> map = new HashMap<ShiftSchedule, Cost>();
			for(ShiftSchedule shiftSchedule : factory.getShiftplans()){
				map.put(shiftSchedule, factoryNode.getCost(shiftSchedule.getName()));
			}
			costs.put(factoryNode, map);
		}
	}

	public double getValue(ProductionPlan plan){
		Time t = new Time(plan.getPlanStartDate(), plan.getPlanStartDate());
		t.moveT(plan.getFinishingTime());
		return getValue(plan, t, new TaskSet());
	}
	
	public double getValue(ProductionPlan plan, Time t, TaskSet tasks){
		double result;
		double lateness = punishingLateness * calculateLateness(plan, t, tasks);
		double cost = plan.getCost();
		double energy = plan.getEnergyProfile().getMaximum() < maxEnergyLevel ? 
							0.0 : punishingEnergy;
		result = cost + plan.getEnergyCost() + energy + lateness;
		if(result == Double.MAX_VALUE){
			result--;
		}
		return result;
	}

	
	
	private double calculateLateness(ProductionPlan plan, Time t, TaskSet tasks) {
		double result = 0.0;
		//Latness of dispatched tasks
		for(Workplace workplace : plan.getEntries().keySet()){
			Schedule<ProductionPlanEntry> schedule = plan.getEntries().get(workplace);
			for(int i = 0; i<schedule.size(); i++){
				ProductionPlanEntry entry = schedule.get(i);
				if(entry.getOperation().getOutputProducts().contains(entry.getOrder().getProduct())){
					double finishingTime = schedule.getTime(i) + entry.getDuration();	
					double endDate = (entry.getOrder().getEndDate().getTimeInMillis() - 
										plan.getPlanStartDate().getTimeInMillis())/1000;
					if(finishingTime > endDate){
						result += finishingTime - endDate;
					}
				}
			}
		}
		//Latness of undispatched Tasks
		for(SchedulableTask task : tasks){
			double finishingTime = t.getT();	// approximate duration of task should be added here to shorten runtime
			double endDate = (task.getOrder().getEndDate().getTimeInMillis() - 
								plan.getPlanStartDate().getTimeInMillis())/1000;
			if(finishingTime > endDate){
				result += finishingTime - endDate;
			}
		}
		return result;
	}

	public void setMaxEnergyLevel(double maxEnergyLevel) {
		this.maxEnergyLevel = maxEnergyLevel;
	}
}
	
	
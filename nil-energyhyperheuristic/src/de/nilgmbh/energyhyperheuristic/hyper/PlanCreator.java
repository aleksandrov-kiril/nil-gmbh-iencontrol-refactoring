package de.nilgmbh.energyhyperheuristic.hyper;

import java.util.Calendar;
import java.util.List;

import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.Order;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.ShiftOffPeriods;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.nilgmbh.energyhyperheuristic.dismantling.Dismantler;
import de.nilgmbh.energyhyperheuristic.dismantling.TaskSet;
import de.nilgmbh.energyhyperheuristic.hyper.solver.HillClimber;
import de.nilgmbh.energyhyperheuristic.hyper.solver.Solver;
import de.nilgmbh.energyhyperheuristic.strategyfactory.StrategyFactory;
import de.nilgmbh.energyhyperheuristic.strategyfactory.StrategyFactory0;
import de.nilgmbh.energyhyperheuristic.strategyfactory.StrategyFactory1;
import de.nilgmbh.energyhyperheuristic.strategyfactory.StrategyFactory2;
import de.nilgmbh.energyhyperheuristic.strategyfactory.StrategyFactory3;
import de.nilgmbh.energyhyperheuristic.util.Neighborhood;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class PlanCreator {
	
	public ProductionPlan createPlan(ProductionPlan oldPlan, List<Order> orders,
			Calendar start, Calendar now, long calculationTimeInSeconds, 
			double maxEnergyLevel, int weight_maxEnergyLevel, int weight_nightShiftAvoidance) {
		
		//simplify shifts for now
		Factory factory = Project.getCurrentProject().getOriginalFactory();
		adaptShiftSchedules(factory);
		
		//Dismantle orders
		Dismantler dismantler = new Dismantler();
		TaskSet tasks = dismantler.dismantle(orders, true);
		
		//Create strategy neighborhood (--> maxEnergyLevel)
		StrategyFactory strategyFactory;
		
		int strategyScenario;
		
		if(weight_maxEnergyLevel == 0 && weight_nightShiftAvoidance == 0){
			strategyScenario = 2; // all shifts and no energy consideration
		} else if(weight_maxEnergyLevel == 0 && weight_nightShiftAvoidance != 0){
			strategyScenario = 1; // night shift consideration
		} else if(weight_maxEnergyLevel != 0 && weight_nightShiftAvoidance == 0){
			strategyScenario = 3; // energy consideration, no shift consideration
		} else {
			strategyScenario = 0; // energy and night shift consideration
		}
		
		//TODO: adapt strategies according to slider values
		
		switch (strategyScenario){
		case 0: 
			//all Strategies
			// 			24-7		24-5		13-5
			// 		DE	DEF	-----	2	-----	4
			//			|			|			|
			//		EN	1	-----	3	-----	5
			strategyFactory = new StrategyFactory0();
			break;
		case 1: 
			//only night shift avoidance, with and without energy	
			// 			24-7		24-5		13-5
			// 		DE							1
			//									|
			//		EN							2
			// !!! MIGHT NOT FIND A SOLUTION IF TASKS ARE TOO LONG FOR ONE SHIFT !!!
			strategyFactory = new StrategyFactory1();
			break;
		case 2:
			//all shifts, no energy peak load avoidance
			// 			24-7		24-5		13-5
			// 		DE	DEF	-----	1	-----	1
			//			
			//		EN	
			strategyFactory = new StrategyFactory2();
			break;
		case 3:
			//only peak load avoidance, all shifts
			// 			24-7		24-5		13-5
			// 		DE	
			//			
			//		EN	1	-----	3	-----	5
			// !!! MIGHT NOT FIND A SOLUTION IF MAX-ENERGY LEVEL IS SET TOO LOW !!!
		default:
			strategyFactory = new StrategyFactory3();
		}
		
		Neighborhood strategies = strategyFactory.createNeighbourhood(maxEnergyLevel);
		
		
		
		//create Objective function
		ObjectiveFunction of = new ObjectiveFunction(Project.getCurrentProject().getOriginalFactory(), maxEnergyLevel);
		
		HyperHeuristic hh = new HyperHeuristic(oldPlan, tasks, strategies, of, start, calculationTimeInSeconds);
		
		//Choose solver
		Solver solver = new HillClimber(hh);
		
		ProductionPlan result = hh.run(solver);
		
		return result;
	}

	

	
	/**
	 * Overwrites the ShiftShedules in the given factory, with a 24-7 and a non-weekend shift.
	 * @param factory
	 */
	private void adaptShiftSchedules(Factory factory){
		factory.getShiftplans().clear();
		
		ShiftOffPeriods s0 = new ShiftOffPeriods();
		ShiftSchedule schedule0 = new ShiftSchedule(factory, "24-7", s0, 3);
		
		ShiftOffPeriods s1 = new ShiftOffPeriods();
		s1.addAvoidedWeekday(ShiftOffPeriods.Day.SUNDAY);
		s1.addAvoidedWeekday(ShiftOffPeriods.Day.SATURDAY);
		//s1.addAvoidedDayTime(14*3600, 15*3600);
		ShiftSchedule schedule1 = new ShiftSchedule(factory, "24-5", s1, 3);
		
		ShiftOffPeriods s2 = new ShiftOffPeriods();
		s2.addAvoidedWeekday(ShiftOffPeriods.Day.SUNDAY);
		s2.addAvoidedWeekday(ShiftOffPeriods.Day.SATURDAY);
		//s2.addAvoidedDayTime(14*3600, 15*3600);
		s2.addAvoidedDayTime(22*3600, 24*3600);
		s2.addAvoidedDayTime(0*3600, 7*3600);
		ShiftSchedule schedule2 = new ShiftSchedule(factory, "13-5", s2, 3);
		
		factory.addShiftplan(schedule0);
		factory.addShiftplan(schedule1);
		factory.addShiftplan(schedule2);
	}
}

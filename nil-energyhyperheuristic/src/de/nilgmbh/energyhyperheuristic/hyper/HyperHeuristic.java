package de.nilgmbh.energyhyperheuristic.hyper;

import java.util.Calendar;

import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.nilgmbh.energyhyperheuristic.dismantling.TaskSet;
import de.nilgmbh.energyhyperheuristic.dispatching.Strategy;
import de.nilgmbh.energyhyperheuristic.hyper.solver.Solver;
import de.nilgmbh.energyhyperheuristic.util.Neighborhood;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class HyperHeuristic {

	private ProductionPlan oldProductionPlan;
	private TaskSet taskSet;
	private Neighborhood strategies;
	private ObjectiveFunction objectiveFunction;
	private double bound;
	private Calendar start;
	private long calculationTime;
	private Solution bestSolution;

	public HyperHeuristic(ProductionPlan oldProductionPlan, TaskSet taskSet,
			Neighborhood strategies, ObjectiveFunction objectiveFunction, Calendar start, long calculationTimeInSeconds) {
		super();
		this.oldProductionPlan = oldProductionPlan;
		this.taskSet = taskSet;
		this.strategies = strategies;
		this.objectiveFunction = objectiveFunction;
		this.start = start;
		this.bound = Double.MAX_VALUE;
		this.calculationTime = calculationTimeInSeconds * 1000;
		bestSolution = null;
	}
	
//	private class SolverRunner implements Runnable {
//		private Solver solver;
//		
//		public SolverRunner(Solver solver){
//			this.solver = solver;
//		}
//		
//		public void run() {
//			bestSolution = solver.solve(bestSolution	);
//		}
//	}

	public ProductionPlan run(Solver solver){
		ProductionPlan result;
		long startInstance = Calendar.getInstance().getTimeInMillis();
		
		bestSolution = generateInitialSolution();
		bound = bestSolution.getObjectiveFunctionValue();
		System.out.println("Start-Solution: \t" + bestSolution.getObjectiveFunctionValue() + " \t" 
											  + bestSolution.getStrategySet().toString());
		long remainingCalcTime = calculationTime - 
				Calendar.getInstance().getTimeInMillis() + startInstance; 
		
		if (bound < Double.MAX_VALUE && remainingCalcTime > 0){
			solver.setStartingPoint(bestSolution);
			Thread worker = new Thread(solver);
			worker.start();
			try {
				Thread.sleep(remainingCalcTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			solver.interruptWorkers();
			try {
				worker.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			result = bestSolution.getNewProductionPlan();
		} else {
			System.out.println("Could not generate Solution: " +
					"Default Strategy Set was unable to find a feasible Plan");
			result = bestSolution.getNewProductionPlan();
		}
		
		return result;
	}
	
	private Solution generateInitialSolution(){
		double duration = taskSet.estimateDuration();
		Strategy startStrategy = strategies.getStart();
		Solution result = new Solution(new StrategySet(startStrategy, duration), this);
		return result;
	}
	
	public ProductionPlan getOldProductionPlan() {
		return oldProductionPlan;
	}

	public TaskSet getTaskSet() {
		return taskSet;
	}
	
	public double getBound() {
		return bound;
	}
	
	public Calendar getStart() {
		return start;
	}

	public long getCalculationTime() {
		return calculationTime;
	}

	public void setBound(double bound) {
		this.bound = bound;
	}

	public ObjectiveFunction getObjectiveFunction() {
		return objectiveFunction;
	}
	
	public Solution getBestSolution() {
		return bestSolution;
	}

	public void setBestSolution(Solution bestSolution) {
		this.bestSolution = bestSolution;
	}
	
}

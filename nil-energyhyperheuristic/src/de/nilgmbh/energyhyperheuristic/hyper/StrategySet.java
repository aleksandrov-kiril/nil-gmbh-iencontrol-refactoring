package de.nilgmbh.energyhyperheuristic.hyper; 
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.nilgmbh.energyhyperheuristic.dispatching.Strategy;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class StrategySet {
	
	private Map<Workplace,TreeMap<Double, Strategy>> strategies;
	
	/**
	 * expected total duration of the {@link ProductionPlan} that will result from this
	 * StrategySet. Used for refining the StrategySet
	 */
	private double totalDuration;
	
	public StrategySet(Strategy defaultStrategy, double totalDuration){
		Factory factory = Project.getCurrentProject().getOriginalFactory();
		List<Workplace> workplaces = EntityList.getWorkplaceList(factory);
		strategies = new HashMap<Workplace, TreeMap<Double,Strategy>>();
		for(Workplace workplace : workplaces){
			TreeMap<Double, Strategy> map = new TreeMap<Double, Strategy>();
			map.put(0.0, defaultStrategy);
			strategies.put(workplace, map);
		}
		this.setTotalDuration(totalDuration);
	}
	
	private StrategySet() {
		super();
	}

	public Strategy getStrategy(Workplace workplace, double t) {
		return strategies.get(workplace).floorEntry(t).getValue();
	}
	
	public Map<Workplace, TreeMap<Double, Strategy>> getStrategies() {
		return strategies;
	}

	private void setStrategies(Map<Workplace,TreeMap<Double, Strategy>> strategies){
		this.strategies = strategies;
	}
		
	public void alterStrategy(Workplace workplace, double t, Strategy strategy){
		 Double key = strategies.get(workplace).floorKey(t);
		 strategies.get(workplace).put(key, strategy);
	}
	
	public StrategySet refineStrategies(){
		StrategySet result = (StrategySet) this.clone();
		
		for(Workplace workplace : strategies.keySet()){
			Set<Entry<Double, Strategy>> entries = strategies.get(workplace).entrySet();
			for(Entry<Double, Strategy> entry : entries){
				if(!strategies.get(workplace).lastEntry().equals(entry)){
					double interval = strategies.get(workplace).higherKey(entry.getKey()) - entry.getKey();
					result.getStrategies().get(workplace).put(interval/2 + entry.getKey(), entry.getValue());
				} else {
					double interval = getTotalDuration() - entry.getKey();
					result.getStrategies().get(workplace).put(interval/2 + entry.getKey(), entry.getValue());
				}
			}
		}
		
		return result;
	}

	public double getTotalDuration() {
		return totalDuration;
	}

	public void setTotalDuration(double totalDuration) {
		this.totalDuration = totalDuration;
	}

	@Override
	public String toString() {
		String result = "[" ;
		for(Workplace workplace : strategies.keySet()){
			result += "{" + workplace.getName() + ": " + strategies.get(workplace) + "}";
		}
		result += "]";
		return result;
	}
	
	@Override
	public Object clone(){
		StrategySet result = new StrategySet();
		Map<Workplace,TreeMap<Double, Strategy>> strat = new HashMap<Workplace, TreeMap<Double,Strategy>>();
		for(Workplace key : strategies.keySet()){
			strat.put(key, new TreeMap<Double, Strategy>());
			for(Entry<Double,Strategy> entry : strategies.get(key).entrySet()){
				strat.get(key).put(entry.getKey(), entry.getValue());
			}
		}
		result.setStrategies(strat);
		result.setTotalDuration(totalDuration);
		
		return result;
	}

	public Set<Double> getStrategyChangingPoints() {
		Set<Double> result = new HashSet<Double>();
		for(Workplace workplace : strategies.keySet()){
			result.addAll(strategies.get(workplace).keySet());
		}
		return result;
	}

	
	/**
	 * sets the given strategy at the given time for ALL workplaces
	 * @param t
	 * @param strategy
	 */
	public void alterStrategy(double t, Strategy strategy) {
		for(Workplace workplace : strategies.keySet()){
			Double key = strategies.get(workplace).floorKey(t);
			strategies.get(workplace).put(key, strategy);
		}	
	}
	
}

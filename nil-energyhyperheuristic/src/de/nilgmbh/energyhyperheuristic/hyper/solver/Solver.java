package de.nilgmbh.energyhyperheuristic.hyper.solver;

import de.nilgmbh.energyhyperheuristic.hyper.Solution;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public abstract class Solver implements Runnable{

	protected Solution startingPoint;
	
	public void setStartingPoint(Solution startingPoint) {
		this.startingPoint = startingPoint;
	}
	
	public abstract void interruptWorkers();
	
}

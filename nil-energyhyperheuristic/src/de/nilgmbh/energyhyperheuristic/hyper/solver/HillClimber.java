package de.nilgmbh.energyhyperheuristic.hyper.solver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.fzi.wenpro.core.model.Workplace;
import de.nilgmbh.energyhyperheuristic.dispatching.Strategy;
import de.nilgmbh.energyhyperheuristic.hyper.HyperHeuristic;
import de.nilgmbh.energyhyperheuristic.hyper.Solution;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class HillClimber extends Solver implements Runnable{
	
	private List<Solution> neighbors;
	private int searchDepth;
	private Iterator<Solution> iterator;
	private HyperHeuristic hyperHeuristic;
	private Thread[] workers;
	private boolean interrupted;
//	int nThreads = Runtime.getRuntime().availableProcessors();
	private final int nThreads = 1;
	
	public HillClimber(HyperHeuristic hyperHeuristic){
		this.searchDepth = 0;
		neighbors = new ArrayList<Solution>();
		this.hyperHeuristic = hyperHeuristic;
		startingPoint = null;
		workers = new Thread[nThreads];
		interrupted = false;
	}
	//TODO activate History to save runtime;

	private class HillClimberWorker implements Runnable {
		
		@Override
		public void run() {
			Solution solution = getNextSolution();
			while (solution != null) {
				System.out.println("Solution checked: \t" + solution.getObjectiveFunctionValue() +
						" \t" + solution.getStrategySet().toString());
				setSolution(solution);
				solution = getNextSolution();
			}
		}  
	}
  
	synchronized private Solution getNextSolution() {
		if (iterator.hasNext() && !interrupted) {
			return iterator.next();
		}
		return null;
	}
	
	@Override
	public void run() {
		renewNeigbors(startingPoint);
		interrupted = false;
		while(!interrupted){
	        for (int i = 0; i < nThreads; i++) {
	                Thread t = new Thread(new HillClimberWorker());
	                t.start();
	                workers[i] = t;
	        }
	        for (int i = 0; i < nThreads; i++) {
	                try {
	                        workers[i].join();
	                } catch (InterruptedException e) {
	                       e.printStackTrace();
	                }
	        }
	        forceSolution(hyperHeuristic.getBestSolution().refine());
	        searchDepth++;
		}
	}
	
	
	synchronized private void setSolution(Solution solution){
		if(solution.compareTo(hyperHeuristic.getBestSolution()) < 0){
			forceSolution(solution);
		}
	}
	
	private void forceSolution(Solution solution){
		hyperHeuristic.setBestSolution(solution);
		System.out.println("new Best: \t\t" + solution.getObjectiveFunctionValue() +
				" \t" + solution.getStrategySet().toString());
		renewNeigbors(solution);
	}

	private void renewNeigbors(Solution solution) {
		neighbors.clear();
		// switch strategies on all workplaces simultaneously
		if(searchDepth == 0){
			Workplace oneWorkplace = solution.getStrategySet().getStrategies().keySet().iterator().next();
			Strategy oneStrategy = solution.getStrategySet().getStrategy(oneWorkplace, 0.0);
			for(Strategy neighbor : oneStrategy.getNeighbors()){
				Solution newSolution = (Solution) solution.copy();
				newSolution.getStrategySet().alterStrategy(0.0, neighbor);
				neighbors.add(newSolution);
			}
		} 
		// switch strategies on workplaces separately
		else {
			for(Workplace workplace : solution.getStrategySet().getStrategies().keySet()){
				for(double time : solution.getStrategySet().getStrategies().get(workplace).keySet()){
					for(Strategy neighbor : solution.getStrategySet().getStrategies().get(workplace).get(time).getNeighbors()){
						Solution newSolution = (Solution) solution.copy();
						newSolution.getStrategySet().alterStrategy(workplace, time, neighbor);
						neighbors.add(newSolution);
					}
				}		
			}
		}
		iterator = neighbors.iterator();
	}
	
	@Override
	public void interruptWorkers(){
		interrupted = true;
	}
}

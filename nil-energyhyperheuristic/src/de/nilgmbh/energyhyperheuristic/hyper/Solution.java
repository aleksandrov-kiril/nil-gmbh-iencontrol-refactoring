package de.nilgmbh.energyhyperheuristic.hyper;

import java.util.Calendar;

import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.fzi.wenpro.util.Tuple;
import de.nilgmbh.energyhyperheuristic.dismantling.TaskSet;
import de.nilgmbh.energyhyperheuristic.dispatching.Dispatcher;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class Solution implements Comparable<Solution> {

	/**
	 * The HyperHueristik this class is a solution to.
	 */
	private HyperHeuristic hyperHeuristic;

	/**
	 * The set of Strategies that defines this Solution
	 */
	private StrategySet strategySet;
	
	/**
	 * Solution of the dispatching heuristic, so it only needs to be executed once. 
	 * {@code null} if dispatching heuristic has not yet been executed.
	 */
	private ProductionPlan newProductionPlan;
	
	/**
	 * Objective Function Value of this productionPlan
	 */
	private Double objectiveFunctionValue;
	
	/**
	 * time it took to execute this Solution in millisec
	 */
	private Long calculationTime;
	
	public Solution(StrategySet strategySet, HyperHeuristic hyperHeuristic){
		this(hyperHeuristic, strategySet, null, null, null);
	}
	
	
	
	public Solution(HyperHeuristic hyperHeuristic,
			StrategySet strategySet, ProductionPlan newProductionPlan,
			Double objectiveFunctionValue, Long calculationTime) {
		super();
		this.hyperHeuristic = hyperHeuristic;
		this.strategySet = strategySet;
		this.newProductionPlan = newProductionPlan;
		this.objectiveFunctionValue = objectiveFunctionValue;
		this.calculationTime = calculationTime;
	}



	private void execute(){
		long start = Calendar.getInstance().getTimeInMillis();
		Tuple<ProductionPlan, Double> newPlan = 
				Dispatcher.dispatch(hyperHeuristic.getOldProductionPlan(),
									hyperHeuristic.getStart(),
									(TaskSet) hyperHeuristic.getTaskSet().clone(),
									strategySet,
									hyperHeuristic.getBound(),
									hyperHeuristic.getObjectiveFunction());
		newProductionPlan = newPlan.getFirst();
		objectiveFunctionValue = newPlan.getSecond();
		long end = Calendar.getInstance().getTimeInMillis();
		calculationTime = end - start;
	}

	public ProductionPlan getNewProductionPlan() {
		if(newProductionPlan==(null)){
			execute();
		}
		return newProductionPlan;
	}

	public Double getObjectiveFunctionValue() {
		if(objectiveFunctionValue==(null)){
			execute();
		}
		return objectiveFunctionValue;
	}
	
	public StrategySet getStrategySet(){
		return strategySet;
	}
	
	public Solution refine(){
		StrategySet strategySet = getStrategySet().refineStrategies();
		Solution result = new Solution(strategySet, hyperHeuristic);
		
		return result;
	}
	
	public long getCalculationTime() {
		if(calculationTime == null){
			execute();
		}
		return calculationTime;
	}

	/**
	 * In contrast to the {@code clone()} method, this method will leave the fields
	 * newProductionPlan, objectivFunctionValue and calculationTime unassigned. 
	 * The resulting Solution will therefore have to be dispatched again.
	 * 
	 * @return unsolved Solution-Object
	 */
	public Object copy(){
		return new Solution((StrategySet) strategySet.clone(), hyperHeuristic);			
	}
	
	@Override
	public Object clone(){
		return new Solution(hyperHeuristic,
							(StrategySet) strategySet.clone(),
							newProductionPlan.clone(),
							objectiveFunctionValue,
							calculationTime);
	}



	@Override
	public int compareTo(Solution arg0) {
		if(this.getObjectiveFunctionValue() < arg0.getObjectiveFunctionValue()){
			return -1;
		} else if (this.getObjectiveFunctionValue() == arg0.getObjectiveFunctionValue()){
			return 0;
		} else {
			return 1;
		}
	}
}

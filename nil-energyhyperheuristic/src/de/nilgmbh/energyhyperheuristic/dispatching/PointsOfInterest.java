package de.nilgmbh.energyhyperheuristic.dispatching;


import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.Time;
import de.fzi.wenpro.core.model.ShiftOffPeriods.DayTime;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.core.model.productionplan.TimeLine;
import de.nilgmbh.energyhyperheuristic.dismantling.SchedulableTask;
import de.nilgmbh.energyhyperheuristic.dismantling.TaskSet;
import de.nilgmbh.energyhyperheuristic.hyper.StrategySet;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class PointsOfInterest {

	private TreeSet<Double> pointsOfInterest;
	
	private List<ShiftSchedule> shiftplans;
	
	/**
	 * keeps track how long no noncyclic PoI has ocured
	 */
	private long cycleLength;
	
	private long maxCycleLength;
	
	public PointsOfInterest(Time t, TaskSet taskSet, Factory factory, StrategySet strategySet){
		pointsOfInterest = new TreeSet<Double>();
		shiftplans = factory.getShiftplans();
		cycleLength = 0;
		maxCycleLength = 0;
		for(ShiftSchedule schedule : shiftplans){
			maxCycleLength = (schedule.getOffPeriods().getCYCLE_TIME_IN_MILLISEC() > maxCycleLength)?
					schedule.getOffPeriods().getCYCLE_TIME_IN_MILLISEC() : maxCycleLength;
		}
		addPoIs(taskSet, t.getStart());
		addPoIs(strategySet);
	}
	
	private void addPoIs(StrategySet strategySet) {
		for(Double d : strategySet.getStrategyChangingPoints()){
			add(d);
		}
	}
	
	private void addPoIs(TaskSet taskSet, Calendar planStartDate) {
		for(SchedulableTask task : taskSet){
			Time t = new Time(task.getOrder().getStartDate(), planStartDate);
			if(t.getT()>=0.0){
				add(t.getT());
			}
		}
	}
	
	public void addPoIs(ProductionPlanEntry entry, double t) {
		//PoI's caused by changes in Stock
		Collection<TimeLine<Integer>> timelines = entry.getStockTimelines(t).values();
		for(TimeLine<Integer> timeline : timelines){
			for(Double time : timeline.getAllTimes()){
				add(time);
			}
		}
		//PoI's caused by changes in Energy Profile
		for(Double time : entry.getEntryEnergyProfile().getProfile().keySet()){
			add(time + t);
		}
	}
	
	private void add(double time){
		long t = Math.round(time * 1000);
		time = t/1000.0;
		pointsOfInterest.add(time);
	}
	
	/**
	 * returns the smallest PoI strictly greater than the given point in time.
	 * @param t
	 * @return next PoI, {@code: null} if there is no next PoI.
	 */
	public Double getNextPoI(Time t){
		Double result;
		double result2;
		if(isEndlessCycle()){
			result = null;
		}else{
			double regularNextPoI;		
			if(pointsOfInterest.higher(t.getT())==null){
				regularNextPoI = Double.MAX_VALUE;
			}else{
				regularNextPoI = pointsOfInterest.higher(t.getT());
				clearFlags();
			}
			result = regularNextPoI;
			result2 = getNextCyclicPoI(t);
			result = (result < result2) ? result : result2;	
		}
		return result;		
	}

	private double getNextCyclicPoI(Time t){
		Set<Time> results = new HashSet<Time>();
		
		Time workingTime;
		
		for(ShiftSchedule ss : shiftplans){
			for(DayTime dt : ss.getOffPeriods().getAvoidedDayTimes()){
				workingTime = new Time(t);
				workingTime.getNow().set(Calendar.MILLISECOND, 0);
				workingTime.getNow().set(Calendar.SECOND, 0);
				workingTime.getNow().set(Calendar.MINUTE, 0);
				workingTime.getNow().set(Calendar.HOUR_OF_DAY, 0);
				workingTime.getNow().add(Calendar.MILLISECOND, (int)(dt.getEnd()*1000.0));
				if(!workingTime.getNow().after(t.getNow())){
					workingTime.getNow().add(Calendar.DAY_OF_YEAR, 1);
				}
				results.add(new Time(workingTime));
			}
			for(int day : ss.getOffPeriods().getAvoidedWeekDays()){
				workingTime = new Time(t);
				workingTime.getNow().set(Calendar.MILLISECOND, 0);
				workingTime.getNow().set(Calendar.SECOND, 0);
				workingTime.getNow().set(Calendar.MINUTE, 0);
				workingTime.getNow().set(Calendar.HOUR_OF_DAY, 0);
				workingTime.getNow().set(Calendar.DAY_OF_WEEK, day);
				workingTime.getNow().add(Calendar.DAY_OF_WEEK, 1);
				if(!workingTime.getNow().after(t.getNow())){
					workingTime.getNow().add(Calendar.WEEK_OF_YEAR, 1);
				}
				results.add(new Time(workingTime));
			}
		}
		
		double result = Double.MAX_VALUE;
		for(Time time : results){
			if (result > time.getT()){
				result = time.getT();
			}
		}
		cycleLength += (result - t.getT())*1000;
		return result;
	}
	
	private void clearFlags() {
		cycleLength = 0;
	}

	private boolean isEndlessCycle(){
		boolean result = maxCycleLength < cycleLength;
		
		return result;
	}

		
}

package de.nilgmbh.energyhyperheuristic.dispatching;

import java.util.Calendar;
import java.util.List;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.Time;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.util.Tuple;
import de.nilgmbh.energyhyperheuristic.dismantling.TaskSet;
import de.nilgmbh.energyhyperheuristic.hyper.ObjectiveFunction;
import de.nilgmbh.energyhyperheuristic.hyper.StrategySet;
import de.nilgmbh.energyhyperheuristic.objectivefunctions.Labor;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class Dispatcher {
	
	private static PointsOfInterest pointsOfInterest;
	
	/**
	 *  Dispatches the given {@link TaskSet} onto the the given {@link EnergyProductionPlan} according 
	 *  to the given {@link StrategySet}
	 * 
	 * The objective function value has to be calculated simultaneously for reasons of bounding.
	 * Once the ofv rises above the Dispatcher's bound, the dispatching process will be aborted,
	 * since a better solution has already been generated. 
	 * 
	 * @param plan The preexisting plan
	 * @param start Point in time at which the new tasks should start to be added
	 * @param tasks Tasks that need to be added to the plan.
	 * @param strategySet Set of Strategies from the {@link Solution}
	 * @return Returns a Tuple consisting of the resulting {@link EnergyProductionPlan} and its
	 * 		   objective function value.
	 */
	public static Tuple<ProductionPlan, Double> dispatch(ProductionPlan plan, Calendar start,
															TaskSet tasks, StrategySet strategySet,
															double bound, ObjectiveFunction objectiveFunction){
		ProductionPlan workingPlan = (ProductionPlan) plan.clone();
		PointInTime.setProductionPlan(workingPlan);
		
		Factory factory = Project.getCurrentProject().getOriginalFactory();
		List<Workplace> workplaces = EntityList.getWorkplaceList(factory);
		
		if (start.before(workingPlan.getPlanStartDate())){
			start = workingPlan.getPlanStartDate();
		}
		
		Time t = new Time(start, workingPlan.getPlanStartDate());
		pointsOfInterest = new PointsOfInterest(t, tasks, factory, strategySet);
		PointInTime.updatePointInSchedule(t, workingPlan);
		
		boolean stop = false;
		do{
			for(Workplace workplace : workplaces){ //this loop should be executed workplaces.size()-times to theoretically reach every thinkable solution
				Strategy strategy = strategySet.getStrategy(workplace, t.getT());
				ProductionPlanEntry entry = strategy.getBestTask(tasks, workplace);
				if(entry!=null){
					workingPlan.addWithRetooling(entry, t.getT());
					entry.calculateCost();
					entry.setCost(entry.getCost() + Labor.calculateLaborCost(entry, t));
					PointInTime.updatePointInSchedule(entry);
					pointsOfInterest.addPoIs(entry, t.getT());
				}
			}
			boolean endless = moveT(t);
			stop = endless || tasks.allDispatched() ||
				   bound <= objectiveFunction.getValue(workingPlan, t, tasks);
		} while(!stop);
		
		Double ofv = objectiveFunction.getValue(workingPlan);
		//mark incomplete plan:
		if (!tasks.allDispatched()){
			ofv = Double.MAX_VALUE;
		}
		Tuple<ProductionPlan, Double> result = 
				new Tuple<ProductionPlan, Double>(workingPlan, ofv);
		
		return result;
	}

	
	private static boolean moveT(Time t) {
		boolean result;
		Double newT = pointsOfInterest.getNextPoI(t);
		if (newT == null){
			result = true;
		} else {
			result = false;
			t.setT(newT);
			PointInTime.updatePointInSchedule(t);
		}
		return result;
	}

}

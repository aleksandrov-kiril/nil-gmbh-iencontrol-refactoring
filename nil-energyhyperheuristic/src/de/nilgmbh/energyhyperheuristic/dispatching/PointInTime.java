package de.nilgmbh.energyhyperheuristic.dispatching;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.Time;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.core.model.productionplan.Schedule;
import de.fzi.wenpro.core.model.productionplan.TimeLine;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class PointInTime {
	
	private static Time time;
	private static ProductionPlan productionPlan;
	private static Map<Workplace, Double> idleTimes = new HashMap<Workplace, Double>();
	private static Map<Product, Integer> minStocks = new HashMap<Product, Integer>();
	private static Map<Product, Integer> finalStocks = new HashMap<Product, Integer>();
	private static NavigableMap<Double, Double> energyLevel;
	private static Map<Workplace, WorkplaceState> workplaceStates = new HashMap<Workplace, WorkplaceState>();
	
	public static void updatePointInSchedule(Time t, ProductionPlan productionPlan){
		setProductionPlan(productionPlan);
		updatePointInSchedule(t);
	}
		
	
	/**
	 * Recalculates all Class-Variables of PointInSchedule
	 */
	public static void updatePointInSchedule(Time t){
		setTime(t);
		idleTimes.clear();
		minStocks.clear();
		finalStocks.clear();
		workplaceStates.clear();
		
		Factory factory = Project.getCurrentProject().getOriginalFactory();
		List<Workplace> workplaces = EntityList.getWorkplaceList(factory);
		Collection<Product> products = EntityList.getProductList(factory);
		
		//Calculate the remaining Idle Time per workplace
		for(Workplace workplace : workplaces){
			updateIdleTime(workplace);
		}
		
		//Calculate Min/Max Stock Levels
		for (Product product : products){
			updateStock(product);
		}
		
		//check Energy Levels
		updateEnergyLevel();
		
		//update WorkplaceStates
		for(Workplace workplace : workplaces){
			updateWorkplaceState(workplace);
		}
	}
	
	/**
	 * Recalculates only class variables that could have been affected by the given
	 * {@link ProductionPlanEntry}. Time remains unchanged.
	 * @param entry
	 */
	public static void updatePointInSchedule(ProductionPlanEntry entry){
		updateIdleTime(entry.getWorkplace());
		
		for (Product product : entry.getOperation().getInputProducts()){
			updateStock(product);
		}
		for (Product product : entry.getOperation().getOutputProducts()){
			updateStock(product);
		}
		
		updateEnergyLevel();
		
		updateWorkplaceState(entry.getWorkplace());
	}
	
	
	private static void updateIdleTime(Workplace workplace){
		double idleTime = Double.MAX_VALUE;
		Schedule<ProductionPlanEntry> schedule = productionPlan.getEntries().get(workplace);
		double[] times = new double[0];
		if(schedule != null){
			times = schedule.getAllTimes();
		} 
		
		
		for(double t : times){
			int index = productionPlan.getEntries().get(workplace).getIndexByTime(t) - 1;
			double duration = productionPlan.getEntries().get(workplace).getEntry(index).getObject().getDuration();
			double now = time.getT();
			if (t + duration > now) {
				if (t <= now){
					idleTime = 0.0;
				} else {
					if (t - now < idleTime){
						idleTime = t - now;
					}
				}
			}
		}
		
		idleTimes.put(workplace, idleTime);
	}
	
	private static void updateStock(Product product){
		int minStock = 0;
		int finalStock = 0;
		TimeLine<Integer> timeline = productionPlan.getStock(product);
		if(timeline.size()!=0){
			int startIndex = timeline.getIndexByTime(time.getT());
			if (startIndex == 0){
				minStock = 0;
			}else{
				startIndex--;
				int stopIndex = timeline.size()-1;
				
				minStock = timeline.get(startIndex);
				finalStock = timeline.get(stopIndex);
				for(int i = startIndex + 1; i <= stopIndex; i++){
					int stock = timeline.get(i);
					minStock = (stock < minStock) ? stock : minStock;
				}
			} 
		}
		minStocks.put(product, minStock);
		finalStocks.put(product, finalStock);
	}
	
	

	private static void updateWorkplaceState(Workplace workplace){
		WorkplaceState state = productionPlan.getWorkplaceState(workplace, time.getT());
		workplaceStates.put(workplace, state);
	}
	
	private static void updateEnergyLevel(){
		TreeMap<Double, Double> energyMap = productionPlan.getEnergyProfile().getProfile();
		if(!energyMap.containsKey(time.getT())){
			Entry<Double, Double> previousLevel = energyMap.floorEntry(time.getT());
			energyMap.put(time.getT(), previousLevel == null ? 0.0 : previousLevel.getValue());
		}
		energyLevel = energyMap.subMap(time.getT(), true, energyMap.lastKey(), true);
	}
	
	public static Time getTime() {
		return time;
	}

	public static ProductionPlan getProductionPlan() {
		return productionPlan;
	}

	public static Map<Workplace, Double> getIdleTimes() {
		return idleTimes;
	}

	public static Map<Product, Integer> getMinStocks() {
		return minStocks;
	}

	public static Map<Product, Integer> getFinalStocks() {
		return finalStocks;
	}
		
	public static NavigableMap<Double, Double> getEnergyLevel() {
		return energyLevel;
	}

	public static double getMaxEnergyLevel() {
		if(energyLevel != null && !energyLevel.values().isEmpty()){
			return Collections.max(energyLevel.values());
		} else {
			return 0.0;
		}
	}

	public static Map<Workplace, WorkplaceState> getWorkplaceStates() {
		return workplaceStates;
	}

	public static void setTime(Time time) {
		PointInTime.time = time;
	}

	public static void setProductionPlan(ProductionPlan productionPlan) {
		PointInTime.productionPlan = productionPlan;
	}
	
}

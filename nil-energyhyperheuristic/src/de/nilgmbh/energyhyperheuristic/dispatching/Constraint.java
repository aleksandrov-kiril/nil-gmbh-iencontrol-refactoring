package de.nilgmbh.energyhyperheuristic.dispatching;

import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public abstract class Constraint {
	
	/**
	 * Determines if a Constraints utilization value can drop below 1.0 again, once having surpassed
	 * this value (with increasing entry length)
	 * 
	 * E.g. Shift-related utilization will never decrease, since the shiftschedule will never be 
	 * "un-violated" with increasing entry length.
	 * 
	 * Maximum-Energy-Level-related utilization on the other Hand, might very well go back to being feasible
	 * with increasing entry-length. It might have been only the entry's ending energy that overstepped 
	 * the maximum energy level.
	 */
	protected final boolean convex;
	
	private String name;
		 
	public Constraint(String name, boolean convex){
		this.name = name;
		this.convex = convex;
	}
	
	/**
	 * Calculates the {@link Constraint}'s utilization. If a constraint is utilized to a value higher then 1.0
	 * by an {@link EnergyProductionPlanEntry}, the entry cannot be scheduled at the current {@link PointInTime}.
	 * In terms of optimization a low constraint utilization is considered good.
	 * @param entry EnergyProductionPlanEntry to be tested
	 * @return double value of this constraint's utilization
	 */
	public abstract double calculateValue(ProductionPlanEntry entry);

	public boolean isConvex() {
		return convex;
	}

	@Override
	public String toString() {
		return name;
	}
	
}

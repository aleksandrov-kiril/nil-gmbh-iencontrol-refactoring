package de.nilgmbh.energyhyperheuristic.dispatching;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.util.Tuple;
import de.nilgmbh.energyhyperheuristic.dismantling.SchedulableTask;
import de.nilgmbh.energyhyperheuristic.dismantling.TaskSet;
import de.nilgmbh.energyhyperheuristic.hyper.StrategySet;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class Strategy {

	private String name;
	
	/**
	 * The strategy's constraints determine its behavior. A Constraint is assigned a weight that 
	 * determines its importance in the task-selecting-decision.
	 */
	private Map<Constraint, Double> constraints;
	
	private double summedWeights;
	
	private Set<Strategy> neighbors;
	
	/**
	 * 
	 * @param name
	 * @param constraints Map containing constraints and their weights
	 */
	public Strategy(String name, Map<Constraint, Double> constraints) {
		super();
		this.name = name;
		this.constraints = constraints;
		this.summedWeights = 0.0;
		for (double d : constraints.values()){
			summedWeights += d;
		}
		this.neighbors = new HashSet<Strategy>();
	}

	/**
	 * Selects the best task from all tasks in the {@link TaskSet} that can be executed on the 
	 * given {@ Workplace} according to this {@link Strategy}.
	 * @param tasks Tasks that still need to be scheduled
	 * @param workplace Workplace assigned to this Strategy in the {@link StrategySet}
	 * @return {@link EnergyProductionPlanEntry} representing the best task, on the given machine in the
	 * 		optimal duration.
	 * 			{@code null} if no Task can/should be scheduled according to this strategy
	 */
	public ProductionPlanEntry getBestTask(TaskSet tasks,	Workplace workplace) {
		ProductionPlanEntry bestEntry = null;
		SchedulableTask bestTask = null;
		TreeMap<Double, Tuple<ProductionPlanEntry, SchedulableTask>> results = 
				new TreeMap<Double, Tuple<ProductionPlanEntry, SchedulableTask>>();
		
		for(SchedulableTask task : tasks){
			Integer producedQuantity = 0;
			do{
				producedQuantity = task.getNextProducableQuantity(producedQuantity, workplace);
				Set<ProductionPlanEntry> entries = task.createEntry(producedQuantity, workplace);
				
				if(entries != (null)){
					boolean flag = true;
					for(ProductionPlanEntry entry : entries){
						double value = getAverageConstraintUtilization(entry);
						if(value != Double.MAX_VALUE){
							flag = false; //constraint is convex
						}
						if(value <= 1.0){
							results.put(value, 
									new Tuple<ProductionPlanEntry, SchedulableTask>(entry, task));
						}
					}
					if(flag){
						break;
					}
				}
			}while (producedQuantity != null && producedQuantity < task.getQuantity());
		}
		if(!results.isEmpty()){
			double bestKey = results.firstKey();
			bestEntry = results.get(bestKey).getFirst();
			bestTask = results.get(bestKey).getSecond();
			
			if(bestEntry.getOperationCount() != bestTask.getQuantity()){
				@SuppressWarnings("unused")
				int i = 0;
			} 
			
		}
		
		//remove or reduce selected task 
		//FIXME: ???What needs to happen to side-products at this point???
		if(bestEntry!=null){
			
			/*
			System.out.print("[");
			for(Entry<Double, Tuple<ProductionPlanEntry, SchedulableTask>> entry : results.entrySet()){
				float value = (float) (Math.round(entry.getKey()*100.0f)/100.0f);
				System.out.print( value +
						": " + entry.getValue().getFirst() + " ");
			}
			System.out.println("] ==> " + bestEntry);
			*/
			
			int producedQuantity = bestEntry.getOperationCount() * bestEntry.getOperation().getOutputPiecesPerOperation(bestTask.getProduct());
			int remainingQuantity = bestTask.getQuantity() - producedQuantity;
			if (remainingQuantity > 0){
				bestTask.setQuantity(remainingQuantity);
			} else {
				tasks.remove(bestTask);
			}
		}
		return bestEntry;
		
	}
	
	/**
	 * Evaluates how a {@link EnergyProductionPlanEntry} utilizes all constraints of this strategy on average.
	 * @param entry EnergyProductionPlanEntry to be tested
	 * @return Returns the weighted Average of Utilization of all constraints in this Strategy.
	 * 		   Returns a value <= 1.0 if the entry can currently be added.
	 * 		   Returns a value > 1.0 and < Double.MaxValue if entry is currently not feasible but should 
	 * 			be investigated further.
	 * 		   Returns Double.MaxValue if entry is currently not feasible and has no chance of becoming 
	 * 			feasible if its length is increased.
	 */
	private double getAverageConstraintUtilization(ProductionPlanEntry entry){
		double numerator = 0.0;
		double result = 999.0;
		boolean flag = false;
		
		for(Constraint constraint : constraints.keySet()){
			double value = constraint.calculateValue(entry);
			if(value <= 1.0){
				numerator += value*constraints.get(constraint);
			}
			else{
				if(constraint.isConvex()){
					result = Double.MAX_VALUE;
				} else {
					result = value;
				}
				flag = true;
				break;
			}
		}
		if(!flag){
			//calculate weighted average
			result = numerator / summedWeights;
			assert result <= 1.0;
		}
	
		return result;
	}

	@Override
	public String toString() {
		return name;
	}

	public Set<Strategy> getNeighbors() {
		return neighbors;
	}
	
}

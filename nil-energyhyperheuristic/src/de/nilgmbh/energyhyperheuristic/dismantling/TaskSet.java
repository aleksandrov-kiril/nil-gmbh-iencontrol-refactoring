package de.nilgmbh.energyhyperheuristic.dismantling;

import java.util.HashSet;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class TaskSet extends HashSet<SchedulableTask> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7071499005172184821L;

	public boolean allDispatched() {
		boolean result = true;
		for (SchedulableTask task : this){
			if(task.getProduct().isDisposable()){
				result = false;
				break;
			}
		}
		return result;
	}

	public double estimateDuration(){
		double result = 0.0;
		//TODO: Generate a more accurate estimation
		//has to be related to oldPlan
		for (SchedulableTask task : this){
			result += task.getQuantity()*task.getProduct().getOperations().get(0).getTime();
		}
		return result;
	}
	
	@Override
	public Object clone(){
		TaskSet result = new TaskSet();
		for(SchedulableTask task : this){
			result.add((SchedulableTask) task.clone());
		}
		return result;
	}
}

package de.nilgmbh.energyhyperheuristic.dismantling;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;

import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Order;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Project;

/**
 * 
 * A dismantling heuristic transmutes {@link Order}s into a set of tasks ({@link TaskSet}). Once obtained, 
 * the set of tasks can be assigned to a schedule by the low level heuristic. 
 * 
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class Dismantler {

	private TaskSet taskSet;
	private Map<Product, Integer> stocks;
	
	public Dismantler(){
		taskSet = new TaskSet();
	}
	
	

	class OrderStartDateComparator implements Comparator<Order>{

		@Override
		public int compare(Order o1, Order o2) {
			int result;
			// o1 < o2 ==> -1
			if(o1.getStartDate().before(o2.getEndDate())){
				result = -1;
			} else // o1 > 02 ==> 1
				if(o1.getStartDate().after(o2.getStartDate())){
				result = 1;
			} else { // o1 == o2 ==> 0
				result = 0;
			}
			return result;
		}
		
	}
	
	/**
	 * Dismantles the open Orders into a taskSet. Orders that are already scheduled must
	 * not be dismantled again.
	 * @param orders Orders to be dismantled
	 * @param clear Should the existing TaskSet be cleared before dismantling
	 * @return taskSet containing all necessary Jobs, that have to be scheduled 
	 */
	public TaskSet dismantle(List<Order> orders, boolean clear){
		if(clear){
			taskSet.clear();
		}
		
		//In case of Preexisting stocks, they should be set here
		stocks = new HashMap<Product, Integer>();
		Factory factory = Project.getCurrentProject().getOriginalFactory();
		for(Product p : factory.getProducts()){
			stocks.put(p, 0);
		}
		
		SortedSet<Order> sortedOrders = new TreeSet<Order>(new OrderStartDateComparator());
		sortedOrders.addAll(orders);
		
		for(Order order : sortedOrders){
			Product product  = order.getProduct();
			int netQuantity = order.getQuantity() - stocks.get(product);
			List<Operation> operations = product.getOperations();
			Operation leadingOperation = getLeadingOperation(operations);
			operations = getSymmetricalOperations(leadingOperation, operations);
			
			//calculate grossQuantity that needs to be produced due to lot sizes
			int lotSize = leadingOperation.getLotSize();
			int grossQuantity = (netQuantity % lotSize == 0) ? 
					netQuantity : (netQuantity / lotSize + 1) * lotSize;
			
			//Add surplus to stock
			for(Product prod : leadingOperation.getOutputProducts()){
				int surplus = (grossQuantity - netQuantity) * leadingOperation.getOutputPiecesPerOperation(prod);
				stocks.put(prod, surplus);
			}
			
			//create task and start recursive dismantling of subtasks
			SchedulableTask task = new SchedulableTask(product, grossQuantity, operations, order);
			taskSet.add(task);
			dismantle(task);
		}
		
		consolidateTaskSet();
		
		return taskSet;
	}

	private void consolidateTaskSet() {
		// TODO Unite equal tasks, which's orders have the same Start-Date (or even different start dates?)
		
	}

	public TaskSet getOriginalTaskSet() {
		return taskSet;
	}
	
	private void dismantle(SchedulableTask task){
		Order order = task.getOrder();
		List<Operation> operations = task.getOperations();
		Operation leadingOperation = getLeadingOperation(operations);
		
		Map<Product, Integer> conversionRates = new HashMap<Product, Integer>();
		for(Product product : leadingOperation.getInputProducts()){
			conversionRates.put(product, leadingOperation.getInputPiecesPerOperation(product));
		}
		
		for(Entry<Product, Integer> entry : conversionRates.entrySet()){
			if(!entry.getKey().isPurchasable()){ //Are products always either self-made or purchasable or can they be both?
				Product product = entry.getKey();
				List<Operation> subOperations = product.getOperations();
				Operation subLeadingOperation = getLeadingOperation(subOperations);
				subOperations = getSymmetricalOperations(subLeadingOperation, subOperations);
				
				int stock = stocks.get(product);
				int netQuantity = entry.getValue() * task.getQuantity() - stock;
				int lotSize = subLeadingOperation.getLotSize();
				int grossQuantity = (netQuantity % lotSize == 0) ? 
						netQuantity : (netQuantity / lotSize + 1) * lotSize;			
		
				SchedulableTask subTask = new SchedulableTask(product,
															  grossQuantity,
															  subOperations,
															  order);
				taskSet.add(subTask);
				//Add surplus to stock
				for(Product prod : subLeadingOperation.getOutputProducts()){
					int surplus = (grossQuantity - netQuantity) * leadingOperation.getOutputPiecesPerOperation(prod);
					stocks.put(prod, surplus);
				}
				dismantle(subTask);
			}
		}
	}

	private List<Operation> getSymmetricalOperations(
			Operation leadingOperation, List<Operation> operations) {
		List<Operation> result = new ArrayList<Operation>();
		
		if(operations.contains(leadingOperation)){
			result.add(leadingOperation);
			// TODO determine which operations in operations are symmetrical to leadingOperation
		} else {
			result = null;
		}
		return result;
	}

	private Operation getLeadingOperation(List<Operation> operations) {
		// TODO Select a favorable operation. This operation's inputs and LotSize are used during dismantling.
		// only operations with equal inputs and similar lotsizes can be used to create this task.
		return operations.get(0);
	}
	
	//FIXME: OperationStates have to enforce a minimal entry duration. If all operation States are producing this can cause a problem, 
	//because entries have a minimal duration and thus a minimal consumption. this minimal consumption might not have been encountered 
	//during dismantling. Dispatching might eventually run out of stock...
	
}

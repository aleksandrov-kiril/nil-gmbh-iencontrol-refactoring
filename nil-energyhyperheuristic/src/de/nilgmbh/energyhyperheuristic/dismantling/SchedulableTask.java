package de.nilgmbh.energyhyperheuristic.dismantling; 
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Order;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.nilgmbh.energyhyperheuristic.dispatching.PointInTime;

/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 20.05.2015
 */
public class SchedulableTask {
		
	private Product product;
	private int quantity;
	private List<Operation> operations;
	private Order order;

	public SchedulableTask(Product product, int quantity,
			List<Operation> operations, Order order) {
		super();
		this.product = product;
		this.quantity = quantity;
		this.operations = operations;
		this.order = order;
	}

	/**
	 * returns a set of all {@link ProductionPlanEntry}s that could be produced on this 
	 * {@link Workplace} in this Quantity.
	 * The Product could be produced in different lotsizes. E.g.: Operation A has lotsize 
	 * 50, Operation B has lotsize 30. An OutputQuantity of 150 could be generated in two 
	 * fashions.
	 * @param outputQuantity
	 * @param workplace
	 * @return {@code null} if this product cannot produced in this lotsize on this workplace
	 * otherwise a Set of all imaginable Entries.
	 */
	public Set<ProductionPlanEntry> createEntry(Integer outputQuantity, Workplace workplace){
		if (outputQuantity == null){
			return null;
		}
		
		
		Set<ProductionPlanEntry> result = new HashSet<ProductionPlanEntry>();
		for(Operation operation : getPossibleOperations(outputQuantity, workplace)){
			InternalOperation io = (InternalOperation) operation;
			boolean retooling = !io.getOwner().equals(PointInTime.getWorkplaceStates().get(workplace));
			ProductionPlanEntry entry = new ProductionPlanEntry(workplace, 
												io.getOwner(), 
												operation, 
												order,
												outputQuantity / operation.getOutputPiecesPerOperation(product),
												0,		 
												retooling);
			entry.calculateCost(); //FIXME: Cost added correctly?
			result.add(entry); 
		}
		
		if(result.isEmpty()){
			return null;
		} else {
			return result;
		}
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}
	
	/**
	 * Determines in which larger quantity the {@link Product} could be produced.
	 * E.g.: A product that can be produced in LotSizes 20 and 50. It is currently 
	 * produced at a Quantity 150 (3x50). The next possible Quantity would be 160 
	 * (8x20). (given that one Operation produces one Output)
	 * @param currentQuantity current number of Output-Products (OutputQuantity * OpertionCount)
	 * @return Next higher allowed number of Output-Products. 
	 * {@code null} if no lotSizes could be found in this SchedulableTask
	 */
	public Integer getNextProducableQuantity(int currentQuantity, Workplace workplace){
		Set<Integer> results = new HashSet<Integer>();
		currentQuantity++;
		for(Operation operation : operations){
			InternalOperation io = (InternalOperation) operation;
			if (io.getOwner().getWorkplace().equals(workplace)){
				int lots = currentQuantity / (operation.getLotSize() * operation.getOutputPiecesPerOperation(product));
				if(currentQuantity % (operation.getLotSize() * operation.getOutputPiecesPerOperation(product)) > 0){
					lots++;
				}
				results.add(lots * operation.getOutputPiecesPerOperation(product) * operation.getLotSize());
			}	
		}
		
		if(!results.isEmpty()){
			return Collections.min(results);
		} else {
			return null;
		}
	}
	
	/**
	 * Determines which {@link Operation}s of this SchedulableTask would allow production
	 * in the given OutputQuantity.
	 * @param outputQuantity 
	 * @param workplace 
	 * @return Set of Operations that can produce the product of this SchedulableTask
	 * in the given Quantity. Returns an empty set if no Operation can produce the
	 * given Quantity.
	 */
	public Set<Operation> getPossibleOperations(int outputQuantity, Workplace workplace){
		Set<Operation> result = new HashSet<Operation>();
		for(Operation operation : operations){
			InternalOperation io = (InternalOperation) operation;
			if(io.getOwner().getWorkplace().equals(workplace) && 
					outputQuantity % (operation.getLotSize()
							* operation.getOutputPiecesPerOperation(product))== 0 ){
				result.add(operation);
			}
		}
		return result;
	}

	@Override
	public String toString() {
		return "SchedulableTask [product=" + product + ", quantity=" + quantity
				+ "]";
	}

	public Order getOrder() {
		return order;
	}

	@Override 
	public Object clone(){
		return new SchedulableTask(product, quantity, operations, order);
	}
}

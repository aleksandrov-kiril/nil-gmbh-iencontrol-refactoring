/**
 * Ecoflex 2009
 * de.fzi.ecoflex.i18n.Messagesi18n.java
 */
package de.nilgmbh.i18n;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;



/**
 * @author aleksa
 *
 */
public class Messagesi18n {
	
	private static String BUNDLE_NAME = "de.nilgmbh.i18n.messages";
	
	private Messagesi18n(){
	}
	
	private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME, Locale.GERMANY);

	
	public static String getString(String key){
		try{
			return rb.getString(key);
		}
		catch(MissingResourceException e){
			return e + "!"; 
		}
	}
	
}

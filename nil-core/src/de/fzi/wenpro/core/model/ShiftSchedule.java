package de.fzi.wenpro.core.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;

@Entity
public class ShiftSchedule implements Serializable, Cloneable {
	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -5344125140649420761L;

	private int id;
	private String name;
	private double hoursPerDay;
	private double daysPerPeriod;
	private double shiftsPerDay;
	private Factory factory;
	private ShiftOffPeriods offPeriods;
	
	public ShiftSchedule(Factory factory, String name, ShiftOffPeriods offPeriods, double shiftsPerDay){
		this.factory = factory;
		this.name = name;
		this.offPeriods = offPeriods;
		this.hoursPerDay = offPeriods.getHoursPerWorkingDay();
		this.daysPerPeriod = offPeriods.getDaysPerPeriod();
		// shiftsPerDay cannot be derived from offPeriods unambiguously.
		// e.g. a working time between 8 and 16 could be one 8hr or two 4hr shifts.
		this.shiftsPerDay = shiftsPerDay;
	}
	
	public ShiftSchedule(Factory factory, String name, double hoursPerDay, double shiftsPerDay, double daysPerPeriod){
		this.factory = factory;
		this.name = name;
		this.hoursPerDay = hoursPerDay;
		this.shiftsPerDay = shiftsPerDay;
		this.daysPerPeriod = daysPerPeriod;
	}
	
	protected ShiftSchedule(ShiftSchedule s, Factory factory){
		this.factory = factory;
		name = new String(s.name);
		hoursPerDay = s.hoursPerDay;
		daysPerPeriod = s.daysPerPeriod;
		shiftsPerDay = s.shiftsPerDay;
		offPeriods = s.offPeriods;
	}
	
	protected ShiftSchedule(ShiftSchedule s){
		this(s, s.factory);
	}
	
	public ShiftSchedule clone(){
		return new ShiftSchedule(this);
	}
	
	public ShiftSchedule clone(Factory factory){
		return new ShiftSchedule(this, factory);
	}
	
	// getter and setter methods ==============================================
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
		getFactory().notifyAllObservers();
	}
	
	public double getDaysPerPeriod(){
		return daysPerPeriod;
	}
	
	public void setDaysPerPeriod(double daysPerPeriod){
		this.daysPerPeriod = daysPerPeriod;
		getFactory().notifyAllObservers();
	}
	
	@ManyToOne(cascade = CascadeType.ALL)
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public Factory getFactory(){
		return factory;
	}
	
	public void setFactory(Factory factory){
		this.factory = factory;
	}
	
	public double getHoursPerDay(){
		return hoursPerDay;
	}
	
	public void setHoursPerDay(double hoursPerDay){
		this.hoursPerDay = hoursPerDay;
		getFactory().notifyAllObservers();
	}
	
	public double getShiftsPerDay(){
		return shiftsPerDay;
	}
	
	public void setShiftsPerDay(double shiftsPerDay){
		this.shiftsPerDay = shiftsPerDay;
		getFactory().notifyAllObservers();
	}
	
	@Transient
	public double getHoursPerPeriod(){
		return getDaysPerPeriod() * getHoursPerDay();
	}
	
	@Transient
	public double getSecondsPerPeriod(){
		return getDaysPerPeriod() * getHoursPerDay() * 3600;
	}
	
	@Transient
	public double getSecondsPerDay(){
		return getHoursPerDay() * 3600;
	}
	
	@Transient
	public double getSecondsPerShift(){
		return getSecondsPerDay() / getShiftsPerDay();
	}
	
	public ShiftOffPeriods getOffPeriods() {
		return offPeriods;
	}

	@Override
	public String toString() {
		return 	"ShiftSchedule (" + id + ") " + name + ": " + 
				hoursPerDay + " , " + daysPerPeriod + " , " + shiftsPerDay + " , " + factory;
	}
}

package de.fzi.wenpro.core.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;



public class ShiftOffPeriods implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7518241922720720384L;

	/** 
	 * Enables to prohibit work on a certain day of the week, e.g. on Weekends.
	 * @author ev_prohl
	 *
	 */
	public enum Day{
		MONDAY(2), TUESDAY(3), WEDNESDAY(4), THURSDAY(5), FRIDAY(6), SATURDAY(7), SUNDAY(1);
		
		private int calendarValue;
		
		Day(int calendarValue){
			this.calendarValue = calendarValue;
		}
		
		public int getCalendarValue(){
			return calendarValue;
		}
		
		Day addDays(int numberOfDays){
			for (Day d : Day.values()){
				if ((getCalendarValue()+numberOfDays) % 7 == d.getCalendarValue() % 7){
					return d;
				}
			}
			return null;
		}
	}
	
	/**
	 * DayTime is used to prohibit work during the defined interval.
	 * The interval is defined by 2 doubles.
	 * 
	 * To prohibit work between e.g. 22:00 and 6:00, two DayTime-Objects 
	 * need to be used. 22:00 - 24:00 and 0:00 - 6:00.
	 * @author ev_prohl
	 *
	 */
	public class DayTime {
		private double begin;
		private double end;
		
		/**
		 * Creates an interval of time in which work is not allowed
		 * @param b begin as daytime in seconds
		 * @param e end as daytime in seconds
		 */
		public DayTime(double b, double e){
			assert((b > 0.0) && (b < e) && (e < 24.0*60.0*60.0));
			begin = b;
			end = e;
		}
		
		/**
		 * Checks if a given time-object's daytime lies in the interval
		 * @param t Time-object to be checked
		 * @return true, if t's daytime falls in the interval
		 */
		boolean contains(Time t){
			return (begin <= t.getDaytime()) && (t.getDaytime() < end);
		}
		
		/**
		 * 
		 * @return end of daytime in seconds
		 */
		public double getEnd(){
			return end;
		}
	}
	
	private Set<Integer> avoidedWeekDays;
	private Set<DayTime> avoidedDayTimes;
	private final long CYCLE_TIME_IN_MILLISEC = 7*24*60*60*1000;
	
	public ShiftOffPeriods(){
		avoidedWeekDays = new HashSet<Integer>();
		avoidedDayTimes = new HashSet<DayTime>();
	}
	
		
	public void addAvoidedWeekday(Day d){
		avoidedWeekDays.add(d.getCalendarValue()); //Days in util.Calendar are numbered 1...7
	}
	
	/**
	 * To prohibit work between e.g. 22:00 and 6:00, two DayTime-Objects 
	 * need to be used. 22:00 - 24:00 and 0:00 - 6:00.
	 */
	public void addAvoidedDayTime(double begin, double end){
		DayTime dt = new DayTime(begin, end);
		avoidedDayTimes.add(dt);
	}
	
	/**
	 * Checks if this ShiftSchedule allows Work at the given point in time.
	 * @param now: Time-object to be checked
	 * @return true, if work is allowed
	 */
	public boolean isWorking(Time now){
		boolean result = true;
		//check Weekdays
		result = !avoidedWeekDays.contains(now.getNow().get(Calendar.DAY_OF_WEEK));
		//check Daytimes
		if (result) {
			for(DayTime dt : avoidedDayTimes){
				if (dt.contains(now)) {
					result = false;
				}
			}
		}
		
		
		
		
		return result;
	}
	
	/**
	 * @param now: Time Object that represents the current point in time
	 * 			   in a schedule that is being built
	 * @return in seconds the amount of time until this ShiftSchedule allows to work
	 * 		   again
	 */
	public double getRemainingNonWorkingTime(Time now){
		double result = 0;
		if (!isWorking(now)){
			Time t = new Time(now);
			//days
			if (avoidedWeekDays.size() == 7) { //avoid infinite loop if all 7  weekdays are avoided
				result = Double.MAX_VALUE;
			} else {
				while(!isWorking(t)){
					while ((avoidedWeekDays.contains(t.getNow().get(Calendar.DAY_OF_WEEK)))){
						result += 24.0 * 3600.0 - t.getDaytime();
						t.moveT(24.0 * 3600.0 - t.getDaytime());
					}
					double additionalDayTime = 0.0;
					boolean stop = false;
					while (!stop){
						stop = true;
						for(DayTime dt : avoidedDayTimes){
							if (dt.contains(t)) {
								stop = false;
								additionalDayTime += (dt.end - t.getDaytime());
								t.moveT(dt.end - t.getDaytime());
							}
						}
						if (additionalDayTime > 24.0 * 3600.0){ //in case all 24h are avoided, we need to break infinite loop. TODO: Not working, because daybreak at 24:00 interrupts and additional daytime is set to 0 again.
							result = Double.MAX_VALUE;
							additionalDayTime = 0.0;
							break;
						}	
					}
					result += additionalDayTime;
				}	
			}
		}
		
		return result;
	}
	
	/**
	 * @param now: Time object that represents the current point in time
	 * 			   in a schedule that is being built
	 * @return in seconds the remaining amount of time this ShiftSchedule allows to work
	 * 		   uninterruptedly
	 */
	public double getRemainingWorkingTime(Time time){
		double result = 0.0;
		double result2 = 0.0;
		if(isWorking(time)){
			Set<Double> begins = new HashSet<Double>();
			begins.add(Double.MAX_VALUE);
			for(DayTime dt : avoidedDayTimes){
				if(dt.begin - time.getDaytime() > 0){
					begins.add(dt.begin - time.getDaytime());
				} else {
					begins.add(24.0*3600.0 + (dt.begin - time.getDaytime()));
				}
			}
			result = Collections.min(begins);
			if (avoidedWeekDays.size() > 0){
				Time t = new Time(time);
				do {
					result2 += 24.0 * 3600.0 - t.getDaytime();
					t.moveT(24.0 * 3600.0 - t.getDaytime());
				} while (!(avoidedWeekDays.contains(t.getNow().get(Calendar.DAY_OF_WEEK))));
				if (result2 < result) {
					result = result2;
				}
			}
		}
		return result;
	}

	/**
	 * Calculates time that was allowed working time prior to the given time-object
	 * E.g. now = 22:00 and work is prohibited from 02:00 to 10:00. This leads to a 
	 * previousWorkingTime of 12 hrs (value is returned in seconds). Does not include 
	 * workspace occupancy.
	 * 
	 * BEWARE: Only incorporates this ShiftSchedule. However a different ShiftSchedule 
	 * could be active, depending on how far you go back.
	 * 
	 * @param now Time-object to be checked
	 * @return time in seconds that could have been worked previously, according to 
	 * 		   this ShiftSchedule
	 */
	public double getPreviousWorkingTime(Time now) {
		double result = 0.0;
		double result2 = 0.0;
		if(isWorking(now)){
			Set<Double> begins = new HashSet<Double>();
			begins.add(-Double.MAX_VALUE);
			for(DayTime dt : avoidedDayTimes){
				if(dt.end - now.getDaytime() <= 0){
					begins.add(dt.end - now.getDaytime());
				} else {
					begins.add((dt.end - now.getDaytime()) - 24.0*3600.0 );
				}
			}
			result = Collections.max(begins);
			if (avoidedWeekDays.size() > 0){
				Time t = new Time(now);
				result2 += t.getDaytime() - 24.0 * 3600.0;
				t.moveT(t.getDaytime() - 48.0 * 3600.0 );
				while (!(avoidedWeekDays.contains(t.getNow().get(Calendar.DAY_OF_WEEK)))){
					result2 +=  - 24.0 * 3600.0;
					t.moveT( - 24.0 * 3600.0 );
				}
				if (result2 > result) {
					result = result2;
				}
			}
		}
		
		return result;
	}

	public double getHoursPerWorkingDay() {
		double result = 24.0 * 3600.0;
		for(DayTime dt : avoidedDayTimes){
			result -= (dt.end - dt.begin);
		}
		return result/3600.0;
	}

	public double getDaysPerPeriod() {
		return 7 - avoidedWeekDays.size();
	}


	public Set<Integer> getAvoidedWeekDays() {
		return avoidedWeekDays;
	}


	public Set<DayTime> getAvoidedDayTimes() {
		return avoidedDayTimes;
	}


	public long getCYCLE_TIME_IN_MILLISEC() {
		return CYCLE_TIME_IN_MILLISEC;
	}
	
}

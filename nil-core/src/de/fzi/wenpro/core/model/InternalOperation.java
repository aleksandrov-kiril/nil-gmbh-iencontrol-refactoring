/**
 * ecoFLEXiP 2010
 * de.fzi.ecoflex.core.model.InternalOperation.java
 */
package de.fzi.wenpro.core.model;

import java.io.Serializable;

import javax.persistence.Transient;

/**
 * @author wicaksono
 *
 */
public class InternalOperation extends Operation implements Serializable{

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 1658041606053018925L;
	private WorkplaceState owner;
	private double scrapRate;
	
	//TODO:---------only for Gloss
	private double mmf = 1;
	private double componentFactor = 1;
	//--------------
	public InternalOperation(){
		super();
	}

	public InternalOperation(String name, WorkplaceState owner){
		super(name);
		this.owner = owner;
	}

	public InternalOperation(WorkplaceState owner, Product product, Product inputProduct, double time, int lotSize, double scrapRate){
		super(product, inputProduct, time, lotSize);
		this.owner = owner;
		this.setFactory(owner.getWorkplace().getFactory());
		owner.getOperations().add(this);
		this.scrapRate = scrapRate;
		if (lotSize == 0){
			this.lotSize = getDefaultLotSize();
		}
	}

	public InternalOperation(WorkplaceState owner, double time, int lotSize, double scrapRate){
		super();
		this.time = time;
		this.lotSize = lotSize;
		this.owner = owner;
		this.setFactory(owner.getWorkplace().getFactory());
		owner.getOperations().add(this);
		this.scrapRate = scrapRate;
		if (lotSize == 0){
			this.lotSize = getDefaultLotSize();
		}
	}
	
	public InternalOperation(WorkplaceState owner, Product product, double time, double scrapRate, int lotSize) {
		this(owner, product, null, time, lotSize, scrapRate);		
	}
	
	
	public InternalOperation(WorkplaceState owner, Product product, double time, double scrapRate) {
		this(owner, product, time, scrapRate, 0);	
	}
	
	public InternalOperation(InternalOperation internalOperation){
		super(internalOperation);
		this.owner = internalOperation.owner;
		this.setFactory(owner.getWorkplace().getFactory());
		this.scrapRate = internalOperation.scrapRate;
	}
	
	public InternalOperation(InternalOperation internalOperation, Factory factory, WorkplaceState owner){
		super(internalOperation);
		this.owner = owner;
		this.setFactory(owner.getWorkplace().getFactory());
		this.scrapRate = internalOperation.scrapRate;
		setFactory(factory);
	}
	
	public InternalOperation(WorkplaceState state, double time, int lotSize,
			double scrapRate, double mmf, double componentFactor) {
		this (state, time, lotSize, scrapRate);
		this.mmf = mmf;
		this.componentFactor = componentFactor;
	}

	public InternalOperation clone(Factory factory, WorkplaceState owner){
		return new InternalOperation(this, factory, owner);
	}

	@Override
	public InternalOperation clone(){
		return new InternalOperation(this);
	}

	public WorkplaceState getOwner() {
		return owner;
	}

	public void setOwner(WorkplaceState owner) {
		this.owner = owner;
	}

	public double getScrapRate() {
		return scrapRate;
	}

	public void setScrapRate(double scrapRate) {
		this.scrapRate = scrapRate;
	}
	
	
	public double getMmf() {
		return mmf;
	}

	public void setMmf(double mmf) {
		this.mmf = mmf;
	}

	public double getComponentFactor() {
		return componentFactor;
	}

	public void setComponentFactor(double componentFactor) {
		this.componentFactor = componentFactor;
	}

	/**
	 * Calculates the profit made by creating one piece, without regarding the
	 * variable costs.
	 * @param theoretical: If this is <code>true</code>, the scrap rate is ignored.
	 * @return The profit made.
	 */
	@Transient
	@Deprecated
	public double getProfit(boolean theoretical){
		//TODO: check usage
		double result = 0;
		for (ProductQuantity pq : outputs){
			result += pq.getProduct().getValue();
		}
		if (theoretical){
			result *= getScrapRate();
		}
		for (ProductQuantity pq : inputs){
			result -= pq.getProduct().getValue();
		}
		return result;
	}
	
	@Override
	public boolean isPurchased() {
		return false;
	}
	
	private int getDefaultLotSize(){
		if (getOwner().getRetoolingTime() > 0 && this.time > 0){
			return (int)Math.ceil(getOwner().getRetoolingTime()/this.time);
		}else{
			return 1;
		}
	}
}

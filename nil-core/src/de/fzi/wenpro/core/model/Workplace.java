package de.fzi.wenpro.core.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionOfElements;

import de.fzi.wenpro.core.energy.model.facility.ProductionFacility;

/**
 * @author Kiril Aleksandrov
 * 
 */
@Entity
public class Workplace extends FactoryNode implements Serializable {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 3103080585029394068L;

	protected int multiplicity;
	private int workerLimit;
	//	private List<Produces> produces = new ArrayList<Produces>();
	private Map<ShiftSchedule, Double> malfunctionTime = new HashMap<ShiftSchedule, Double>();
	protected double personnelAttendanceRate;
	private List<WorkplaceState> states = new ArrayList<WorkplaceState>();
	private WorkplaceState activeState;
	
	// additional attribute for wenpro
	private ProductionFacility productionFacility;
	

	// constructors
	// ==============================================================

	public Workplace(){
		super();
	}

	protected Workplace(Workplace w, FactoryNode parent){
		super(w, parent);
		//		for (Produces p : w.produces)
		//			produces.add(p.clone(this));

		for (int i = 0; i < getFactory().getShiftplans().size(); i++){
			ShiftSchedule shiftplan = getFactory().getShiftplans().get(i);
			if (i < w.getFactory().getShiftplans().size()){
				ShiftSchedule originalShiftplan = w.getFactory().getShiftplans().get(i);
				malfunctionTime.put(shiftplan, w.malfunctionTime.get(originalShiftplan));
			}else{
				malfunctionTime.put(shiftplan, 0d);
			}
		}
		multiplicity = w.getMultiplicity();
		workerLimit = w.getWorkerLimit();
		personnelAttendanceRate = w.getPersonnelAttendanceRate();

		for (WorkplaceState wsSrc: w.states) {
			WorkplaceState clonedState = wsSrc.clone(this);
			this.states.add(clonedState);
			if (wsSrc.equals(w.activeState)) {
				this.activeState = clonedState;
			}
		}
	}

	/**
	 * Creates a clone of the given Workplace using its parent and ShiftSchedule
	 * references.
	 * @param w The Workplace that is cloned.
	 */
	protected Workplace(Workplace w){
		super(w, w.getParent());
		//		for (Produces p : w.produces)
		//			produces.add(p.clone(this));
		for (ShiftSchedule shiftplan : w.getFactory().getShiftplans())
			malfunctionTime.put(shiftplan, w.getMalfunctionTime(shiftplan));
		multiplicity = w.getMultiplicity();
		workerLimit = w.getWorkerLimit();
		personnelAttendanceRate = w.getPersonnelAttendanceRate();

		for (WorkplaceState wsSrc: w.states) {
			WorkplaceState clonedState = wsSrc.clone(this);
			this.states.add(clonedState);
			if (wsSrc.equals(w.activeState)) {
				this.activeState = clonedState;
			}
		}
	}

	public Workplace(String name, FactoryNode parent, int multiplicity){
		super(name, parent);
		this.multiplicity = multiplicity;
		this.setWorkerLimit(1);
		personnelAttendanceRate = 1;
	}

	public Workplace(String name, FactoryNode parent){
		this(name, parent, 1);
	}

	@Override
	public Workplace clone(FactoryNode parent){
		return new Workplace(this, parent);

	}

	@Override
	public Workplace clone(){
		return new Workplace(this, this.getParent());
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
		in.defaultReadObject();
//		createOO();
	}

	// getter and setter methods
	// =========================================================

	public int getMultiplicity(){
		return multiplicity;
	}

	public void setMultiplicity(int multiplicity){
		this.multiplicity = multiplicity;
		getFactory().notifyAllObservers();
	}

	public void setWorkerLimit(int workerLimit){
		this.workerLimit = workerLimit;
		getFactory().notifyAllObservers();
	}

	public int getWorkerLimit(){
		return workerLimit;
	}

	public void setPersonnelAttendanceRate(double personnelAttendanceRate){
		this.personnelAttendanceRate = personnelAttendanceRate;
		getFactory().notifyAllObservers();
	}

	public double getPersonnelAttendanceRate(){
		return personnelAttendanceRate;
	}


	@CollectionOfElements(targetElement = Double.class)
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public Map<ShiftSchedule, Double> getMalfunctionTime(){
		return malfunctionTime;
	}

	public void setMalfunctionTime(Map<ShiftSchedule, Double> malfunctionTime){
		this.malfunctionTime = malfunctionTime;
		getFactory().notifyAllObservers();
	}

	public double getMalfunctionTime(ShiftSchedule shiftplan){
		Double result = getMalfunctionTime().get(shiftplan);
		return result == null ? 0 : result;
	}

	public void setMalfunctionTime(ShiftSchedule shiftplan, double malfunctionTime){
		getMalfunctionTime().put(shiftplan, malfunctionTime);
		getFactory().notifyAllObservers();
	}

	public void setMalfunctionTimeProportion(ShiftSchedule shiftplan, double malfunctionTimeProportion){
		getMalfunctionTime().put(shiftplan, malfunctionTimeProportion * shiftplan.getSecondsPerPeriod());
		getFactory().notifyAllObservers();
	}

	public void setMalfunctionTime(double malfunctionTime){
		for (ShiftSchedule s : getFactory().getShiftplans()){
			getMalfunctionTime().put(s, malfunctionTime);
		}
		getFactory().notifyAllObservers();
	}

	public void setMalfunctionTimeRelative(double malfunctionTime){
		for (ShiftSchedule s : getFactory().getShiftplans()){
			getMalfunctionTime().put(s, malfunctionTime * s.getSecondsPerPeriod());
		}
		getFactory().notifyAllObservers();
	}

	public List<WorkplaceState> getStates() {
		return states;
	}

	public void setStates(List<WorkplaceState> states) {
		this.states = states;
	}
	
	public void addState(WorkplaceState state) {
		states.add(state);
	}

	public WorkplaceState getActiveState() {
		return activeState;
	}

	public void setActiveState(WorkplaceState activeState) {
		this.activeState = activeState;
	}
	
	
	
	// operations getters and setter ============================
	
	
//	/**
//	 * @return the output
//	 */
//	@Transient
//	public WorkplaceOutputOperations getWOO(){ // FIXME Override getOO()?
//		return (WorkplaceOutputOperations)getOO();
//	}
//
//	@Override
//	protected void createOO(){
//		this.output = new WorkplaceOutputOperations(this);
//	}

	public ProductionFacility getProductionFacility() {
		return productionFacility;
	}

	public void setProductionFacility(ProductionFacility productionFacility) {
		this.productionFacility = productionFacility;
		this.productionFacility.setWorkplace(this);
	}

	/**
	 * A list of all operations in this {@link Workplace} the produce the given
	 * {@link Product}.
	 * @param p a {@link Product}
	 * @return a list of {@link Operation}s
	 */
	@Transient
	public List<Operation> getOperation(Product p){
		List<Operation> result = new ArrayList<Operation>();
		for (WorkplaceState ws : this.getStates()){
			result.addAll(ws.getOperation(p));
		}
		return result;
	}

	/**
	 * The maximal time that this Workplace can produce in one period.
	 * @param shiftplan The shift model.
	 * @return The working time in seconds.
	 */
	@Transient
	public double getMaximalProductionTime(ShiftSchedule shiftplan){
		if (isEnabledAncestors()){
			return getMultiplicity() * getWorkerLimit() * (shiftplan.getSecondsPerPeriod() - getMalfunctionTime(shiftplan));
		}else{
			return 0;
		}
	}

	/**
	 * The maximal time that this Workplace can theoretically produce in one
	 * period (not counting the malfunctioning time).
	 * @param shiftplan The shift model.
	 * @return The working time in seconds.
	 */
	@Transient
	public double getMaximalProductionTimeTh(ShiftSchedule shiftplan){
		if (isEnabledAncestors()){
			return getMultiplicity() * getWorkerLimit() * shiftplan.getSecondsPerPeriod();
		}else{
			return 0;
		}
	}

	/**
	 * Checks if this node may have subnodes.
	 */
	@Override
	@Transient
	public boolean mayHaveSubNodes(){
		return false;
	}

	/**
	 * Checks if this node may have other subnodes than Workplaces.
	 */
	@Override
	@Transient
	public boolean mayHaveNonWorkplaceSubNodes(){
		return false;
	}

	@Override
	@Transient
	public int getLevel(){
		return 3;
	}

	/**
	 * Returns a list of all products relevant for this node.
	 */
	@Transient
	public Product[] getRelevantProducts(){
		Set<Product> result = new HashSet<Product>();
		for (WorkplaceState ws : this.getStates()){
			for (Operation op: ws.getOperations()){
				result.addAll(op.getOutputProducts());
			}
		}
		return result.toArray(new Product[0]);

	}

	/**
	 * Checks whether a {@link Product} can be produced in current Workplace
	 * @return true if the product can be produced in current Workplace
	 */
	@Transient
	public boolean isProduced(Product product){
		for (Product p : getRelevantProducts())
			if (p.equals(product))
				return true;
		return false;
	}
	
	
	/**
	 * Checks whether a the Products of a list of ProductQantitys can be produced in current Workplace
	 * 
	 * @param products
	 * @return false if producs empty or if one of the ProductQantity contains Product that is not produced on this Workplace, true otherwise
	 */
	public boolean areProduced(List<ProductQuantity> products){
		if(products.isEmpty()){
			return false;
		}
		for(ProductQuantity pq:products){
			if(!isProduced(pq.getProduct())){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Creates a {@link WorkplaceState} with the given name.
	 * @param name the name of the new WorkplaceState
	 * @return On success, the new WorkplaceState is returned. If the WorkplaceState
	 *         couldn't be added, <code>null</code> is returned.
	 */
	public WorkplaceState createWorkplaceState(String name) {
		WorkplaceState result = new WorkplaceState(name, this);
		this.states.add(result);
		return result;
	}
	
	/**
	 * Creates a {@link WorkplaceState} with the given name and retooling time.
	 * @param name the name of the new WorkplaceState
	 * @param retoolingTime the retooling time in seconds
	 * @return On success, the new WorkplaceState is returned. If the WorkplaceState
	 *         couldn't be added, <code>null</code> is returned.
	 */
	public WorkplaceState createWorkplaceState(String name, double retoolingTime) {
		WorkplaceState result = new WorkplaceState(name, retoolingTime, this);
		this.states.add(result);
		return result;
	}
	
	public Operation produce (Product p, double time) {
		if(this.getActiveState()==null){
			WorkplaceState state = new WorkplaceState(this);
			state.setName("default");
			this.setActiveState(state);
			this.getStates().add(state);
		}
		Operation op = this.getActiveState().produce(p, time);
		return op;
	}
	
	/**
	 * Returns a list of {@link WorkplaceState} that produce the specified product
	 * @param product
	 * @return On success, the corresponding WorkplaceState is returned. 
	 *         Otherwise <code>null</code> is returned.
	 */
	public List<WorkplaceState> getProducingWorkplaceState(Product product) {
		List<WorkplaceState> producingStates = new ArrayList<WorkplaceState>();
		for(Operation op : this.getOperation(product)){
			if(op instanceof InternalOperation){
				if(producingStates.contains(((InternalOperation) op).getOwner())){
					// workplace state allready in the list
				}else {
					producingStates.add(((InternalOperation) op).getOwner());
				}
			}
		}
		return producingStates;
	}
	
	/**
	 * Returns the {@link WorkplaceState} that operates the specified operation
	 * @param operation
	 * @return On success, the corresponding WorkplaceState is returned. 
	 *         Otherwise <code>null</code> is returned.
	 */
	public WorkplaceState getWorkplaceStateByOperation(Operation operation) {
		for (WorkplaceState ws: this.states) {
			for (Operation op : ws.getOperations()) {
				if (op.equals(operation)) {
					return ws;
				}
			}
		}
		return null;
	}
	
	
}

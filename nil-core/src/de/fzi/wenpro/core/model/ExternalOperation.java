/**
 * ecoFLEXiP 2010
 * de.fzi.ecoflex.core.model.ExternalOperation.java
 */
package de.fzi.wenpro.core.model;

import java.io.Serializable;

public class ExternalOperation extends Operation implements Serializable{

	private static final long serialVersionUID = 6082967093693559207L;

	public ExternalOperation(){
		super();
	}
	
	public ExternalOperation(String name, Factory factory) {
		super(name);
		this.setFactory(factory);
	}
	
	public ExternalOperation(Factory factory, Product outputProduct) {
		this();
		addOutput(outputProduct);
		this.setFactory(factory);
	}
	
	public ExternalOperation(ExternalOperation externalOperation) {
		super(externalOperation);
	}
	
	public ExternalOperation(ExternalOperation externalOperation, Factory factory) {
		super(externalOperation);
		setFactory(factory);
	}
	
	@Override
	public boolean isPurchased() {
		return true;
	}

	@Override
	public ExternalOperation clone(){
		return new ExternalOperation(this);
	}

	public ExternalOperation clone(Factory factory){
		return new ExternalOperation(this, factory);
	}
}

package de.fzi.wenpro.core.model;

import java.io.Serializable;
import java.util.Calendar;

public class Order implements Serializable, Cloneable {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 1806774144713673701L;
	
	private Product product;
	private int quantity;
	private Calendar startDate;
	private Calendar endDate;

	/**
	 * Constructor Order
	 * @param product
	 * @param quantity
	 * @param startPeriod
	 * @param finishingPerion
	 */
	public Order(Product product, int quantity, Calendar startPeriod, Calendar finishingPerion){
		super();
		this.product = product;
		this.quantity = quantity;
		this.startDate = startPeriod;
		this.endDate = finishingPerion;
	}
	
	@Override
	public Order clone(){
		try{
			return (Order)super.clone();
		}catch (CloneNotSupportedException e){
			return null;
		}
	}
	
	public Product getProduct(){
		return product;
	}

	public void setProduct(Product product){
		this.product = product;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
	@Override
	public String toString() {
		return "Order [product=" + product + ", quantity=" + quantity
				+ ", startDate=" + startDate.getTime() + ", endDate=" + endDate.getTime() + "]";
	}
}

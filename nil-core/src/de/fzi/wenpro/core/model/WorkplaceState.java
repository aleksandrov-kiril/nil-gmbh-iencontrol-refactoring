/**
 * ecoFLEXiP 2010
 * de.fzi.ecoflex.core.model.WorkplaceState.java
 */
package de.fzi.wenpro.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

/**
 * @author wicaksono
 *
 */
public class WorkplaceState implements Serializable{
	
	/**
	 * Generated UID
	 */
	private static final long serialVersionUID = 1892000914834175517L;
	
	private int id;
	private String name;
	private Workplace workplace;
	private List<Operation> operations = new ArrayList<Operation>();
	private double retoolingTime;
	private double retoolingCost;
	
	public WorkplaceState(){
	}
	
	public WorkplaceState(Workplace workplace){
		this.workplace = workplace;
	}

	public WorkplaceState(String name, Workplace workplace){
		this(workplace);
		this.name = name;
	}
	
	public WorkplaceState(String name, double retoolingTime, Workplace workplace){
		this(name, workplace);
		this.retoolingTime = retoolingTime;
	}
	
	/**
	 * Copy constructor
	 * @return
	 */
	public WorkplaceState(WorkplaceState ws, Workplace w) {
		this.id = ws.id;
		this.name = ws.name;
		this.workplace = w;
		for (Operation op: ws.getOperations()) {
			this.operations.add(((InternalOperation)op).clone(w.getFactory(), this));
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Workplace getWorkplace() {
		return workplace;
	}

	public void setWorkplace(Workplace workplace) {
		this.workplace = workplace;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}
	
	public double getRetoolingTime() {
		return retoolingTime;
	}

	public void setRetoolingTime(double retoolingTime) {
		this.retoolingTime = retoolingTime;
	}

	public double getRetoolingCost() {
		return retoolingCost;
	}

	public void setRetoolingCost(double retoolingCost) {
		this.retoolingCost = retoolingCost;
	}

	public WorkplaceState clone(Workplace w) {
		return new WorkplaceState(this, w);
	}
	
	public Operation produce(Product output, Product input, int quantity, double time, double scrapRate, int lotSize){
		if (lotSize == 0){
			if (this.retoolingTime > 0 && time > 0){
				lotSize = (int)Math.ceil(this.retoolingTime/time);
			}else{
				lotSize = 1;
			}
		}
		// Notify the Factory about the products in case it doesn't know it
		this.workplace.getFactory().addProduct(output);
		this.workplace.getFactory().addProduct(input);
		
		Operation operation = new InternalOperation(this, output, time, scrapRate, lotSize);
		operation.setFactory(this.workplace.getFactory());
		output.addOperation(operation);
		if (input != null){
			operation.addInput(input, quantity);
		}
		this.workplace.setActiveState(this);
		this.workplace.getFactory().notifyAllObservers();
		return operation;
	}
	
	public Operation produce(Product output, double time, double scrapRate, int lotSize){
		return produce(output, null, 0, time, scrapRate, lotSize);
	}
	
	public Operation produce(Product output, Product input, int quantity, double time, double scrapRate){
		return produce(output, input, quantity, time, scrapRate, 0);
	}
	
	public Operation produce(Product output, double time){
		return produce(output, null, 0, time, 0.0, 0);
	}
	
	public Operation produce(Product output, double time, double scrapRate){
		return produce(output, null, 0, time, scrapRate, 0);
	}
	
	public Operation produce(Product output, Product input, int quantity, double time){
		return produce(output, input, quantity, time, 0.0, 0);
	}

	/**
	 * A list of all operations in this {@link WorkplaceState} the produce the
	 * given {@link Product}.
	 * @param p a {@link Product}
	 * @return a list of {@link Operation}s
	 */
	@Transient
	public List<Operation> getOperation(Product p){
		List<Operation> result = new ArrayList<Operation>();
		for (Operation op : getOperations()){
			for (ProductQuantity pq : op.getOutputs()){
				if (pq.getProduct().equals(p)){
					result.add(op);
				}
			}
		}
		return result;
	}


	@Override
	public String toString() {
		return "Umrüstzustand [name=" + name + "]";
	}
}

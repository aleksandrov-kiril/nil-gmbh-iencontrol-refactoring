package de.fzi.wenpro.core.model;

import static de.nilgmbh.i18n.Messagesi18n.getString;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionOfElements;

@Entity
public class Cost implements Serializable {

	// constants ==============================================================

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 1L;

	// inner classes ==========================================================
	public static enum Type {
		CONSTANT, BYSHIFTPLAN
	}
	
	// FIXME: PERSECOND, PERPRODUCT, PERPERIODE
	public static enum Unit {
		PERPERIOD, PERSECOND, PERSHIFT, PERPIECE, PERDAY, PERHOUR
	}

	// objects variables
	// ==============================================================

	private int id;
	private String name;
	private Factory factory;
	private Type type;
	private Unit unit;
	private double value;
	private Map<ShiftSchedule, Double> values;

	// constructors
	// ==============================================================
	public Cost() {
		values = new HashMap<ShiftSchedule, Double>();
	}

	public Cost(String name, Factory factory, Type type, Unit unit, double value) {
		this();
		this.name = name;
		this.factory = factory;
		this.type = type;
		this.unit = unit;
		this.value = value;
		if (factory.getShiftplans() != null) {
			for (ShiftSchedule s : factory.getShiftplans()) {
				values.put(s, value);
			}
		}
	}

	/**
	 * Creates a clone of the passed cost object. The copy uses the same factory
	 * as the original.
	 */
	protected Cost(Cost c) {
		this();
		name = new String(c.name);
		factory = c.factory;
		type = c.type;
		unit = c.unit;
		value = c.value;
		if (factory.getShiftplans() != null) {
			for (ShiftSchedule s : factory.getShiftplans()) {
				values.put(s, new Double(c.values.get(s)));
			}
		}
	}

	/**
	 * Creates a clone of the passed cost object which uses the specified
	 * factory.
	 */
	protected Cost(Cost c, Factory factory) {
		this();
		name = new String(c.name);
		this.factory = factory;
		type = c.type;
		unit = c.unit;
		value = c.value;
		for (int i = 0; i < factory.getShiftplans().size(); i++) {
			ShiftSchedule shiftplan = factory.getShiftplans().get(i);
			if (i < c.factory.getShiftplans().size()) {
				ShiftSchedule originalShiftplan = c.factory.getShiftplans().get(i);
				values.put(shiftplan, c.values.get(originalShiftplan));
			} else {
				values.put(shiftplan, value);
			}
		}
	}
	
	// getter and setter methods ==============================================
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		getFactory().notifyAllObservers();
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public Factory getFactory() {
		return factory;
	}

	public void setFactory(Factory factory) {
		this.factory = factory;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
		getFactory().notifyAllObservers();
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
		getFactory().notifyAllObservers();
	}

	@CollectionOfElements(targetElement = Double.class)
	public Map<ShiftSchedule, Double> getValues() {
		return values;
	}

	public void setValues(Map<ShiftSchedule, Double> values) {
		this.values = values;
		getFactory().notifyAllObservers();
	}

	public double getValue() {
		return value;
	}

	/**
	 * Sets the constant cost value and the default value for new shiftplans.
	 */
	public void setValue(double value) {
		this.value = value;
		getFactory().notifyAllObservers();
	}

	/**
	 * Creates a clone of the passed cost object. The copy uses the same factory
	 * as the original.
	 */
	public Object clone() {
		return new Cost(this);
	}

	/**
	 * Creates a clone of the passed cost object which uses the specified
	 * factory.
	 * 
	 * @param factory
	 *            The factory the new cost object will belong to.
	 * @return The clone of this object.
	 */
	public Cost clone(Factory factory) {
		return new Cost(this, factory);
	}
	
	/**
	 * Adds a shiftplan to the internal values list. NOTE: this should only be
	 * called by this cost object's factory.
	 * @param shiftplan the shiftplan to be added
	 */
	void addShiftplan(ShiftSchedule shiftplan) {
		getValues().put(shiftplan, value);
	}

	/**
	 * Removes a shiftplan from the internal values list. NOTE: this should only
	 * be called by this cost object's factory.
	 * @param shiftplan the shiftplan to be removed
	 */
	void removeShiftplan(ShiftSchedule shiftplan) {
		getValues().remove(shiftplan);
	}

	/**
	 * Returns the cost value for the selected {@link ShiftSchedule}.
	 * @return The cost value for the selected {@link ShiftSchedule}, or the default
	 *         value if this cost's type is CONSTANT.
	 */
	@Transient
	public double getValue(ShiftSchedule shiftplan) {
		if (isConstant()){
			return value;
		}else{
			return values.get(shiftplan);
		}
	}

	@Transient
	public double getDefaultValue() {
		if (isConstant()){
			return value;
		}else{
			return values.get(getFactory().getDefaultShiftplan());
		}
	}
	
	/**
	 * Sets the cost value for the selected shiftplan, if possible.
	 * @param value the value to be set
	 * @param shiftplan the shiftplan the value is set for. It is not possible
	 *        to store a value that isn't known to this cost object.
	 * @return <code>true</code> is the value has been set, <code>false</code>
	 *         if the shiftplan is not known to this cost object
	 */
	@Transient
	public boolean setValue(double value, ShiftSchedule shiftplan) {
		if (values.containsKey(shiftplan)) {
			values.put(shiftplan, value);
			getFactory().notifyAllObservers();
			return true;
		} else {
			System.err.println("Cost.setValue: this shouldn't happen!");
			return false;
		}
	}

	/**
	 * Returns the cost value for the given shift plan and period, calculated
	 * for one second. The values for PERPIECE and PERPERIOD costs can't be
	 * calculated this way.
	 */
	@Transient
	public double getValuePerSecond(ShiftSchedule shiftplan) {
		switch (this.getUnit()) {
		case PERDAY:
			return getValue(shiftplan) / shiftplan.getSecondsPerDay();
		case PERSHIFT:
			return getValue(shiftplan) / shiftplan.getSecondsPerShift();
		case PERHOUR:
			return getValue(shiftplan) / 3600;
		case PERSECOND:
			return getValue(shiftplan);
		case PERPIECE:
			// Fall through - can't calculate this
		case PERPERIOD:
			// Fall through - can't calculate this
		default:
			return 0;
		}
	}

	@Transient
	public boolean isConstant() {
		return this.getType() == Type.CONSTANT;
	}

	@Transient
	public boolean isByShiftplan() {
		return this.getType() == Type.BYSHIFTPLAN;
	}

	@Transient
	public boolean isPerPeriod() {
		return this.getUnit() == Unit.PERPERIOD;
	}

	@Transient
	public boolean isPerSecond() {
		return this.getUnit() == Unit.PERSECOND;
	}

	@Transient
	public boolean isPerShift() {
		return this.getUnit() == Unit.PERSHIFT;
	}

	@Transient
	public boolean isPerPiece() {
		return this.getUnit() == Unit.PERPIECE;
	}

	@Transient
	public boolean isPerDay() {
		return this.getUnit() == Unit.PERDAY;
	}

	@Transient
	public String getUnitString(){
		return getString("Cost.Unit." + this.getUnit());
	}

	@Transient
	public String getTypeString() {
		switch (this.getType()) {
		case CONSTANT:
			return "Constant";
		case BYSHIFTPLAN:
			return "By shiftplan";
		default:
			return "";
		}
	}
	
	@Override
	public String toString() {
		return getName()+" Typ:"+getTypeString()+" Einheit:"+getUnitString();
	}
}

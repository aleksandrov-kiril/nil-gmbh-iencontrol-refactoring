package de.fzi.wenpro.core.model;

import java.io.Serializable;

/**
 * @author siebel
 * 
 */
public class ProductQuantity implements Serializable{
	
	/**
	 * generated serial uid
	 */
	private static final long serialVersionUID = 5863673473158099283L;
	
	private final Product product;
	private int quantity;
	
	public ProductQuantity(Product product){
		this.product = product;
		this.quantity = 1;
	}
	
	public ProductQuantity(Product product, int quantity){
		this.product = product;
		this.quantity = quantity;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	@Override
	public String toString() {
		return product.getName();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + quantity;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductQuantity other = (ProductQuantity) obj;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (quantity != other.quantity)
			return false;
		return true;
	}

	public void increaseQuantity(int quantity) {
		this.quantity+=quantity;
	}
}

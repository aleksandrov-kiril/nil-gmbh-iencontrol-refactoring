package de.fzi.wenpro.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.deprecated.Supply;

@Entity
public class Product implements Serializable {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 2124690298761254652L;

	private int id;
	private String name;
	private Double value;
	private double ratio;
	private boolean disposable = false;
	private boolean isPurchasable = false;
	private double purchasePrice = 0;
	private double transferPrice = 0;
	private double annualConsumption;
	private Supply supply;

	private List<Operation> operations = new ArrayList<Operation>();
	private Factory factory;

	public Product(Factory factory, String name, double value, boolean disposable){
		this.factory = factory;
		this.name = name;
		this.value = value;
		this.disposable = disposable;
	}

	protected Product(Product p){
		this(p, p.factory);
	}

	protected Product(Product p, Factory factory){
		this.factory = factory;
		name = new String(p.name);
		value = p.value;
		this.ratio = p.ratio;
		disposable = p.disposable;
		transferPrice = p.transferPrice;
		isPurchasable = p.isPurchasable;
		purchasePrice = p.purchasePrice;
		//TODO: clone supply
	}

	public Product clone(Factory factory){
		return new Product(this, factory);
	}

	// getter and setter methods ==============================================
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public double getRatio(){
		return ratio;
	}

	public void setRatio(double ratio){
		this.ratio = ratio;
	}

	public Double getValue(){
		return value;
	}

	public void setValue(Double value){
		this.value = value;
	}

	public void setDisposable(boolean disposable){
		this.disposable = disposable;
	}

	public boolean isDisposable(){
		return disposable;
	}

	public void setPurchasable(boolean isPurchasable){
		this.isPurchasable = isPurchasable;
		this.purchasePrice = 0;
	}

	public boolean isPurchasable(){
		return isPurchasable;
	}

	public void setPurchasePrice(double purchasePrice){
		this.purchasePrice = purchasePrice;
	}

	public double getPurchasePrice(){
		return purchasePrice;
	}

	public double getTransferPrice(){
		return transferPrice;
	}

	public void setTransferPrice(double transferPrice){
		this.transferPrice = transferPrice;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public void addOperation(Operation operation) {
		this.operations.add(operation);
	}
	

	public double getAnnualConsumption() {
		return annualConsumption;
	}

	public void setAnnualConsumption(double annualConsumption) {
		this.annualConsumption = annualConsumption;
	}

	public Supply getSupply() {
		return supply;
	}

	public void setSupply(Supply supply) {
		this.supply = supply;
	}

	public void removeOperation(Operation operation) {
		this.operations.remove(operation);
	}

	@Transient
	public Set<Product> getInputProducts() {
		Set<Product> result = new HashSet<Product>();
		for (Operation op : this.operations){
			result.addAll(op.getInputProducts());
		}
		return result;
	}

	@Transient
	public Set<Product> getComponents() {
		return getInputProducts();
	}


	/**
	 * FIXME: javadoc not up-to-date
	 * Adds the specified product as a component with the specified quantity.
	 * If the product already was a component, the quantity is overwritten.
	 * @return <code>True</code> if the product has successfully been added as
	 *         a component, <code>false</code> if not.
	 */
	public boolean addComponent(Product product, int quantity){
		if(operations.isEmpty()){
			//FIXME: this Operation is not assigned to a WorkplaceState!
			Operation op = new InternalOperation();
			op.addOutput(this);
			this.addOperation(op);
		}
		for (Operation op: operations){
			op.addInput(product, quantity);
		}
		return true;
	}
	
	@ManyToOne(cascade = CascadeType.ALL)
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public Factory getFactory(){
		return factory;
	}

	public void setFactory(Factory factory){
		this.factory = factory;
	}

	public Object clone(){
		return new Product(this);
	}

	@Override
	public String toString() {
		return "Product [" + name + "]";
	}
	
	/**
	 * A list of all {@link Product}s that can use this one as a component.
	 * @return a set of {@link Product}s
	 */
	@Deprecated
	public Set<Product> getOutputProducts(){
		Set<Product> result = new HashSet<Product>();
		for (Operation op : EntityList.getOperationList(this.factory)){
			for (ProductQuantity pq : op.inputs){
				if (pq.getProduct().equals(this)){
					result.addAll(op.getOutputProducts());
				}
			}
		}
		return result;
	}
	
	public int getProductIndex(){
		int result = getFactory().getProducts().indexOf(this);
		//FIXME: remove this after testing
		if (result == -1){
			throw new IllegalArgumentException("This product (" + this + ")hasn't been added to its Factory ("+getFactory()+")!");
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(annualConsumption);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (disposable ? 1231 : 1237);
		result = prime * result + (isPurchasable ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		temp = Double.doubleToLongBits(purchasePrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(ratio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(transferPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (Double.doubleToLongBits(annualConsumption) != Double
				.doubleToLongBits(other.annualConsumption))
			return false;
		if (disposable != other.disposable)
			return false;
		if (isPurchasable != other.isPurchasable)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(purchasePrice) != Double.doubleToLongBits(other.purchasePrice))
			return false;
		if (Double.doubleToLongBits(ratio) != Double.doubleToLongBits(other.ratio))
			return false;
		if (Double.doubleToLongBits(transferPrice) != Double.doubleToLongBits(other.transferPrice))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
package de.fzi.wenpro.core.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.energy.model.facility.AncillaryFacility;
import de.fzi.wenpro.core.energy.model.facility.ConversionFacility;
import de.fzi.wenpro.core.energy.model.facility.TransportFacility;
import de.fzi.wenpro.core.printing.FactoryNodeOutputOperations;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class FactoryNode extends FactoryResource implements Cloneable,
		Serializable {
	// constants ==============================================================

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -8667910734518670743L;

	// objects variables
	// ==============================================================

	private List<FactoryNode> subNodes;
	private FactoryNode parent;
	private boolean enabled;

	// operations objects
	protected transient FactoryNodeOutputOperations output; // oo

	/**
	 * Contains a reference to the object this FactoryNode was cloned from. If
	 * it's not a clone, contains null.
	 */
	protected FactoryNode original;

	// additional attributes for wenpro
	private List<TransportFacility> transportFacilities;
	private List<AncillaryFacility> ancillaryFacilities;
	private List<ConversionFacility> conversionFacilities;

	// constructors and clone methods and cache management
	// ===========================

	protected FactoryNode() {
		// createOO();
		parent = null;
		enabled = true;
		subNodes = new ArrayList<FactoryNode>();
		transportFacilities = new ArrayList<TransportFacility>();
		ancillaryFacilities = new ArrayList<AncillaryFacility>();
		conversionFacilities = new ArrayList<ConversionFacility>();
		// costs = new ArrayList<Cost>();
	}

	public FactoryNode(String name) {
		super(name);
		parent = null;
		enabled = true;
		subNodes = new ArrayList<FactoryNode>();
		transportFacilities = new ArrayList<TransportFacility>();
		ancillaryFacilities = new ArrayList<AncillaryFacility>();
		conversionFacilities = new ArrayList<ConversionFacility>();
	}

	public FactoryNode(String name, FactoryNode parent) {
		this(name);
		this.parent = parent;
		if (parent == null)
			setFactory((Factory) this);
		else
			setFactory(parent.getFactory());
		original = null;
		enabled = true;
	}

	protected FactoryNode(FactoryNode f, FactoryNode parent) {
		this();
		this.parent = parent;
		if (parent == null)
			setFactory((Factory) this);
		else
			setFactory(parent.getFactory());
		this.setName(f.getName());
		for (FactoryNode subnode : f.subNodes) {
			addSubNode(subnode.clone(this));
		}
		for (Cost c : f.getCosts())
			this.getCosts().add(c.clone(this.getFactory()));

		original = f;
		enabled = f.enabled;
	}

	public FactoryNode clone() {
		return new FactoryNode(this, getParent());
	}

	public FactoryNode clone(FactoryNode parent) {
		return new FactoryNode(this, parent);
	}

	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException {
		in.defaultReadObject();
		// createOO();
	}

	/**
	 * Calls the setChanged and notifyObservers method in this object and all
	 * children.
	 */
	protected void notifyAllObservers() {
		setChanged();
		notifyObservers();
		for (FactoryNode f : getSubNodes()) {
			f.notifyAllObservers();
		}
	}

	// getter and setter methods ==============================================

	public void setName(String name) {
		super.setName(name);
		getFactory().notifyAllObservers();
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "parent")
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public List<FactoryNode> getSubNodes() {
		return subNodes;
	}

	public void setSubNodes(List<FactoryNode> subNodes) {
		this.subNodes = subNodes;
		getFactory().notifyAllObservers();
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public FactoryNode getParent() {
		return parent;
	}

	public void setParent(FactoryNode parent) {
		this.parent = parent;
		getFactory().notifyAllObservers();
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public FactoryNode getOriginal() {
		return original;
	}

	public void setOriginal(FactoryNode original) {
		this.original = original;
		getFactory().notifyAllObservers();
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		getFactory().notifyAllObservers();
	}

	public void setFactory(Factory factory) {
		super.setFactory(factory);
	}

	public Factory getFactory() {
		return super.getFactory();
	}

	public List<TransportFacility> getTransportFacilities() {
		return transportFacilities;
	}

	public void setTransportFacilities(
			List<TransportFacility> transportFacilities) {
		this.transportFacilities = transportFacilities;
	}

	public List<AncillaryFacility> getAncillaryFacilities() {
		return ancillaryFacilities;
	}

	public void setAncillaryFacilities(
			List<AncillaryFacility> ancillaryFacilities) {
		this.ancillaryFacilities = ancillaryFacilities;
	}

	public List<ConversionFacility> getConversionFacilities() {
		return conversionFacilities;
	}

	public void setConversionFacilities(
			List<ConversionFacility> conversionFacilities) {
		this.conversionFacilities = conversionFacilities;
	}

	/**
	 * Adds the given factory node to the subnodes.
	 * 
	 * @return <code>true</code> if the node has been added successfully
	 */
	public boolean addSubNode(FactoryNode f) {
		if (mayAddSubNode(f)) {
			getSubNodes().add(f);
			getFactory().notifyAllObservers();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Adds the given factory node to the subnode list, at the specified
	 * position.
	 * 
	 * @return <code>True</code> if the node has been added successfully.
	 */
	public boolean addSubNode(int position, FactoryNode f) {
		if (mayAddSubNode(f)) {
			getSubNodes().add(position, f);
			getFactory().notifyAllObservers();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Creates a subnode with the given name.
	 * 
	 * @return The new subnode, or null if the subnode couldn't be added.
	 */
	public FactoryNode createSubNode(String name) {
		FactoryNode result = new FactoryNode(name, this);
		if (addSubNode(result)) {
			return result;
		} else {
			return null;
		}
	}

	/**
	 * Creates a {@link Workplace} with the given name and multiplicity.
	 * 
	 * @param name
	 *            the name of the new Workplace
	 * @param multiplicity
	 *            the multiplicity of the new Workplace.
	 * @return On success, the new Workplace is returned. If the Workplace
	 *         couldn't be added, <code>null</code> is returned.
	 */
	public Workplace createWorkplace(String name, int multiplicity) {
		Workplace result = new Workplace(name, this, multiplicity);
		if (addSubNode(result)) {
			return result;
		} else {
			return null;
		}
	}

	/**
	 * Creates a {@link Workplace} with the given name and multiplicity 1.
	 * 
	 * @param name
	 *            the name of the new Workplace
	 * @return On success, the new Workplace is returned. If the Workplace
	 *         couldn't be added, <code>null</code> is returned.
	 */
	public Workplace createWorkplace(String name) {
		return createWorkplace(name, 1);
	}

	/**
	 * Removes the FactoryNode with the specified index from this node. Only
	 * direct subnodes can be removed that way.
	 * 
	 * @param index
	 *            the index of the subnode to be removed
	 * @return Returns <code>true</code> if a node has been removed.
	 */
	public boolean removeSubnode(int index) {
		if (0 <= index && index < getSubNodes().size()) {
			getSubNodes().remove(index);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Removes the specified FactoryNode from this node. Only direct subnodes
	 * can be removed that way.
	 * 
	 * @return Returns <code>true</code> if a node has been removed.
	 */
	public boolean removeSubnode(FactoryNode f) {
		if (getSubNodes().remove(f)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return the fnoutput
	 */
	@Transient
	public FactoryNodeOutputOperations getOO() {
		return output;
	}

	/**
	 * @param output
	 *            the output to set
	 */
	protected void createOO() {
		this.output = new FactoryNodeOutputOperations(this);
	}

	/**
	 * Checks if the given factory node can be added to the subnodes.
	 */
	public boolean mayAddSubNode(FactoryNode f) {
		if (f instanceof Workplace) {
			return mayHaveSubNodes();
		} else {
			return mayHaveNonWorkplaceSubNodes();
		}
	}

	/**
	 * Checks if this node may have subnodes.
	 */
	public boolean mayHaveSubNodes() {
		return true;
	}

	/**
	 * Checks if this node may have other subnodes than Workplaces.
	 */
	public boolean mayHaveNonWorkplaceSubNodes() {
		return getLevel() < 2;
	}

	public boolean contains(FactoryNode t) {
		return this == t || (t.getParent() != null && contains(t.getParent()));
	}

	/**
	 * Finds a workplace by its name.
	 * 
	 * @param name
	 *            the name of the sought subnode
	 * @return The subnode.
	 */
	@Transient
	public Workplace getWorkplace(String name) {
		for (Workplace w : EntityList.getWorkplaceList(this)) {
			if (w.getName().equalsIgnoreCase(name)) {
				return w;
			}
		}
		return null;
	}

	/**
	 * Finds a workplace by its ID.
	 * 
	 * @param id
	 *            the ID of the sought subnode
	 * @return The subnode.
	 */
	@Transient
	public Workplace getWorkplaceByID(String id) {
		for (Workplace w : EntityList.getWorkplaceList(this)) {
			if (w.getId().equalsIgnoreCase(id)) {
				return w;
			}
		}
		return null;
	}

	/**
	 * Recursively searches the {@link FactoryNode} tree for a node with the
	 * given name.
	 * 
	 * @param name
	 *            the name of the sought subnode
	 * @return a {@link FactoryNode}
	 */
	@Transient
	public FactoryNode getSubnode(String name) {
		if (getName().equals(name)) {
			return this;
		} else {
			for (FactoryNode f : getSubNodes()) {
				FactoryNode result = f.getSubnode(name);
				if (result != null) {
					return result;
				}
			}
		}
		return null;
	}

	@Transient
	public FactoryNode getSubnode(int i) {
		return getSubNodes().get(i);
	}

	@Transient
	public FactoryNode getSubnode(int i, int j) {
		return getSubnode(i).getSubnode(j);
	}

	@Transient
	public FactoryNode getSubnode(int i, int j, int k) {
		return getSubnode(i).getSubnode(j).getSubnode(k);
	}

	/**
	 * Returns the distance to the tree root. In a normally structured factory,
	 * the returned values are:
	 * <ul>
	 * <li>0 if the node is a factory</li>
	 * <li>1 if the node is a segment</li>
	 * <li>2 if the node is a line</li>
	 * <li>3 if the node is a workplace</li>
	 * </ul>
	 */
	@Transient
	public int getLevel() {
		int result = 0;
		FactoryNode f = getParent();
		while (f != null) {
			result++;
			f = f.getParent();
		}
		return result;
	}

	/**
	 * Checks if this node and all ancestors are enabled.
	 * 
	 * @return <code>true</code> if all ancestors are enabled,
	 *         <code>false</code> otherwise.
	 */
	@Transient
	public boolean isEnabledAncestors() {
		FactoryNode f = this;
		do {
			if (!f.isEnabled()) {
				return false;
			}
			f = f.getParent();
		} while (f != null);
		return true;
	}
}
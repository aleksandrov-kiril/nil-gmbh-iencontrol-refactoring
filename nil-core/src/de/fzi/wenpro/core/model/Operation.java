/**
 * ecoFLEXiP 2010
 * de.fzi.ecoflex.core.model.Operation.java
 */
package de.fzi.wenpro.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Transient;

/**
 * @author wicaksono
 *
 */
public abstract class Operation extends FactoryResource implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1364622822726640088L;
	
	protected List<ProductQuantity> inputs = new ArrayList<ProductQuantity>();
	protected List<ProductQuantity> outputs = new ArrayList<ProductQuantity>();
	protected double time;
	protected int lotSize;
	
	public Operation(){
		super();
	}
	
	public Operation(String name) {
		super(name);
	}
	
	public Operation(Product product, Product inputProduct, double t, int lotSize) {
		this();
		this.outputs.add(new ProductQuantity(product));
		if (inputProduct != null){
			this.inputs.add(new ProductQuantity(inputProduct));
		}
		this.time = t;
		this.lotSize = lotSize;
	}
	
	
	public Operation(Operation operation){
		this();
		this.outputs.addAll(operation.outputs);
		this.inputs.addAll(operation.inputs);
		this.lotSize = operation.lotSize;
		this.time = operation.time;
	}
	
	/**
	 * Clones an {@link Operation} for a given {@link Factory}. All references
	 * are updates to that {@link Factory}.
	 * @param operation the {@link Operation} to be cloned
	 * @param factory the {@link Factory} the new {@link Operation} belongs to
	 */
	public Operation(Operation operation, Factory factory){
		this();
		if (operation.getFactory() == factory){
			this.outputs.addAll(operation.outputs);
			this.inputs.addAll(operation.inputs);
		}else{
			for (ProductQuantity pq : operation.inputs){
				int productIndex = operation.getFactory().getProductIndex(pq.getProduct());
				Product product = factory.getProducts().get(productIndex);
				inputs.add(new ProductQuantity(product, pq.getQuantity()));
			}
			for (ProductQuantity pq : operation.outputs){
				int productIndex = operation.getFactory().getProductIndex(pq.getProduct());
				Product product = factory.getProducts().get(productIndex);
				outputs.add(new ProductQuantity(product, pq.getQuantity()));
			}
		}
		this.lotSize = operation.lotSize;
		this.time = operation.time;
	}
	
	@Override
	public abstract Operation clone();
	
	public List<ProductQuantity> getInputs(){
		return inputs;
	}

	public Set<Product> getInputProducts(){
		Set<Product> result = new HashSet<Product>();
		for (ProductQuantity pq : inputs){
			result.add(pq.getProduct());
		}
		return result;
	}

	public void setInputs(List<ProductQuantity> inputs) {
		this.inputs = inputs;
	}

	public List<ProductQuantity> getOutputs(){
		return outputs;
	}

	public Set<Product> getOutputProducts(){
		Set<Product> result = new HashSet<Product>();
		for (ProductQuantity pq : outputs){
			result.add(pq.getProduct());
		}
		return result;
	}

	public void setOutput(List<ProductQuantity> outputs) {
		this.outputs = outputs;
	}

	public void addOutput(Product product){
		addOutput(product, 1);
	}

	public void addOutput(Product product, int quantity){
		outputs.add(new ProductQuantity(product, 1));
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}
	
	public int getLotSize() {
		return lotSize;
	}

	public void setLotSize(int lotSize) {
		this.lotSize = lotSize;
	}

	@Transient
	//FIXME: workaround for differentiation between InternalOperation
	//and ExternalOperation
	public abstract boolean isPurchased();
	
	public void addInput(Product input){
		addInput(input, 1);
	}
	
	public void addInput(Product input, int quantity) {
		this.inputs.add(new ProductQuantity(input, quantity));
	}
	
	public void removeInput(Product input){
		Iterator<ProductQuantity> iterator = inputs.iterator();
		while (iterator.hasNext()){
			ProductQuantity pq = iterator.next();
			if (pq.getProduct().equals(input)){
				iterator.remove();
			}
		}
	}
	
	/**
	 * The number of pieces of a given {@link Product} that is used as an input for this {@link Operation}.
	 * @param product an input {@link Product}
	 * @return the used piece number
	 */
	public int getInputQuantity(Product product){
		int quantity = 0;
		for (ProductQuantity pq : inputs){
			if (pq.getProduct().equals(product)){
				quantity += pq.getQuantity();
			}
		}
		return quantity;
	}

	/**
	 * The total number of pieces that is used if this {@link Operation} is
	 * performed once.
	 * @return the number of pieces
	 */
	public int getInputPiecesPerOperation(){
		int result = 0;
		for (ProductQuantity pq : inputs){
			result += pq.getQuantity();
		}
		return result;
	}

	/**
	 * The total number of pieces of the given {@link Product} that is used
	 * if this {@link Operation} is performed once.
	 * @return the number of pieces
	 */
	public int getInputPiecesPerOperation(Product product){
		int result = 0;
		for (ProductQuantity pq : inputs){
			if (pq.getProduct() == product){
				result += pq.getQuantity();
			}
		}
		return result;
	}

	/**
	 * The total number of pieces that is created if this {@link Operation} is
	 * performed once.
	 * @return the number of pieces
	 */
	public int getOutputPiecesPerOperation(){
		int result = 0;
		for (ProductQuantity pq : outputs){
			result += pq.getQuantity();
		}
		return result;
	}

	/**
	 * The total number of pieces of the given {@link Product} that is created
	 * if this {@link Operation} is performed once.
	 * @return the number of pieces
	 */
	public int getOutputPiecesPerOperation(Product product){
		int result = 0;
		for (ProductQuantity pq : outputs){
			if (pq.getProduct() == product){
				result += pq.getQuantity();
			}
		}
		return result;
	}
	
	@Override
	public String toString() {
		String string = "Operation ";
		for(Product p: getOutputProducts()){
			string = string + " " + p.getName();
		}
		return string ;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((inputs == null) ? 0 : inputs.hashCode());
		result = prime * result + lotSize;
		result = prime * result + ((outputs == null) ? 0 : outputs.hashCode());
		long temp;
		temp = Double.doubleToLongBits(time);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operation other = (Operation) obj;
		if (inputs == null) {
			if (other.inputs != null)
				return false;
		} else if (!inputs.equals(other.inputs))
			return false;
		if (lotSize != other.lotSize)
			return false;
		if (outputs == null) {
			if (other.outputs != null)
				return false;
		} else if (!outputs.equals(other.outputs))
			return false;
		if (Double.doubleToLongBits(time) != Double.doubleToLongBits(other.time))
			return false;
		return true;
	}

	public int getOutputQuantity(Product product) {
		int quantity = 0;
		for (ProductQuantity pq : outputs){
			if (pq.getProduct().equals(product)){
				quantity += pq.getQuantity();
			}
		}
		return quantity;
	}
}

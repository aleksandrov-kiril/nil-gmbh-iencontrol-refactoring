/**
 * 
 */
package de.fzi.wenpro.core.model.deprecated;

import java.io.Serializable;

import de.fzi.wenpro.core.model.Product;

/**
 * @author wicaksono
 *
 */
public class Supply implements Serializable{
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 5189750561104019832L;

	private Supplier supplier;
	private Product product;
	private double replenishmentTime;
	
	public Supply(Supplier supplier, Product product, double replenishmentTime) {
		this.supplier = supplier;
		this.product = product;
		this.replenishmentTime = replenishmentTime;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public double getReplenishmentTime() {
		return replenishmentTime;
	}
	public void setReplenishmentTime(double replenishmentTime) {
		this.replenishmentTime = replenishmentTime;
	}
	
	
}

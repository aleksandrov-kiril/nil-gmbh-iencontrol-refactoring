/**
 * 
 */
package de.fzi.wenpro.core.model.deprecated;

import java.io.Serializable;

/**
 * @author wicaksono
 *
 */
public class Supplier implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3089640770573177725L;
	public enum QualityType {
		A, B, C
	}
	private int id;
	private String name;
	private QualityType quality;
	
	public Supplier(int supplierID, String name){
		this.id = supplierID;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public QualityType getQuality() {
		return quality;
	}
	public void setQuality(QualityType quality) {
		this.quality = quality;
	}
	
	
	
}

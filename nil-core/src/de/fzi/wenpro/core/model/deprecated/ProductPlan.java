package de.fzi.wenpro.core.model.deprecated;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fzi.wenpro.core.calculations.CostCalculations;
import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.printing.ProductPlanOutputOperations;
import de.fzi.wenpro.util.Vector;

public class ProductPlan {
	
	private final FactoryNode factoryNode;
	private final double[] volume;
	private final ShiftSchedule shiftplan;
	
	private final boolean isValid;
	
	private ProductPlanOutputOperations oo;
	
	private static ProductPlan INVALID = null;
	
	/**
	 * Creates a product plan.
	 * @param volume An array of piece numbers. The size of the array must
	 *        match the number of Produces objects in the factory node, so
	 *        <code>
	 * array.length == {@link FactoryNode#countProduces() 
	 * factoryNode.countProduces()}</code>.
	 * @param factoryNode
	 * @param shiftplan
	 */
	public ProductPlan(double[] volume, FactoryNode factoryNode, ShiftSchedule shiftplan){
		this.oo = new ProductPlanOutputOperations(this);
		this.volume = volume;
		this.factoryNode = factoryNode;
		this.shiftplan = shiftplan;
		this.isValid = (volume != null);
	}
	
	/**
	 * An invalid ProductPlan.
	 * @return an invalid ProductPlan
	 */
	public static ProductPlan getInvalid(){
		if (INVALID == null){
			INVALID = new ProductPlan(null, null, null);
		}
		return INVALID;
	}
	
	/**
	 * The factory node to which this product plan applies.
	 * @return The factory node to which this product plan applies.
	 */
	public FactoryNode getFactoryNode(){
		return factoryNode;
	}
	
	/**
	 * The product plan's piece number array.
	 * @return An array of piece numbers.
	 */
	public double[] getVolume(){
		return volume;
	}
	
	/**
	 * The shiftplan on which the production plan is based.
	 * @return This product plan's shiftplan.
	 */
	public ShiftSchedule getShiftplan(){
		return shiftplan;
	}
	
	/**
	 * @return the oo
	 */
	public ProductPlanOutputOperations getOO(){
		return oo;
	}
	
	/**
	 * Gets the piece numbers by product.
	 * @return an array of piece numbers
	 */
	
	public double[] getVolumeByProduct(){
		if (isValid){
			double[] result = new double[getFactoryNode().getFactory().getProducts().size()];
			int i = 0;
			for (Operation op : EntityList.getOperationList(getFactoryNode())){
				double scrapFactor = op.isPurchased() ? 1 : (1 - ((InternalOperation) op).getScrapRate());
				for (ProductQuantity pq : op.getOutputs()){
					result[getFactoryNode().getFactory().getProductIndex(pq.getProduct())] += volume[i] * pq.getQuantity() * scrapFactor;
				}
				i++;
			}
			return result;
		}else{
			return null;
		}
	}
	
	public Map<FactoryNode, Double> getFinalComponentVolumeByFactoryNode(){
		if (isValid){
			Map<FactoryNode, Double> result = new HashMap<FactoryNode, Double>();
			
			result.put(getFactoryNode(), getFinalComponentsVolume());
			
			for (ProductPlan p : split()){
				result.putAll(p.getFinalComponentVolumeByFactoryNode());
			}
			
			return result;
		}else{
			return null;
		}
	}
	
	/**
	 * Gets the piece numbers by {@link Workplace}.
	 * @return an array of piece numbers
	 */
	public double[] getVolumeByWorkplace(){
		return mapProducesToWorkplace(volume);
	}
	
	/**
	 * Gets the number of workers by {@link Workplace}.
	 * @return an array of piece numbers
	 */
	public double[] getWorkersByWorkplace(){
		return mapProducesToWorkplace(getWorkers());
	}
	
	/**
	 * Gets the number of workers by subnode.
	 * @return a map with the number of workers for each subnode
	 */
	public Map<FactoryNode, Double> getWorkersByFactoryNode(){
		return createAccumulatedNodeMap(getWorkers());
	}
	
	/**
	 * Gets the number of workers for each Produces.
	 */
	public double[] getWorkers(){
		if (isValid){
			double[] result = getWorkerTime();
			double spp = getShiftplan().getSecondsPerPeriod();
			for (int i = 0; i < result.length; i++){
				result[i] /= spp;
			}
			return result;
		}else{
			return null;
		}
	}
	
	/**
	 * Sums up an array which contains values for each Produces to contain
	 * values for each {@link Workplace}.
	 * @param values an array of values for each Produces
	 * @return an array of values for each {@link Workplace}
	 */
	
	private double[] mapProducesToWorkplace(double[] values){
		if (values == null){
			return null;
		}else{
			List<Workplace> workplaces = EntityList.getWorkplaceList(getFactoryNode());
			List<Operation> operationList = EntityList.getOperationList(getFactoryNode());
			int nProduces = operationList.size();
			double[] result = new double[workplaces.size()];
			int wi = 0, pi = 0;
			for (Workplace w : workplaces){
				if (!operationList.get(pi).isPurchased()){					
					while (pi < nProduces && ((InternalOperation) operationList.get(pi)).getOwner().getWorkplace() == w){
						result[wi] += values[pi];
						pi++;
					}
				}
				wi++;
			}
			return result;
		}
	}
	
	/**
	 * Calculates the sum of a Produces value array for each node: The value
	 * of a node is the sum of all values that belong to a Produces of any
	 * subnode. If there is no workplace assigned to the produces, the value is
	 * only added to the top factory node, i.e. the node that this ProductPlan
	 * is assigned to.
	 * @param values an array of values
	 * @return a Map with a Double entry for each factory node
	 */	
	private Map<FactoryNode, Double> createAccumulatedNodeMap(double[] values){
		if (isValid){
			Map<FactoryNode, Double> result = new HashMap<FactoryNode, Double>();
			FactoryNode factoryNode = getFactoryNode();
			int i = 0;
			double sum = 0;
			for (Operation op : EntityList.getOperationList(factoryNode)){
				if (!op.isPurchased()) {
					FactoryNode f = ((InternalOperation) op).getOwner().getWorkplace();
					if (f != null){
						do{
							Double v = values[i];
							if (result.containsKey(f)){
								v += result.get(f);
							}
							result.put(f, v);
							
							f = f.getParent();
						}while (f != factoryNode);
					}
					sum += values[i];
					i++;
				}	
			}
			result.put(factoryNode, sum);
			
			return result;
		}else{
			return null;
		}
	}
	
	/**
	 * Gets the piece numbers by product, on theoretical production.
	 * @return an array of piece numbers
	 */	
	public double[] getVolumeByProductTh(){
		if (isValid){
			double[] result = new double[getFactoryNode().getFactory().getProducts().size()];
			int i = 0;
			for (Operation op : EntityList.getOperationList(getFactoryNode())){
				for (ProductQuantity pq : op.getOutputs()){
					result[getFactoryNode().getFactory().getProductIndex(pq.getProduct())] += volume[i] * pq.getQuantity();
				}
				i++;
			}
			return result;
		}else{
			return null;
		}
	}
	
	/**
	 * Splits this product plan to an array of product plans, one for each
	 * subnode.
	 * @return An array of subnode production plans.
	 */
	
	public ProductPlan[] split(){
		ProductPlan[] result = new ProductPlan[getFactoryNode().getSubNodes().size()];
		if (isValid){
			int pos = 0;
			int i = 0;
			for (FactoryNode subNode : getFactoryNode().getSubNodes()){
				int nProduces = EntityList.countOperations(subNode);
				result[i++] = new ProductPlan(
						Vector.sub(volume, pos, nProduces),
						subNode,
						getShiftplan()
						);
				pos += nProduces;
			}
		}else{
			Arrays.fill(result, ProductPlan.getInvalid());
		}
		return result;
	}
		
	public ProductPlan getSubProductPlan(FactoryNode subFactoryNode){
		if (!isValid){
			return ProductPlan.getInvalid();
		}else if (EntityList.countOperations(subFactoryNode) == 0){
			return new ProductPlan(new double[0], subFactoryNode, getShiftplan());
		}else{
			Operation firstOperation = EntityList.getOperationList(subFactoryNode).get(0);
			int index = EntityList.getOperationList(getFactoryNode()).indexOf(firstOperation);
			if (index >= 0){
				double[] subVolume = new double[EntityList.countOperations(subFactoryNode)];
				System.arraycopy(volume, index, subVolume, 0, subVolume.length);
				return new ProductPlan(subVolume, subFactoryNode, getShiftplan());
			}else{
				return null;
			}
		}
	}
	
	/**
	 * The total production volume.
	 * @return the total piece number of this product plan
	 */
	public double getTotalVolume(){
		if (isValid){
			return Vector.sum(volume);
		}else{
			return Double.NaN;
		}
	}
	
	/**
	 * Returns the workplace load in percent of a product plan, indexed by
	 * produces combinations.
	 * @return
	 */	
	public double[] getLoad(){
		if (isValid){
			double[] result = new double[EntityList.countOperations(getFactoryNode())];
			ShiftSchedule shiftplan = getShiftplan();
			int i = 0;
			for (Operation op : EntityList.getOperationList(getFactoryNode())){
				if (op.isPurchased()){
					result[i] = 0;
				}else{					
					result[i] = 100.0 * volume[i] * op.getTime() / ((InternalOperation) op).getOwner().getWorkplace().getMaximalProductionTime(shiftplan);
				}
				i++;
			}
			return result;
		}else{
			return null;
		}
	}
	
	
	/**
	 * Returns the workplace load in percent of a product plan, indexed by
	 * workplaces.
	 * @return
	 */
	public double[] getLoadByWorkplace(){
		return mapProducesToWorkplace(getLoad());
	}
	
	/**
	 * Returns the real profit made with the given product plan.
	 */
	public double getProfit(){
		if (isValid){
			return Vector.sprod(volume, CostCalculations.getProfitByProduces(getFactoryNode(), getShiftplan())) - CostCalculations.getConstantCost(getFactoryNode(), getShiftplan());
		}else{
			return 0;
		}
	}
	
	/**
	 * Calculates how many pieces of each product are created and used in the
	 * factory node. The difference of these vectors is the number of pieces
	 * that leaves the factory node. Pieces that are lost due to the scrap
	 * rates do not appear in the produced array.
	 * @return an array of the form {produced, used}, where produced and used
	 *         are arrays of piece numbers, indexed by getProducts().
	 */
	// TODO: check with Jan
	private double[][] getProductFlow(){
		double[] produced = new double[getFactoryNode().getFactory().getProducts().size()];
		double[] used = new double[getFactoryNode().getFactory().getProducts().size()];
		
		if (isValid){
			int i = 0;
			for (Operation op : EntityList.getOperationList(getFactoryNode())){
				double scrapFactor = op.isPurchased() ? 1 : (1 - ((InternalOperation)op).getScrapRate());
				for (ProductQuantity pq : op.getOutputs()){
					produced[pq.getProduct().getProductIndex()] += volume[i] * pq.getQuantity() * scrapFactor;
				}
				for (ProductQuantity pq : op.getInputs()){
					used[pq.getProduct().getProductIndex()] += volume[i] * pq.getQuantity();
				}
				i++;
			}
			return new double[][]{produced, used};
		}else{
			return null;
		}
	}
	
	/**
	 * Calculates the number of pieces that are produced in this product plan,
	 * but not used up as components.
	 * @return The piece number sum.
	 */
	public double getFinalComponentsVolume(){
		double[][] flow = getProductFlow();
		double[] produced = flow[0];
		double[] used = flow[1];
		
		double result = 0;
		for (int i = 0; i < produced.length; i++){
			if (produced[i] > used[i]){
				result += produced[i] - used[i];
			}
		}
		return result;
	}
	
	/**
	 * The piece number sum of all disposable products. This does not count the
	 * disposable intermediate product which are used up as components.
	 * @return the number of pieces
	 */	
	public double getDisposableProductVolume(){
		double result = 0;
		int i = 0;
		for (Operation op : EntityList.getOperationList(factoryNode)){
			for (ProductQuantity pq : op.getOutputs()){
				if (pq.getProduct().isDisposable()){
					result += volume[i] * pq.getQuantity();
				}
			}
			for (ProductQuantity pq : op.getInputs()){
				if (pq.getProduct().isDisposable()){
					result -= volume[i] * pq.getQuantity();
				}
			}
			i++;
		}
		return result;
	}
	
	/**
	 * The number of workers required for the production.
	 * @return the number of workers required
	 */
	public double getTotalWorkers(){
		return getWorkerTimeTotal() / shiftplan.getSecondsPerPeriod();
	}
	
	/**
	 * The time that a worker spends on each produces object.
	 * @return an array of working times in seconds
	 */
	public double[] getWorkerTime(){
		double[] result = volume.clone();
		int i = 0;
		for (Operation op : EntityList.getOperationList(getFactoryNode())){
			result[i] *= op.getTime();
			if (!op.isPurchased()){
				result[i] *= ((InternalOperation) op).getOwner().getWorkplace().getPersonnelAttendanceRate();
			}
			i++;
		}
		return result;
	}
	
	
	
	/**
	 * The total time of human work in the production plan.
	 * @return the time in seconds
	 */
	public double getWorkerTimeTotal(){
		double result = 0;
		for (double d : getWorkerTime()){
			result += d;
		}
		return result;
	}
	
	/**
	 * The time of human work in this production plan, by workplace.
	 * @return an array of working times in seconds
	 */
	public double[] getWorkerTimeByWorkplace(){
		return mapProducesToWorkplace(getWorkerTime());
	}
	
	/**
	 * Returns <code>true</code> if the production plan is valid (contains
	 * piece numbers). If this returns [code]false[/code], the calculation that
	 * created this product plan's data could not be completed.
	 * @return <code>true</code> if the production plan is valid,
	 *         <code>false</code> otherwise.
	 */
	public boolean isValid(){
		return isValid;
	}
}

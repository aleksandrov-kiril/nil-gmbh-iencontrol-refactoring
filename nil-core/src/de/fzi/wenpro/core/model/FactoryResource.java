/**
 * ecoFLEXiP 2010
 * de.fzi.ecoflex.core.model.FactoryResource.java
 */
package de.fzi.wenpro.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;

/**
 * This class is the super class of classes representing objects which
 * generates costs.
 * 
 * @author wicaksono
 *
 */

public abstract class FactoryResource extends Observable implements Serializable{
	
	private static final long serialVersionUID = -9038401618084699176L;
	private String id;
	private String name;
	private List<Cost> costs = new ArrayList<Cost>();
	private Factory factory;
	
	public FactoryResource(){
		
	}
	
	public FactoryResource(String name) {
		this.name = name;
	}
	
	public FactoryResource(String name, Factory factory) {
		this.name = name;
		this.factory = factory;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "factoryNode_id")
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public List<Cost> getCosts() {
		return costs;
	}
	public void setCosts(List<Cost> costs) {
		this.costs = costs;
	}

	public Factory getFactory() {
		return factory;
	}

	public void setFactory(Factory factory) {
		this.factory = factory;
	}
	
	/**
	 * Adds a new cost object with the specified attributes for its name, type,
	 * unit and value.
	 */
	public Cost addCost(String name, Cost.Type type, Cost.Unit unit,
			double value){
		Cost result = new Cost(name, getFactory(), type, unit, value);
		this.getCosts().add(result);
		getFactory().notifyAllObservers();
		return result;
	}
	
	/**
	 * Changes the value of an existing cost object which is specified by its
	 * name. If the object doesn't exist a new one is created.
	 */
	public Cost setCost(String name, Cost.Type type, Cost.Unit unit, double value){
		Cost c = getCost(name);
		if (c == null){
			c = addCost(name, type, unit, value);
		}else{
			c.setType(type);
			c.setUnit(unit);
			c.setValue(value);
			getFactory().notifyAllObservers();
		}
		return c;
	}
	
	/**
	 * Returns a cost object from this node, specified by its name.
	 * @param name the name of the cost object to be returned.
	 * @return The cost object, or <code>null</code> if no cost object in this
	 *         node has the specified name.
	 */
	@Transient
	public Cost getCost(String name){
		for (Cost c : getCosts()){
			if (c.getName().equals(name)){
				return c;
			}
		}
		return null;
	}
	
	/**
	 * Removes the specified cost object from this node.
	 * @param cost the cost object which will be removed
	 * @return Returns <code>true</code> if a node has been removed. If
	 *         <code>false</code> is returned, the specified cost object did
	 *         not exist in this FactoryNode.
	 */
	public boolean removeCost(Cost cost){
		if (this.getCosts().remove(cost)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public String toString() {
		return "FactoryResource [name=" + name + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FactoryResource other = (FactoryResource) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}

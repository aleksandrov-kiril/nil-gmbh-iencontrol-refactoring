package de.fzi.wenpro.core.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.energy.model.FactoryEnergyInformation;
import de.fzi.wenpro.core.energy.model.facility.ExternEnergySourceFacility;
import de.fzi.wenpro.core.energy.model.facility.InternEnergySourceFacility;
import de.fzi.wenpro.core.printing.FactoryOutputOperations;

@Entity
public class Factory extends FactoryNode implements Cloneable, Serializable {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -6238414109482424088L;

	private List<Product> products;
	private List<ShiftSchedule> shiftplans;

	/** The target improvement of the ratio capacity. */
	private double expansionFlexibilityTarget = 1.0;
	private FactoryNode expansionFlexibilityNode = null;

	private List<Order> orders = new ArrayList<Order>();

	// additional attributes for wenpro
	private InternEnergySourceFacility internEnergySource;
	private ExternEnergySourceFacility externEnergySource;
	private FactoryEnergyInformation factoryEnergyInformation;
	
	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public void addOrder(Order order) {
		orders.add(order);
	}
	
	

	public FactoryEnergyInformation getFactoryEnergyInformation() {
		return factoryEnergyInformation;
	}

	public void setFactoryEnergyInformation(
			FactoryEnergyInformation factoryEnergyInformation) {
		this.factoryEnergyInformation = factoryEnergyInformation;
	}



	@Transient
	private boolean stopNotifications;

	// ////////////////////////////////////////////////////////////////////////
	// ///////////////////////// Structural //////////////////////////////////

	// constructors
	// ==============================================================

	public Factory() {
		super();
		products = new ArrayList<Product>();
		shiftplans = new ArrayList<ShiftSchedule>();
		orders = new ArrayList<Order>();
		setFactory(this);
	}

	public Factory(String name) {
		super(name, null);
		products = new ArrayList<Product>();
		shiftplans = new ArrayList<ShiftSchedule>();
		orders = new ArrayList<Order>();
		setFactory(this);
	}

	protected Factory(Factory f) {
		super();
		setFactory(this);
		setName(f.getName());

		// Needs to be done before any shiftplans are added!
		setCosts(new ArrayList<Cost>());
		setSubNodes(new ArrayList<FactoryNode>());

		shiftplans = new ArrayList<ShiftSchedule>();
		setShiftplans(new ArrayList<ShiftSchedule>());
		for (ShiftSchedule s : f.shiftplans) {
			s.clone(this);
		}

		products = new ArrayList<Product>();
		for (Product p : f.products) {
			products.add(p.clone(this));
		}

		for (FactoryNode subnode : f.getSubNodes()) {
			addSubNode(subnode.clone(this));
		}

		for (Cost c : f.getCosts())
			getCosts().add(c.clone(getFactory()));

		original = f;

		for (Operation op : EntityList.getOperationList(this)) {
			for (Product p : op.getOutputProducts()) {
				p.addOperation(op);
			}
		}
	}

	public Factory clone() {
		return new Factory(this);
	}

	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException {
		in.defaultReadObject();
		// createOO();
	}

	// getter and setter methods ==============================================

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "factory_id")
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> product) {
		this.products = product;
		notifyAllObservers();
	}

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "factory_id")
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public List<ShiftSchedule> getShiftplans() {
		return shiftplans;
	}

	public void setShiftplans(List<ShiftSchedule> shiftplan) {
		this.shiftplans = shiftplan;
		notifyAllObservers();
	}

	/**
	 * @return the expansionFlexibilityTarget
	 */
	public double getExpansionFlexibilityTarget() {
		return expansionFlexibilityTarget;
	}

	/**
	 * @param expansionFlexibilityTarget
	 *            the expansionFlexibilityTarget to set
	 */
	public void setExpansionFlexibilityTarget(
			FactoryNode expansionFlexibilityNode,
			double expansionFlexibilityTarget) {
		this.expansionFlexibilityNode = expansionFlexibilityNode;
		this.expansionFlexibilityTarget = expansionFlexibilityTarget;
		notifyAllObservers();
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@Cascade(value = org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public FactoryNode getExpansionFlexibilityNode() {
		return expansionFlexibilityNode;
	}

	public void setExpansionFlexibilityNode(FactoryNode expansionFlexibilityNode) {
		this.expansionFlexibilityNode = expansionFlexibilityNode;
		notifyAllObservers();
	}

	public InternEnergySourceFacility getInternEnergySource() {
		return internEnergySource;
	}

	public void setInternEnergySource(
			InternEnergySourceFacility internEnergySource) {
		this.internEnergySource = internEnergySource;
	}

	public ExternEnergySourceFacility getExternEnergySource() {
		return externEnergySource;
	}

	public void setExternEnergySource(
			ExternEnergySourceFacility externEnergySource) {
		this.externEnergySource = externEnergySource;
	}

	/**
	 * Calls the setChanged and notifyObservers method in this object and all
	 * children.
	 */
	public void notifyAllObservers() {
		if (!stopNotifications) {
			super.notifyAllObservers();
		}
	}

	/**
	 * After this is called, no observers are notified until startNotificytions
	 * is called.
	 */
	public void stopNotifications() {
		stopNotifications = true;
	}

	/**
	 * Reenables notifications to observers.
	 */
	public void startNotifications() {
		stopNotifications = false;
	}

	/**
	 * Add a new product with the specified parameters to this factory.
	 * 
	 * @return The new product
	 */
	public Product addProduct(String name, double value, boolean disposable) {
		Product p = new Product(this.getFactory(), name, value, disposable);
		getProducts().add(p);
		notifyAllObservers();
		return p;
	}

	/**
	 * Add a new product with the specified parameters to this factory.
	 * 
	 * @return The new product
	 */
	public Product addProduct(String name, double value, double ratio,
			boolean disposable) {
		Product p = new Product(this.getFactory(), name, value, disposable);
		p.setRatio(ratio);
		getProducts().add(p);
		notifyAllObservers();
		return p;
	}

	/**
	 * Adds a product to this factory and sets its ratio.
	 * 
	 * @return The new product
	 */
	public Product addProduct(Product product) {
		if (!getProducts().contains(product) && product != null) {
			this.getProducts().add(product);
			notifyAllObservers();
		}
		return product;
	}

	public void addShiftplan(ShiftSchedule s) {
		getShiftplans().add(s);
		for (Cost c : EntityList.getCostList(this)) {
			c.addShiftplan(s);
		}
		notifyAllObservers();
	}

	public ShiftSchedule addShiftplan(String name, double hoursPerDay,
			double shiftsPerDay, double daysPerPeriod) {
		ShiftSchedule shiftplan = new ShiftSchedule(this, name, hoursPerDay,
				shiftsPerDay, daysPerPeriod);
		addShiftplan(shiftplan);
		return shiftplan;
	}

	public void removeShiftplan(ShiftSchedule s) {
		getShiftplans().remove(s);
		for (Cost c : EntityList.getCostList(this)) {
			c.removeShiftplan(s);
		}
		notifyAllObservers();
	}

	public int countShiftplans() {
		if (getShiftplans() == null)
			return 0;
		else
			return getShiftplans().size();
	}

	public Workplace getWorkplace(int i) {
		return (Workplace) getSubnode(i);
	}

	public Workplace getWorkplace(int i, int j) {
		return (Workplace) getSubnode(i, j);
	}

	public Workplace getWorkplace(int i, int j, int k) {
		return (Workplace) getSubnode(i, j, k);
	}

	/**
	 * Sets the expansion flexibility target.
	 * 
	 * @param The
	 *            new expansion flexibility target.
	 */
	public void setExpansionFlexibilityTarget(double expansionFlexibilityTarget) {
		this.expansionFlexibilityTarget = expansionFlexibilityTarget;
		notifyAllObservers();
	}

	@Transient
	public FactoryOutputOperations getOO() {
		return (FactoryOutputOperations) output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzi.ecoflex.core.model.FactoryNode#createOO()
	 */
	@Override
	protected void createOO() {
		this.output = new FactoryOutputOperations(this);
	}

	/**
	 * Returns the internal index of a product. If the product isn't used in the
	 * factory yet, it will be added to the factory's internal product list.
	 * 
	 * @param The
	 *            product.
	 * @return The index of the product.
	 */
	@Transient
	public int getProductIndex(Product p) {
		int i = getProducts().indexOf(p);
		if (i == -1) {
			addProduct(p); // FIXME: don't add products here
			return getProducts().size() - 1;
		} else {
			return i;
		}
	}

	/**
	 * Returns the product by its name
	 * 
	 * @param s
	 *            the name of the product
	 * @return the product with the given name
	 */
	@Transient
	public Product getProductByName(String s) {
		for (Product p : getProducts()) {
			if (p.getName().equals(s)) {
				return p;
			}
		}
		return null;
	}

	/**
	 * Removes a product from this Factory. All objects referring to the product
	 * (that is the Operation objects in the Factory's Workplaces and the
	 * component references) are also removed.
	 * 
	 * @param product
	 *            The product which is to be deleted from this Factory.
	 */
	public void removeProduct(Product product) {
		for (Operation op : EntityList.getOperationList(this)) {
			Iterator<ProductQuantity> iterator = op.getInputs().iterator();
			while (iterator.hasNext()) {
				if (iterator.next().getProduct().equals(product)) {
					iterator.remove();
				}
			}
			iterator = op.getOutputs().iterator();
			while (iterator.hasNext()) {
				if (iterator.next().getProduct().equals(product)) {
					iterator.remove();
				}
			}
		}
		this.getProducts().remove(product);
	}

	@Transient
	public ShiftSchedule getDefaultShiftplan() {
		// TODO
		if(getFactory().getShiftplans().size()<=0)
			return null;
		return getFactory().getShiftplans().get(0);
	}

	/**
	 * Returns a corresponding FactoryNode from a selected alternative Factory
	 * to the given original FacotryNode from the original factory
	 * 
	 * @param fnode
	 *            an Original FactoryNode
	 * @return the corresponding FactoryNode from the alternative factory
	 */
	@Transient
	public FactoryNode getNodeByOriginal(FactoryNode fnode) {
		for (FactoryNode node : EntityList.getSubnodeList(this)) {
			if (node.getOriginal() != null && node.getOriginal().equals(fnode)) {
				return node;
			}
		}
		return null;
	}

	/**
	 * Returns the workplace object with the given workplaceID
	 * 
	 * @param workplaceID
	 * @return the corresponding workplace, null when no Workplace with the
	 *         given ID found
	 */
	public Workplace getWorkplaceByID(String workplaceID) {
		for (Workplace wp : EntityList.getWorkplaceList(this)) {
			if (wp.getId().equals(workplaceID))
				return wp;
		}
		return null;
	}

}

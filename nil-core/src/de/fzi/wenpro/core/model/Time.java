package de.fzi.wenpro.core.model;

import java.util.Calendar;

public class Time {
	private Calendar now;
	private Calendar start;

	
	public Time(Calendar now, Calendar start) {
		this.now = (Calendar) now.clone();
		this.start = (Calendar) start.clone();
	}

	public Time(Time t) {
		this.now = (Calendar) t.getNow().clone();
		this.start = (Calendar) t.getStart().clone();
	}

	@Deprecated
	public Time(Calendar now) {
		this.now = now;
		this.start = now;
	}

	public Calendar getStart() {
		return start;
	}

	public Calendar getNow() {
		return now;
	}

	public double getT() {
		return (double)(now.getTimeInMillis() - start.getTimeInMillis())/1000;
	}

	//FIXME: Test behavior induced by rounding
	public void setT(double t) {
		long s = start.getTimeInMillis();
		double tms = Math.round(t*1000);
		long lt = (long)(tms);
		now.setTimeInMillis(s + lt);
	}
	
	/**
	 * moves the Time-Object to the next hourOfDay.
	 * E.g. if obj is 22:00:00, obj.moveToHourOfDay(4:00:00) will move obj to 4 o'clock
	 * of the next day.
	 * @param hourOfDay		time to move to
	 */
	public void moveT(double t) {
		this.setT(this.getT() + t);
	}
	
	public double getDaytime(){
		long daytime = 0;
		daytime += now.get(Calendar.HOUR_OF_DAY)*60*60*1000;
		daytime += now.get(Calendar.MINUTE)*60*1000;
		daytime += now.get(Calendar.SECOND)*1000;
		daytime += now.get(Calendar.MILLISECOND);
		
		return (double)(daytime)/1000;
	}
	
	/**
	 * @return double value that represents the last midnight relatively in seconds to the plan start date
	 * can be negative, if last midnight earlier then planStartDate
	 */
	public double getLastMidnight(){
		long midnight = now.getTimeInMillis();
		midnight -= now.get(Calendar.HOUR_OF_DAY)*60*60*1000;
		midnight -= now.get(Calendar.MINUTE)*60*1000;
		midnight -= now.get(Calendar.SECOND)*1000;
		midnight -= now.get(Calendar.MILLISECOND);
		midnight -= start.getTimeInMillis();
		return (double)(midnight)/1000;
	}
	
	/**
	 * @return double value that represents the last Midnight between Saturday and Sunday 
	 * relatively in seconds to the plan start date can be negative, if last Sunday is 
	 * earlier then planStartDate
	 */
	public double getLastSunday(){
		long time = now.getTimeInMillis();
		time -= now.get(Calendar.DAY_OF_WEEK)*24*60*60*1000;
		time -= now.get(Calendar.HOUR_OF_DAY)*60*60*1000;
		time -= now.get(Calendar.MINUTE)*60*1000;
		time -= now.get(Calendar.SECOND)*1000;
		time -= now.get(Calendar.MILLISECOND);
		time -= start.getTimeInMillis();
		return (double)(time)/1000;
	}
	
	public String toString(){
		return "" + now.getTime();
	}
}




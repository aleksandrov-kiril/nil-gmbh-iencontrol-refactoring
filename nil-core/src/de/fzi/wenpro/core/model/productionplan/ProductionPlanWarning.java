package de.fzi.wenpro.core.model.productionplan;

import java.io.Serializable;
import java.util.Calendar;

import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan.ProductionPlanWarningType;

public class ProductionPlanWarning implements Serializable{
	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -2077292829058006365L;
	
	public final ProductionPlan productionPlan;
	public final ProductionPlanWarningType warningType;
	public final Workplace workplace;
	public final WorkplaceState workplaceState;
	public final Operation operation;
	public final Product product;
	public final double startTime;
	public final double endTime;
	
	final static String date2Format = "%2$td.%2$tm.%2$tY %2$tH:%2$tM:%2$tS";
	final static String date3Format = "%3$td.%3$tm.%3$tY %3$tH:%3$tM:%3$tS";
	
	/**
	 * @param productionPlan
	 * @param warningType
	 * @param workplace
	 * @param workplaceState
	 * @param operation
	 * @param product
	 * @param startTime
	 * @param endTime
	 */
	public ProductionPlanWarning(ProductionPlan productionPlan, ProductionPlanWarningType warningType, Workplace workplace, WorkplaceState workplaceState, Operation operation, Product product, double startTime, double endTime){
		super();
		this.productionPlan = productionPlan;
		this.warningType = warningType;
		this.workplace = workplace;
		this.workplaceState = workplaceState;
		this.operation = operation;
		this.product = product;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	public String getErrorMessage(){
		switch (warningType){
		case STOCK_TOO_LOW:
			if (startTime == endTime){
				return String.format("Erzeugnis %s: nicht gen�gend Vorrat an Zeitpunkt " + date2Format, product.getName(), getCalendar(startTime));
			}else if (endTime == Double.POSITIVE_INFINITY){
				return String.format("Erzeugnis %s: nicht gen�gend Vorrat ab Zeitpunkt " + date2Format, product.getName(), getCalendar(startTime));
			}else{
				return String.format("Erzeugnis %s: nicht gen�gend Vorrat im Zeitraum " + date2Format + " bis " + date3Format, product.getName(), getCalendar(startTime), getCalendar(endTime));
			}
		case PRODUCTION_OVERLAP:
			return String.format("�berlappende Production in Arbeitsplatz %s bei " + date2Format + " bis " + date3Format, workplace, getCalendar(startTime), getCalendar(endTime));
		case RETOOLING_UNNECESSARY:
			return String.format("Unben�tigte Umr�stung in Arbeitsplatz %s bei " + date2Format, workplace, getCalendar(startTime));
		case RETOOLING_REQUIRED:
			return String.format("Umr�stung ben�tigt in Arbeitsplatz %s bei " + date2Format, workplace, getCalendar(startTime));
		case ENERGY_PROFILE_VIOLATED:
			return String.format("Verletztes Energie Profile in Arbeitsplatz %s bei " + date2Format, workplace, getCalendar(startTime));
		default:
			return String.format("Fehler: " + warningType);
		}
	}
	
	protected Calendar getCalendar(double time){
		Calendar result = productionPlan.getPlanStartDate();
		if (result == null){
			result = Calendar.getInstance();
		}
		result = (Calendar)result.clone();
		result.add(Calendar.SECOND, (int)time);
		return result;
	}
	
	public String getWarningTypeString() {
		return warningType.toString();
	}
}

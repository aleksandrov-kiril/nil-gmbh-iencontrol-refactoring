
package de.fzi.wenpro.core.model.productionplan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.fzi.wenpro.core.energy.model.EnergyForm;
import de.fzi.wenpro.core.energy.model.productionplan.EnergyProfile;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Order;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.fzi.wenpro.core.model.productionplan.TimeLine.Entry;

/**
 * The production plan containing the scheduling of all provided orders to the available resources.
 * 
 * @author Kiril Aleksandrov
 *
 */
public class ProductionPlan implements Cloneable, Iterable<ProductionPlanEntry>, Serializable{

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 5476583163586781979L;

	/**
	 * The list of all {@link ProductionPlanEntry}s in the factory. The list is
	 * sorted so that for each {@link Workplace}, its {@link TimedEntry}s appear
	 * ordered by time.
	 */
	protected final Map<Workplace, Schedule<ProductionPlanEntry>> schedules;
	
	protected final Map<Workplace, WorkplaceState> initialWorkplaceState;

	protected final Map<Product, TimeLine<Integer>> stock;
	
	protected final Map<Product, Integer> initialStock;
	
	protected final Map<Workplace, EnergyProfile> energyProfiles;
	
	@Deprecated
	protected final double MAXIMUMENERGYLEVEL = 40.0;
	
	/**
	 * Calendar object that represents the starting date for the production plan
	 * calculation
	 */
	private Calendar planStartDate;
	private Calendar nowFromPlan;

	private Set<Order> orders;
	private Set<Order> finishedOrders;
	
	public ProductionPlan(){
		schedules = new HashMap<Workplace, Schedule<ProductionPlanEntry>>();
		initialWorkplaceState = new HashMap<Workplace, WorkplaceState>();
		stock = new HashMap<Product, TimeLine<Integer>>();
		initialStock = new HashMap<Product, Integer>();
		orders = new HashSet<Order>();
		finishedOrders = new HashSet<Order>();
		energyProfiles = new HashMap<Workplace, EnergyProfile>();
	}

	public ProductionPlan(Calendar startDate){
		this();
		setPlanStartDate(startDate);
	}

	public ProductionPlan(Calendar startDate, List<Order> orders){
		this();
		setPlanStartDate(startDate);
		addOrder(orders);
	}

	public ProductionPlan(ProductionPlan productionPlan){
		this(productionPlan.getPlanStartDate());
		for (Workplace w : productionPlan.schedules.keySet()){
			Schedule<ProductionPlanEntry> t = productionPlan.schedules.get(w);
			if (t != null){
				Schedule<ProductionPlanEntry> tResult = new Schedule<ProductionPlanEntry>();
				for (int i=0; i<t.size(); i++){
					tResult.put(t.getTime(i), t.get(i).clone());
				}
				schedules.put(w, tResult);
			}
		}
		for (Product p : productionPlan.stock.keySet()){
			stock.put(p, productionPlan.stock.get(p).clone());
		}
		for(Workplace workplace : productionPlan.getEnergyProfiles().keySet()){
			energyProfiles.put(workplace, (EnergyProfile) productionPlan.getEnergyProfiles().get(workplace).clone());
		}
		initialStock.putAll(productionPlan.initialStock);
		initialWorkplaceState.putAll(productionPlan.initialWorkplaceState);
		orders.addAll(productionPlan.orders);
		finishedOrders.addAll(productionPlan.finishedOrders);
	}

	/**
	 * Creates a copy of a {@link ProductionPlan} where all entries before a
	 * given point in time are deleted. The production of the deleted entries is
	 * stored in the initial stock.
	 * @param productionPlan the original {@link ProductionPlan}
	 * @param offset the time cut off, in seconds
	 * @param calendar the new starting date
	 */
	@Deprecated //Operations are not handled in accordance to Operation States ==> wrong durations are set
	public ProductionPlan(ProductionPlan productionPlan, double offset, Calendar calendar){
		this(calendar);
		initialStock.putAll(productionPlan.initialStock);
		initialWorkplaceState.putAll(productionPlan.initialWorkplaceState);
		finishedOrders.addAll(productionPlan.finishedOrders);
		for (Workplace w : productionPlan.schedules.keySet()){
			Schedule<ProductionPlanEntry> t = productionPlan.schedules.get(w);
			if (t != null){
				Schedule<ProductionPlanEntry> tResult = getOrCreateSchedule(w);
				for (int i=0; i<t.size(); i++){
					ProductionPlanEntry entry = t.getEntry(i).object;
					double startTime = t.getTime(i);
					double newStartTime = startTime - offset;
					if (startTime + entry.getDuration() < offset){
						// skip this entry
						addOperationToInitialStock(entry.getOperation(), entry.getOperationCount());
						initialWorkplaceState.put(w, entry.getWorkplaceState());
					}else if (startTime < offset){
						//cut off this entry
						int nCutOffOperations = (int)Math.ceil((offset - (startTime + entry.getRetoolingTime())) / entry.getOperation().getTime());
						if (nCutOffOperations < 0){
							// entry is cut off while retooling
							nCutOffOperations = 0;
						}
						int incompleteLot = nCutOffOperations % entry.getOperation().getLotSize();
						if (incompleteLot > 0){
							int nOperationUntilNextLot = entry.getOperation().getLotSize() - incompleteLot;
							
							ProductionPlanEntry newEntry = entry.clone();
							newEntry.setRetooling(false);
							newEntry.setDuration(entry.getOperation().getTime() * nOperationUntilNextLot);
							
							tResult.put(newStartTime + entry.getRetoolingTime() + nCutOffOperations * entry.getOperation().getTime(), newEntry);
							
							nCutOffOperations += nOperationUntilNextLot;
						}
						if (nCutOffOperations < entry.getOperationCount()){
							ProductionPlanEntry newEntry = entry.clone();
							newEntry.setRetooling(false);
							newEntry.setDuration(entry.getDuration() - entry.getRetoolingTime() - nCutOffOperations * entry.getOperation().getTime());
							double retoolingCost = entry.isDoRetooling() ? entry.getWorkplaceState().getRetoolingCost() : 0;
							newEntry.setCost((entry.getCost() - retoolingCost) * newEntry.getProductionTime() / entry.getProductionTime());
							
							tResult.put(newStartTime + entry.getRetoolingTime() + nCutOffOperations * entry.getOperation().getTime(), newEntry);
						}
						addOperationToInitialStock(entry.getOperation(), nCutOffOperations);
						initialWorkplaceState.put(w, entry.getWorkplaceState());
					}else{
						tResult.put(newStartTime, t.get(i).clone());
					}
				}
			}
		}
		
		List<Order> orders = new ArrayList<Order>();
		orders.addAll(productionPlan.orders);
		Collections.sort(orders, new Comparator<Order>(){
			@Override
			public int compare(Order o1, Order o2){
				return o1.getEndDate().compareTo(o2.getEndDate());
			}
		});
		for (Order o : orders){
			Product p = o.getProduct();
			if (o.getEndDate().compareTo(planStartDate) < 0){
				setInitialStock(p, getInitialStock(p) - o.getQuantity());
				finishedOrders.add(o);
			}else if (o.getStartDate().compareTo(planStartDate) < 0){
				int stock = getInitialStock(p);
				if (stock < o.getQuantity()){
					Order oNew = o.clone();
					oNew.setQuantity(oNew.getQuantity() - stock);
					oNew.setStartDate(planStartDate);
					setInitialStock(p, 0);
					finishedOrders.add(o);
					this.orders.add(oNew);
				}else{
					setInitialStock(p, getInitialStock(p) - o.getQuantity());
					finishedOrders.add(o);
				}
			}else{
				this.orders.add(o);
			}
		}
		
		rebuildStock();
	}

	public Map<Workplace, Schedule<ProductionPlanEntry>> getEntries() {
		return schedules;
	}

	public ProductionPlan clone(){
		return new ProductionPlan(this);
	}

	/**
	 * Adds products to the initial stock as if the given operation had been
	 * performed before the beginning of this {@link ProductionPlan}. Input
	 * products are removed, output products are added.
	 * @param operation the {@link Operation} performed
	 * @param count the number of times the {@link Operation} has been performed
	 */
	private void addOperationToInitialStock(Operation operation, int count){
		for (ProductQuantity pq : operation.getInputs()){
			Integer initial = initialStock.get(pq.getProduct());
			if (initial == null){
				initial = 0;
			}
			initial -= pq.getQuantity() * count;
			initialStock.put(pq.getProduct(), initial);
		}
		for (ProductQuantity pq : operation.getOutputs()){
			Integer initial = initialStock.get(pq.getProduct());
			if (initial == null){
				initial = 0;
			}
			initial += pq.getQuantity() * count;
		}
	}

	/**
	 * @return the planStartDate for the current ProductionPlan as a {@link Calendar} object 
	 */
	public Calendar getPlanStartDate() {
		return planStartDate;
	}

	protected void setPlanStartDate(Calendar planStartDate) {
		this.planStartDate = planStartDate;
	}
	
	/**
	 * Returns the list of {@link Order}s in this {@link ProductionPlan}.
	 * Changes made in this list also affect this <code>ProductionPlan</code>.
	 * @return a list of <code>Order</code>s
	 */
	public Set<Order> getOrders(){
		return orders;
	}
	
	/**
	 * Adds an {@link Order} to this {@link ProductionPlan}.
	 * @param order an <code>Order</code>
	 */
	public void addOrder(Order order){
		if (!finishedOrders.contains(order)){
			orders.add(order);
		}
	}
	
	/**
	 * Adds a list of {@link Order}s to this {@link ProductionPlan}.
	 * @param orders some <code>Order</code>s
	 */
	public void addOrder(Collection<Order> orders){
		for (Order o : orders){
			addOrder(o);
		}
	}

	public ProductionPlan rescale(int factor){
		ProductionPlan result = new ProductionPlan();
		for (Workplace w : schedules.keySet()){
			Schedule<ProductionPlanEntry> t = schedules.get(w);
			Schedule<ProductionPlanEntry> tResult = new Schedule<ProductionPlanEntry>();
			for (int i=0; i<t.size(); i++){
				tResult.put(t.getTime(i)*factor, t.get(i).rescale(factor));
			}
			result.schedules.put(w, tResult);
		}
		return result;
	}

	public ProductionPlan(ProductionPlanEntry entry){
		this();
		Workplace workplace = entry.getWorkplace();
		Schedule<ProductionPlanEntry> s = getOrCreateSchedule(workplace);
		s.put(0, entry);

		Operation o = entry.getOperation();
		int operationCount = entry.getOperationCount();
		int steps = operationCount / o.getLotSize();
		
		for (ProductQuantity pq : o.getOutputs()){
			TimeLine<Integer> t = getOrCreateStock(pq.getProduct());
			for (int i=0; i<steps; i++){
				t.put((i+1)*o.getTime()*o.getLotSize(), i*o.getLotSize()*pq.getQuantity());
			}
			if (steps * o.getLotSize() < operationCount){
				t.put(operationCount*o.getTime(), operationCount*pq.getQuantity());
			}
			stock.put(pq.getProduct(), t);
		}
		for (ProductQuantity pq : o.getInputs()){
			TimeLine<Integer> t = getOrCreateStock(pq.getProduct());
			int i;
			for (i=0; i<steps; i++){
				t.put(i*o.getTime()*o.getLotSize(), -i*o.getLotSize()*pq.getQuantity());
			}
			if (steps * o.getLotSize() < operationCount){
				t.put(i*o.getTime()*o.getLotSize(), operationCount*pq.getQuantity());
			}
		}
	}

	/**
	 * The time at which the given {@link Workplace} finishes its last
	 * production entry.
	 * @param workplace a {@link Workplace}
	 * @return the finishing time
	 */
	public double getFinishingTime(Workplace workplace) {
		return getOrCreateSchedule(workplace).getFinishingTime();
	}

	/**
	 * The time at which all {@link ProductionPlanEntry}s are finished.
	 * @return the finishing time
	 */
	public double getFinishingTime(){
		double result = 0;
		for (Schedule<ProductionPlanEntry> s : schedules.values()){
			double time = s.getFinishingTime();
			if (time > result){
				result = time;
			}
		}
		return result;
	}

	/**
	 * The time at which the {@link ProductionPlanEntry} is finished.
	 * @param entry a <code>ProductionPlanEntry</code>
	 * @return the finishing time
	 */
	public double getFinishingTime(ProductionPlanEntry entry){
		Schedule<ProductionPlanEntry> s = schedules.get(entry.getWorkplace());
		if (s != null){
			return s.getFinishingTime(entry);
		}else{
			return Double.NaN;
		}
	}

	public ProductionPlan merge(ProductionPlan productionPlan){
		//TODO
		ProductionPlan result = clone();
		double finishingTime = getFinishingTime();
		for (Workplace w : productionPlan.schedules.keySet()){
			Schedule<ProductionPlanEntry> t = productionPlan.schedules.get(w);
			Schedule<ProductionPlanEntry> tResult = getOrCreateSchedule(w);
			for (int i=0; i<t.size(); i++){
				Entry<ProductionPlanEntry> e = t.getEntry(i);
				tResult.put(finishingTime + e.time, e.object);
			}
		}
		return result;
	}

	/**
	 * Adds a ProductionPlanEntry to this ProductionPlan at the given time.
	 * @param entry the ProductionPlanEntry to be added
	 * @param time the ProductionPlanEntry's starting time 
	 */
	public void add(ProductionPlanEntry entry, double time){
		getOrCreateSchedule(entry.getWorkplace()).put(time, entry);
		addToStock(time, entry);
		addToEnergyProfile(time, entry);
	}
	
	public Map<Product, Integer> getStockAtTime(Calendar next, Calendar now) {
		Map<Product,Integer> result = new HashMap<Product, Integer>();
		int time = (int) ((next.getTimeInMillis() - now.getTimeInMillis()) / 1000);
		for (Map.Entry<Product, TimeLine<Integer>> entry : stock.entrySet()) {
			int index1 = entry.getValue().getPreviousIndexByTime(time);
			int index2 = entry.getValue().getIndexByTime(time);
			if (index2 < entry.getValue().size()) {
				result.put(entry.getKey(), entry.getValue().get(index2));				
			} else if (index1 > 0) {
				result.put(entry.getKey(), entry.getValue().get(index1));
			}
		}
		return result;
	}

	protected void addToStock(double time, ProductionPlanEntry entry){		
		Map<Product, TimeLine<Integer>> newStocks = entry.getStockTimelines(time);
		for(Product product : newStocks.keySet()){
			addTimeline(getOrCreateStock(product), newStocks.get(product));
		}
	}

	/**
	 * Removes the given {@link ProductionPlanEntry} from this {@link ProductionPlan}.
	 * @param entry the entry to be removed
	 * @return <code>true</code> if this <code>ProductionPlan<code> has changed
	 */
	
	
	public boolean remove(ProductionPlanEntry entry){
		//TODO: UNTESTED
		Schedule<ProductionPlanEntry> s = schedules.get(entry.getWorkplace());
		if (s != null){
			int i = s.indexOf(entry);
			if (i >= 0){
				double time = s.getTime(i);
				Map<Product, TimeLine<Integer>> newStocks = entry.getStockTimelines(time);
				for(Product product : newStocks.keySet()){
					invert(newStocks.get(product));
					addTimeline(getOrCreateStock(product), newStocks.get(product));
				}
				removeFromEnergyProfile(time, entry);
				s.remove(i);
				return true;
			}
		}
		return false;
	}

	private void invert(TimeLine<Integer> timeLine) {
		for(Entry<Integer> entry : timeLine){
			timeLine.remove(timeLine.indexOf(entry.getObject()));
			timeLine.put(entry.getTime(), -entry.getObject());
		}
		
	}

	/**
	 * Moves a {@link ProductionPlanEntry} to another start point. The entry
	 * remains on the same {@link Workplace}.
	 * @param entry the entry to be moved
	 * @param time the entry's new start time
	 * @return <code>true</code> if this <code>ProductionPlan<code> has changed
	 */
	public boolean move(ProductionPlanEntry entry, double time){
		Schedule<ProductionPlanEntry> s = schedules.get(entry.getWorkplace());
		if (s != null){
			if (remove(entry)){
				add(entry, time);
				return true;
			}
		}
		return false;
	}

	/**
	 * Moves a {@link ProductionPlanEntry} to another {@link Workplace}. The entry is updated.
	 * @param entry the entry to be moved
	 * @param workplace the <code>Workplace</code> the entry will be asigned to
	 * @param time the new start time of the entry
	 * @return <code>true</code> if this <code>ProductionPlan<code> has changed
	 */
	public boolean moveToWorkplace(ProductionPlanEntry entry, Workplace workplace, double time){
		if (remove(entry)){
			entry.setWorkplace(workplace);
			add(entry, time);
			return true;
		}
		return false;
	}

	/**
	 * Adds a step function with an additional final step to the given {@link TimeLine}.
	 * @param timeline a <code>Timeline</code>
	 * @param startTime the time of the first step
	 * @param timePerStep the duration per step
	 * @param nSteps the number of full steps, the final step is not counted here
	 * @param piecesPerStep the pieces added per step
	 * @param finalStepTime the duration of the final step
	 * @param finalStepPieces the pieces added in the final step
	 */
	private void addStepFunction(TimeLine<Integer> timeline, double startTime, double timePerStep, int nSteps, int piecesPerStep, double finalStepTime, int finalStepPieces){
		double time = startTime;
		int pieces = piecesPerStep;
		TimeLine<Integer> t2 = new TimeLine<Integer>();
		for (int i=0; i<nSteps; i++){
			t2.put(time, pieces);
			time += timePerStep;
			pieces += piecesPerStep;
		}
		if (finalStepPieces != 0){
			t2.put(time-timePerStep+finalStepTime, pieces-piecesPerStep+finalStepPieces);
		}
		addTimeline(timeline, t2);
	}

	/**
	 * Adds two Integer {@link TimeLine}s, and writes the result into the first argument.
	 * @param t1 the <code>Timeline</code> the sum is written into
	 * @param t2 the <code>Timeline</code> that is added
	 */
	protected void addTimeline(TimeLine<Integer> t1, TimeLine<Integer> t2){
		if (t1.size() == 0){
			t1.putAll(t2);
		}else if (t2.size() == 0){
			// do nothing
		}else{
			TimeLine<Integer> t1Tmp = new TimeLine<Integer>(t1);
			t1.clear();
			
			Iterator<Entry<Integer>> iterator1 = t1Tmp.iterator();
			Iterator<Entry<Integer>> iterator2 = t2.iterator();
			Entry<Integer> e1 = iterator1.next();
			Entry<Integer> e2 = iterator2.next();
			Integer prev1 = 0;
			Integer prev2 = 0;
			boolean done = false;
			Integer prevVal = 0;
			Integer val;
			while (!done){
				if (e1.getTime() < e2.time){
					val = prev2 + e1.object;
					if (!val.equals(prevVal)){
						t1.putOverwrite(e1.time, val);
					}
					prev1 = e1.object;
					if (iterator1.hasNext()){
						e1 = iterator1.next();
					}else{
						e1 = new Entry<Integer>(Double.POSITIVE_INFINITY, null);
						done = e2.time == Double.POSITIVE_INFINITY;
					}
				}else{
					val = prev1 + e2.object;
					if (!val.equals(prevVal)){
						t1.putOverwrite(e2.time, val);
					}
					prev2 = e2.object;
					if (iterator2.hasNext()){
						e2 = iterator2.next();
					}else{
						e2 = new Entry<Integer>(Double.POSITIVE_INFINITY, null);
						done = e1.time == Double.POSITIVE_INFINITY;
					}
				}
				prevVal = val;
				
				int size = t1.size();
				if (size > 0){
					if (size == 1){
						if (t1.getEntry(0).object == 0){
							t1.remove(0);
						}
					}else if (t1.getEntry(size-2).object.equals(t1.getEntry(size-1).object)){
						t1.remove(size-1);
					}
				}
			}
		}
	}

	/**
	 * Adds a ProductionPlanEntry to this ProductionPlan at the given time, and
	 * adds a retooling entry if necessary.
	 * @param entry the ProductionPlanEntry to be added
	 * @param time the ProductionPlanEntry's starting time 
	 */
	public void addWithRetooling(ProductionPlanEntry entry, double time){
		Schedule<ProductionPlanEntry> s = getOrCreateSchedule(entry.getWorkplace());
		addWithRetooling(s, entry, time);
	}

	/**
	 * Adds a {@link ProductionPlanEntry} at the end of the schedule without
	 * checking if the dependencies are met.
	 * @param timedEntry
	 */
	public void addAtEnd(ProductionPlanEntry entry){
		Schedule<ProductionPlanEntry> s = getOrCreateSchedule(entry.getWorkplace());
		double time = s.getFinishingTime();
		add(entry, time);
	}

	/**
	 * Adds a {@link ProductionPlanEntry} at the end of the schedule, and
	 * adds a retooling entry if necessary.
	 * @param entry the ProductionPlanEntry to be added
	 * @return the time the entry is added
	 */
	public double addAtEndWithRetooling(ProductionPlanEntry entry){
		Schedule<ProductionPlanEntry> s = getOrCreateSchedule(entry.getWorkplace());
		double time = s.getFinishingTime();
		addWithRetooling(s, entry, time);
		return time;
	}

	/**
	 * Adds a {@link ProductionPlanEntry} at the end of the schedule, but not
	 * earlier than the given minimum. A retooling is added if necessary.
	 * @param entry the {@link ProductionPlanEntry} to be added
	 * @param timeMinimum the earliest time the entry may be added
	 * @return the time the entry is added
	 */
	public double addAtEndWithRetooling(ProductionPlanEntry entry, boolean updateDuration, double timeMinimum){
		Schedule<ProductionPlanEntry> s = getOrCreateSchedule(entry.getWorkplace());
		double time = s.getFinishingTime();
		time = Math.max(time, timeMinimum);
		addWithRetooling(s, entry, updateDuration, time);
		return time;
	}

	/**
	 * Adds a {@link ProductionPlanEntry} at the end of the schedule, but not
	 * earlier than the given minimum. A retooling is added if necessary.
	 * @param entry the {@link ProductionPlanEntry} to be added
	 * @param timeMinimum the earliest time the entry may be added
	 * @return the time the entry is added
	 */
	public double addAtEndWithRetooling(ProductionPlanEntry entry, double timeMinimum){
		return addAtEndWithRetooling(entry, false, timeMinimum);
	}

	/**
	 * Adds a {@link ProductionPlanEntry} to this {@link ProductionPlan} at the
	 * given time, and adds a retooling entry if necessary.
	 * @param schedule the {@link Schedule} the <code>ProductionPlanEntry</code>
	 *        belongs into
	 * @param entry the ProductionPlanEntry to be added
	 * @param time the ProductionPlanEntry's starting time
	 */
	protected void addWithRetooling(Schedule<ProductionPlanEntry> schedule, ProductionPlanEntry entry, boolean updateDuration, double time){
		WorkplaceState previousState = getWorkplaceState(schedule, entry.getWorkplace(), time);
		entry.setRetooling(previousState != entry.getWorkplaceState(), updateDuration);
		add(entry, time);
	}

	/**
	 * Adds a {@link ProductionPlanEntry} to this {@link ProductionPlan} at the
	 * given time, and adds a retooling entry if necessary.
	 * @param schedule the {@link Schedule} the <code>ProductionPlanEntry</code>
	 *        belongs into
	 * @param entry the ProductionPlanEntry to be added
	 * @param time the ProductionPlanEntry's starting time
	 */
	protected void addWithRetooling(Schedule<ProductionPlanEntry> schedule, ProductionPlanEntry entry, double time){
		addWithRetooling(schedule, entry, false, time);
	}

	private Schedule<ProductionPlanEntry> getOrCreateSchedule(Workplace workplace){
		Schedule<ProductionPlanEntry> result = schedules.get(workplace);
		if (result == null){
			result = new Schedule<ProductionPlanEntry>();
			schedules.put(workplace, result);
		}
		return result;
	}

	protected TimeLine<Integer> getOrCreateStock(Product product){
		TimeLine<Integer> result = stock.get(product);
		if (result == null){
			result = new TimeLine<Integer>();
			stock.put(product, result);
		}
		return result;
	}
	
	public TimeLine<Integer> getStock(Product product) {
		return getOrCreateStock(product);
	}
	
	/**
	 * The initial stock of the given {@link Product}. The number returned is
	 * the number of pieces that are in stock at the beginning of this
	 * {@link ProductionPlan}.
	 * @param product a {@link Product}
	 * @return the initial stock for this {@link Product}
	 */
	public int getInitialStock(Product product){
		Integer result = initialStock.get(product);
		if (result == null){
			return 0;
		}else{
			return result;
		}
	}
	
	public Map<Product, Integer> cloneInitialStock() {
		Map<Product, Integer> clone = new HashMap<Product, Integer>();
		for (Map.Entry<Product, Integer> entry : initialStock.entrySet()) {
			clone.put(entry.getKey(), entry.getValue().intValue());
		}
		return clone;
	}
	
	/**
	 * Sets the initial stock for the given {@link Product}, that is the number
	 * of pieces that are in stock at the beginning of this
	 * {@link ProductionPlan}.
	 * NOTE: after setting the initial stock, {@link # rebuildStock()} must be called!
	 * @param product a {@link Product}
	 * @param initialProductStock the initial stock for this {@link Product}
	 */
	public void setInitialStock(Product product, int initialProductStock){
		initialStock.put(product, initialProductStock);
	}
	
	/**
	 * The {@link WorkplaceState} of the given {@link Workplace} at the
	 * beginning of this {@link ProductionPlan}.
	 * @param workplace a {@link Workplace}
	 * @return the initial {@link WorkplaceState}
	 */
	public WorkplaceState getInitialWorkplaceState(Workplace workplace){
		WorkplaceState result = initialWorkplaceState.get(workplace);
		if (result == null){
			result = workplace.getActiveState();
			initialWorkplaceState.put(workplace, result);
		}
		return result;
	}

	/**
	 * Returns the state of a {@link Workplace} at the given time. The state is
	 * determined by the ProductionPlanEntry that has most recently started. If
	 * no ProductionPlanEntry has started yet,
	 * {@link Workplace#getActiveState()} is returned.
	 * @param workplace a <code>Workplace</code>
	 * @param time the time
	 * @return the state of the <code>Workplace</code>
	 */
	public WorkplaceState getWorkplaceState(Workplace workplace, double time){
		Schedule<ProductionPlanEntry> s = schedules.get(workplace);
		if (s == null){
			return getInitialWorkplaceState(workplace);
		}else{
			return getWorkplaceState(s, workplace, time);
		}
	}

	/**
	 * Returns the state of a {@link Workplace} at the given time. The state is
	 * determined by the ProductionPlanEntry that has most recently started. If
	 * no ProductionPlanEntry has started yet,
	 * {@link Workplace#getActiveState()} is returned.
	 * @param schedule the Schedule that belongs to the <code>Workplace</code>,
	 *        may not be <code>null</code>
	 * @param workplace a <code>Workplace</code>
	 * @param time the time
	 * @return the state of the <code>Workplace</code>
	 */
	private WorkplaceState getWorkplaceState(Schedule<ProductionPlanEntry> schedule, Workplace workplace, double time){
		if (schedule == null){
			return getInitialWorkplaceState(workplace);
		}else{
			int i = schedule.getPreviousIndexByTime(time);
			if (i == -1){
				return getInitialWorkplaceState(workplace);
			}else{
				return schedule.get(i).getWorkplaceState();
			}
		}
	}

	public double getCost(){
		double result = 0;
		for (Schedule<ProductionPlanEntry> s : schedules.values()){
			for (Entry<ProductionPlanEntry> e : s){
				result += e.object.getCost();
			}
		}
		return result;
	}
	

	public enum ProductionPlanWarningType{
		STOCK_TOO_LOW,
		PRODUCTION_OVERLAP,
		RETOOLING_UNNECESSARY,
		RETOOLING_REQUIRED,
		ENERGY_PROFILE_VIOLATED
	}

	/**
	 * Recalculates all stocks.
	 */
	public void rebuildStock(){
		stock.clear();
		for (Product p : initialStock.keySet()){
			getStock(p).put(0, initialStock.get(p));
		}
		for (Workplace w : schedules.keySet()){
			Schedule<ProductionPlanEntry> s = schedules.get(w);
			for (Entry<ProductionPlanEntry> e : s){
				addToStock(e.time, e.object);
			}
		}
	}

	public List<ProductionPlanWarning> getWarningList(){
		List<ProductionPlanWarning> result = new ArrayList<ProductionPlanWarning>();
		for (Product p : stock.keySet()){
			if (!p.isPurchasable()){
				TimeLine<Integer> s = stock.get(p);
				double tooLowSince = Double.NaN;
				double tooLowUntil = Double.NaN;
				for (Entry<Integer> e : s){
					if (e.getObject() < 0){
						if (Double.isNaN(tooLowSince)){
							tooLowSince = e.getTime();
						}
						tooLowUntil = e.getTime();
					}else{
						if (!Double.isNaN(tooLowSince)){
							if (!p.isPurchasable()){
								//FIXME: display warnings for purchasable products? 
								result.add(new ProductionPlanWarning(this, ProductionPlanWarningType.STOCK_TOO_LOW, null, null, null, p, tooLowSince, tooLowUntil));
							}
							tooLowSince = tooLowUntil = Double.NaN;
						}
					}
				}
				if (!Double.isNaN(tooLowSince)){
					if (!p.isPurchasable()){
						//FIXME: display warnings for purchasable products?
						result.add(new ProductionPlanWarning(this, ProductionPlanWarningType.STOCK_TOO_LOW, null, null, null, p, tooLowSince, Double.POSITIVE_INFINITY));
					}
					tooLowSince = tooLowUntil = Double.NaN;
				}
			}
		}
		for (Workplace w : schedules.keySet()){
			Schedule<ProductionPlanEntry> s = schedules.get(w);
			Entry<ProductionPlanEntry> previous = null;
			for (Entry<ProductionPlanEntry> e : s){
				WorkplaceState previousState;
				if (previous == null){
					previousState = getInitialWorkplaceState(w);
				}else{
					if (previous.getTime() + previous.getObject().getDuration() > e.getTime()){
						result.add(new ProductionPlanWarning(this, ProductionPlanWarningType.PRODUCTION_OVERLAP, w, null, null, null, e.getTime(), previous.getTime() + previous.getObject().getDuration()));
					}
					previousState = previous.getObject().getWorkplaceState();
				}
				if (previousState == e.getObject().getWorkplaceState()){
					if (e.getObject().doRetooling()){
						result.add(new ProductionPlanWarning(this, ProductionPlanWarningType.RETOOLING_UNNECESSARY, w, null, null, null, e.getTime(), e.getTime()));
					}
				}else{
					if (!e.getObject().doRetooling()){
						result.add(new ProductionPlanWarning(this, ProductionPlanWarningType.RETOOLING_REQUIRED, w, null, null, null, e.getTime(), e.getTime()));
					}
				}
				previous = e;
			}
		}
		for (Workplace w : schedules.keySet()){
			Schedule<ProductionPlanEntry> s = schedules.get(w);
			for (Entry<ProductionPlanEntry> e : s){
				ProductionPlanEntry entry = (ProductionPlanEntry) e.getObject();
				if (entry.isEnergyProfileViolated()){
					result.add(new ProductionPlanWarning(this, ProductionPlanWarningType.ENERGY_PROFILE_VIOLATED, w, null, null, null, e.getTime(), e.getTime()));
				}
			}
		}
		return result;
	}

	/**
	 * Checks if this {@link ProductionPlan} is valid, i.e. if there are no warning. Should return the same result as <code>getWarningList().isEmpty()</code>.
	 * @return <code>true</code> if this {@link ProductionPlan} is valid
	 */
	public boolean isValid(){
		for (Product p : stock.keySet()){
			if (!p.isPurchasable()){
				TimeLine<Integer> s = stock.get(p);
				for (Entry<Integer> e : s){
					if (e.object < 0){
						return false;
					}
				}
			}
		}
		for (Workplace w : schedules.keySet()){
			Schedule<ProductionPlanEntry> s = schedules.get(w);
			Entry<ProductionPlanEntry> previous = null;
			for (Entry<ProductionPlanEntry> e : s){
				WorkplaceState previousState;
				if (previous == null){
					previousState = getInitialWorkplaceState(w);
				}else{
					if (previous.time + previous.object.getDuration() > e.time){
						return false;
					}
					previousState = previous.object.getWorkplaceState();
				}
				if (previousState == e.object.getWorkplaceState()){
					if (e.object.doRetooling()){
						return false;
					}
				}else{
					if (!e.object.doRetooling()){
						return false;
					}
				}
				previous = e;
			}
		}
		return true;
	}

	public void printWarningList(){
		for (ProductionPlanWarning w : getWarningList()){
			System.out.println(w.getErrorMessage());
		}
	}

	@Override
	public Iterator<ProductionPlanEntry> iterator(){
		return new Iterator<ProductionPlanEntry>(){

			int[] positions = new int[schedules.size()];
			List<Schedule<ProductionPlanEntry>> scheduleList = new ArrayList<Schedule<ProductionPlanEntry>>(schedules.values());

			int iMin = -1;

			@Override
			public boolean hasNext(){
				for (int i=0; i<positions.length; i++){
					if (positions[i] < scheduleList.get(i).size()){
						return true;
					}
				}
				return false;
			}

			@Override
			public ProductionPlanEntry next(){
				double tMin = Double.POSITIVE_INFINITY;
				iMin = 0;
				for (int i=0; i<positions.length; i++){
					if (positions[i]<scheduleList.get(i).size()){
						double t = scheduleList.get(i).getEntry(positions[i]).time;
						if (t < tMin){
							tMin = t;
							iMin = i;
						}
					}
				}
				return scheduleList.get(iMin).getEntry(positions[iMin]++).object;
			}

			@Override
			public void remove(){
				scheduleList.get(iMin).remove(positions[iMin]-1);
				iMin = -1;
			}
		};
	}
	
	public void printStock(Product p){
		TimeLine<Integer> s = stock.get(p);
		if (s == null){
			System.out.println(p.getName() + ": -");
		}else{
			System.out.println(p.getName() + ": " + s);
		}
	}
	
	public double[] getFinishingPeriods() {
		Factory f = Project.getCurrentProject().getOriginalFactory();
		int productCount = f.getProducts().size();
		double[] finishingPeriods = new double[productCount];
		
		for (Schedule<ProductionPlanEntry> s : schedules.values()){
			for (Entry<ProductionPlanEntry> entry : s){
				double finishingTime = getFinishingTime(entry.object);
				for (ProductQuantity pq : entry.object.getOperation().getOutputs()){
					int productIndex = pq.getProduct().getProductIndex();
					finishingPeriods[productIndex] = Math.max(finishingPeriods[productIndex], finishingTime);
				}
			}
		}
		
		return finishingPeriods;
	}

	public Workplace getNextFreeWorkplace(Workplace... workplaces){
		Workplace result = null;
		double time = Double.POSITIVE_INFINITY;
		for (Workplace w : workplaces){
			double t = getFinishingTime(w);
			if (t < time){
				time = t;
				result = w;
			}
		}
		return result;
	}

	/**
	 * Checks if the given {@link ProductionPlanEntry} requires a retooling if
	 * it is added at the end of this {@link ProductionPlan} at the given
	 * {@link Workplace}
	 * @param pe a {@link ProductionPlanEntry}
	 * @return <code>true</code> if a retooling is required
	 */
	public boolean isRetoolingRequired(ProductionPlanEntry pe){
		Schedule<ProductionPlanEntry> s = schedules.get(pe.getWorkplace());
		if (s != null && s.size() > 0){
			return s.getLastEntry().object.getWorkplaceState() != pe.getWorkplaceState();
		}else{
			return getInitialWorkplaceState(pe.getWorkplace()) != pe.getWorkplaceState();
		}
	}
	
	public ArrayList<Entry<ProductionPlanEntry>> getEntriesByTime (String workplaceID, Calendar timeCalendar){
		Workplace workplace = Project.getCurrentProject().getOriginalFactory().getWorkplaceByID(workplaceID);
		if(workplace == null){
			throw new IllegalArgumentException("No Workplace object for the given workplaceID: "+workplaceID+ " found");
		}else{
			double time = (timeCalendar.getTimeInMillis() - getNowFromPlan().getTimeInMillis())/1000;		
			Schedule <ProductionPlanEntry> scheduleInWorkplace = schedules.get(workplace);
			ArrayList<Entry<ProductionPlanEntry>> entries = new ArrayList();
			Entry<ProductionPlanEntry> currentEntry = null;
			Entry<ProductionPlanEntry> retoolingEntry = null;
			boolean afterCurrentEntry = false;
			for(Entry<ProductionPlanEntry> entry : scheduleInWorkplace){
				double duration = entry.getObject().getDuration();
				if(entry.getTime() <= time && (entry.getTime() + duration) >= time ){
					currentEntry = entry;
					afterCurrentEntry = true;
					entries.add(entry);
					
				} else if(afterCurrentEntry && entry.getObject().isDoRetooling()){
					retoolingEntry = entry;
					entries.add(retoolingEntry);
					break;
				}
			}
			return entries;
		}
		
	}

	public void setNowFromPlan(Calendar nowFromPlan) {
		this.nowFromPlan = nowFromPlan;
	}

	public Calendar getNowFromPlan() {
		return nowFromPlan;
	}

	/**
	 * Merges all workplace specific {@link EnergyProfile}s to one combined profile
	 * @return
	 */
	public EnergyProfile getEnergyProfile() {
		EnergyProfile result = new EnergyProfile();
		
		for (Workplace workplace : energyProfiles.keySet()){
			result.mergeAtTime(energyProfiles.get(workplace), 0);
		}
		
		return result;
	}
	
	public Map<Workplace, EnergyProfile> getEnergyProfiles() {
		return energyProfiles;
	}
	
	@Deprecated
	public double getMaximumEnergyLevel() {
		return MAXIMUMENERGYLEVEL;
	}
	
	protected void addToEnergyProfile(double time, ProductionPlanEntry entry){
		if (energyProfiles.containsKey(entry.getWorkplace())){
			energyProfiles.get(entry.getWorkplace()).mergeAtTime(entry.getEntryEnergyProfile(), time);
		} else {
			EnergyProfile ep = new EnergyProfile();
			ep.mergeAtTime(entry.getEntryEnergyProfile(), time);
			energyProfiles.put(entry.getWorkplace(), ep);
		}
	}
	
	protected void removeFromEnergyProfile(double time, ProductionPlanEntry entry){
		EnergyProfile ep = entry.getEntryEnergyProfile().getInverse();
		energyProfiles.get(entry.getWorkplace()).mergeAtTime(ep, time);
	}
	
	/**
	 * total energy consumption of this plan in kWs
	 * @return
	 */
	public double getEnergyConsumption(){
		double result = 0;
		for (Schedule<ProductionPlanEntry> s : schedules.values()){
			for (Entry<ProductionPlanEntry> e : s){
				result += e.getObject().getEnergyConsumption();
			}
		}
		return result;
	}
	
	/**
	 * Only takes cost of {@link EnergyForm} ELECTRICITY into account
	 * @return Energy Cost of all {@link ProductionPlanEntry}s on this plan in Euro.
	 */
	public double getEnergyCost(){
		Factory f = Project.getCurrentProject().getOriginalFactory();
		double result = getEnergyConsumption() / 3600.0 * f.getFactoryEnergyInformation().getConsumptionCost(EnergyForm.ELECTRICITY);
		result += getPeakLoadCost();
		return result;
	}
	
	private double getPeakLoadCost() {
		Factory f = Project.getCurrentProject().getOriginalFactory();
		EnergyProfile ep = getEnergyProfile();
		double difference = ep.getMaximum() - ep.getAverage();
		double price = f.getFactoryEnergyInformation().getPeakCost(EnergyForm.ELECTRICITY);
		return difference * price;
	}

	public double getEPI() {
		return getCost()/getEnergyCost();
	}
}

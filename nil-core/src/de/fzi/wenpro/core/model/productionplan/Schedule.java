package de.fzi.wenpro.core.model.productionplan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Schedule<V extends ScheduleEntry> extends TimeLine<V> implements Serializable{

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 7550851143506635558L;

	public Schedule(Schedule<V> timeLine){
		super(timeLine.size());
		list.addAll(timeLine.list);
	}
	
	public Schedule(){
		super();
	}

	public Schedule<V> clone(){
		return new Schedule<V>(this);
	}
	
	public void put(V object){
		double time = getFinishingTime();
		list.add(getIndexByTime(time), new Entry<V>(time, object));
	}
	
	/**
	 * The {@link Entry} in this {@link TimeLine} that finishes last. If the
	 * <code>TimeLine</code> is empty, <code>null</code> is returned.
	 * @return the last entry
	 */
	public Entry<V> getLastFinishingEntry(){
		Entry<V> result = null;
		double maxTime = 0;
		for (Entry<V> e : list){
			double time = getFinishingTime(e.getObject());
			if (time > maxTime){
				maxTime = time;
				result = e;
			}
		}
		return result;
	}

	/**
	 * The time when all entries have finished.
	 * @return the finishing time
	 */
	public double getFinishingTime(){
		Entry<V> lastEntry = getLastFinishingEntry();
		if (lastEntry == null){
			return 0;
		}else{
			return getFinishingTime(lastEntry.getObject());
		}
	}
	
	/**
	 * The time when the given {@link ScheduleEntry} has finished.
	 * @param entry
	 * @return
	 */
	public double getFinishingTime(V entry){
		int i = indexOf(entry);
		if (i == -1){
			return Double.NaN;
		}else{
			return getTime(i) + entry.getDuration();
		}
	}
	
	public boolean checkOverlap(V entry, double startTime){
		for (Entry<V> e : list){
			if (e.getTime() <= startTime){
				if (e.getTime() + e.getObject().getDuration() > startTime){
					return true;
				}	
			}else{
				if (startTime + entry.getDuration() > e.getTime()){
					return true;
				}
			}
		}
		return false;
	}
	
	public List<Entry<V>> getOverlapList(V entry, double startTime){
		List<Entry<V>> result = new ArrayList<Entry<V>>();
		for (Entry<V> e : list){
			if (e.getTime() <= startTime){
				if (e.getTime() + e.getObject().getDuration() > startTime){
					result.add(e);
				}	
			}else{
				if (startTime + entry.getDuration() > e.getTime()){
					result.add(e);
				}
			}
		}
		return result;
	}

	public void remove(int i){
		list.remove(i);
	}
}

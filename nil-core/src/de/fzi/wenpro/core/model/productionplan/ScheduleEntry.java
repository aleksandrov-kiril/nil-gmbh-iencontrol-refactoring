package de.fzi.wenpro.core.model.productionplan;

public interface ScheduleEntry{
	public double getDuration();
}

package de.fzi.wenpro.core.model.productionplan;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import de.fzi.wenpro.core.calculations.CostCalculations;
import de.fzi.wenpro.core.energy.model.facility.EnergyConsumingOperation;
import de.fzi.wenpro.core.energy.model.productionplan.EnergyProfile;
import de.fzi.wenpro.core.energy.model.productionplan.EntryState;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Order;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.fzi.wenpro.util.Tuple;

/**
 * Represents a part of a {@link ProductionPlan} in which one action is performed. This can either be an operation (performed one or multiple times) or the retooling of a {@link Workplace}.
 * @author siebel
 *
 */
public class ProductionPlanEntry implements Cloneable, ScheduleEntry, Serializable{
	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -7372432872984504312L;
	
	private Workplace workplace;
	private WorkplaceState workplaceState;
	
	private boolean energyProfileViolated;
	private TreeSet<EntryState> entryStates;
	
	private double duration;
	private double cost;
	protected Operation operation;
	private Order order;
	private boolean doRetooling;
	
	/**
	 * Creates a ProductionPlanEntry in which an {@link Operation} is performed
	 * one or many times.
	 * @param workplace the {@link Workplace} in which the operation is
	 *        performed
	 * @param state the state the workplace is in after (and during) the
	 *        operation
	 * @param operation the operation that is performed
	 * @param duration the time the ProductionPlanEntry takes on the workplace
	 * @param cost the cost caused by this ProductionPlanEntry
	 * @param doRetooling <code>true</code> if a retooling is performed at the
	 *        beginning of this entry
	 */
	public ProductionPlanEntry(Workplace workplace, WorkplaceState state, Operation operation, Order order, double duration, double cost, boolean doRetooling){
		this.workplace = workplace;
		this.workplaceState = state;
		this.operation = operation;
		this.duration = duration;
		this.cost = cost;
		this.doRetooling = doRetooling;
		this.order = order;
		this.setStateProfile();
	}

	public ProductionPlanEntry(Workplace workplace, WorkplaceState state, Operation operation, double duration, double cost, boolean doRetooling){
		this(workplace, state, operation, null, duration, cost, doRetooling);
	}
	
	/**
	 * Creates a ProductionPlanEntry in which an {@link Operation} is performed
	 * one or many times.
	 * @param workplace the {@link Workplace} in which the operation is
	 *        performed
	 * @param state the state the workplace is in after (and during) the
	 *        operation
	 * @param operation the operation that is performed
	 * @param operationCount the number of times the operation is performed
	 * @param duration the time the ProductionPlanEntry takes on the workplace
	 * @param cost the cost caused by this ProductionPlanEntry
	 */
	public ProductionPlanEntry(Workplace workplace, WorkplaceState state, Operation operation, double duration, double cost){
		this(workplace, state, operation, duration, cost, false);
	}
	
	/**
	 * Creates a ProductionPlanEntry in which an {@link Operation} is performed
	 * one or many times.
	 * @param workplace the {@link Workplace} in which the operation is
	 *        performed
	 * @param state the state the workplace is in after (and during) the
	 *        operation
	 * @param operation the operation that is performed
	 * @param operationCount the number of times the operation is performed
	 * @param cost the cost caused by this ProductionPlanEntry
	 * @param doRetooling <code>true</code> if a retooling is performed at the
	 *        beginning of this entry
	 */
	public ProductionPlanEntry(Workplace workplace, WorkplaceState state, Operation operation, int operationCount, double cost, boolean doRetooling){
		this(workplace, state, operation, null, operationCount, cost, doRetooling);
	}
	
	/**
	 * Creates a ProductionPlanEntry in which an {@link Operation} is performed
	 * one or many times.
	 * @param workplace the {@link Workplace} in which the operation is
	 *        performed
	 * @param state the state the workplace is in after (and during) the
	 *        operation
	 * @param operation the operation that is performed
	 * @param order the {@link Order} that evoked creation of this Entry
	 * @param operationCount the number of times the operation is performed
	 * @param cost the cost caused by this ProductionPlanEntry
	 * @param doRetooling <code>true</code> if a retooling is performed at the
	 *        beginning of this entry
	 */
	public ProductionPlanEntry(Workplace workplace, WorkplaceState state, Operation operation, Order order, int operationCount, double cost, boolean doRetooling){
		this(workplace,
				state,
				operation, 
				order,
				((EnergyConsumingOperation)operation).calculateDuration(operationCount) + (doRetooling ? state.getRetoolingTime() : 0),
				cost, 
				doRetooling);
	}
	
	/**
	 * Creates a ProductionPlanEntry in which an {@link Operation} is performed
	 * one or many times.
	 * @param workplace the {@link Workplace} in which the operation is
	 *        performed
	 * @param state the state the workplace is in after (and during) the
	 *        operation
	 * @param operation the operation that is performed
	 * @param operationCount the number of times the operation is performed
	 * @param cost the cost caused by this ProductionPlanEntry
	 */
	public ProductionPlanEntry(Workplace workplace, WorkplaceState state, Operation operation, int operationCount, double cost){
		this(workplace, state, operation, operationCount, cost, false);
	}
	
	public ProductionPlanEntry clone(){
		try{
			return (ProductionPlanEntry)(super.clone());
		}catch (CloneNotSupportedException e){
			return null;
		}
	}
	
	public ProductionPlanEntry rescale(int factor){
		double newDuration;
		if (doRetooling){
			newDuration = (duration-workplaceState.getRetoolingTime()) * factor + workplaceState.getRetoolingTime();
		}else{
			newDuration = factor * duration;
		}
		return new ProductionPlanEntry(workplace, workplaceState, operation, newDuration, cost * factor, doRetooling);
	}
	
	public Workplace getWorkplace() {
		return workplace;
	}

	public WorkplaceState getWorkplaceState() {
		return workplaceState;
	}

	public boolean isDoRetooling(){
		return doRetooling;
	}

	/**
	 * Sets if retooling is done, without updating the total duration.
	 * @param doRetooling if the retooling is to be done
	 */
	public void setRetooling(boolean doRetooling){
		setRetooling(doRetooling, false);
	}
	
	/**
	 * Sets if retooling is done.
	 * @param doRetooling if the retooling is to be done
	 * @param updateDuration if this is <code>true</code>, the total duration is
	 *        updated to keep the previous operation count
	 */
	public void setRetooling(boolean doRetooling, boolean updateDuration){
		if (updateDuration){
			duration += ((doRetooling?1:0)-(this.doRetooling?1:0)) * workplaceState.getRetoolingTime();
		}
		this.doRetooling = doRetooling;
		this.setStateProfile();
	}

	public void setState(WorkplaceState state){
		this.workplaceState = state;
	}

	public void setDuration(double duration){
		this.duration = duration;
		this.setStateProfile();
	}

	public void setCost(double cost){
		this.cost = cost;
	}
	
	public double getCost() {
		return cost;
	}
	
	/**
	 * The time free for production, i.e. the total time minus the retooling
	 * time and minus time of non-producing entry States
	 * @return the production time in seconds
	 */
	public double getProductionTime(){
		double result = 0.0;
		for (EntryState entryState : entryStates){
			double duration = entryState.getDuration();
			result += entryState.isProducing()? duration : 0.0;
		}
		return result;
	}
	
	public double getRetoolingTime(){
		if (doRetooling){
			return workplaceState.getRetoolingTime();
		}else{
			return 0;
		}
	}

	public Operation getOperation() {
		return operation;
	}

	public int getOperationCount() {
		double result;
		double prodTime = getProductionTime();
		double timePerOp = operation.getTime();
		result = (prodTime / timePerOp);
		return (int)result;
	}

	public boolean doRetooling() {
		return doRetooling;
	}
	
	@Override
	public double getDuration(){
		return duration;
	}

	public boolean setWorkplace(Workplace workplace){
		this.workplace = workplace;
		for (WorkplaceState ws : workplace.getStates()){
			for (Operation o : ws.getOperations()){
				if (o.getOutputs().equals(operation.getOutputs())){
					setWorkplaceState(ws);
					setOperation(o);
					return true;
				}
			}
		}
		return false;
	}
	
	public void setWorkplaceState(WorkplaceState workplaceState){
		this.workplaceState = workplaceState;
	}
	
	public void setOperation(Operation operation){
		this.operation = operation;
	}

	/**
	 * Sets the variable cost caused by this {@link ProductionPlanEntry},
	 * including retooling costs.
	 * @return the calculated costs
	 */
	public double calculateCost(){
		int operationCount = getOperationCount();
		double variableCostsPerOperation = CostCalculations.getVariableCostsPerOperation(operation, workplace.getFactory().getDefaultShiftplan());
		return cost = operationCount * variableCostsPerOperation + (doRetooling ? getWorkplaceState().getRetoolingCost() : 0);
	}
	
	@Override
	public String toString(){
		return getOperationCount() + " � " + operation + " (at " + workplace + ")";
	}
	
	/**
	 * Generates a {@code entryEnergyProfile} for this {@link EnergyProductionPlanEntry} according to
	 * its current duration. Additionally it sets the flag {@code energyProfileViolated} in case the current duration
	 * is too short (duration < retooling + startingDuration + endingDuration)
	 */
	public void setStateProfile() {
		double retoolingTime = this.isDoRetooling()?this.getRetoolingTime():0.0;
		Tuple<Set<EntryState>, Boolean> tuple = ((EnergyConsumingOperation)operation).createEntryStateProfile(this.getDuration(), retoolingTime);
		entryStates = new TreeSet<EntryState>(new EntryState.EntryStateOrdinalComparator());
		entryStates.addAll(tuple.getFirst());
		energyProfileViolated = tuple.getSecond();
	}
	
	public EnergyProfile getEntryEnergyProfile() {
		EnergyProfile result = new EnergyProfile();
		double totalDuration = 0;
		for(EntryState es : entryStates){
			totalDuration += es.getDuration();
		}
		// first set total EnergyLevel to 0 in case of retooling
		result.addEntry(0, totalDuration, 0);
		for(EntryState es : entryStates){
			result.addEntry(es.getStartingTime(), es.getDuration(), es.getEnergyLevel());
		}
		return result;
	}
	
	public boolean isEnergyProfileViolated() {
		return energyProfileViolated;
	}
	
	public TreeSet<EntryState> getEntryStates() {
		return entryStates;
	}
	
	/**
	 * Generates a {@link TimeLine} with entries according to lotsize and {@link EntryState}s 
	 * of this EnergyProductionPlanEntry
	 * @param time time in seconds, when the entry is supposed to start including retooling
	 * @return A timeline containing only the stock generated by this entry
	 */
	public Map<Product, TimeLine<Integer>> getStockTimelines(double time) {
		Map<Product, TimeLine<Integer>> result = new HashMap<Product, TimeLine<Integer>>();
		
		Set<Product> outputs = operation.getOutputProducts();
		Set<Product> inputs = operation.getInputProducts();
		
		for(Product product : outputs){
			result.put(product, new TimeLine<Integer>());
		}
		for(Product product : inputs){
			result.put(product, new TimeLine<Integer>());
		}
		
		int opCount = getOperationCount();
		if(opCount > 0){
			int lastLot = opCount % operation.getLotSize();
			int totalLots = (opCount - lastLot) / operation.getLotSize();
			if(lastLot > 0){
				totalLots++;
			}
			int lotCount = 0;
			double timePerLot = (Math.round(operation.getLotSize() * operation.getTime()*1000))/1000.0;
			
			if(doRetooling()){
				time += getRetoolingTime();
			}
			
			double overlapDuration = 0.0;
			Map<Product, Integer> currentStocks = new HashMap<Product, Integer>();
			for(Product product : outputs){
				currentStocks.put(product, 0);
			}
			for(Product product : inputs){
				currentStocks.put(product, 0);
			}
			double lotStartTime = time;
			for(EntryState entryState : entryStates){
				if(entryState.isProducing()){
					lotStartTime = overlapDuration == 0.0 ? time : lotStartTime; // is a new lot starting?
					double remainingDuration = entryState.getDuration();
					while(remainingDuration >= timePerLot - overlapDuration){
						remainingDuration -= (timePerLot - overlapDuration); 
						for(Product product : inputs){
							if(!product.isPurchasable()){	
								currentStocks.put(product, currentStocks.get(product) - operation.getLotSize() * 
										operation.getInputPiecesPerOperation(product));
								result.get(product).put(lotStartTime, currentStocks.get(product));
							}
						}
						time += (timePerLot - overlapDuration);
						overlapDuration = 0.0;
						for(Product product : outputs){
							currentStocks.put(product, currentStocks.get(product) + operation.getLotSize() * 
									operation.getOutputPiecesPerOperation(product));
							result.get(product).put(time, currentStocks.get(product));
						}
						lotCount++;
						lotStartTime = time;
					}
					overlapDuration += remainingDuration;
					time += remainingDuration;
				}
			}
			if(overlapDuration > 0.0){
				for(Product product : inputs){
					if(!product.isPurchasable()){
						currentStocks.put(product, currentStocks.get(product) - (int)(overlapDuration /
								operation.getTime()) * operation.getInputPiecesPerOperation(product));
						result.get(product).put(time, currentStocks.get(product));
					}
				}
				time += overlapDuration;
				for(Product product : outputs){
					currentStocks.put(product, currentStocks.get(product) + (int)(overlapDuration /
							operation.getTime()) * operation.getOutputPiecesPerOperation(product));
					result.get(product).put(time, currentStocks.get(product));
				}
				if((int)(overlapDuration / operation.getTime())>0){
					lotCount++;
				}
			}
			
			int test = (int)(Math.round((overlapDuration /operation.getTime())*1000)/1000.0);
			
			if(!(lotCount == totalLots)){
				System.out.println("FEHLER 1 in getStockTimelines in ProductionPlanEntry");
			}
			if(!(lastLot == test)){
				System.out.println("FEHLER 2 in getStockTimelines in ProductionPlanEntry");
			}
		} else {
			System.out.println("Entryduration = 0: " + operation);
		}
		return result;
	}

	public Order getOrder() {
		return order;
	}
	
	/**
	 * 
	 * @return Energy consumption of this {@link ProductionPlanEntry} in kWs.
	 */
	public double getEnergyConsumption(){
		double result = 0;
		for(EntryState state : entryStates){
			result += state.getDuration() * state.getEnergyLevel();
		}
		return result;
	}
}

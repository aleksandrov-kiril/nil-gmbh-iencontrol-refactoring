package de.fzi.wenpro.core.model.productionplan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TimeLine<V> implements Iterable<TimeLine.Entry<V>>, Cloneable, Serializable{

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 4557982514736952185L;

	public static class Entry<W> implements Serializable{
		/**
		 * Generated serial version UID
		 */
		private static final long serialVersionUID = 5826850331773570701L;
		
		final double time;
		final W object;
		
		Entry(double time, W object){
			this.time = time;
			this.object = object;
		}
		
		@Override
		public String toString(){
			return "(" + getTime() + " => " + getObject() + ")";
		}

		public W getObject() {
			return object;
		}

		public double getTime() {
			return time;
		}
	}
	
	protected final List<Entry<V>> list;
	
	public TimeLine(){
		list = new ArrayList<Entry<V>>();
	}
	
	public TimeLine(int initialCapacity){
		list = new ArrayList<Entry<V>>(initialCapacity);
	}
	
	public TimeLine(TimeLine<V> timeLine){
		this(timeLine.size());
		list.addAll(timeLine.list);
	}
	
	public TimeLine<V> clone(){
		return new TimeLine<V>(this);
	}
	
	public void clear(){
		list.clear();
	}
	
	public void put(double time, V object){
		list.add(getIndexByTime(time), new Entry<V>(time, object));
	}
	
	/**
	 * Adds an {@link Entry} into this {@link TimeLine}. If there exists an
	 * <code>Entry</code> as this time, it is overwritten.
	 * @param time
	 * @param object
	 */
	public void putOverwrite(double time, V object){
		int index = getIndexByTime(time);
		if (size() > 0 && index > 0 && list.get(index-1).getTime() == time){
			list.set(index-1, new Entry<V>(time, object));
		}else{
			list.add(index, new Entry<V>(time, object));
		}
	}
	
	public V get(int index){
		return list.get(index).getObject();
	}
	
	public double getTime(int index){
		return list.get(index).time;
	}
	
	public Entry<V> getEntry(int index){
		return list.get(index);
	}
	
	/**
	 * The {@link Entry} in this {@link TimeLine} that starts last. If the
	 * <code>TimeLine</code> is empty, <code>null</code> is returned.
	 * @return the last entry
	 */
	public Entry<V> getLastEntry(){
		if (list.size() == 0){
			return null;
		}else{
			return list.get(list.size()-1);
		}
	}
	
	public void set(int index, V object){
		list.set(index, new Entry<V>(list.get(index).getTime(), object));
	}
	
	public void set(int index, double time, V object){
		list.set(index, new Entry<V>(time, object));
	}
	
	public void setTime(int index, double time){
		list.set(index, new Entry<V>(time, list.get(index).getObject()));
	}
	
	public int size(){
		return list.size();
	}
	
	/**
	 * The first index that is after the given time.
	 * @param time the time
	 * @return the index of the entry
	 */
	public int getIndexByTime(double time){
		if (size() == 0 || time < list.get(0).getTime()){
			return 0;
		}else if (time >= list.get(size()-1).getTime()){
			return size();
		}else{
			// Invariant: time[i1] <= time < time[i2]
			int i1 = 0, i2 = size()-1;
			while (i1 < i2-1){
				int i = (i1+i2)/2;
				if (getTime(i) <= time){
					i1 = i;
				}else{
					i2 = i;
				}
			}
			return i2;
		}
	}
	
	/**
	 * The last index that is before the given time.
	 * @param time the time
	 * @return the index of the entry
	 */
	public int getPreviousIndexByTime(double time){
		return getIndexByTime(time)-1;
		//TODO: what if multiple entries are at the same time?
	}
	
	public double[] getAllTimes(){
		double[] result = new double[size()];
		int i = 0;
		for (Entry<V> e : list){
			result[i++] = e.getTime();
		}
		return result;
	}

	public V[] getAllObjects(){
		@SuppressWarnings("unchecked")
		V[] result = (V[])new Object[size()];
		int i = 0;
		for (Entry<V> e : list){
			result[i++] = e.getObject();
		}
		return result;
	}
	
	//@Override
	public Iterator<Entry<V>> iterator(){
		return new Iterator<Entry<V>>(){

			int index = 0;
			
			//@Override
			public boolean hasNext(){
				return index < size();
			}

			//@Override
			public Entry<V> next(){
				return list.get(index++);
			}

			//@Override
			public void remove(){
				list.remove(index);
			}
		};
	}

	public int indexOf(V object){
		int i = 0;
		for (Entry<V> e : list){
			if (e.getObject() == object){
				return i;
			}
			i++;
		}
		return -1;
	}

	public void putAll(TimeLine<V> timeLine){
		if (size() == 0){
			list.addAll(timeLine.list);
		}else if (timeLine.size() > 0){
			int i = getIndexByTime(timeLine.getTime(0));
			for (Entry<V> e : timeLine){
				while (i < list.size() && list.get(i).getTime() <= e.getTime()){
					i++;
				}
				list.add(i++, e);
			}
		}
	}
	
	@Override
	public String toString(){
		String result = "[";
		int i=0;
		for (; i<size()-1 && i<15; i++){
			result += list.get(i) + ", ";
		}
		if (i < size()-15){
			i = size()-15;
			result += "..., ";
		}
		for (; i<size()-1; i++){
			result += list.get(i) + ", ";
		}
		if (size() > 0){
			result += list.get(i);
		}
		result += "]";
		return result;
	}

	public void remove(int index){
		list.remove(index);
	}

	public boolean containsTime(double t) {
		boolean result = false;
		for (Entry<V> e: list){
			if (e.time == t) {
				result = true;
			}
		}
		return result;
	}
}

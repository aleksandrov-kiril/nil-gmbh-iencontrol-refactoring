/**
 * Ecoflex 2009
 * de.fzi.ecoflex.core.EcoflexProjectManager.java
 */
package de.fzi.wenpro.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;


/**
 * 
 * @author Kiril Aleksandrov
 */
@Entity
public class Project implements Serializable{

	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -702439405380804814L;

	private transient static Project currentProject;

	private transient static boolean isProjectLoaded=false;
	
	private Factory originalFactory;
	private List<Factory> alternativeFactoryList;
	private int currentAlternativeIndex;
	private int id;
	
	private static ProductionPlan currentProductionPlan;
	
	public static ProductionPlan getCurrentProductionPlan() {
		return currentProductionPlan;
	}
	
	public static void setCurrentProductionPlan(ProductionPlan currentProductionPlan) {
		Project.currentProductionPlan = currentProductionPlan;
	}
	

	public Project(){
		this(null, new ArrayList<Factory>());
	}

	public Project(Factory factory){
		this();
//		if (factory == null){
//			originalFactory = Ecoflex.getExampleFactory();
//			alternativeFactoryList = Ecoflex.getAlternativeList();
//		}else{
			originalFactory = factory;
//		}
	}
	
	public Project(Factory factory, List<Factory> alternativeList){
		originalFactory = factory;
		alternativeFactoryList = alternativeList;
	}
	

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	public static Project getCurrentProject(){
		if (currentProject == null){
			currentProject = new Project(null);
		}
		return currentProject;
	}
	
	public static void setCurrentProject(Project project){
		currentProject = project;
	}	
	
	public static Project createCurrentProject(){
		setCurrentProject(new Project());
		return getCurrentProject();
	}


	@ManyToOne(cascade = CascadeType.ALL)
	@Cascade(value=org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public Factory getOriginalFactory(){
		return this.originalFactory;
	}
	/**
	 * @param originalFactory the originalFactory to set
	 */
	public void setOriginalFactory(Factory originalFactory) {
		this.originalFactory = originalFactory;
	}
	
	
	@OneToMany(cascade = CascadeType.ALL)
	@Cascade(value=org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public List<Factory> getAlternativeFactoryList() {
		return alternativeFactoryList;
	}
	/**
	 * @param alternativeFactoryList the alternativeFactoryList to set
	 */
	public void setAlternativeFactoryList(List<Factory> alternativeFactoryList) {
		this.alternativeFactoryList = alternativeFactoryList;
	}
	
	@Transient
	public Factory getAlternativeByName(String name){
		for (Factory f : getAlternativeFactoryList()){
			if (f.getName().equals(name)){
				return f;
			}
		}
		return null;
	}

	/**
	 * Creates a new alternative factory with default target node and target capacity
	 */
	public Factory createAlternative() {
		Factory alternative = getOriginalFactory().clone();
		alternative.setName(getNextAlternativeName());
		alternative.setExpansionFlexibilityNode(alternative);
		alternative.setExpansionFlexibilityTarget(1.0);
		
		getAlternativeFactoryList().add(alternative);
		

			
		return alternative;
	}
	
	/**
	 * Copies the currentAlternative
	 * @return the copy of the currentAlternative
	 */
	public Factory copyAlternative(){
		Factory newAlternative = getCurrentAlternativeFactory().clone();
		newAlternative.setName(getNextAlternativeName());
		newAlternative.setExpansionFlexibilityTarget(newAlternative, 1.0);
		
		for (FactoryNode node : EntityList.getSubnodeList(newAlternative)){
			node.setOriginal(node.getOriginal().getOriginal());
		}

		getAlternativeFactoryList().add(newAlternative);
		
		return newAlternative;
	}

	public void removeAlternative(Factory alternative){
		getAlternativeFactoryList().remove(alternative);
	}

	public void removeAlternative(String name) {
		for (Factory alternative : getAlternativeFactoryList()){
			if (alternative.getName().equals(name)){
				removeAlternative(alternative);
			}
		}
	}
	
	@Transient
	public Factory getCurrentAlternativeFactory(){
		if (getAlternativeFactoryList().size() == 0){
			return null;
		}else{
			return getAlternativeFactoryList().get(currentAlternativeIndex);
		}
	}
	
	/**
	 * Sets an alternative factory instance as current alternative to work with
	 * @param factory 
	 */
	public void setCurrentAlternativeFactory(Factory factory) {
		currentAlternativeIndex = getAlternativeFactoryList().indexOf(factory);
		if (currentAlternativeIndex == -1){
			currentAlternativeIndex = 0;
		}
	}
	
	@Transient
	private String getNextAlternativeName(){
		return "Alternative Fabrik " + (getAlternativeFactoryList().size()+1);
	}

	public static boolean isProjectLoaded() {
		// TODO Auto-generated method stub
		return isProjectLoaded;
	}
	
	public static void setProjectLoaded() {
		Project.isProjectLoaded = true;
	}
}

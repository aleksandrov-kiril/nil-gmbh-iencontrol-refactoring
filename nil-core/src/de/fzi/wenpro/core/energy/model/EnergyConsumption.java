/**
 * 
 */
package de.fzi.wenpro.core.energy.model;

import java.io.Serializable;

import de.fzi.wenpro.core.energy.model.facility.EnergySourceFacility;

/**
 * @author wicaksono
 * 
 */
public class EnergyConsumption implements Serializable {

	private static final long serialVersionUID = 8207638581091860401L;

	public final static String PROPERTY_MEASUREMENT = "ConsumptionMeasurementTime";
	public final static String PROPERTY_CONSUMPTION = "consumptionValue";
	public final static String PROPERTY_PEAKLOAD = "peakLoadValue";
	public final static String PROPERTY_ENERGYFORM = "energyform";

	private EnergySourceFacility energySource;
	private EnergyForm energyForm;
	protected double measurementTime;
	private double consumptionValue;
	private double peakLoadValue;

	public EnergySourceFacility getEnergySource() {
		return energySource;
	}

	public void setEnergySource(EnergySourceFacility energySource) {
		this.energySource = energySource;
	}

	public EnergyForm getEnergyForm() {
		return energyForm;
	}

	public void setEnergyForm(EnergyForm eneryForm) {
		this.energyForm = eneryForm;
	}

	public double getConsumptionValue() {
		return consumptionValue;
	}

	public void setConsumptionValue(double consumptionValue) {
		this.consumptionValue = consumptionValue;
	}

	public double getPeakLoadValue() {
		return peakLoadValue;
	}

	public void setPeakLoadValue(double peakLoadValue) {
		this.peakLoadValue = peakLoadValue;
	}

	public double getMeasurementTime() {
		return measurementTime;
	}

	public void setMeasurementTime(double measurementTime) {
		this.measurementTime = measurementTime;
	}

	public double getEnegryCostProHoure() {
		double consumptionProHour = consumptionValue / measurementTime;
		return consumptionProHour * energySource.getEnergyPrice(this);
	}

	public double getPeakCostProHouer() {
		double peakCostproHouer = peakLoadValue / measurementTime;
		return peakCostproHouer * energySource.getPeakPrice(this);
	}

	public double getEnertyProHouer() {
		return consumptionValue / measurementTime;
	}

	public double getPeakCost() {
		return (peakLoadValue * energySource.getPeakPrice(this));
	}

}

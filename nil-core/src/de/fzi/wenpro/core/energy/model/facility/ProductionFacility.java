/**
 * 
 */
package de.fzi.wenpro.core.energy.model.facility;

import java.util.List;

import de.fzi.wenpro.core.energy.model.EnergyConsumption;
import de.fzi.wenpro.core.energy.model.EnergyForm;
import de.fzi.wenpro.core.model.Workplace;

/**
 * @author wicaksono
 * 
 */
public class ProductionFacility extends Facility {

	private static final long serialVersionUID = 3491588888569069872L;

	private String name;

	/*
	 * Energy sources that are used for inputs, for example from utility
	 * company, photovoltaic, diesel-generator, etc
	 */
	private List<EnergyConsumption> inputs;

	/* corresponding workplace from wertpronet */
	private Workplace workplace;
	private EnergyForm energyForm;

	public ProductionFacility() {
		super();
	}

	public ProductionFacility(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EnergyConsumption> getInputs() {
		return inputs;
	}

	public void setInputs(List<EnergyConsumption> inputs) {
		this.inputs = inputs;
	}

	public Workplace getWorkplace() {
		return workplace;
	}

	public void setWorkplace(Workplace workplace) {
		this.workplace = workplace;
	}

	public EnergyForm getEnergyForm() {
		return energyForm;
	}

	public void setEnergyForm(EnergyForm energyForm) {
		this.energyForm = energyForm;
	}

}

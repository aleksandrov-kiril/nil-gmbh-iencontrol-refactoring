package de.fzi.wenpro.core.energy.model.facility;

import java.util.ArrayList;
import java.util.List;

import de.fzi.wenpro.core.energy.model.EnergyConsumption;

/**
 * @author uryr
 * 
 */
public abstract class EnergySourceFacility extends Facility {

	private static final long serialVersionUID = 3728347843817540530L;

	protected int id;
	protected String name;

	protected List<EnergyConsumption> outputs;

	public int getId() {
		return id;
	}

	public EnergySourceFacility() {
		super();
		outputs = new ArrayList<EnergyConsumption>();
	}

	public EnergySourceFacility(String name) {
		this();
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		String oldName = this.name;
		this.name = name;
		getListeners()
				.firePropertyChange(Facility.PROPERTY_NAME, oldName, name);
	}

	public List<EnergyConsumption> getOutputs() {
		return outputs;
	}

	public abstract double getEnergyPrice(EnergyConsumption ec);

	// Name �ndern!!!
	public abstract double getPeakPrice(EnergyConsumption ec);
}

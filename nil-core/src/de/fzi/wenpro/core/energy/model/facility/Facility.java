/**
 * 
 */
package de.fzi.wenpro.core.energy.model.facility;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.fzi.wenpro.core.energy.model.EnergyConsumption;

/**
 * @author uryr
 * 
 */
public abstract class Facility implements Serializable {

	private static final long serialVersionUID = -7380547142458466129L;

	public static final String PROPERTY_NAME = "facilityName";
	public static final String PROPERTY_OPERATING_DURATION = "operatingDuration";
	public static final String PROPERTY_MEASUREMENT_TIME = "measurementTime";

	protected List<EnergyConsumption> energyInputs;
	protected double operatingDuration;
	protected double measurementTime;

	protected PropertyChangeSupport listeners;

	public Facility() {
		listeners = new PropertyChangeSupport(this);
		energyInputs = new ArrayList<EnergyConsumption>();
		operatingDuration = 1;
	}

	public double getMeasurementTime() {
		return measurementTime;
	}

	public void setMeasurementTime(double measurementTime) {
		this.measurementTime = measurementTime;
	}

	public double getOperatingDuration() {
		return operatingDuration;
	}

	public void setOperatingDuration(double operatingDuration) {
		this.operatingDuration = operatingDuration;
	}

	public List<EnergyConsumption> getEnergyInputs() {
		return energyInputs;
	}

	public void setEnergyInputs(List<EnergyConsumption> energyInputs) {
		this.energyInputs = energyInputs;
	}

	public double getTotalCost() {
		return getGenerationCostHouer() + getPeakLoadCostHouer();
	}

	public double getGenerationCostHouer() {
		double energyCost = 0.0;
		for (EnergyConsumption ec : energyInputs)
			energyCost += ec.getEnegryCostProHoure();
		return energyCost;
	}

	public double getPeakLoadCost() {
		double peakLoadCost = 0.0;
		for (EnergyConsumption ec : energyInputs)
			peakLoadCost += ec.getPeakCost();
		return peakLoadCost;
	}

	public double getPeakLoadCostHouer() {
		return getPeakLoadCost() / getOperatingDuration();
	}

	public PropertyChangeSupport getListeners() {
		return listeners;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		listeners.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		listeners.removePropertyChangeListener(listener);
	}
}

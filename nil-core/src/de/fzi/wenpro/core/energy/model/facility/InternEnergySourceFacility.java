/**
 * 
 */
package de.fzi.wenpro.core.energy.model.facility;

import de.fzi.wenpro.core.energy.model.EnergyConsumption;
import de.fzi.wenpro.core.energy.model.EnergyForm;

/**
 * @author uryr
 * 
 */
public class InternEnergySourceFacility extends EnergySourceFacility {

	private static final long serialVersionUID = 8880641035534341887L;

	public InternEnergySourceFacility() {
		super("Intern_Network");
	}

	private double getInputPeak(EnergyForm ef) {
		double inPeaksum = 0.0;
		for (EnergyConsumption ec : energyInputs)
			if (ec.getEnergyForm() == ef)
				inPeaksum += ec.getPeakLoadValue();
		return inPeaksum;
	}

	// private double getOutputPeak(EnergyForm ef) {
	// double outPeaksum = 0.0;
	// for (EnergyConsumption ec : outputs)
	// if (ec.getEnergyForm() == ef)
	// outPeaksum += ec.getPeakLoadValue();
	// return outPeaksum;
	// }

	private double getInputPeakCost(EnergyForm ef) {
		double inPeakCostsum = 0.0;
		for (EnergyConsumption ec : energyInputs)
			if (ec.getEnergyForm() == ef)
				inPeakCostsum += ec.getPeakCost();
		return inPeakCostsum;
	}

	private double getInputEnergyHouer(EnergyForm ef) {
		double inEnergySum = 0.0;
		for (EnergyConsumption ec : energyInputs)
			if (ec.getEnergyForm() == ef)
				inEnergySum += ec.getEnertyProHouer();
		return inEnergySum;
	}

	private double getInputEnergyHouerCost(EnergyForm ef) {
		double inEnergyCostsum = 0.0;
		for (EnergyConsumption ec : energyInputs)
			if (ec.getEnergyForm() == ef)
				inEnergyCostsum += ec.getEnegryCostProHoure();
		return inEnergyCostsum;
	}

	@Override
	public double getEnergyPrice(EnergyConsumption ec) {
		EnergyForm ef = ec.getEnergyForm();
		return getInputEnergyHouerCost(ef) / getInputEnergyHouer(ef);
	}

	@Override
	public double getPeakPrice(EnergyConsumption ec) {
		EnergyForm ef = ec.getEnergyForm();
		return (getInputPeakCost(ef) / getInputPeak(ef));
	}

}

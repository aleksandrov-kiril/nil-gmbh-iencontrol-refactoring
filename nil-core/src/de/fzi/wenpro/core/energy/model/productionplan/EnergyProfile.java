package de.fzi.wenpro.core.energy.model.productionplan;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

/**
 * 
 * @author ev_prohl
 *
 */

public class EnergyProfile implements Iterable<Entry<Double, Double>>, Cloneable, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1323960967636158276L;
	
	private TreeMap<Double, Double> profile;
	private double maximum;
	
	public EnergyProfile(){
		this.profile = new TreeMap<Double, Double>();
		this.maximum = 0.0;
	}
	
	public EnergyProfile(TreeMap<Double, Double> profile, double maximum){
		this.profile = profile;
		this.maximum = maximum;
	}

	@Override
	public Iterator<Entry<Double, Double>> iterator() {
		return profile.entrySet().iterator();
	}
	
	/**
	 * Adds linear entry onto energy profile
	 * @param time: at what time the new entry should be added
	 * @param duration: how long the new entry should last
	 * @param energyLevel: how much energy consumption should be added, due to the new entry
	 */
	public void addEntry(double time, double duration, double energyLevel){
		
		if (!profile.containsKey(time)){
			Entry<Double, Double> entry = profile.floorEntry(time);			
			profile.put(time, entry==null?0.0:entry.getValue());
		}
		
		if (!profile.containsKey(time+duration)){
			double level = profile.floorEntry(time+duration).getValue();
			profile.put(time+duration, level);
		}
		
		for(Entry<Double,Double> e : profile.subMap(time, time + duration).entrySet()){
			double newLevel = e.getValue() + energyLevel;
			e.setValue(newLevel);
			if (newLevel > maximum){
				maximum = newLevel;
			}
		}
	}
	
	/**
	 * Adds an Energy Profile to this Energy Profile at a given point in time
	 * @param profile: the Energy Profile that should be added
	 * @param time: point in time where to add the profile
	 */
	public void mergeAtTime(EnergyProfile p, double time){
		Entry<Double, Double> entry = p.firstEntry();
		if (!(entry==null)){
			while (!(p.higherKey(entry.getKey()) == null)){
				double t = entry.getKey() + time;
				double duration = p.higherKey(entry.getKey()) - entry.getKey();
				double energyLevel = entry.getValue();
				
				this.addEntry(t, duration, energyLevel);
				
				entry = p.higherEntry(entry.getKey());
			}
		}
	}
	
	/**
	 *  Returns the resulting maximum EnergyLevel of this Profile, if the 
	 *  EnergyProfile p would be added at time t.
	 *  The method does not evoke any changes in the given Profiles. 
	 * @param p EnergyProfile to be added
	 * @return Maximum Energy Level that would result from merging
	 */
	public double tryMergeAtTime(EnergyProfile p, double time){
		EnergyProfile clone = (EnergyProfile) this.clone();
		clone.mergeAtTime(p, time);
		return clone.getMaximum();
	}
	
	/**
	 * Calculates the smallest increment (starting at the given time) between two entries
	 * in the EnergyProfile. {@code null} if no next step exists in the EnergyProfile.
	 * @param time time at which profile would be added
	 * @return time inbetween the two closest entries
	 */
	public Double getSmallestTimeStep(double time){
		Double result = null;
		if(!(profile.ceilingKey(time)==null)){
			if(profile.containsKey(time)){
				if (!(profile.higherKey(time)==null)){
					result = profile.higherKey(time) - time;
				}
			}else{
				result = profile.ceilingKey(time) - time;
			}
		}
		
		/*if (!(profile.lastKey()==null)){
			Set<Double> possibleResults = new HashSet<Double>();
			SortedMap<Double, Double> subMap = profile.subMap(profile.ceilingKey(time),
																!profile.containsKey(time),
																profile.lastKey(),
																false);
			if (!(subMap.size()==0)){
				possibleResults.add(subMap.firstKey() - time);
				for(Double t : subMap.keySet()){
					possibleResults.add(profile.higherKey(t) - t);
				}
				result = Collections.min(possibleResults);
			}
		}*/
		return result;
	}
	
	public double getMaximum() {
		return maximum;
	}

	public Double higherKey(Double key){
		return profile.higherKey(key);
	}
	
	public Entry<Double, Double> higherEntry(Double key) {
		return profile.higherEntry(key);
	}
	
	public Entry<Double, Double> lastEntry(){
		return profile.lastEntry();
	}
	
	public Entry<Double, Double> firstEntry(){
		return profile.firstEntry();
	}
	
	public Set<Double> keySet(){
		return profile.keySet();
	}
	
	public String toString(){
		return profile.toString();
	}

	public void clearProfile() {
		profile.clear();
	}
	
	public TreeMap<Double, Double> getProfile() {
		return profile;
	}
	
	/**
	 * calculates integral below this {@link EnergyProfile} in kWh
	 * @return value in kWh for this profile
	 */
	public double getKwh(){
		double result = 0;
		
		Entry<Double, Double> entry = profile.firstEntry();
		
		if(entry != null){
			double lastKey = profile.lastKey();
			while(entry.getKey() < lastKey){
				double duration = profile.higherKey(entry.getKey()) - entry.getKey();
				result += duration / 3600 * entry.getValue();
				entry = profile.higherEntry(entry.getKey());
			}
		}
		
		return result;
	}

	/**
	 * Creates a copy of this EnergyProfile with negative values. Can be used to remove 
	 * this EnergyProfile from the total EnergyProfile.
	 * @return EnergyProfile with negative values 
	 */
	public EnergyProfile getInverse() {
		TreeMap<Double, Double> resultMap = new TreeMap<Double, Double>();
		for(Entry<Double, Double> e : profile.entrySet()){
			double v = -e.getValue();
			resultMap.put(e.getKey(), v);
		}
		return new EnergyProfile(resultMap, 0);
	}
	
	/**
	 * calculates average load of the EnergyProfile. Disregards periods, where the
	 * no Energy is consumed.
	 * @return
	 */
	public double getAverage(){
		double kwH = 0;
		double hrs = 0;
		Entry<Double, Double> entry = profile.firstEntry();
		
		if(entry != null){
			double lastKey = profile.lastKey();
			while(entry.getKey() < lastKey){
				if(entry.getValue() != 0){
					double duration = (profile.higherKey(entry.getKey()) - entry.getKey()) / 3600;
					hrs  += duration;
					kwH += duration  * entry.getValue();
				}
				entry = profile.higherEntry(entry.getKey());
			}
		}
		return kwH / hrs;
	}
	
	public Object clone(){
		TreeMap<Double, Double> clone = (TreeMap<Double, Double>) profile.clone();
		EnergyProfile result = new EnergyProfile(clone, maximum);
		return result;
	}
}

/**
 * 
 */
package de.fzi.wenpro.core.energy.model.facility;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wicaksono
 * 
 */
public class AncillaryFacility extends Facility {
	
	private static final long serialVersionUID = 1372330049308945441L;

	public static final String PROPERTY_PERCENTAGE = "propertyPercentage";

	private int id;
	private String name;
	private Map<Facility, Double> outputs;

	public AncillaryFacility() {
		super();
		outputs = new HashMap<Facility, Double>();
	}

	public AncillaryFacility(String name) {
		this();
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		String oldName = this.name;
		this.name = name;
		getListeners()
				.firePropertyChange(Facility.PROPERTY_NAME, oldName, name);
	}

	public Map<Facility, Double> getOutputs() {
		return outputs;
	}

	public void setOutputs(Map<Facility, Double> outputs) {
		this.outputs = outputs;
	}

}

/**
 * 
 */
package de.fzi.wenpro.core.energy.model;

import java.io.Serializable;


/**
 * @author wicaksono
 * 
 */
public class EnergySource implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -116032288754787596L;
	private int id;
	private String name;
	private EnergyForm energyForm;

	public int getId() {
		return id;
	}

	public EnergySource() {
		super();
	}

	public EnergySource(String name) {
		this();
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EnergyForm getEnergyForm() {
		return energyForm;
	}

	public void setEnergyForm(EnergyForm energyForm) {
		this.energyForm = energyForm;
	}

	protected double getEnergyBalance() {
		return 0;
	}

}

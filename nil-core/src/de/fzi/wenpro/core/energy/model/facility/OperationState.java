package de.fzi.wenpro.core.energy.model.facility;

import java.io.Serializable;
import java.util.Comparator;

import de.fzi.wenpro.core.model.Operation;

public class OperationState implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4932064342466913370L;
	/**
	 * power in kW
	 */
	private double energyLevel;
	/**
	 * duration of operation state in second(s)
	 */
	private double duration;
	private String name;
	/**
	 * determines in which order states will be aborted in case of a too short scheduling entry
	 */
	private int priority;
	/**
	 * determines in which order states will be executed during a scheduling entry
	 */
	private int ordinal;
	/**
	 *  determines if the {@link Operation} is producing output during this {@link OperationState}
	 */
	private boolean producing;
	
	public OperationState(String name, double energyLevel, double duration, 
			int priority, int ordinal) {
		this.energyLevel = energyLevel;
		this.duration = duration;
		this.name = name;
		this.priority = priority;
		this.ordinal = ordinal;
		this.producing = true;
	}
	
	public OperationState(String name, double energyLevel, double duration, 
			int priority, int ordinal, boolean producing) {
		this(name, energyLevel, duration, priority, ordinal);
		this.producing = producing;
	}

	static class OperationStatePriorityComparator implements Comparator<OperationState>{

		@Override
		public int compare(OperationState state1, OperationState state2) {
			int priority1 = state1.getPriority();
			int priority2 = state2.getPriority();
			
			if(priority1 > priority2)
				return 1;
			else if(priority1 < priority2)
				return -1;
			else
				return 0;
		}
	}
	
	static class OperationStateOrdinalComparator implements Comparator<OperationState>{

		@Override
		public int compare(OperationState state1, OperationState state2) {
			int ordinal1 = state1.getOrdinal();
			int ordinal2 = state2.getOrdinal();
			
			if(ordinal1 > ordinal2)
				return 1;
			else if(ordinal1 < ordinal2)
				return -1;
			else
				return 0;
		}
	}

	public double getEnergyLevel() {
		return energyLevel;
	}

	public int getOrdinal() {
		return ordinal;
	}

	public double getDuration() {
		return duration;
	}

	public String getName() {
		return name;
	}

	public int getPriority() {
		return priority;
	}

	public boolean isProducing() {
		return producing;
	}

	@Override
	public String toString() {
		return "OperationState [name=" + name + ", energyLevel=" + energyLevel + ", duration="
				+ duration + "]";
	}
	
}

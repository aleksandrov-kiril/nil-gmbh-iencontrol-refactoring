/**
 * 
 */
package de.fzi.wenpro.core.energy.model.facility;

import de.fzi.wenpro.core.energy.model.EnergyConsumption;

/**
 * @author wicaksono
 * @author uryr
 * 
 */
public class ConversionFacility extends EnergySourceFacility {

	private static final long serialVersionUID = -3685859419308328989L;
	public static final String PROPERTY_GENERATION = "conversionGeneration";
	public static final String PROPERTY_PEAKLOAD = "conversionPeakload";

	private double generationHouer;
	private double generationValue;
	private double peakLoadValue;

	public ConversionFacility() {
	}

	public ConversionFacility(String name) {
		this();
		this.name = name;
	}

	public double getGenerationValue() {
		return generationValue;
	}

	public double getPeakLoadValue() {
		peakLoadValue = 0.0;
		for (EnergyConsumption ec : outputs)
			peakLoadValue += ec.getPeakLoadValue();
		return peakLoadValue;
	}

	public double getGenerationValueHouer() {
		generationHouer = 0.0;
		for (EnergyConsumption ec : outputs)
			generationHouer += (ec.getConsumptionValue() / ec
					.getMeasurementTime());
		return generationHouer;
	}

	@Override
	public double getEnergyPrice(EnergyConsumption ec) {
		return (((ec.getConsumptionValue() / ec.getMeasurementTime()) / this
				.getGenerationValueHouer()) * this.getGenerationCostHouer())
				/ (ec.getConsumptionValue() / ec.getMeasurementTime());
	}

	@Override
	public double getPeakPrice(EnergyConsumption ec) {
		return ((ec.getPeakLoadValue() / this.getPeakLoadValue()) * this
				.getPeakLoadCost()) / ec.getPeakLoadValue();
	}

}

/**
 * 
 */
package de.fzi.wenpro.core.energy.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wicaksono
 * 
 */
public class FactoryEnergyInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5246927052148482852L;

	private List<EnergyUnitCost> energyUnitCost;

	/* periode for calculating energy in hour */
	private double calculationPeriode;

	public FactoryEnergyInformation() {
		energyUnitCost = new ArrayList<EnergyUnitCost>();
	}

	public double getPeakCost(EnergyForm ef) {
		for (EnergyUnitCost euc : energyUnitCost)
			if (euc.getEnergyForm() == ef)
				return euc.getDemandRate();
		return 0.0;
	}

	public double getConsumptionCost(EnergyForm ef) {
		for (EnergyUnitCost euc : energyUnitCost)
			if (euc.getEnergyForm() == ef)
				return euc.getEnergyRate();
		return 0.0;
	}

	public List<EnergyUnitCost> getEnergyUnitCost() {
		return energyUnitCost;
	}

	public double getCalculationPeriode() {
		return calculationPeriode;
	}

	public void setCalculationPeriode(double calculationPeriode) {
		this.calculationPeriode = calculationPeriode;
	}

	/**
	 * Adds {@link EnergyUnitCost} to this {@link FactoryEnergyInformation}. If the FactoryEnergyInformation already 
	 * contains an {@link EnergyUnitCost} with the same {@link EnergyForm} the preexisting EnergyUnitCost is removed
	 * from this FactoryEnergyInformation, before the new one is added.
	 * @param euc The new EnergyUnitCost
	 */
	public void addEnergyUnitCost(EnergyUnitCost euc){
		EnergyForm ef = euc.getEnergyForm();
		for (EnergyUnitCost euCost : energyUnitCost)
			if (euCost.getEnergyForm() == ef)
				energyUnitCost.remove(euCost);
		energyUnitCost.add(euc);
	}
}

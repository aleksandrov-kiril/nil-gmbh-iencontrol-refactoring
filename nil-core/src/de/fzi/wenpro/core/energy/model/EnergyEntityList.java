package de.fzi.wenpro.core.energy.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import de.fzi.wenpro.core.energy.model.facility.AncillaryFacility;
import de.fzi.wenpro.core.energy.model.facility.ConversionFacility;
import de.fzi.wenpro.core.energy.model.facility.ProductionFacility;
import de.fzi.wenpro.core.energy.model.facility.TransportFacility;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.Workplace;

public class EnergyEntityList implements Observer {

	private static EnergyEntityList singleton = new EnergyEntityList();
	private static Map<FactoryNode, List<AncillaryFacility>> ancillaryFacilityListCache = new HashMap<FactoryNode, List<AncillaryFacility>>();
	private static Map<FactoryNode, List<TransportFacility>> transportFacilityListCache = new HashMap<FactoryNode, List<TransportFacility>>();
	private static Map<FactoryNode, List<ProductionFacility>> productionFacilityListCache = new HashMap<FactoryNode, List<ProductionFacility>>();
	private static Map<FactoryNode, List<ConversionFacility>> conversionFacilityListCache = new HashMap<FactoryNode, List<ConversionFacility>>();

	/**
	 * Return a list of all {@link ProductionFacility}s in the given
	 * {@link FactoryNode} recursively
	 * 
	 * @param factoryNode
	 *            a {@link FactoryNode}
	 * @return a list of {@link ProductionFacility}s
	 * @param factoryNode
	 * @return
	 */
	public static List<ProductionFacility> getProductionFacilityList(
			FactoryNode factoryNode) {
		List<ProductionFacility> result = productionFacilityListCache
				.get(factoryNode);
		if (result == null) {
			result = new ArrayList<ProductionFacility>();
			if (factoryNode instanceof Workplace) {
				Workplace wp = (Workplace) factoryNode;
				if (wp.getProductionFacility() != null
						&& !result.contains(wp.getProductionFacility()))
					result.add(wp.getProductionFacility());
			} else {
				for (FactoryNode f : factoryNode.getSubNodes()) {
					result.addAll(getProductionFacilityList(f));
				}
			}
			productionFacilityListCache.put(factoryNode, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}

	/**
	 * Return a list of all {@link TransportFacility}s in the given
	 * {@link FactoryNode} recursively
	 * 
	 * @param factoryNode
	 *            a {@link FactoryNode}
	 * @return a list of {@link TransportFacility}s
	 * @param factoryNode
	 * @return
	 */
	public static List<TransportFacility> getTransportFacilityList(
			FactoryNode factoryNode) {
		List<TransportFacility> result = transportFacilityListCache
				.get(factoryNode);
		if (result == null) {
			Set<TransportFacility> resultSet = new HashSet<TransportFacility>();
			if (factoryNode instanceof FactoryNode) {
				FactoryNode wp = (FactoryNode) factoryNode;
				if (wp.getTransportFacilities() != null)
					resultSet.addAll(wp.getTransportFacilities());
			}
			for (FactoryNode f : factoryNode.getSubNodes()) {
				resultSet.addAll(getTransportFacilityList(f));
			}

			result = new ArrayList<TransportFacility>(resultSet);
			transportFacilityListCache.put(factoryNode, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}

	/**
	 * Return a list of all {@link AncillaryFacility}s in the given
	 * {@link FactoryNode} recursively
	 * 
	 * @param factoryNode
	 *            a {@link FactoryNode}
	 * @return a list of {@link AncillaryFacility}s
	 * @param factoryNode
	 * @return
	 */
	public static List<AncillaryFacility> getAncillaryFacilityList(
			FactoryNode factoryNode) {
		List<AncillaryFacility> result = ancillaryFacilityListCache
				.get(factoryNode);
		if (result == null) {
			Set<AncillaryFacility> resultSet = new HashSet<AncillaryFacility>();
			if (factoryNode instanceof FactoryNode) {
				FactoryNode fn = (FactoryNode) factoryNode;
				if (fn.getAncillaryFacilities() != null)
					resultSet.addAll(fn.getAncillaryFacilities());
			}
			for (FactoryNode f : factoryNode.getSubNodes()) {
				resultSet.addAll(getAncillaryFacilityList(f));
			}

			result = new ArrayList<AncillaryFacility>(resultSet);
			ancillaryFacilityListCache.put(factoryNode, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}

	/**
	 * Return a list of all {@link ConversionFacility}s in the given
	 * {@link FactoryNode} recursively
	 * 
	 * @param factoryNode
	 *            a {@link FactoryNode}
	 * @return a list of {@link ConversionFacility}s
	 * @param factoryNode
	 * @return
	 */
	public static List<ConversionFacility> getConversionFacilityList(
			FactoryNode factoryNode) {
		List<ConversionFacility> result = conversionFacilityListCache
				.get(factoryNode);
		if (result == null) {
			Set<ConversionFacility> resultSet = new HashSet<ConversionFacility>();
			if (factoryNode instanceof FactoryNode) {
				FactoryNode fn = (FactoryNode) factoryNode;
				if (fn.getConversionFacilities() != null)
					resultSet.addAll(fn.getConversionFacilities());
			}
			for (FactoryNode f : factoryNode.getSubNodes()) {
				resultSet.addAll(getConversionFacilityList(f));
			}

			result = new ArrayList<ConversionFacility>(resultSet);
			conversionFacilityListCache.put(factoryNode, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}

	@Override
	public void update(Observable o, Object arg1) {
		// TODO:
		o.deleteObserver(this);
	}
}

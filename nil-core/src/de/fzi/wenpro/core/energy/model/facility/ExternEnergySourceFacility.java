/**
 * 
 */
package de.fzi.wenpro.core.energy.model.facility;

import de.fzi.wenpro.core.energy.model.EnergyConsumption;
import de.fzi.wenpro.core.energy.model.FactoryEnergyInformation;

/**
 * @author uryr
 * 
 */
public class ExternEnergySourceFacility extends EnergySourceFacility {

	private static final long serialVersionUID = -7412136705999725523L;

	private FactoryEnergyInformation energyInfo;

	public ExternEnergySourceFacility(FactoryEnergyInformation energyInfo) {
		super("Extern_Source");
		this.energyInfo = energyInfo;
	}

	public FactoryEnergyInformation getEnergyInformation() {
		return energyInfo;
	}

	@Override
	public double getEnergyPrice(EnergyConsumption ec) {
		return energyInfo.getConsumptionCost(ec.getEnergyForm());
	}

	@Override
	public double getPeakPrice(EnergyConsumption ec) {
		return energyInfo.getPeakCost(ec.getEnergyForm());
	}

}

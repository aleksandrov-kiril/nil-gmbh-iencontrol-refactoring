/**
 * 
 */
package de.fzi.wenpro.core.energy.model.facility;

/**
 * @author wicaksono
 * 
 */
public class TransportFacility extends Facility {

	private static final long serialVersionUID = -2506918073014696537L;

	private int id;
	private String name;

	private ProductionFacility origin;
	private ProductionFacility target;

	public TransportFacility() {
		super();
	}

	public TransportFacility(String name) {
		this();
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		String oldName = this.name;
		this.name = name;
		getListeners()
				.firePropertyChange(Facility.PROPERTY_NAME, oldName, name);
	}

	public ProductionFacility getOrigin() {
		return origin;
	}

	public void setOrigin(ProductionFacility origin) {
		this.origin = origin;
	}

	public ProductionFacility getTarget() {
		return target;
	}

	public void setTarget(ProductionFacility target) {
		this.target = target;
	}

}

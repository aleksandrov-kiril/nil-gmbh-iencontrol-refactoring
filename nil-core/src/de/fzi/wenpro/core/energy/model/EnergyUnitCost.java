/**
 * 
 */
package de.fzi.wenpro.core.energy.model;

import java.io.Serializable;

/**
 * @author wicaksono
 * 
 */
public class EnergyUnitCost implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 162404672677888871L;

	private EnergyForm energyForm;
	// cost per kWh
	private double energyRate;
	// cost per kwP
	private double demandRate;
	private double certificateCost;

	public EnergyUnitCost(EnergyForm energyForm) {
		this.energyForm = energyForm;
	}

	public EnergyForm getEnergyForm() {
		return energyForm;
	}

	public void setEnergyForm(EnergyForm energyForm) {
		this.energyForm = energyForm;
	}

	public double getEnergyRate() {
		return energyRate;
	}

	public void setEnergyRate(double energyRate) {
		this.energyRate = energyRate;
	}

	public double getDemandRate() {
		return demandRate;
	}

	public void setDemandRate(double demandRate) {
		this.demandRate = demandRate;
	}

	public double getCertificateCost() {
		return certificateCost;
	}

	public void setCertificateCost(double certificateCost) {
		this.certificateCost = certificateCost;
	}

}

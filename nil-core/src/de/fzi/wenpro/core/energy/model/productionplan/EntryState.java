package de.fzi.wenpro.core.energy.model.productionplan;

import java.io.Serializable;
import java.util.Comparator;

import de.fzi.wenpro.core.energy.model.facility.OperationState;

public class EntryState implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6331617017311228919L;
	private OperationState operationState;
	/**
	 * Starting time measured relatively to the beginning of the {@link EnergyProductionPlanEntry}
	 * not to the beginning of the {@link EnergyProductionPlan}  
	 */
	
	private double duration;
	private double startingTime;
	
	
	
	public EntryState(OperationState state,double startingTime, double duration){
		this.operationState = state;
		this.startingTime = startingTime;
		this.duration = duration;
	}
	
	public static class EntryStatePriorityComparator implements Comparator<EntryState>{

		@Override
		public int compare(EntryState state1, EntryState state2) {
			int priority1 = state1.getPriority();
			int priority2 = state2.getPriority();
			
			if(priority1 > priority2)
				return 1;
			else if(priority1 < priority2)
				return -1;
			else
				return 0;
		}
	}
	
	public static class EntryStateOrdinalComparator implements Comparator<EntryState>{

		@Override
		public int compare(EntryState state1, EntryState state2) {
			int ordinal1 = state1.getOrdinal();
			int ordinal2 = state2.getOrdinal();
			
			if(ordinal1 > ordinal2)
				return 1;
			else if(ordinal1 < ordinal2)
				return -1;
			else
				return 0;
		}
	}
	
	
	public static class EntryStateStartingTimeComparator implements Comparator<EntryState>{

		@Override
		public int compare(EntryState state1, EntryState state2) {
			double startingTime1 = state1.getStartingTime();
			double startingTime2 = state2.getStartingTime();
			
			if(startingTime1 >= startingTime2)
				return 1;
			else if(startingTime1 < startingTime2)
				return -1;
			else
				return 0;
		}
	}
	
	public double getDuration() {
		return duration;
	}

	public int getOrdinal() {
		return operationState.getOrdinal();
	}

	public int getPriority() {
		return operationState.getPriority();
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public double getEnergyLevel() {
		return operationState.getEnergyLevel();
	}

	public String getName() {
		return operationState.getName();
	}

	public double getStartingTime() {
		return startingTime;
	}
	
	public boolean isProducing() {
		return operationState.isProducing();
	}

	public void setStartingTime(double startingTime) {
		this.startingTime = startingTime;
	}

	@Override
	public String toString() {
		return "EntryState [" + operationState.getName() + " " +
						duration + "]";
	}
	
	
}

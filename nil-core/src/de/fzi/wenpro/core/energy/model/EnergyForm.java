/**
 * 
 */
package de.fzi.wenpro.core.energy.model;

/**
 * @author wicaksono
 *
 */
public enum EnergyForm {
	ELECTRICITY,
	GAS,
	HEAT,
	STEAM,
	COMPRESSED_AIR,
	FUEL_OIL,
	WOOD_PELLET,
	DIESEL
}

/**
 * 
 */
package de.fzi.wenpro.core.energy.model.facility;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import de.fzi.wenpro.core.energy.model.productionplan.EntryState;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanWarning;
import de.fzi.wenpro.util.Tuple;

/**
 * @author wicaksono
 *
 */
public class EnergyConsumingOperation extends InternalOperation{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7009030332809453429L;
	
	private Set<OperationState> states;
		
	public EnergyConsumingOperation(Set<OperationState> states) {
		super();
		this.states = states;
	}

	public EnergyConsumingOperation(WorkplaceState state1, Set<OperationState> states, double time, int lotSize, double scrapRate) {
		super(state1, time, lotSize, scrapRate);
		this.states = states;
	}
	
	
	public EnergyConsumingOperation(WorkplaceState state1, Set<OperationState> states, double time, int lotSize, double scrapRate, double mmf, double componentFactor) {
		super(state1, time, lotSize, scrapRate, mmf, componentFactor);
		this.states = states;
	}
	
	
	public Tuple<Set<EntryState>, Boolean> createEntryStateProfile(double duration, double retoolingDuration){
		
		Set<EntryState> entryStates = new HashSet<EntryState>();
		boolean energyProfileViolated = false;
		
		TreeSet<OperationState> ordinalStateSet = new TreeSet<OperationState>(new OperationState.OperationStateOrdinalComparator());
		ordinalStateSet.addAll(states);
		TreeSet<OperationState> priorityStateSet = new TreeSet<OperationState>(new OperationState.OperationStatePriorityComparator());
		priorityStateSet.addAll(states);
		
		Map<OperationState, Double> durations = new HashMap<OperationState, Double>();
		
		double workingDuration = duration;
		// check if retooling (if necessary) can be executed within given duration, and add to EnergyProfile
		if (workingDuration >= retoolingDuration){
			workingDuration -= retoolingDuration;
		} else {
			retoolingDuration = workingDuration;
			workingDuration = 0.0;
			energyProfileViolated = true;
		}

		// calculate durations for all states except last
		for (OperationState state : priorityStateSet.subSet(priorityStateSet.first(), priorityStateSet.last())){
			if (workingDuration >= state.getDuration()){
				durations.put(state, state.getDuration());
				workingDuration -= state.getDuration();
			} else {
				durations.put(state, workingDuration);
				workingDuration = 0.0;
				energyProfileViolated = true;
			}
		}
		//round working duration to millisec
		workingDuration = Math.round(workingDuration * 1000.0)/1000.0;
		//assign remaining working Duration to the state with least priority
		durations.put(priorityStateSet.last(), workingDuration);
		//add states to EnergyProfile according to their calculated durations
		double stateStartingTime = retoolingDuration;
		for (OperationState state : ordinalStateSet){
			entryStates.add(new EntryState(state, stateStartingTime, durations.get(state)));
			stateStartingTime += durations.get(state);
		}
				
		Tuple<Set<EntryState>,Boolean> result = new Tuple<Set<EntryState>, Boolean>(entryStates, energyProfileViolated);
		
		return result;
	}
	
	/**
	 * Due to producing and non-producing {@link OperationState}s the duration of a {@link ProductionPlanEntry}
	 * is not  linked to the number of executions of the operation. This method derives this
	 * duration, when given a required number of operations 
	 * @param operationCount how many times should the operation be executed
	 * @return time in sec required to execute the given number of Operations or the minimal 
	 * 			duration to not create a ENERGY_PROFILE_VIOLATED {@link ProductionPlanWarning}
	 * 			(whichever value is greater)
	 */
	public double calculateDuration(int operationCount){
		double result = 0.0;
		double producingDuration = 0.0;
		TreeSet<OperationState> priorityStateSet = new TreeSet<OperationState>(new OperationState.OperationStatePriorityComparator());
		priorityStateSet.addAll(states);
		priorityStateSet.remove(priorityStateSet.last());
		for(OperationState state : priorityStateSet){
			result += state.getDuration();
			if(state.isProducing()){
				producingDuration += state.getDuration();
			}
		}
		double remainingDuration = operationCount * getTime() - producingDuration;
		result += (remainingDuration > 0)? remainingDuration : 0.0; 
		return result;
	}
}

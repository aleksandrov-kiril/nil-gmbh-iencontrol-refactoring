/**
 * ecoFLEXiP 2010
 * de.fzi.ecoflex.core.model.operations.ProductionTime.java
 */
package de.fzi.wenpro.core.calculations;

import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;


/**
 * @author Zerael
 *
 */
public class ProductionTime {

	
	//////////////////////////////////////////////////////////////////////////
	////////////////////// Matrices and vectors /////////////////////////////
	
	/**
	 * A matrix containing the production times for all products. Multiply it
	 * to a vector of piece numbers (indexed by produces) to get the active
	 * time for all workplaces. 
	 * @return The time matrix.
	 */

	public static double[][] getTimeMatrix(FactoryNode factoryNode){
		double[][] result = new double[EntityList.countWorkplace(factoryNode)][EntityList.countOperations(factoryNode)];		
		int i=0, j=0;
		for(Workplace w : EntityList.getWorkplaceList(factoryNode)){
			for (WorkplaceState state : w.getStates()) {
				for (Operation op: state.getOperations()) {
					result[i][j] = op.getTime();
					j++;
				}
			}
			i++;
		}
		/* time values of purchased products are zero */
		return result;
	}
	
	
	public static  double[] getTimeLimitVector(FactoryNode factoryNode, ShiftSchedule shiftplan, boolean theoretical){
		double[] result = new double[EntityList.countWorkplace(factoryNode)];
		int i=0;
		for (Workplace w : EntityList.getWorkplaceList(factoryNode)){
			if (theoretical){
				result[i] = w.getMaximalProductionTimeTh(shiftplan);
			}else{
				result[i] = w.getMaximalProductionTime(shiftplan);
			}
			i++;
		}
		/* time limit values of purchased products are zero */
		return result;
	}
	
	/**
	 * The production time of each Operation object
	 * @return all times as an array
	 */
	public static double[] getProductionTimes(FactoryNode factoryNode) {
		double[] result = new double[EntityList.countOperations(factoryNode)];
		int i = 0;
		for (Operation op : EntityList.getOperationList(factoryNode)){
			result[i++] = op.getTime();
		}
		return result;
	}
	
}

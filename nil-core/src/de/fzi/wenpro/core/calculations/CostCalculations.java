/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.model.Cost;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.HashMap2d;

/**
 * @author Jan Siebel, Hendro Wicaksono
 *
 */
public class CostCalculations implements Observer{

	private static CostCalculations singleton = new CostCalculations();
	
	private static HashMap2d<FactoryNode, ShiftSchedule, Double> constantCostCache = new HashMap2d<FactoryNode, ShiftSchedule, Double>();
	private static HashMap2d<FactoryNode, ShiftSchedule, double[]> variableCostsCache = new HashMap2d<FactoryNode, ShiftSchedule, double[]>();
	private static HashMap2d<FactoryNode, ShiftSchedule, double[]> profitCache = new HashMap2d<FactoryNode, ShiftSchedule, double[]>();
	private static HashMap2d<FactoryNode, ShiftSchedule, double[]> theoreticalProfitCache = new HashMap2d<FactoryNode, ShiftSchedule, double[]>();
	private static HashMap2d<Factory, Product, Double> cumulatedProductCost = new HashMap2d<Factory, Product, Double>();
	private static HashMap2d<Factory, ShiftSchedule, Double> variableCostsPerOperationCache = new HashMap2d<Factory, ShiftSchedule, Double>();
	private static HashMap2d<Factory, ShiftSchedule, Double> variableCostsPerSecondCache = new HashMap2d<Factory, ShiftSchedule, Double>();
	
	public static double getConstantCost(FactoryNode factoryNode, ShiftSchedule shiftplan){
		Double result = constantCostCache.get(factoryNode, shiftplan);
		
		if (result == null){
			result = 0.0;
			for (Cost c: factoryNode.getCosts()){
				if(c.isPerPeriod()){
					result += c.getValue(shiftplan);
				}
			}
			if (factoryNode instanceof Workplace){
				result *= ((Workplace)factoryNode).getMultiplicity();
			}else{
				for (FactoryNode f : factoryNode.getSubNodes()){
					result += getConstantCost(f, shiftplan);
				}
			}
			constantCostCache.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		
		return result;
	}

	public static double[] getVariableCosts(FactoryNode factoryNode, ShiftSchedule shiftplan){
		double[] result = variableCostsCache.get(factoryNode, shiftplan);
		if (result == null){
			if (factoryNode instanceof Factory){
				result = calculateVariableCosts((Factory)factoryNode, shiftplan);
			}else if (factoryNode instanceof Workplace){
				result = calculateVariableCosts((Workplace)factoryNode, shiftplan);
			}else{
				result = calculateVariableCosts(factoryNode, shiftplan);
			}
			variableCostsCache.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}

	public static double[] getProfitByProduces(FactoryNode factoryNode, ShiftSchedule shiftplan){
		double[] result = profitCache.get(factoryNode, shiftplan);
		if (result == null){
			result = new double[EntityList.countOperations(factoryNode)];
			double[] variableCost = getVariableCosts(factoryNode, shiftplan);
			
			int i=0;
			for(Operation op : EntityList.getOperationList(factoryNode)){
				if (!op.isPurchased()) {
					result[i] = ((InternalOperation)op).getProfit(false) - variableCost[i];
					i++;
				}				
			}
			profitCache.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}

	public static double[] getTheoreticalProfitByProduces(FactoryNode factoryNode, ShiftSchedule shiftplan){
		double[] result = theoreticalProfitCache.get(factoryNode, shiftplan);
		if (result == null){
			result = new double[EntityList.countOperations(factoryNode)];
			double[] variableCost = getVariableCosts(factoryNode, shiftplan);

			int i=0;
			for(Operation op : EntityList.getOperationList(factoryNode)){
				if (!op.isPurchased()) {
					result[i] = ((InternalOperation)op).getProfit(true) - variableCost[i];
					i++;
				}
			}
			theoreticalProfitCache.put(factoryNode, shiftplan, result);
		}
		return result;
	}
	
	/**
	 * The total variable costs per second for the given {@link Operation} and
	 * the given {@link ShiftSchedule}. This includes the {@link Cost}s in the
	 * {@link Operation}'s {@link Workplace} and {@link FactoryNode}s.
	 * @param operation an {@link Operation}
	 * @param shiftSchedule a {@link ShiftSchedule}
	 * @return the total costs per second
	 */
	public static double getVariableCostsPerSecond(Operation operation, ShiftSchedule shiftSchedule){
		Double result = variableCostsPerSecondCache.get(operation.getFactory(), operation);
		if (result == null){
			double sum = 0;
			for (Cost c : operation.getCosts()){
				if (c.isPerPeriod()){
					// no variable costs
				}else if (c.isPerPiece()){
					sum += c.getValue(shiftSchedule);
				}else{
					sum += c.getValuePerSecond(shiftSchedule) / operation.getTime();
				}
			}
			FactoryNode f;
			if (operation instanceof InternalOperation){
				f = ((InternalOperation)operation).getOwner().getWorkplace();
			}else{
				f = operation.getFactory();
			}
			do{
				for (Cost c : f.getCosts()){
					if (c.isPerPeriod()){
						// no variable costs
					}else if (c.isPerPiece()){
						sum += c.getValue(shiftSchedule) / operation.getTime();
					}else{
						sum += c.getValuePerSecond(shiftSchedule);
					}
				}
				f = f.getParent();
			}while (f != null);
			result = sum;
		}
		return result;
	}

	
	/**
	 * The total variable costs per execution for the given {@link Operation}
	 * and the given {@link ShiftSchedule}. This includes the {@link Cost}s in
	 * the {@link Operation}'s {@link Workplace} and {@link FactoryNode}s.
	 * @param operation an {@link Operation}
	 * @param shiftSchedule a {@link ShiftSchedule}
	 * @return the total costs per execution of the {@link Operation}
	 */
	public static double getVariableCostsPerOperation(Operation operation, ShiftSchedule shiftSchedule){
		Double result = variableCostsPerOperationCache.get(operation.getFactory(), operation);
		if (result == null){
			double sum = 0;
			for (Cost c : operation.getCosts()){
				if (c.isPerPeriod()){
					// no variable costs
				}else if (c.isPerPiece()){
					sum += c.getValue(shiftSchedule);
				}else{
					sum += c.getValuePerSecond(shiftSchedule) * operation.getTime();
				}
			}
			FactoryNode f;
			if (operation instanceof InternalOperation){
				f = ((InternalOperation)operation).getOwner().getWorkplace();
			}else{
				f = operation.getFactory();
			}
			do{
				for (Cost c : f.getCosts()){
					if (c.isPerPeriod()){
						// no variable costs
					}else if (c.isPerPiece()){
						sum += c.getValue(shiftSchedule);
					}else{
						sum += c.getValuePerSecond(shiftSchedule) * operation.getTime();
					}
				}
				f = f.getParent();
			}while (f != null);
			result = sum;
		}
		return result;
	}
	
	/**
	 * @param factory
	 * @param shiftplan
	 */

	private static double[] calculateVariableCosts(Factory factory, ShiftSchedule shiftplan){
		double[] result = calculateVariableCosts((FactoryNode)factory, shiftplan);

		List<Operation> purchaseOperationList = EntityList.getPurchaseOperationList(factory);
		if (purchaseOperationList.size() > 0){
			int i = EntityList.countOperations(factory) - purchaseOperationList.size();
			for (Operation p : purchaseOperationList){
				result[i++] += getTotalVariableCostPerPiece(p, shiftplan);
			}
		}
		
		return result;
	}

	/**
	 * @param workplace
	 * @param shiftplan
	 */

	private static double[] calculateVariableCosts(Workplace workplace, ShiftSchedule shiftplan){
		double[] result = calculateVariableCosts((FactoryNode)workplace, shiftplan);
		int i=0;
		for (Operation op : EntityList.getOperationList(workplace)){
			result[i++] += getVariableCostPerPiece(op, shiftplan); 
		}
		return result;
	}
	
	/**
	 * @param factoryNode
	 * @param shiftplan
	 */

	private static double[] calculateVariableCosts(FactoryNode factoryNode, ShiftSchedule shiftplan){
		double[] result = new double[EntityList.countOperations(factoryNode)];
		
		double perPiece=0, perSecond=0;
		for (Cost c: factoryNode.getCosts()){
			double value = c.getValue(shiftplan);
			if (c.isPerPiece()){
				perPiece += value;
			}else if (!c.isPerPeriod()){
				perSecond += c.getValuePerSecond(shiftplan);
			}
		}
		int i=0;
		for (Operation op : EntityList.getOperationList(factoryNode)){
			result[i++] = perPiece + perSecond * op.getTime();
		}
		i=0;
		for (FactoryNode f : factoryNode.getSubNodes()){
			for (double d : getVariableCosts(f, shiftplan)){
				result[i++] += d;
			}
		}
		return result;
	}
	
	public static double[] getMeanCostByProduct(FactoryNode factoryNode, ShiftSchedule shiftplan, ProductPlan productPlan){
		List<Operation> operations = EntityList.getOperationList(factoryNode);
		if (!productPlan.isValid()){
			return null;
		}else if (operations.size() == 0){
			return new double[factoryNode.getFactory().getProducts().size()];
		}else{
			double[] variableCosts = getVariableCosts(productPlan.getFactoryNode(), shiftplan);
			int index = 0;
			for (Operation op : EntityList.getOperationList(productPlan.getFactoryNode())){
				if (op.equals(operations.get(0))){
					break;
				}else{
					index++;
				}
			}
			double[] volume = productPlan.getVolume();
			if (index < volume.length){
				int nProducts = factoryNode.getFactory().getProducts().size();
				double[] totalCost = new double[nProducts];
				double[] pieceSum = new double[nProducts];
				double[] count = new double[nProducts];
				double[] costSum = new double[nProducts];
				for (Operation op : operations){
					int piecesPerOperation = op.getOutputPiecesPerOperation();
					for (ProductQuantity pq : op.getOutputs()){
						int pIndex = pq.getProduct().getProductIndex();
						double costPerOperation = variableCosts[index];
						double costPerPiece = costPerOperation * pq.getQuantity() / piecesPerOperation;
						totalCost[pIndex] += volume[index] * costPerPiece;
						pieceSum[pIndex] += volume[index] * pq.getQuantity();
						count[pIndex]++;
						costSum[pIndex] = costPerPiece;
					}
					index++;
				}
				double[] result = new double[nProducts];
				for (int i=0; i<nProducts; i++){
					if (pieceSum[i] > 0){
						result[i] = totalCost[i] / pieceSum[i];
					}else if (count[i] > 0){
						result[i] = costSum[i] / count[i];
					}else{
						result[i] = 0;
					}
				}
				return result;
			}else{
				return null;
			}
		}
	}

	//TODO: check this
	public static double getCumulatedProductCost(Factory factory, Product product){
		Double result = cumulatedProductCost.get(factory, product);
		if (result == null){
			ShiftSchedule shiftplan = factory.getDefaultShiftplan();
			ProductPlan p = RatioCapacity.getTheoreticalNonProfitableRatioCapacity(factory, shiftplan);
			
			if (p.isValid()){
				double volume[] = p.getVolume();
				double producedVolume=0, producedVolumeScrap=0, cost=0;
				
				int i=0;
				for (Operation operation : EntityList.getOperationList(factory)){
					if (volume[i]>0){
						for (ProductQuantity pq : operation.getOutputs()){
							if (pq.getProduct().equals(product)){
								if (operation.isPurchased()){
									cost += volume[i] * product.getPurchasePrice();
								}else{
									cost += volume[i] * getTotalVariableCostPerPiece(operation, shiftplan) * pq.getQuantity();
									producedVolume += volume[i] * pq.getQuantity();
									producedVolumeScrap += volume[i] * pq.getQuantity() * (1-((InternalOperation)operation).getScrapRate());
								}
							}
						}
					}
					i++;
				}
				if (producedVolume > 0){
					for (Operation operation : EntityList.getOperationList(factory)){
						if (operation.getOutputs().contains(product)){
							for (ProductQuantity pq : operation.getInputs()){
								cost += producedVolume * pq.getQuantity() * getCumulatedProductCost(factory, pq.getProduct());
							}
						}
						
					}
				}
				result = cost / producedVolumeScrap;
			}else{
				result = Double.NaN;
			}
			cumulatedProductCost.put(factory, product, result);
			factory.addObserver(singleton);
		}
		return result;
	}
	
	public static double getCumulatedProductCost(Operation operation){
		if (operation.isPurchased()){
			double result = 0;
			for (ProductQuantity pq : operation.getOutputs()){
				result += pq.getQuantity() * pq.getProduct().getPurchasePrice();
			}
			return result;
		}else{
			double result = getTotalVariableCostPerPiece(operation, operation.getFactory().getDefaultShiftplan());

			for (ProductQuantity pq : operation.getOutputs()){
				result += pq.getQuantity() * getCumulatedProductCost(operation.getFactory(), pq.getProduct());
			}
			return result;
		}
	}
	
	/**
	 * @param ratioCapacity
	 * @return
	 */
	public static double[] getPurchasedMeanCostByProduct(Factory factory, ProductPlan ratioCapacity){
		double[] result = new double[factory.getProducts().size()];
		int index = 0;
		for (Product p : factory.getProducts()){
			if (p.isPurchasable()){
				result[index] = p.getPurchasePrice();
			}
			index++;
		}
		return result;
	}

	/**
	 * The sum of the variable costs in the given {@link Operation}, scaled as
	 * per piece.
	 * @param operation an {@link Operation} object
	 * @param shiftplan the {@link ShiftSchedule} used for the calculation
	 * @return the total costs per piece
	 */
	public static double getVariableCostPerPiece(Operation operation, ShiftSchedule shiftplan){
		double result = 0;
		for (Cost c : operation.getCosts()){
			if (c.isPerPiece()){
				result += c.getValue(shiftplan);
			}else if (!c.isPerPeriod()){
				result += c.getValuePerSecond(shiftplan) * operation.getTime();
			}
		}
		return result;
	}
	
	/**
	 * The sum of the variable costs in the given {@link Operation}, scaled as
	 * per piece, including the costs that are defined in the parent
	 * FactoryNodes.
	 * @param produces a {@link Operation} object
	 * @param shiftplan the {@link ShiftSchedule} used for the calculation
	 * @return the total costs per piece
	 */
	public static double getTotalVariableCostPerPiece(Operation operation, ShiftSchedule shiftplan){
		double result = getVariableCostPerPiece(operation, shiftplan);

		FactoryNode f = ((InternalOperation)operation).getOwner().getWorkplace();
		while (f != null){
			for (Cost c : f.getCosts()){
				if (c.isPerPiece()){
					result += c.getValue(shiftplan);
				}else if (!c.isPerPeriod()){
					result += c.getValuePerSecond(shiftplan) * operation.getTime();
				}
			}
			f = f.getParent();
		}
		return result;
	}

	@Override
	public void update(Observable o, Object arg){
		constantCostCache.remove(o);
		variableCostsCache.remove(o);
		profitCache.remove(o);
		theoreticalProfitCache.remove(o);
		variableCostsPerOperationCache.remove(o);
		variableCostsPerSecondCache.remove(o);
		o.deleteObserver(this);
	}
}

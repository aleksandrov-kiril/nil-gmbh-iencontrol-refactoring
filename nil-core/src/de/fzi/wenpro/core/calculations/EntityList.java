/**
 * ecoFLEXiP 2010
 * de.fzi.ecoflex.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import de.fzi.wenpro.core.model.Cost;
import de.fzi.wenpro.core.model.ExternalOperation;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.FactoryResource;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;



/**
 * @author Jan Siebel
 * 
 */
public class EntityList implements Observer {

	private static EntityList singleton = new EntityList();
	private static Map<FactoryNode, List<Workplace>> workplaceListCache = new HashMap<FactoryNode, List<Workplace>>();
	private static Map<FactoryNode, List<Operation>> operationListCache = new HashMap<FactoryNode, List<Operation>>();


	/**
	 * A list of all {@link Operation} in the given node and all children that
	 * can produce all of the {@link Product}s given in the <code>product</code>
	 * list. If the list is empty, all operations are returned.
	 * @param factoryNode a {@link FactoryNode}
	 * @param product a list of {@link Product}s
	 * @return a list of {@link #Operation} objects
	 */
	public static List<Operation> getOperationList(FactoryNode factoryNode, Product... products){
		List<Operation> result;
		if (products.length == 0){
			result = operationListCache.get(factoryNode);
			if (result == null) {
				result = new ArrayList<Operation>();
				for (Workplace w: getWorkplaceList(factoryNode)) {
					for (WorkplaceState ws: w.getStates()){
						result.addAll(ws.getOperations());
					}
				}
				if (factoryNode instanceof Factory){
					Factory factory = ((Factory)factoryNode);
					for (Product p : factory.getProducts()){
						if (p.isPurchasable()){
							result.add(new ExternalOperation(factory, p));
						}
					}
				}
				operationListCache.put(factoryNode, result);
				factoryNode.addObserver(singleton);
			}
		}else{
			result = new ArrayList<Operation>();
			for (Operation o : getOperationList(factoryNode)){
				boolean include = true;
				Set<Product> outputProducts = o.getOutputProducts();
				for (Product p : products){
					if (!outputProducts.contains(p)){
						include = false;
						break;
					}
				}
				if (include){
					result.add(o);
				}
			}
			return result;
		}
		return result;
	}

	/**
	 * The number of {@link Operation} in the given node and all children.
	 * @param factoryNode a {@link FactoryNode}
	 * @return the number of {@link Operation}
	 */
	public static int countOperations(FactoryNode factoryNode){
		return getOperationList(factoryNode).size();
	}


	/**
	 * A list of all {@link Workplace}s in the given {@link FactoryNode} and all
	 * children
	 * @param factoryNode a {@link FactoryNode}
	 * @return a list of {@link Workplace}s
	 */
	public static List<Workplace> getWorkplaceList(FactoryNode factoryNode){
		List<Workplace> result = workplaceListCache.get(factoryNode);
		if (result == null){
			result = new ArrayList<Workplace>();
			if (factoryNode instanceof Workplace){
				result.add((Workplace)factoryNode);
			}else{
				for (FactoryNode f : factoryNode.getSubNodes()){
					result.addAll(getWorkplaceList(f));
				}
			}
			workplaceListCache.put(factoryNode, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}

	/**
	 * The number of {@link Workplace}s in the given node and all children.
	 * @param factoryNode a {@link FactoryNode}
	 * @return the number of {@link Workplace}s
	 */
	public static int countWorkplace(FactoryNode factoryNode){
		return getWorkplaceList(factoryNode).size();
	}


	/**
	 * Returns a list of all {@link FactoryNode}s in this node's sub
	 * {@link FactoryNode} tree, including this node itself.
	 * @param factoryNode a {@link FactoryNode}
	 */
	public static List<FactoryNode> getSubnodeList(FactoryNode factoryNode){
		return getSubnodeList(factoryNode, 0, Integer.MAX_VALUE);
	}

	/**
	 * Returns a list of all {@link FactoryNode}s in this node's sub FactoryNode
	 * tree that have distance to this node which is in the given range. NOTE:
	 * the level definition differs from the one used in getLevel()!
	 * @param factoryNode a {@link FactoryNode}
	 * @param minLevelsBelowThis the minimum distance between this node an the
	 *        result nodes
	 * @param maxLevelsBelowThis the maximum distance between this node an the
	 *        result nodes
	 * @return a list of {@link FactoryNode}s
	 */
	public static List<FactoryNode> getSubnodeList(FactoryNode factoryNode, int minLevelsBelowThis, int maxLevelsBelowThis){
		List<FactoryNode> result = new ArrayList<FactoryNode>();
		if (maxLevelsBelowThis >= 0){
			fillSubnodeList(factoryNode, result, minLevelsBelowThis, maxLevelsBelowThis);
		}
		return result;
	}

	private static void fillSubnodeList(FactoryNode factoryNode, List<FactoryNode> result, int minLevelsBelowThis, int maxLevelsBelowThis){
		if (minLevelsBelowThis <= 0){
			result.add(factoryNode);
		}
		if (maxLevelsBelowThis > 0){
			for (FactoryNode f : factoryNode.getSubNodes()){
				fillSubnodeList(f, result, minLevelsBelowThis-1, maxLevelsBelowThis-1);
			}
		}
	}

	/**
	 * Creates an list of all {@link Cost} object in the given node, all
	 * subnodes, and all {@link Produces} objects.
	 * @param factoryNode a {@link FactoryNode}
	 */
	public static List<Cost> getCostList(FactoryNode factoryNode){
		List<Cost> result = new ArrayList<Cost>();
		fillCostList(factoryNode, result);
		return result;
	}

	//TODO : ask Jan Retooling cost included??
	private static void fillCostList(FactoryResource factoryResource, List<Cost> result){
		for (Cost cost : factoryResource.getCosts()){
			result.add(cost);
		}
		if (factoryResource instanceof Operation){
			result.addAll(((Operation)factoryResource).getCosts());
		}
		else if (factoryResource instanceof Workplace) {
			for (WorkplaceState ws: ((Workplace)factoryResource).getStates()) {
				for (Operation op: ws.getOperations()) {
					fillCostList(op, result);
				}
			}
		}else {
			for (FactoryNode f : ((FactoryNode)factoryResource).getSubNodes()){
				fillCostList(f, result);
			}
		}
	}

	/**
	 * Returns a list of all products that are created in the given node.
	 * @param factoryNode a {@link FactoryNode}
	 */
	public static Collection<Product> getProductList(FactoryNode factoryNode){
		Set<Product> result = new HashSet<Product>();
		for (Operation op : getOperationList(factoryNode)){
			result.addAll(op.getOutputProducts());
		}
		return result;
	}


	/**
	 * Creates a list of all products in the factory that are disposable and
	 * final, that means that they are no components to any other product.
	 * @param factory a {@link Factory}
	 * @return a list of all final disposable products
	 */

	public static List<Product> getFinalProductsList(Factory factory){
		List<Product> result = new ArrayList<Product>();
		for (Product p : factory.getProducts()){
			if (p.isDisposable()){
				result.add(p);
			}
		}
		for (Operation op: getOperationList(factory)){
			result.removeAll(op.getInputProducts());
		}
		return result;
	}
	
	/**
	 * The list of final components in the given FactoryNode, calculated without
	 * a product plan. A final component is a {@link Product} that is produced
	 * in the given {@link FactoryNode}, but never used there as an operation
	 * input.
	 * @param factoryNode a {@link FactoryNode}
	 * @return the final products
	 */
	public static Collection<Product> getFinalComponentList(FactoryNode factoryNode){
		Set<Product> result = new HashSet<Product>();
		Set<Product> components = new HashSet<Product>();
		for (Operation op : getOperationList(factoryNode)){
			for (Product p : op.getOutputProducts()){
				if (!p.isPurchasable()){
					result.add(p);
				}
			}
		}
		for (Operation op: getOperationList(factoryNode)){
			components.addAll(op.getInputProducts());
		}
		result.removeAll(components);
		return result;
	}
	
	/**
	 * Returns if the given product is a final component. A product is a final
	 * component if it is not disposable, and if there is no line in the factory
	 * that both creates the product and uses it as a component.
	 * @param product a {@link Product}
	 * @return <code>true</code> if the product is a final component
	 */
	public static boolean isFinalComponent(Product product) {
		if (product.isDisposable()){
			return false;			
		}else{
			Factory factory = product.getFactory();
			Set<FactoryNode> lines = new HashSet<FactoryNode>();
			for (Operation op : EntityList.getOperationList(factory)){
				for (Product p : op.getOutputProducts()){
					if (p.equals(product)) {
						if (( (InternalOperation)op).getOwner().getWorkplace() !=null && ( (InternalOperation)op).getOwner().getWorkplace().getParent().getLevel() == 2){
							lines.add(( (InternalOperation)op).getOwner().getWorkplace().getParent());
						}
					}
				}
			}
			for (FactoryNode f : lines){
				for (Operation op : EntityList.getOperationList(f)){
					for (Product p : op.getInputProducts()){
						if (p.equals(product)) {
							return false;
						}
					}
				}
			}
			return true;
		}
	}
	/**
	 * Creates a list of all products in the factory that are disposable.
	 * @param factory a {@link Factory} 
	 * @return a list of disposable products
	 */
	public static List<Product> getDisposableProductList(Factory factory){
		List<Product> result = new ArrayList<Product>();
		for (Product p : factory.getProducts()){ 
			if (p.isDisposable()){
				result.add(p);
			}
		}
		return result;
	}

	/**
	 * The number of products in the given {@link Factory} that are disposable.
	 * @param factory a {@link Factory} 
	 * @return: The number of disposable products.
	 */
	public static int countDisposableProducts(Factory factory){
		int result = 0;
		for (Product p : factory.getProducts()){
			if(p.isDisposable()){
				result++;
			}
		}
		return result;
	}

	/**
	 * A list of operations for all products that are purchased.
	 * @return an array of Operation
	 */
	public static List<Operation> getPurchaseOperationList(Factory factory){
		List<Operation> result = new ArrayList<Operation>();
		for (Product p : factory.getProducts()){
			if (p.isPurchasable()){
				result.add(new ExternalOperation(factory, p));
			}
		}
		return result;
	}
	
	
	
	/**
	 * Returns all {@link Workplace}s that produce the given product.
	 * @param product a {@link Product}
	 * @return a list of {@link Workplace}s
	 */
	public static List<Workplace> getProducingWorkplaceList(Product product){
		ArrayList<Workplace> workplaces = new ArrayList<Workplace>();
		for (Workplace w : getWorkplaceList(product.getFactory())){
			if (w.isProduced(product)){
				workplaces.add(w);
			}
		}
		return workplaces;
	}

	@Override
	public void update(Observable o, Object arg){
		workplaceListCache.remove(o);
		operationListCache.remove(o);
		o.deleteObserver(this);
	}
}

/**
 * Ecoflex 2009
 * de.fzi.wenpro.core.model.operations.WorkplaceOutputOperations.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.fzi.wenpro.core.printing.FactoryNodeOutputOperations;

/**
 * @author aleksa
 *
 */
public class WorkplaceOutputOperations extends FactoryNodeOutputOperations{

	/**
	 * Constructor WorkplaceOutputOperations
	 * @param node
	 */
	public WorkplaceOutputOperations(Workplace node) {
		super(node);
	}
	
	@Override
	public void print(String indent) {
		super.print(indent);
		for (WorkplaceState state : ((Workplace)getDO()).getStates()) {
			for (Operation op : state.getOperations()) {
				System.out.println(indent+"\t" + productQuantityListToString(op.getOutputs()));
			}
		}		
	}
}

/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.calculations.CostCalculations;
import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.calculations.ProductionTime;
import de.fzi.wenpro.core.calculations.RatioMatrix;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.HashMap2d;
import de.fzi.wenpro.util.Matrix;
import de.fzi.wenpro.util.Simplex;

/**
 * @author Jan Siebel, Hendro Wicaksono
 * 
 */
public class ZeroProduction implements Observer {
	
	private static ZeroProduction singleton = new ZeroProduction();
	private static HashMap2d<FactoryNode, Product, ProductPlan> zeroProductionCache = new HashMap2d<FactoryNode, Product, ProductPlan>();
	private static HashMap2d<FactoryNode, Product, Double> zeroProductionProfitCache = new HashMap2d<FactoryNode, Product, Double>();

	/**
	 * Calculates the zero production of the factory, i.e. the factory's maximal
	 * profit if a given product isn't produced in a given node.
	 * @param factoryNode the {@link FactoryNode} for which the zero production
	 *        is calculated
	 * @param product the product that is not produced
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getZeroProduction(FactoryNode factoryNode, Product product){
		ProductPlan result = zeroProductionCache.get(factoryNode, product);
		
		if (result == null){
			result = calculateZeroProduction((Factory)factoryNode, product);
			zeroProductionCache.put(factoryNode, product, result);
			zeroProductionProfitCache.put(factoryNode, product, result.getProfit());
			factoryNode.addObserver(singleton);
		}
		
		return result;
	}
	
	/**
	 * Calculates the profit of the zero production of the factory, i.e. the
	 * factory's maximal profit if a given product isn't produced in a given
	 * node.
	 * @param factoryNode the {@link FactoryNode} for which the zero production
	 *        is calculated
	 * @param product the product that is not produced
	 * @return the resulting {@link ProductPlan}
	 */
	public static double getZeroProductionProfit(FactoryNode factoryNode, Product product){
		Double result = zeroProductionProfitCache.get(factoryNode, product);
		
		if (result == null){
			ProductPlan p = calculateZeroProduction(factoryNode, product);
			result = p.getProfit();
			zeroProductionCache.put(factoryNode, product, p);
			zeroProductionProfitCache.put(factoryNode, product, result);
			factoryNode.addObserver(singleton);
		}
		
		return result;
	}
	
	/**
	 * Calculates the factory's maximal profit if a given product isn't 
	 * produced in a given node.
	 */
	private static ProductPlan calculateZeroProduction(FactoryNode factoryNode, Product product){
		Factory factory = factoryNode.getFactory();
		ShiftSchedule shiftplan = OptimalProduction.getHighestOptimalProduction(factoryNode).getShiftplan();
		Simplex s = new Simplex(
				EntityList.countOperations(factory), EntityList.countWorkplace(factory)+EntityList.countDisposableProducts(factory),

				Matrix.merge(ProductionTime.getTimeMatrix(factory), RatioMatrix.getConditionMatrixDisposable(factory, shiftplan)),
				ProductionTime.getTimeLimitVector(factory, shiftplan, false),
				CostCalculations.getProfitByProduces(factory, shiftplan), 
				Matrix.append(RatioMatrix.getRatioMatrixNonDisposable(factory, shiftplan), getRatioMatrixZeroProduct(factoryNode, product))
				);
		return new ProductPlan(s.solve(), factory, shiftplan);
	}
	
	/**
	 * A ratio matrix line that prevents the production of the given product in this
	 * factory node.
	 * @param product the product which won't be produced
	 * @return the ratio matrix line
	 */
	private static double[] getRatioMatrixZeroProduct(FactoryNode factoryNode, Product product){
		List<Operation> operationList = EntityList.getOperationList(factoryNode);
		List<Operation> operationListFactory = EntityList.getOperationList(factoryNode.getFactory());
		double[] result = new double[operationListFactory.size()];
		
		int i = 0;
		while (operationListFactory.get(i).equals(operationList.get(0))){
			i++;
		}
		boolean empty = true;
		for (Operation op : operationList){
			if (op.getOutputProducts().contains(product)){
				result[i] = 1;
				empty = false;
			}
			i++;
		}
		if (empty){
			return null;
		}else{
			return result;
		}
	}
	
	@Override
	public void update(Observable o, Object arg){
		zeroProductionCache.remove(o);
		zeroProductionProfitCache.remove(o);
		o.deleteObserver(this);
	}
}

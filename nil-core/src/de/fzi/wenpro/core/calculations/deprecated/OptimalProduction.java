/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.calculations.CostCalculations;
import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.calculations.ProductionTime;
import de.fzi.wenpro.core.calculations.RatioMatrix;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.HashMap2d;
import de.fzi.wenpro.util.Matrix;
import de.fzi.wenpro.util.Simplex;

/**
 * @author Jan Siebel, Hendro Wicaksono
 * 
 */
public class OptimalProduction implements Observer {
	
	private static OptimalProduction singleton = new OptimalProduction();
	private static HashMap2d<FactoryNode, ShiftSchedule, ProductPlan> optimalProductionCache = new HashMap2d<FactoryNode, ShiftSchedule, ProductPlan>();

	/**
	 * Calculates the optimal production of the factory, i.e. the factory's maximal
	 * profit if the product ratio is ignored.
	 * @param factoryNode the {@link FactoryNode} for which the opimtal production
	 *        is calculated
	 * @param shiftplan the {@link ShiftSchedule} the calculation is based on
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getOptimalProduction(FactoryNode factoryNode, ShiftSchedule shiftplan){
		ProductPlan result = optimalProductionCache.get(factoryNode, shiftplan);
		
		if (result == null){
			if (factoryNode instanceof Factory){
				result = calculateOptimalProduction((Factory)factoryNode, shiftplan);
				optimalProductionCache.put(factoryNode, shiftplan, result);
				factoryNode.addObserver(singleton);
			}else{
				FactoryNode parent = factoryNode.getParent();
				ProductPlan productPlan = getOptimalProduction(parent, shiftplan);
				ProductPlan[] productPlans = productPlan.split();
				
				int i = 0;
				for (FactoryNode f : parent.getSubNodes()){
					optimalProductionCache.put(f, shiftplan, productPlans[i++]);
					f.addObserver(singleton);
				}
				result = optimalProductionCache.get(factoryNode, shiftplan);
			}
		}
		
		return result;
	}
	
	/**
	 * The the highest optimal production over all {@link ShiftSchedule}s.
	 * @param factoryNode the {@link FactoryNode} for which the highest optimal
	 *        production is calculated
	 * @return a {@link ProductPlan}
	 */
	public static ProductPlan getHighestOptimalProduction(FactoryNode factoryNode){
		double max = Double.NEGATIVE_INFINITY;
		ProductPlan result = null;
		for (ShiftSchedule s : factoryNode.getFactory().getShiftplans()){
			ProductPlan p = getOptimalProduction(factoryNode, s);
			double profit = p.getProfit();
			if (profit > max){
				result = p;
				max = profit;
			}
		}
		
		return result;
	}
	
	/**
	 * Calculates the optimal production for a factory.
	 */	
	private static ProductPlan calculateOptimalProduction(Factory factory, ShiftSchedule shiftplan){
		Simplex s = new Simplex(
				EntityList.countOperations(factory), EntityList.countWorkplace(factory)+EntityList.countDisposableProducts(factory),
				
				Matrix.merge(ProductionTime.getTimeMatrix(factory), RatioMatrix.getConditionMatrixDisposable(factory, shiftplan)),
				ProductionTime.getTimeLimitVector(factory, shiftplan,false),
				CostCalculations.getProfitByProduces(factory, shiftplan),
				RatioMatrix.getRatioMatrixNonDisposable(factory, shiftplan)
		);
		return new ProductPlan(s.solve(), factory, shiftplan);
	}
	
	@Override
	public void update(Observable o, Object arg){
		optimalProductionCache.remove(o);
		o.deleteObserver(this);
	}
}

/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.TACalculations.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.calculations.RatioCapacity;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;

/**
 * @author Zerael
 *
 */
public class PersonnelAnalysisCalculations {
	//TODO: malfunctioning times?
	private Factory factory;
	private ShiftSchedule shiftplan;
	private int pieces;
	private double workplaceChangeTime;
	private int workplaceSharingMode;
	
	private Map<FactoryNode, double[]> calculationResult = null;
	private boolean calculationDone = false;
	
	public PersonnelAnalysisCalculations(Factory factory, ShiftSchedule shiftplan, int pieces, double workplaceChangeTime, int workplaceSharingMode){
		if (workplaceSharingMode < 0 || workplaceSharingMode > 2){
			throw new IllegalArgumentException("Illegal workplaceSharingMode");
		}
		this.factory = factory;
		this.shiftplan = shiftplan;
		this.pieces = pieces;
		this.workplaceChangeTime = workplaceChangeTime;
		this.workplaceSharingMode = workplaceSharingMode;
	}
	
	public Map<FactoryNode, double[]> calculate(){
		ProductPlan p = RatioCapacity.getRatioCapacity(factory, shiftplan);
		if (0<= pieces && pieces <= p.getFinalComponentsVolume()){
			calculationResult = new HashMap<FactoryNode, double[]>();
			double loadFactor = pieces / p.getFinalComponentsVolume();
			Workplace[] workplaces = EntityList.getWorkplaceList(factory).toArray(new Workplace[0]);
			
			double[] workers = p.getWorkersByWorkplace();
			double[] volume = p.getVolumeByWorkplace();
			for (int i=0; i<workplaces.length; i++){
				workers[i] *= loadFactor;
				volume[i] *= loadFactor;
				
				/* fill the result map with the workplace data */
				calculationResult.put(workplaces[i], new double[]{workers[i], Math.ceil(workers[i])});
			}
			
			/** the time a worker needs to get to this workplace */
			double[] changeTime = new double[workplaces.length];
			for (int i=0; i<workplaces.length; i++){
				changeTime[i] = workplaceChangeTime * volume[i] / shiftplan.getSecondsPerPeriod();
			}
			
			/* add the workplaces' parents data to the result map */
			int parentSubnodeCount;
			for (int i=0; i<workplaces.length; i += parentSubnodeCount){
				FactoryNode parent = workplaces[i].getParent();
				parentSubnodeCount = parent.getSubNodes().size();
				
				double[] lineWorkers = new double[parentSubnodeCount];
				double lineChangeTime[] = new double[parentSubnodeCount];
				for (int j=0; j<parentSubnodeCount; j++){
					lineWorkers[j] = workers[i+j];
					lineChangeTime[j] = j==0 ? 0 : changeTime[i+j-1] + changeTime[i+j];
				}
				
				double[] line;
				if (workplaceSharingMode == 0){
					line = calculateLineNotShared(lineWorkers, lineChangeTime);
				}else if (workplaceSharingMode == 1){
					line = calculateLineShared(lineWorkers, lineChangeTime);
				}else{
					line = calculateLineFullyShared(lineWorkers, lineChangeTime);
				}
				calculationResult.put(parent, line);
			}
			/** add the data for all other factory nodes */
			fillResult(calculationResult, factory);
			
			if (workplaceSharingMode == 2){
				for (double[] line : calculationResult.values()){
					line[1] = Math.ceil(line[0]);
				}
			}
		}else{
			calculationResult = null;
		}
		calculationDone = true;
		return calculationResult;
	}
	
	private double[] calculateLineNotShared(double workers[], double[] changeTime){
		double[] result = new double[2];
		double currentWorkerTimeLeft = 0;
		for (int w=0; w<workers.length; w++){
			result[0] += workers[w];
			
			double timeLeft = workers[w];
			
			/* do the work on the next workplace if you can do it alone */
			if (timeLeft + changeTime[w] <= currentWorkerTimeLeft){
				result[0] += changeTime[w];
				currentWorkerTimeLeft -= timeLeft + changeTime[w];
			}else{
				result[1] += Math.ceil(timeLeft);
				currentWorkerTimeLeft = Math.ceil(timeLeft) - timeLeft;
			}
			/* */
			
			//			/* help on the next workplace if possible */
			//			if (currentWorkerTimeLeft > changeTime[w]){
			//				currentWorkerTimeLeft -= changeTime[w];
			//				result[0] += changeTime[w];
			//				double min = Math.min(currentWorkerTimeLeft, timeLeft);
			//				currentWorkerTimeLeft -= min;
			//				timeLeft -= min;
			//			}
			//			result[1] += Math.ceil(timeLeft);
			//			currentWorkerTimeLeft = Math.ceil(timeLeft) - timeLeft;
			//			/* */
		}
		return result;
	}
	
	private class WorkPlan{
		public int nWorkers;
		public double workingTime;
		public double workerTimeLeft;
		public boolean workerHasOwnWorkplace;
		
		public WorkPlan(int nWorkers, double workingTime, double workerTimeLeft, boolean workerHasOwnWorkplace){
			this.nWorkers = nWorkers;
			this.workingTime = workingTime;
			this.workerTimeLeft = workerTimeLeft;
			this.workerHasOwnWorkplace = workerHasOwnWorkplace;
		}
	}
	
	public double[] calculateLineShared(double workers[], double[] changeTime){
		List<WorkPlan> solutions = new ArrayList<WorkPlan>();
		List<WorkPlan> newSolutions;
		
		solutions.add(new WorkPlan(0, 0, 0, true));
		for (int w=0; w<workers.length; w++){
			newSolutions = new ArrayList<WorkPlan>();
			
			for (WorkPlan wp : solutions){
				if (wp.workerTimeLeft > changeTime[w]){
					if (wp.workerHasOwnWorkplace || workers[w]<=wp.workerTimeLeft-changeTime[w]){
						/* the recent worker will work on the current workplace */
						double workplaceTimeLeft = workers[w]-(wp.workerTimeLeft-changeTime[w]);
						newSolutions.add(new WorkPlan(
								wp.nWorkers + (int)Math.ceil(workplaceTimeLeft),
								wp.workingTime + workers[w] + changeTime[w],
								Math.ceil(workplaceTimeLeft) - workplaceTimeLeft,
								workplaceTimeLeft <= 0 || wp.workerTimeLeft==0
						));
					}
				}
				if (wp.workerHasOwnWorkplace){
					/* the recent worker won't work on the current workplace */
					newSolutions.add(new WorkPlan(
							wp.nWorkers + (int)Math.ceil(workers[w]),
							wp.workingTime + workers[w],
							Math.ceil(workers[w]) - workers[w],
							true
					));
				}
			}
			solutions = newSolutions;
		}
		
		/* find the best solution */
		int minWorkers = Integer.MAX_VALUE;
		double minWorkTime = Double.POSITIVE_INFINITY;
		for (WorkPlan wp : solutions){
			if (wp.workerHasOwnWorkplace){
				if (wp.nWorkers < minWorkers){
					minWorkers = wp.nWorkers;
					minWorkTime = wp.workingTime;
				}else if (wp.nWorkers == minWorkers && wp.workingTime < minWorkTime){
					minWorkers = wp.nWorkers;
					minWorkTime = wp.workingTime;
				}
			}
		}
		return new double[]{minWorkTime, minWorkers};
	}
	
	public double[] calculateLineFullyShared(double workers[], double[] changeTime){
		double sum = 0;
		for (int w=0; w<workers.length; w++){
			sum += workers[w];
		}
		return new double[]{sum, Math.ceil(sum)};
	}
	
	public double calculatePace(){
		if (!calculationDone){
			calculate();
		}
		if (calculationResult == null){
			return 0;
		}else{
			double maxMalfunctioningTime = 0;
			for (Workplace w : EntityList.getWorkplaceList(factory)){
				if (maxMalfunctioningTime < w.getMalfunctionTime().get(shiftplan)){
					maxMalfunctioningTime = w.getMalfunctionTime().get(shiftplan);
				}
			}
			return (shiftplan.getSecondsPerPeriod()-maxMalfunctioningTime)/pieces;
		}
	}
	
	public double calculateManufactoringTime(){
		if (!calculationDone){
			calculate();
		}
		if (calculationResult == null){
			return 0;
		}else{
			double workers = calculationResult.get(factory)[1];
			workers += calculateStandByMen();
			return workers * shiftplan.getSecondsPerPeriod() / pieces;
		}
	}
	
	public int  calculateStandByMen(){
		if (!calculationDone){
			calculate();
		}
		if (calculationResult == null){
			return 0;
		}else{
			return (int)Math.ceil(calculationResult.get(factory)[1]/14);
		}
	}
	
	private double[] fillResult(Map<FactoryNode, double[]> result, FactoryNode factoryNode){
		double[] resultLine = result.get(factoryNode);
		if (resultLine == null){
			resultLine = new double[2];
			for (FactoryNode f : factoryNode.getSubNodes()){
				double[] fResultLine = fillResult(result, f);
				resultLine[0] += fResultLine[0];
				resultLine[1] += fResultLine[1];
			}
			result.put(factoryNode, resultLine);
		}
		return resultLine;
	}
}

/**
 * Ecoflex 2009
 * de.fzi.wenpro.core.model.operations.MultiPeriodPlan.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import de.fzi.wenpro.core.calculations.CostCalculations;
import de.fzi.wenpro.core.calculations.RatioCapacity;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.CalculationInterruptedException;
import de.fzi.wenpro.util.Numbers;
import de.fzi.wenpro.util.Vector;

/**
 * For a given number of pieces to be produced an a given time span in periods,
 * this finds the optimal shift schedule and worker maximum for each period. 
 * @author siebel
 *
 */
public class MultiPeriodPlan {
	
	public interface ProgressListener{
		public void handleEvent(Object o);
	}
	
	/** The minimum time a period must use for the production */
	private final static double percentageMinumum = 0.1;
	
	private final FactoryNode factoryNode;
	private final int minPeriods, maxPeriods;
	private final int targetPieces;
	private Collection<ShiftSchedule> shiftplans;
	
	private double maxPieces;
	private double[] maxWorkers;
	private PeriodShiftData[] periodShiftData;
	private PeriodShiftData zeroPeriod = new PeriodShiftData(null, null, 0, 0);

	private PeriodShiftDataList bestResult;
	
	private volatile boolean cancel;

	private ProgressListener listener = null;

	public class PeriodShiftData implements Cloneable{
		public ProductPlan productPlan;
		public double cost;
		public double pieces;
		public ShiftSchedule shiftplan;
		public double workers;
		public double percentageOfPeriod;
		
		public PeriodShiftData(ProductPlan productPlan, ShiftSchedule shiftplan, double workers, double percentageOfPeriod) {
			this.productPlan = productPlan;
			if (productPlan==null){
				this.cost = 0;
				this.pieces = 0;
			}else{
				this.cost = getCost(shiftplan, productPlan);
				this.pieces = productPlan.getDisposableProductVolume();
			}
			this.shiftplan = shiftplan;
			this.workers = workers;
			this.percentageOfPeriod = percentageOfPeriod;
		}
		
		public PeriodShiftData clone(){
			try {
				return (PeriodShiftData)super.clone();
			} catch (CloneNotSupportedException e) {
				return null;
			}
		}		
	}

	private class PeriodShiftDataList{
		public double pieces;
		public double cost;
		public PeriodShiftData[] periodShiftData;
		public int minIndex = 0;
		
		public PeriodShiftDataList(PeriodShiftDataList periodShiftDataList, PeriodShiftData periodShiftData){
			this.pieces = periodShiftDataList.pieces + periodShiftData.pieces;
			this.cost = periodShiftDataList.cost + periodShiftData.cost;
			this.periodShiftData = new PeriodShiftData[periodShiftDataList.periodShiftData.length+1];
			System.arraycopy(periodShiftDataList.periodShiftData, 0, this.periodShiftData, 0, periodShiftDataList.periodShiftData.length);
			this.periodShiftData[this.periodShiftData.length-1] = periodShiftData;
		}

		public PeriodShiftDataList(PeriodShiftData periodShiftData){
			this.pieces = periodShiftData.pieces;
			this.cost = periodShiftData.cost;
			this.periodShiftData = new PeriodShiftData[1];
			this.periodShiftData[0] = periodShiftData;
		}
		
		public PeriodShiftDataList(){
			this.pieces = 0;
			this.cost = 0;
			this.periodShiftData = new PeriodShiftData[0];
		}
		
		/**
		 * Sets the <code>percentageOfPeriod</code> value of a period to avoid
		 * production over <code>targetPieces</code>. The period cut is the one
		 * where the most cost can be saved. Note: All PeriodShiftData must have full
		 * <code>percentageOfPeriod</code> before calling this method!
		 */
		public void cutPeriod() {
			double p, costTmp, minp=1, minCost = Double.POSITIVE_INFINITY;
			int mini = 0;
			for (int i=0; i<periodShiftData.length; i++){
				if (pieces - periodShiftData[i].pieces <= targetPieces){
					if (pieces - periodShiftData[i].pieces < targetPieces){
						p = 1-(pieces-targetPieces)/periodShiftData[i].pieces;
					}else{
						p = 1;
					}
					if (p < percentageMinumum){
						p = percentageMinumum;
					}
					costTmp = this.cost - (1-p)*periodShiftData[i].cost;
					if (costTmp < minCost){
						minCost = costTmp;
						mini = i;
						minp = p;
					}
				}
			}
			periodShiftData[mini] = periodShiftData[mini].clone();
			periodShiftData[mini].percentageOfPeriod = minp;
			cost = minCost;
			pieces -= (1-minp) * periodShiftData[mini].pieces;
		}
	}
	

	
	public MultiPeriodPlan(FactoryNode factoryNode, int minPeriods, int maxPeriods, int pieces, Collection<ShiftSchedule> shiftplans) throws CalculationInterruptedException{
		cancel = false;
		this.factoryNode = factoryNode;
		this.minPeriods = minPeriods;
		this.maxPeriods = maxPeriods;
		this.targetPieces = pieces;
		this.shiftplans = shiftplans;
		prepareData();
	}

	public MultiPeriodPlan(FactoryNode factoryNode, int minPeriods, int maxPeriods, int pieces) throws CalculationInterruptedException{
		this(factoryNode, minPeriods, maxPeriods, pieces, factoryNode.getFactory().getShiftplans());
	}

	public MultiPeriodPlan(FactoryNode factoryNode, int maxPeriods, int pieces) throws CalculationInterruptedException{
		this(factoryNode, 0, maxPeriods, pieces, factoryNode.getFactory().getShiftplans());
	}

	private void prepareData() throws CalculationInterruptedException{
		maxWorkers = new double[factoryNode.getFactory().countShiftplans()];
		maxPieces = 0;

		List<PeriodShiftData> periodShiftDataTmp = new ArrayList<PeriodShiftData>();
		int i=0;
		for (ShiftSchedule shiftplan : shiftplans){
			ProductPlan p = RatioCapacity.getRatioCapacity(factoryNode, shiftplan);
			int maxWorkers;
			if (p.isValid()){
				maxWorkers = (int)Math.ceil(p.getTotalWorkers());
			}else{
				maxWorkers = 0;
			}

			this.maxWorkers[i] = maxWorkers;
			for (int workers=1; workers<=maxWorkers; workers++){
				ProductPlan pw = RatioCapacity.getRatioCapacity(factoryNode, shiftplan, false, workers);
				PeriodShiftData psd = new PeriodShiftData(pw, shiftplan, workers, 1);
				if (psd.pieces > maxPieces){
					maxPieces = psd.pieces;
				}
				periodShiftDataTmp.add(psd);
				checkInterruptCalculation();
			}
			i++;
			checkInterruptCalculation();
		}
		List<PeriodShiftData> periodShiftData = new ArrayList<PeriodShiftData>();
		for (PeriodShiftData psd : periodShiftDataTmp){
			if (psd.pieces >= targetPieces-(maxPeriods-1)*maxPieces){
				periodShiftData.add(psd);
			}
		}
		
		/* 
		 * Remove inefficient shiftplans that produce less pieces at a higher
		 * price than another, and all but one of a set of equal shiftplans.
		 */
		List<PeriodShiftData> inefficient = new ArrayList<PeriodShiftData>();
		for (i=0; i<periodShiftData.size(); i++){
			PeriodShiftData psd1 = periodShiftData.get(i);
			for (int j=i+1; j<periodShiftData.size(); j++){
				PeriodShiftData psd2 = periodShiftData.get(j);
				if (psd1.pieces < psd2.pieces){
					if (psd1.cost > psd2.cost){
						inefficient.add(psd1);
					}
				}else if (psd1.pieces == psd2.pieces){
					if (psd1.cost < psd2.cost){
						inefficient.add(psd2);
					}else{
						inefficient.add(psd1);
					}
				}else{
					if (psd1.cost < psd2.cost){
						inefficient.add(psd2);
					}
				}
			}
		}
		periodShiftData.removeAll(inefficient);
		Comparator<PeriodShiftData> c = new Comparator<PeriodShiftData>(){
			public int compare(PeriodShiftData o1, PeriodShiftData o2) {
				return (int)Math.signum(o2.pieces - o1.pieces);
			}
		};
		this.periodShiftData = periodShiftData.toArray(new PeriodShiftData[0]);
		Arrays.sort(this.periodShiftData, c);
	}

	private void sortedInsert(List<PeriodShiftDataList> list, PeriodShiftDataList element){
		if (list.size() == 0){
			list.add(element);
		}else if (element.pieces >= targetPieces){
			/* Of all PeriodShiftDataList that reach the piece target, only the
			 *  one with the least costs can be part of the optimal solution. */
			PeriodShiftDataList last = list.get(list.size()-1);
			if (last.pieces < targetPieces){
				list.add(element);
			}else if (last.cost > element.cost){
				list.remove(list.size()-1);
				list.add(element);
			}
		}else{
			int i;
			/* Find the insert position. */
			if (element.pieces > list.get(list.size()-1).pieces){
				i = list.size();
			}else{
				int i1=0, i2=list.size()-1;
				i = (i1+i2)/2;
				while (i1<i2){
					if (list.get(i).pieces < element.pieces){
						i1 = i+1;
					}else{
						i2 = i;
					}
					i = (i1+i2)/2;
				}
				if (element.cost >= list.get(i).cost){
					return;
				}
			}
			list.add(i, element);
			/* Delete all entries where less pieces are produced at higher costs. */
			if (i>0 && element.cost < list.get(i-1).cost){
				int j;
				if (list.get(0).cost < element.cost){
					j=i-2;
					while (element.cost < list.get(j).cost){
						j--;
					}
				}else{
					j = -1;
				}
				list.subList(j+1, i).clear();
			}
		}
	}
	
	private List<PeriodShiftDataList> combine(List<PeriodShiftDataList> psdlList, PeriodShiftData[] periodShiftData, double minPieces, int period) throws CalculationInterruptedException{
		List<PeriodShiftDataList> result = new ArrayList<PeriodShiftDataList>();
		for (PeriodShiftDataList psdl : psdlList){
			checkInterruptCalculation();
			for (int j=psdl.minIndex; j<periodShiftData.length; j++){
				PeriodShiftData psd = periodShiftData[j];
				PeriodShiftDataList newPsdl = new PeriodShiftDataList(psdl, psd);
				if (newPsdl.pieces < minPieces){
					/* Drop it. */
				}else if (newPsdl.pieces < targetPieces || period<minPeriods){
					sortedInsert(result, newPsdl);
				}else if (period == minPeriods){
					checkResultUncut(newPsdl);
				}else{
					checkResultCut(newPsdl);
				}
			}
		}
		return result;
	}
	
	/**
	 * Compares a PeriodShiftDataList to the currently best PeriodShiftDataList
	 * stored in the <code>bestResult</code> field, and sets the
	 * <code>bestResult</code> if appropriate. No piece number conditions
	 * are checked. One of the PeriodShiftDataList's is truncated to optimize
	 * the production 
	 * @param periodShiftDataList the compared PeriodShiftDataList
	 */
	private void checkResultCut(PeriodShiftDataList periodShiftDataList){
		periodShiftDataList.cutPeriod();
		if (periodShiftDataList.cost < bestResult.cost){
			bestResult = periodShiftDataList;
		}
	}
	
	/**
	 * Compares a PeriodShiftDataList to the currently best PeriodShiftDataList
	 * stored in the <code>bestResult</code> field, and sets the
	 * <code>bestResult</code> if appropriate. No piece number conditions
	 * are checked.
	 * @param periodShiftDataList the compared PeriodShiftDataList
	 */
	private void checkResultUncut(PeriodShiftDataList periodShiftDataList){
		if (periodShiftDataList.cost < bestResult.cost){
			bestResult = periodShiftDataList;
		}
	}
	
	private void sortResult(PeriodShiftData[] periodShiftData){
		Arrays.sort(periodShiftData, new Comparator<PeriodShiftData>(){
			public int compare(PeriodShiftData o1, PeriodShiftData o2) {
				if (o1.percentageOfPeriod == o2.percentageOfPeriod){
					return (int)Math.signum(o2.pieces - o1.pieces);
				}else{
					return (int)Math.signum(o2.percentageOfPeriod - o1.percentageOfPeriod);
				}
			}
		});
	}
	
	public PeriodShiftData[] calculate() throws CalculationInterruptedException{
		cancel = false;
		List<PeriodShiftDataList> psdlList = new ArrayList<PeriodShiftDataList>();
		bestResult = new PeriodShiftDataList();
		bestResult.cost = Double.POSITIVE_INFINITY;
		
		int minIndex=0;
		sendListenerMessage(1);
		for (PeriodShiftData psd : periodShiftData){
			checkInterruptCalculation();
			PeriodShiftDataList newPsdl = new PeriodShiftDataList(psd);
			newPsdl.minIndex = minIndex++;
			if (newPsdl.pieces < targetPieces || minPeriods>1){
				sortedInsert(psdlList, newPsdl);
			}else if(minPeriods==1){
				checkResultUncut(newPsdl);
			}else{
				checkResultCut(newPsdl);
			}
		}
		
		for (int i=1; i<maxPeriods; i++){
			sendListenerMessage(i+1);
			double minPieces = targetPieces - maxPieces * (maxPeriods-1-i);
			psdlList = combine(psdlList, periodShiftData, minPieces, i+1);
		}
		
		if (bestResult.cost == Double.POSITIVE_INFINITY){
			return null;
		}else{
			PeriodShiftData[] result = new PeriodShiftData[maxPeriods];
			Arrays.fill(result, zeroPeriod);
			System.arraycopy(bestResult.periodShiftData, 0, result, 0, bestResult.periodShiftData.length);
			sortResult(result);

			return result;
		}
	}
	
	public void printInput(){
		System.out.println("MultiPeriodPlan for " + factoryNode.getName());
		System.out.println("Task: Produce " + targetPieces + " in " + maxPeriods + " periods.");
		System.out.println();
		System.out.println("ShiftSchedule                \t"+"workers\t"+"pieces\t"+"cost");
		for (PeriodShiftData psd : periodShiftData){
			if (psd.shiftplan == null || psd.productPlan == null){
				System.out.println("-");
			}else{
				String shiftplanName = psd.shiftplan.getName();
				while (shiftplanName.length()<25) shiftplanName += " ";
				System.out.println(shiftplanName + "\t" + psd.workers + "\t" + formatNumber(psd.pieces) + "\t" + formatNumber(psd.cost));
			}
		}
		System.out.println();
	}
	
	public void print(){
		try{
			printResult(calculate());
		}catch (CalculationInterruptedException e){
			System.out.println("Cancelled");
		}
	}

	public void printResult(PeriodShiftData[] result){
		if (result != null){
			int i=1;
			double pieces=0, profit=0;
			for (PeriodShiftData psd : result){
				if (psd.shiftplan==null){
					System.out.println("Period " + i++ + ": -");
				}else{
					String shiftplanName = psd.shiftplan.getName();
					while (shiftplanName.length()<25) shiftplanName += " ";
					System.out.println("Period " + i++ + ": " + Numbers.formatPercent(psd.percentageOfPeriod) + "\t" + shiftplanName + "\t" +  Numbers.formatWorkers(psd.workers) + " workers\t" + Numbers.formatPiecesUnit(psd.pieces*psd.percentageOfPeriod) + "\t" + Numbers.formatMoney(psd.cost*psd.percentageOfPeriod));
					profit += psd.cost * psd.percentageOfPeriod;
					pieces += psd.pieces * psd.percentageOfPeriod;
				}
			}
			System.out.println(Numbers.formatPiecesUnit(pieces) + "\t" + Numbers.formatMoney(profit));
		}else{
			System.out.println("No solution found");
		}
		
	}
	
	private double getCost(ShiftSchedule shiftplan, ProductPlan productPlan){
		return Vector.sprod(
				CostCalculations.getVariableCosts(factoryNode, shiftplan),
				productPlan.getVolume()
			)
			+ CostCalculations.getConstantCost(factoryNode, shiftplan);
	}

	private String formatNumber(double x){
		final int minChars = 12; 
		String result = String.valueOf((int)Math.round(x));
		while (result.length()<minChars){
			result = " " + result;
		}
		return result;
	}
	
	public void cancel(){
		cancel = true;
	}
	
	private void checkInterruptCalculation() throws CalculationInterruptedException{
		if (cancel){
			throw new CalculationInterruptedException();
		}
	}
	
	public void setProgressListener(ProgressListener listener){
		this.listener = listener;
	}

	private void sendListenerMessage(int period) {
		if (listener != null){
			listener.handleEvent(period);
		}
	}
}

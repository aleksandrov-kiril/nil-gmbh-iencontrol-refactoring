/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import java.util.Collection;
import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.util.Numbers;

/**
 * @author Jan Siebel, Hendro Wicaksono
 * 
 */
public class MixFlexibility implements Observer {
	
	/**
	 * Calculates the mix flexibility of a {@link FactoryNode}.
	 * @param factoryNode the {@link FactoryNode} for which the mix flexibility
	 *        point is calculated
	 * @return the mix flexibility value
	 */
	public static double getMixFlexibility(FactoryNode factoryNode){
		return getSqMixFlexibility(factoryNode);
	}
	
	/**
	 * Returns an explanation for the mix flexibility value.
	 * @param factoryNode the {@link FactoryNode} for which the mix flexibility
	 *        is calculated
	 * @return the explanation
	 */
	public static String getMixFlexibilityReason(FactoryNode factoryNode){
		return getSqMixFlexibilityReason(factoryNode);
	}
	
	/**
	 * Returns this node's mix flexibility, calculated using the minimum profit.
	 * @param factoryNode the {@link FactoryNode} for which the mix flexibility
	 *        is calculated
	 * @return the mix flexibility value
	 */
	public static double getMinMixFlexibility(FactoryNode factoryNode){
		Collection<Product> products = getMixFlexibilityProductList(factoryNode);
		
		if (products.size() <= 1){
			return 0;
		}else{
			double result = Double.POSITIVE_INFINITY;
			for (Product p : products){
				result = Math.min(result, ZeroProduction.getZeroProductionProfit(factoryNode, p));
			}
			return Math.max(0, result / OptimalProduction.getHighestOptimalProduction(factoryNode.getFactory()).getProfit());
		}
	}
	
	/**
	 * Returns an explanation for the min-max mix flexibility value.
	 * @param factoryNode the {@link FactoryNode} for which the mix flexibility
	 *        is calculated
	 * @return the explanation
	 */
	public static String getMinMixFlexibilityReason(FactoryNode factoryNode){
		Collection<Product> products = getMixFlexibilityProductList(factoryNode);
		
		if (products.size() <= 1){
			return "Only one product";
		}else{
			double opt = Double.POSITIVE_INFINITY;
			String result = "";
			for (Product p : products){
				double zeroProductionProfit = ZeroProduction.getZeroProductionProfit(factoryNode, p);
				if (zeroProductionProfit <= opt){
					if (zeroProductionProfit == opt){
						result += ", " + p.getName();
					}else{
						result = p.getName();
						opt = zeroProductionProfit;
					}
				}
			}
			return result;
		}
	}
	
	/**
	 * Returns this node's mix flexibility, calculated using the average profit.
	 * @param factoryNode the {@link FactoryNode} for which the mix flexibility
	 *        is calculated
	 * @return the mix flexibility value
	 */
	public static double getAvgMixFlexibility(FactoryNode factoryNode){
		Collection<Product> products = getMixFlexibilityProductList(factoryNode);
		
		if (products.size() <= 1){
			return 0;
		}else{
			double result = 0;
			for (Product p : products){
				result += ZeroProduction.getZeroProductionProfit(factoryNode, p);
			}
			result /= products.size();
			return Math.max(0, result / OptimalProduction.getHighestOptimalProduction(factoryNode.getFactory()).getProfit());
		}
	}
	
	/**
	 * Returns an explanation for the average mix flexibility value.
	 * @param factoryNode the {@link FactoryNode} for which the mix flexibility
	 *        is calculated
	 * @return the explanation
	 */
	public static String getAvgMixFlexibilityReason(FactoryNode factoryNode){
		Collection<Product> products = getMixFlexibilityProductList(factoryNode);
		
		if (products.size() <= 1){
			return "No products";
		}else{
			double result = 0;
			for (Product p : products){
				result += ZeroProduction.getZeroProductionProfit(factoryNode, p);
			}
			result /= products.size();
			return "Average: " + Numbers.formatMoney(result) + ", Optimum: " + Numbers.formatMoney(OptimalProduction.getHighestOptimalProduction(factoryNode.getFactory()).getProfit());
		}
	}
	
	/**
	 * Returns this node's mix flexibility, calculated using the average squared
	 * profit.
	 * @param factoryNode the {@link FactoryNode} for which the mix flexibility
	 *        is calculated
	 * @return the mix flexibility value
	 */
	public static double getSqMixFlexibility(FactoryNode factoryNode){
		Collection<Product> products = getMixFlexibilityProductList(factoryNode);
		
		if (products.size() <= 1){
			return 0;
		}else{
			double opt = OptimalProduction.getHighestOptimalProduction(factoryNode.getFactory()).getProfit();
			if (opt < 0){
				return Double.NaN;
			}else{
				double result = 0;
				for (Product p : products){
					double d = ZeroProduction.getZeroProductionProfit(factoryNode, p) - opt;
					result += d * d;
				}
				result = Math.sqrt(result / products.size());
				result = 1 - result / opt;
				return Math.max(0, result);
			}
		}
	}
	
	/**
	 * Returns an explanation for the square mix flexibility value.
	 * @param factoryNode the {@link FactoryNode} for which the mix flexibility
	 *        is calculated
	 * @return the explanation
	 */
	public static String getSqMixFlexibilityReason(FactoryNode factoryNode){
		Collection<Product> products = getMixFlexibilityProductList(factoryNode);
		
		if (products.size() <= 1){
			return "Only one product";
		}else{
			double opt = OptimalProduction.getHighestOptimalProduction(factoryNode.getFactory()).getProfit();
			if (opt <= 0){
				return "Negative profit at optimal production";
			}else{
				double result = 0;
				for (Product p : products){
					double d = ZeroProduction.getZeroProductionProfit(factoryNode, p) - opt;
					result += d * d;
				}
				result = Math.sqrt(result / products.size());
				return "Average: " + Numbers.formatMoney(opt - result) + ", Optimum: " + Numbers.formatMoney(opt);
			}
		}
	}
	
	/**
	 * The list of all products that are regarded in mix flexibility
	 * calculations.
	 * @param factoryNode the {@link FactoryNode} for which the product list is
	 *        created
	 * @return a collection of products
	 */
	private static Collection<Product> getMixFlexibilityProductList(FactoryNode factoryNode){
		// return getProductListNonComponent();
		// return getOptimalProductionPlan().getCO().getFinalComponents();
		// ShiftSchedule shiftplan = getFactory().getCO().getDefaultShiftplan();
		// return calculateNonprofitableRatioCapacity(shiftplan,
		// false).getCO().getFinalComponents();
		
		/*
		 * easier way, not correct if intermediate products are used both
		 * inside and outside the FactoryNode.
		 */
		return EntityList.getFinalComponentList(factoryNode);
	}
	
	@Override
	public void update(Observable o, Object arg){
	}
}

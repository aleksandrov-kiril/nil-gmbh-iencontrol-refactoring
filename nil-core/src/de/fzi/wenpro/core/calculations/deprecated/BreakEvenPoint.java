/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.calculations.CostCalculations;
import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.calculations.ProductionTime;
import de.fzi.wenpro.core.calculations.RatioCapacity;
import de.fzi.wenpro.core.calculations.RatioMatrix;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.HashMap2d;
import de.fzi.wenpro.util.Matrix;
import de.fzi.wenpro.util.Simplex;
import de.fzi.wenpro.util.Vector;

/**
 * @author Jan Siebel, Hendro Wicaksono
 * 
 */
public class BreakEvenPoint implements Observer {
	
	private static BreakEvenPoint singleton = new BreakEvenPoint();
	private static HashMap2d<FactoryNode, ShiftSchedule, ProductPlan> cache = new HashMap2d<FactoryNode, ShiftSchedule, ProductPlan>();

	/**
	 * Calculates the break even production of the factory, that is the
	 * profitable plan with the least piece numbers. Scrap rates and
	 * malfunctioning times are regarded.
	 * @param factoryNode the {@link FactoryNode} for which the break even point
	 *        is calculated
	 * @param shiftplan The {@link ShiftSchedule} the calculation is based on.
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getBreakEvenPoint(FactoryNode factoryNode, ShiftSchedule shiftplan){
		ProductPlan result = cache.get(factoryNode, shiftplan);
		
		if (result == null){
			if (factoryNode instanceof Factory){
				result = calculate((Factory)factoryNode, shiftplan);
			}else{
				result = calculate((FactoryNode)factoryNode, shiftplan);
			}
			cache.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		
		return result;
	}

	private static ProductPlan calculate(FactoryNode factoryNode, ShiftSchedule shiftplan){
		// Create a list of all FactoryNodes on the same level, where all
		// Workplaces are considered to be at level 3.
		Factory factory = factoryNode.getFactory();
		List<FactoryNode> factoryNodes;
		if (factoryNode instanceof Workplace){
			factoryNodes = new ArrayList<FactoryNode>();
			factoryNodes.addAll(EntityList.getWorkplaceList(factory));
		}else{
			int level = factoryNode.getLevel();
			factoryNodes = EntityList.getSubnodeList(factory, level, level);
			factoryNodes.removeAll(EntityList.getWorkplaceList(factory));
		}
		
		ProductPlan ratioCapacity = RatioCapacity.getRatioCapacity(factory, shiftplan);
		if (ratioCapacity.isValid()){
			double[][] productMeanCost = new double[factoryNodes.size() + 1][];
			for (int i = 0; i < factoryNodes.size(); i++){
				productMeanCost[i] = CostCalculations.getMeanCostByProduct(factoryNodes.get(i), shiftplan, ratioCapacity);
			}
			productMeanCost[factoryNodes.size()] = CostCalculations.getPurchasedMeanCostByProduct(factory, ratioCapacity);
			Matrix.normalizeColumns(productMeanCost);
			Matrix.scaleColumns(productMeanCost, getBreakEvenPoint(factory, shiftplan).getVolumeByProduct());
			
			int factoryNodeIndex = 0;
			int operationOffset = 0;
			List<double[]> ratioMatrix = new ArrayList<double[]>();
			for (FactoryNode f : factoryNodes){
				int productIndex = 0;
				for (Product product : factory.getProducts()){
					if (productMeanCost[factoryNodeIndex][productIndex] != 0){
						double[] r = new double[EntityList.countOperations(factory) + 1];
						int pi = operationOffset;
						for (Operation op : EntityList.getOperationList(f)){
							for (ProductQuantity pc : op.getOutputs()){
								if (pc.getProduct().equals(product)){
									// Only (1-p.scrapRate) of the pieces produced
									// in p come to take part in the ratio.
									if (op.isPurchased()){
										r[pi] = -pc.getQuantity();
									}else{
										r[pi] = -pc.getQuantity() * (1 - ((InternalOperation)op).getScrapRate());
									}
								}
							}
							if (op.getOutputProducts().contains(product)){
							}
							pi++;
						}
						r[r.length - 1] = productMeanCost[factoryNodeIndex][productIndex];
						
						ratioMatrix.add(r);
					}
					productIndex++;
				}
				factoryNodeIndex++;
				operationOffset += EntityList.countOperations(f);
			}
			/* Purchased products: */
			int operationIndex = 0;
			for (Operation operation : EntityList.getOperationList(factory)){
				if (operation.isPurchased()){
					double[] r = new double[EntityList.countOperations(factory) + 1];
					r[operationIndex] = -1;
					r[r.length - 1] = 0;
					for (ProductQuantity pq : operation.getOutputs()){
						r[r.length - 1] += pq.getQuantity() * productMeanCost[factoryNodeIndex][pq.getProduct().getProductIndex()];
					}
					ratioMatrix.add(r);
				}
				operationIndex++;
			}
			
			Simplex s = new Simplex(
					EntityList.countOperations(factory) + 1, EntityList.countWorkplace(factory) + 1,

					Matrix.append(
							ProductionTime.getTimeMatrix(factory),
							Vector.scale(CostCalculations.getProfitByProduces(factory, shiftplan), -1)
							),
					Vector.append(ProductionTime.getTimeLimitVector(factory, shiftplan, false), -CostCalculations.getConstantCost(factory, shiftplan)),
					Vector.block(EntityList.countOperations(factory), -1, 1, 0),
					ratioMatrix.toArray(new double[ratioMatrix.size()][]) // getRatioMatrix(shiftplan)
			);
			ProductPlan result = new ProductPlan(Vector.resize(s.solve(), EntityList.countOperations(factory)), factory, shiftplan);
			
			for (FactoryNode f : factoryNodes){
				cache.put(f, shiftplan, result.getSubProductPlan(f));
			}
		}else{
			for (FactoryNode f : factoryNodes){
				cache.put(f, shiftplan, ProductPlan.getInvalid());
			}
		}
		return cache.get(factoryNode, shiftplan);
	}
		
	private static ProductPlan calculate(Factory factory, ShiftSchedule shiftplan){
		Simplex s = new Simplex(
				EntityList.countOperations(factory) + 1, EntityList.countWorkplace(factory) + 1,

				Matrix.append(
						ProductionTime.getTimeMatrix(factory),
						Vector.scale(CostCalculations.getProfitByProduces(factory, shiftplan), -1)
						),
				Vector.append(ProductionTime.getTimeLimitVector(factory, shiftplan, false), -CostCalculations.getConstantCost(factory, shiftplan)),
				Vector.block(EntityList.countOperations(factory), -1, 1, 0),
				RatioMatrix.getRatioMatrix(factory, shiftplan)
				);
		return new ProductPlan(Vector.resize(s.solve(), EntityList.countOperations(factory)), factory, shiftplan);
	}
	
	/**
	 * Return the product plan with the smallest piece number sum of all
	 * shiftplan's break even points.
	 * @param factoryNode the {@link FactoryNode} for which the break even point
	 *        is returned
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getLeastBreakEvenPoint(FactoryNode factoryNode){
		ProductPlan result = ProductPlan.getInvalid();
		double min = Double.POSITIVE_INFINITY;
		for (ShiftSchedule s : factoryNode.getFactory().getShiftplans()){
			ProductPlan p = getBreakEvenPoint(factoryNode, s);
			if (p.isValid()){
				double val = p.getTotalVolume();
				if (val < min){
					min = val;
					result = p;
				}
			}
		}
		return result;
	}
	
	@Override
	public void update(Observable o, Object arg){
		cache.remove(o);
		o.deleteObserver(this);
	}
}

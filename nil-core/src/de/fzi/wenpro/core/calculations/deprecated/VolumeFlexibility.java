/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.calculations.RatioCapacity;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;

/**
 * @author Jan Siebel, Hendro Wicaksono
 * 
 */
public class VolumeFlexibility implements Observer {
	
	/**
	 * Calculates the volume flexibility of a {@link FactoryNode}.
	 * @param factoryNode the {@link FactoryNode} for which the volume
	 *        flexibility is calculated
	 * @return the volume flexibility value
	 */
	public static double getVolumeFlexibility(FactoryNode factoryNode){
		ProductPlan maxCapacity  = RatioCapacity.getHighestRatioCapacity(factoryNode);
		ProductPlan minBreakEven = BreakEvenPoint.getLeastBreakEvenPoint(factoryNode);
		
		if(maxCapacity.isValid() && minBreakEven.isValid()){
			double maxCapacityValue  = maxCapacity.getTotalVolume();
			double minBreakEvenValue = minBreakEven.getTotalVolume();
			if (maxCapacityValue != 0){
				return Math.max(0, Math.min(1,(maxCapacityValue-minBreakEvenValue)/maxCapacityValue));
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	
	public static double getVolumeFlexibilityMean(FactoryNode factoryNode){
		double sum = 0;
		int count = 0;
		for (FactoryNode f : EntityList.getSubnodeList(factoryNode)){
			sum += getVolumeFlexibility(f);
			count++;
		}
		return sum/count;
	}

	public static double getVolumeFlexibilityStandardDeviation(FactoryNode factoryNode){
		double mean = getVolumeFlexibilityMean(factoryNode);
		double sum = 0;
		int count = 0;
		for (FactoryNode f : EntityList.getSubnodeList(factoryNode)){
			double d = getVolumeFlexibility(f) - mean; 
			sum += d*d;
			count++;
		}
		return Math.sqrt(sum/count);
	}
	
	@Override
	public void update(Observable o, Object arg){
	}
}

/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.calculations.RatioCapacity;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;

/**
 * @author Jan Siebel, Hendro Wicaksono
 * 
 */
public class ExpansionFlexibility implements Observer {
	
	/**
	 * Calculates the expansion flexibility of a {@link Factory}.
	 * @param factory the {@link Factory} for which the expansion flexibility is
	 *        calculated
	 * @return the expansion flexibility value
	 */
	public static double getExpansionFlexibility(Factory factory){
		if (factory.getExpansionFlexibilityNode() == null || factory.getExpansionFlexibilityNode().getOriginal() == null){
			return Double.NaN;
		}else{
			FactoryNode factoryNode = factory.getExpansionFlexibilityNode();
			FactoryNode original = factoryNode.getOriginal();
			
			double flexibilityTarget = factory.getExpansionFlexibilityTarget();
			double originalVolFlex = VolumeFlexibility.getVolumeFlexibility(original);
			double realMaximum = RatioCapacity.getHighestRatioCapacity(factoryNode).getTotalVolume();
			double targetMaximum = flexibilityTarget * RatioCapacity.getHighestRatioCapacity(original).getTotalVolume();
			double breakEven = BreakEvenPoint.getLeastBreakEvenPoint(factoryNode).getTotalVolume();
			
			if (Double.isNaN(realMaximum) || Double.isNaN(targetMaximum) || Double.isNaN(breakEven)){
				return 0;
			}else if(realMaximum<targetMaximum){
				return 0;
			}else if (targetMaximum == Double.NEGATIVE_INFINITY || breakEven == Double.POSITIVE_INFINITY || targetMaximum==0){
				return 0;
			}else if(originalVolFlex == 0){
				return (targetMaximum>breakEven) ? Double.POSITIVE_INFINITY : 0;
			}else{
				return (targetMaximum-breakEven)/targetMaximum / originalVolFlex;
			}
		}
	}
	
	@Override
	public void update(Observable o, Object arg){
	}
}

/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations.deprecated;

import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.calculations.CostCalculations;
import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.calculations.ProductionTime;
import de.fzi.wenpro.core.calculations.RatioMatrix;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.HashMap2d;
import de.fzi.wenpro.util.Matrix;
import de.fzi.wenpro.util.Simplex;
import de.fzi.wenpro.util.Vector;

/**
 * @author Jan Siebel
 * 
 */
public class MaximumProduction implements Observer {
	
	private static MaximumProduction singleton = new MaximumProduction();
	private static HashMap2d<FactoryNode, Product, ProductPlan> maximumProductionCache = new HashMap2d<FactoryNode, Product, ProductPlan>();

	/**
	 * Calculates the maximum production of the factory, i.e. the factory's maximal
	 * profit if a given product is produced as much as possible in a given node.
	 * @param factoryNode the {@link FactoryNode} for which the maximum production
	 *        is calculated
	 * @param product the product that is not produced
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getMaximumProduction(FactoryNode factoryNode, Product product){
		ProductPlan result = maximumProductionCache.get(factoryNode, product);
		
		if (result == null){
			result = calculateMaximumProduction((Factory)factoryNode, product);
			maximumProductionCache.put(factoryNode, product, result);
			factoryNode.addObserver(singleton);
		}
		
		return result;
	}
	
	/**
	 * Calculates the factory's maximal profit if a given product isn't 
	 * produced in a given node.
	 */
	private static ProductPlan calculateMaximumProduction(FactoryNode factoryNode, Product product){
		Factory factory = factoryNode.getFactory();
		ShiftSchedule shiftplan = factory.getDefaultShiftplan();
		double[] maximize = new double[EntityList.countOperations(factory)];
		int i = 0;
		for (Operation op : EntityList.getOperationList(factory)){	
			if (op.getOutputProducts().contains(product)){
				maximize[i] = 1;
			}
			i++;			
		}
		
		Simplex s = new Simplex(
				EntityList.countOperations(factory), EntityList.countWorkplace(factory)+EntityList.countDisposableProducts(factory),

				Matrix.merge(ProductionTime.getTimeMatrix(factory), RatioMatrix.getConditionMatrixDisposable(factory, shiftplan)),
				ProductionTime.getTimeLimitVector(factory, shiftplan, false),
				maximize, 
				RatioMatrix.getRatioMatrixNonDisposable(factory, shiftplan)
				);
		s.solve();		
		return new ProductPlan(s.improve(Vector.resize(CostCalculations.getProfitByProduces(factory, shiftplan), EntityList.countOperations(factory))), factory, shiftplan);
	}
	
	@Override
	public void update(Observable o, Object arg){
		maximumProductionCache.remove(o);
		o.deleteObserver(this);
	}
}

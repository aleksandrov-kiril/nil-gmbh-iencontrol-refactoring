/**
 * ecoFLEXiP 2010
 * de.fzi.wenpro.core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.HashMap2d;
import de.fzi.wenpro.util.Vector;

/**
 * @author Jan Siebel, Hendro Wicaksono
 *
 */
public class RatioMatrix implements Observer{

	private static RatioMatrix singleton = new RatioMatrix();
	
	private static HashMap2d<FactoryNode, ShiftSchedule, double[][]> ratioMatrixComponents = new HashMap2d<FactoryNode, ShiftSchedule, double[][]>();
	private static HashMap2d<FactoryNode, ShiftSchedule, double[][]> ratioMatrixComponentsTh = new HashMap2d<FactoryNode, ShiftSchedule, double[][]>();
	private static HashMap2d<FactoryNode, ShiftSchedule, double[][]> ratioMatrixNoComponents = new HashMap2d<FactoryNode, ShiftSchedule, double[][]>();
	private static HashMap2d<FactoryNode, ShiftSchedule, double[][]> ratioMatrixNoComponentsTh = new HashMap2d<FactoryNode, ShiftSchedule, double[][]>();
	private static HashMap2d<FactoryNode, ShiftSchedule, double[]> productRatio = new HashMap2d<FactoryNode, ShiftSchedule, double[]>();

	public static double[][] getRatioMatrix(FactoryNode factoryNode, ShiftSchedule shiftplan){
		if (factoryNode instanceof Factory){
			return getRatioMatrix(factoryNode, shiftplan, true, true);
		}else{
			return getRatioMatrix(factoryNode, shiftplan, false, true);
		}
	}


	/**
	 * A matrix which combines the restrictions from the ratio
	 * given by {@link #getProductRatio}. The scrap rates can be
	 * included as well as the component information. A piece number
	 * vector multiplied on this matrix results in zero if the
	 * ratio conditions are satisfied.
	 * @param shiftplan: The shiftplan the calculation is based on.
	 * @param addComponents: Adds information about component relations
	 *        to the matrix.
	 * @param useScrapRates: Sets the matrix to implicate the extra
	 *        production caused by the scrap rates.
	 * @return A two dimensional array representing the matrix.
	 */
	public static double[][] getRatioMatrix(FactoryNode factoryNode, ShiftSchedule shiftplan, boolean addComponents, boolean useScrapRates){
		HashMap2d<FactoryNode, ShiftSchedule, double[][]> cache;
		if (addComponents){
			cache = useScrapRates ? ratioMatrixComponents : ratioMatrixComponentsTh;
		}else{
			cache = useScrapRates ? ratioMatrixNoComponents : ratioMatrixNoComponentsTh;
		}

		double[][] result = cache.get(factoryNode, shiftplan);
		if (result == null){
			result = calculateRatioMatrix(factoryNode, shiftplan, addComponents, useScrapRates);
			cache.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}
	
	/**
	 * The product ratio for a factoryNode. The ratio for non-factory nodes is
	 * calculated by the volumes produced at the ancestor's ratio capacity.
	 * @param factoryNode a {@link FactoryNode}
	 * @param shiftplan The shiftplan the calculation is based on.
	 * @return The ratio array, indexed by products.
	 */
	public static double[] getProductRatio(FactoryNode factoryNode, ShiftSchedule shiftplan){
		double[] result = productRatio.get(factoryNode, shiftplan);
		if (result == null){
			result = calculateProductRatio(factoryNode, shiftplan);
			productRatio.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}
	
	public static double[][] calculateRatioMatrix(FactoryNode factoryNode, ShiftSchedule shiftplan, boolean addComponents, boolean useScrapRates){
		List<Operation> operationList = EntityList.getOperationList(factoryNode);
		int nOperation = operationList.size();
		double[][] result = new double[ factoryNode.getFactory().getProducts().size()][nOperation+1];
		int i=0;
		for (Operation op : operationList){
			double scrapFactor = 
					useScrapRates && !op.isPurchased() ?
					// Only (1-p.scrapRate) of the pieces produced in p come to
					// take part in the ratio.
					1 - ((InternalOperation) op).getScrapRate() : 
					1;
			for (ProductQuantity pq : op.getOutputs()){
				result[pq.getProduct().getProductIndex()][i] = -scrapFactor;
			}
			if(addComponents && !op.isPurchased()){
				// An entry n on (k, i) means that each piece of the product
				// made in i consumes n pieces of product k
				for (ProductQuantity pq : op.getOutputs()){
					result[pq.getProduct().getProductIndex()][i] = pq.getQuantity();
				}
			}
			
			i++;
		}
		
		double[] productRatio = getProductRatio(factoryNode, shiftplan);
		for(int j=0; j<factoryNode.getFactory().getProducts().size(); j++){
			result[j][nOperation] = productRatio[j];
		}

		return result;
	}

	private static double[] calculateProductRatio(FactoryNode factoryNode, ShiftSchedule shiftplan){
		double[] result;
		if (factoryNode instanceof Factory){
			result = new double[factoryNode.getFactory().getProducts().size()];	
			int i=0;
			for (Product p : factoryNode.getFactory().getProducts()){
				result[i++] = p.getRatio();
			}	
		}else{
			FactoryNode f = factoryNode;
			while (f.getParent() != null){
				f = f.getParent();
				ProductPlan p = RatioCapacity.getRatioCapacity(f, shiftplan).getSubProductPlan(factoryNode);
				if (p.isValid() && !Vector.isZeroVector(p.getVolume())){
					return p.getVolumeByProduct();
				}
			}
			// if the FactoryNode doesn't produce anything in any ancestor ratio capacity, produce everything equally:
			result = new double[factoryNode.getFactory().getProducts().size()];
			for (Operation op : EntityList.getOperationList(factoryNode)){
				for (ProductQuantity pq : op.getOutputs()){
					result[pq.getProduct().getProductIndex()] = 1;
				}
			}
		}
		return result;
	}

	/**
	 * Creates a matrix which contains the conditions derived from the
	 * information that non-disposable products may not be produced over the
	 * number used as intermediate products.
	 * @param factory the factory for which the matrix is created
	 * @param shiftplan: the {@link ShiftSchedule} the calculation is based on
	 * @return the ratio matrix
	 */
	public static double[][] getRatioMatrixNonDisposable(Factory factory, ShiftSchedule shiftplan){
		double[][] result = new double[factory.getProducts().size()-EntityList.countDisposableProducts(factory)][EntityList.countOperations(factory)];
		double[][] ratio = getRatioMatrix(factory, shiftplan);
		int i=0, j=0;
		for (Product p : factory.getProducts()){
			if(!p.isDisposable())
				System.arraycopy(ratio[i], 0, result[j++], 0, EntityList.countOperations(factory));
			i++;
		}
		return result;
	}

	/**
	 * Creates a matrix which contains the conditions derived from the
	 * information that the piece number of disposable products must be high
	 * enough to cover the usage as intermediate products.
	 * @param factory the factory for which the matrix is created
	 * @param shiftplan: the {@link ShiftSchedule} the calculation is based on
	 * @return the ratio matrix
	 */
	public static double[][] getConditionMatrixDisposable(Factory factory, ShiftSchedule shiftplan){
		double[][] result = new double[EntityList.countDisposableProducts(factory)][EntityList.countOperations(factory)];
		double[][] ratio = getRatioMatrix(factory, shiftplan);
		int i=0, j=0;
		for (Product p : factory.getProducts()){
			if(p.isDisposable())
				System.arraycopy(Vector.scale(ratio[i],1), 0, result[j++], 0, EntityList.countOperations(factory));
			i++;
		}
		return result;
	}
	@Override
	public void update(Observable o, Object arg){
		ratioMatrixComponents.remove(o);
		ratioMatrixComponentsTh.remove(o);
		ratioMatrixNoComponents.remove(o);
		ratioMatrixNoComponentsTh.remove(o);
		o.deleteObserver(this);
	}
}

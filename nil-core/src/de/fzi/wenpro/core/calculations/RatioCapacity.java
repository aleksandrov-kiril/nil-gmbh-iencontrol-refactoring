/**
 * ecoFLEXiP 2010
 * de.fzi.de.fzi.wenpro..core.model.operations.BreakEvenPoint.java
 */
package de.fzi.wenpro.core.calculations;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.HashMap2d;
import de.fzi.wenpro.util.Matrix;
import de.fzi.wenpro.util.Simplex;
import de.fzi.wenpro.util.Vector;

/**
 * @author Jan Siebel, Hendro Wicaksono
 * 
 */
public class RatioCapacity implements Observer {
	
	private static RatioCapacity singleton = new RatioCapacity();
	
	private static HashMap2d<FactoryNode, ShiftSchedule, ProductPlan> profitableRatioCapacityCache = new HashMap2d<FactoryNode, ShiftSchedule, ProductPlan>();
	private static HashMap2d<FactoryNode, ShiftSchedule, ProductPlan> theoreticalProfitableRatioCapacityCache = new HashMap2d<FactoryNode, ShiftSchedule, ProductPlan>();
	private static HashMap2d<FactoryNode, ShiftSchedule, ProductPlan> nonProfitableRatioCapacityCache = new HashMap2d<FactoryNode, ShiftSchedule, ProductPlan>();
	private static HashMap2d<FactoryNode, ShiftSchedule, ProductPlan> theoreticalNonProfitableRatioCapacityCache = new HashMap2d<FactoryNode, ShiftSchedule, ProductPlan>();
	
	/**
	 * Returns the number of produced pieces for each product,
	 * on ratio capacity production.
	 * @param shiftplan: the {@link ShiftSchedule} the calculation is based on
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getRatioCapacity(FactoryNode factoryNode, ShiftSchedule shiftplan){
		if (factoryNode instanceof Factory){
			return getProfitableRatioCapacity(factoryNode, shiftplan);
		}else{
			return getNonProfitableRatioCapacity(factoryNode, shiftplan);
		}
	}
	
	/**
	 * Calculates the ratio capacity, using the given
	 * shiftplan. Non-profitable product plans are not allowed.
	 * @param shiftplan a {@link ShiftSchedule}
	 * @param theoretical if this is <code>true</code>, malfunctioning times
	 *        and scrap rates are ignored
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getProfitableRatioCapacity(FactoryNode factoryNode, ShiftSchedule shiftplan){
		ProductPlan result = profitableRatioCapacityCache.get(factoryNode, shiftplan);
		
		if (result == null){
			result = calculateProfitableRatioCapacity(factoryNode, shiftplan, false);
			profitableRatioCapacityCache.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}
	
	/**
	 * Calculates the theoretical ratio capacity, using the given
	 * shiftplan. Non-profitable product plans are not allowed.
	 * @param shiftplan a {@link ShiftSchedule}
	 * @param theoretical if this is <code>true</code>, malfunctioning times
	 *        and scrap rates are ignored
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getTheoreticalProfitableRatioCapacity(FactoryNode factoryNode, ShiftSchedule shiftplan){
		ProductPlan result = profitableRatioCapacityCache.get(factoryNode, shiftplan);
		
		if (result == null){
			result = calculateProfitableRatioCapacity(factoryNode, shiftplan, true);
			theoreticalProfitableRatioCapacityCache.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}
	
	/**
	 * Calculates the non-profitable ratio capacity, using the given
	 * shiftplan. Non-profitable product plans are not allowed.
	 * @param shiftplan a {@link ShiftSchedule}
	 * @param theoretical if this is <code>true</code>, malfunctioning times
	 *        and scrap rates are ignored
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getNonProfitableRatioCapacity(FactoryNode factoryNode, ShiftSchedule shiftplan){
		ProductPlan result = nonProfitableRatioCapacityCache.get(factoryNode, shiftplan);
		
		if (result == null){
			result = calculateNonProfitableRatioCapacity(factoryNode, shiftplan, false);
			nonProfitableRatioCapacityCache.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}
	
	/**
	 * Calculates the theoretical non-profitable ratio capacity, using the given
	 * shiftplan. Non-profitable product plans are not allowed.
	 * @param shiftplan a {@link ShiftSchedule}
	 * @param theoretical if this is <code>true</code>, malfunctioning times
	 *        and scrap rates are ignored
	 * @return the resulting {@link ProductPlan}
	 */
	public static ProductPlan getTheoreticalNonProfitableRatioCapacity(FactoryNode factoryNode, ShiftSchedule shiftplan){
		ProductPlan result = theoreticalNonProfitableRatioCapacityCache.get(factoryNode, shiftplan);
		
		if (result == null){
			result = calculateNonProfitableRatioCapacity(factoryNode, shiftplan, true);
			theoreticalNonProfitableRatioCapacityCache.put(factoryNode, shiftplan, result);
			factoryNode.addObserver(singleton);
		}
		return result;
	}
	
	/**
	 * Return the product plan with the highest piece number sum of all
	 * shiftplan's break even points.
	 */
	public static ProductPlan getHighestRatioCapacity(FactoryNode factoryNode){
		ProductPlan result = ProductPlan.getInvalid();
		double max = Double.NEGATIVE_INFINITY;
		for (ShiftSchedule s : factoryNode.getFactory().getShiftplans()){
			ProductPlan p = getRatioCapacity(factoryNode, s);
			if (p.isValid()){
				double val = p.getTotalVolume();
				if (val > max){
					max = val;
					result = p;
				}
			}
		}
		return result;
	}
	
	/**
	 * Calculates the theoretical or real ratio capacity, using the given
	 * shiftplan, with the given maximum of available workers. The results
	 * are not balanced (see createEqualizationConstraints).
	 * @param shiftplan a shiftplan
	 * @param theoretical if this is <code>true</code>, malfunctioning times
	 *        and scrap rates are ignored
	 * @param maximumWorkers the maximum of available workers
	 * @return the optimal ratio product plan for the given parameters
	 */	
	public static ProductPlan getRatioCapacity(FactoryNode factoryNode, ShiftSchedule shiftplan, boolean theoretical, double maximumWorkers){
		Simplex s = new Simplex(
				EntityList.countOperations(factoryNode) + 1, EntityList.countWorkplace(factoryNode) + 1,

				Matrix.append(
						ProductionTime.getTimeMatrix(factoryNode),
						ProductionTime.getProductionTimes(factoryNode)
						),
				Vector.append(
						ProductionTime.getTimeLimitVector(factoryNode, shiftplan, theoretical),
						maximumWorkers * shiftplan.getSecondsPerPeriod()
						),
				Vector.constant(EntityList.countOperations(factoryNode), 1),
				RatioMatrix.getRatioMatrix(factoryNode, shiftplan)
				);
		return new ProductPlan(Vector.resize(s.solve(), EntityList.countOperations(factoryNode)), factoryNode, shiftplan);
	}
	
	/**
	 * @param factoryNode
	 * @param shiftplan
	 * @return
	 */
	private static ProductPlan calculateProfitableRatioCapacity(FactoryNode factoryNode, ShiftSchedule shiftplan, boolean theoretical){
		/* Step 1: find any cost-optimal solution */
		int nOperation = EntityList.countOperations(factoryNode);
		Simplex s = new Simplex(
				nOperation + 1, EntityList.countWorkplace(factoryNode) + 1,

				Matrix.append(
						ProductionTime.getTimeMatrix(factoryNode),
						Vector.scale(theoretical ? CostCalculations.getTheoreticalProfitByProduces(factoryNode, shiftplan) : CostCalculations.getProfitByProduces(factoryNode, shiftplan), -1)
						),
				Vector.append(ProductionTime.getTimeLimitVector(factoryNode, shiftplan, theoretical), -CostCalculations.getConstantCost(factoryNode, shiftplan)),
				Vector.block(nOperation, 1, 1, 0),
				RatioMatrix.getRatioMatrix(factoryNode, shiftplan)
				);
		double[] firstResult = s.solve();
		if (firstResult == null){
			return new ProductPlan(null, factoryNode, shiftplan);
		}else{
			/* Step 2: equalize the piece numbers of equal produces */
			List<double[]> newConstraints = new ArrayList<double[]>();
			double[] improve = new double[nOperation + 1];
			if (createEqualizationConstraints(factoryNode, firstResult, shiftplan, newConstraints, improve)){
				
				s = new Simplex(
						nOperation + 1, EntityList.countWorkplace(factoryNode) + 1 + newConstraints.size(),
						Matrix.merge(
								Matrix.append(
										ProductionTime.getTimeMatrix(factoryNode),
										Vector.scale(theoretical ? CostCalculations.getTheoreticalProfitByProduces(factoryNode, shiftplan) : CostCalculations.getProfitByProduces(factoryNode, shiftplan), -1)
								),
								newConstraints.toArray(new double[0][])
						),
						Vector.resize(
								Vector.append(ProductionTime.getTimeLimitVector(factoryNode, shiftplan, theoretical), -CostCalculations.getConstantCost(factoryNode, shiftplan)),
								EntityList.countWorkplace(factoryNode) + 1 + newConstraints.size()
						),
						Vector.block(nOperation, 1, 1, 0),
						RatioMatrix.getRatioMatrix(factoryNode, shiftplan)
				);
				s.solve();
				
				return new ProductPlan(Vector.resize(s.improve(improve), nOperation), factoryNode, shiftplan);
			}else{
				return new ProductPlan(Vector.resize(firstResult, nOperation), factoryNode, shiftplan);
			}
		}
	}

	private static ProductPlan calculateNonProfitableRatioCapacity(FactoryNode factoryNode, ShiftSchedule shiftplan, boolean theoretical){
		/* Step 1: find any cost-optimal solution */
		int nProduces = EntityList.countOperations(factoryNode);
		Simplex s = new Simplex(
				nProduces + 1, EntityList.countWorkplace(factoryNode),
				ProductionTime.getTimeMatrix(factoryNode),
				ProductionTime.getTimeLimitVector(factoryNode, shiftplan, theoretical),
				Vector.constant(nProduces, 1),
				RatioMatrix.getRatioMatrix(factoryNode, shiftplan)
				);
		double[] firstResult = s.solve();
		if (firstResult == null){
			return new ProductPlan(null, factoryNode, shiftplan);
		}else{
			/* Step 2: equalize the piece numbers of equal produces */
			List<double[]> newConstraints = new ArrayList<double[]>();
			double[] improve = new double[nProduces + 1];
			createEqualizationConstraints(factoryNode, firstResult, shiftplan, newConstraints, improve);
			
			s = new Simplex(
					nProduces + 1, EntityList.countWorkplace(factoryNode) + newConstraints.size(),
					Matrix.merge(
							ProductionTime.getTimeMatrix(factoryNode),
							newConstraints.toArray(new double[0][])
							),
					Vector.resize(
							ProductionTime.getTimeLimitVector(factoryNode, shiftplan, theoretical),
							EntityList.countWorkplace(factoryNode) + newConstraints.size()
							),
					Vector.constant(nProduces, 1),
					RatioMatrix.getRatioMatrix(factoryNode, shiftplan)
					);
			s.solve();
			
			return new ProductPlan(Vector.resize(s.improve(improve), nProduces), factoryNode, shiftplan);
		}
	}
	
	/**
	 * Creates simplex parameters to improve a given production result in the
	 * following way: the total cost and all total production values remain the
	 * same, but within these constraints, the production is balanced to have
	 * equal produces create the same amount of pieces (and not, as the simplex
	 * usually would propose, have one workplace create as much as it can, and
	 * then have the rest produce by other workplaces).
	 * @param productionPlan an unbalanced product plan
	 * @param shiftplan the shiftplan used for the calculation
	 * @param newConstraints the new constraints. The list must be initialized
	 *        and is filled.
	 * @param improve the new improve vector. The array must be initialized,
	 *        and have the size <code>countProduces()+1<code>.
	 * @return <code>true</code> if the improve vector will make a difference
	 *         in the result
	 */
	//TODO: only considers the first output product
	private static boolean createEqualizationConstraints(FactoryNode factoryNode, double[] productionPlan, ShiftSchedule shiftplan, List<double[]> newConstraints, double[] improve){
		double[] cost = CostCalculations.getVariableCosts(factoryNode, shiftplan);
		
		/*
		 * For each product and each distinct cost value, create a list of
		 * produces indices that share that cost value.
		 */
		HashMap2d<Product, Double, List<Integer>> m = new HashMap2d<Product, Double, List<Integer>>();
		int producesIndex = 0;
		List<Integer> list;
		for (Operation op : EntityList.getOperationList(factoryNode)){
			if (m.containsKey(op.getOutputs().get(0).getProduct(), cost[producesIndex])){
				list = m.get(op.getOutputs().get(0).getProduct(), cost[producesIndex]);
			}else{
				list = new ArrayList<Integer>();
				m.put(op.getOutputs().get(0).getProduct(), cost[producesIndex], list);
			}
			list.add(producesIndex);
			producesIndex++;
		}
		
		boolean result = false;
		for (Product product : m.keySet()){
			Map<Double, List<Integer>> line = m.get(product);
			/*
			 * Create new constraints and an optimization vector:
			 * 1. find the produces with the highest piece number
			 * 2. reduce its piece number as far as possible, but without
			 * having any other produces with the same cost and product
			 * produce more than it
			 */
			for (List<Integer> indices : line.values()){
				if (indices.size() > 1){
					result = true;
					double max = Double.NEGATIVE_INFINITY;
					int maxi = -1;
					for (Integer index : indices){
						if (productionPlan[index] > max){
							maxi = index;
							max = productionPlan[index];
						}
					}
					
					for (Integer index : indices){
						if (index != maxi){
							double[] c = new double[EntityList.countOperations(factoryNode) + 1];
							c[index] = 1;
							c[maxi] = -1;
							newConstraints.add(c);
						}
					}
					improve[maxi] = -1;
				}
			}
		}
		return result;
	}
	@Override
	public void update(Observable o, Object arg){
		profitableRatioCapacityCache.remove(o);
		theoreticalProfitableRatioCapacityCache.remove(o);
		nonProfitableRatioCapacityCache.remove(o);
		theoreticalNonProfitableRatioCapacityCache.remove(o);
		o.deleteObserver(this);
	}
}

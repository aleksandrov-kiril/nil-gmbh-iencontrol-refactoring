/**
 * Ecoflex 2009
 * de.fzi.ecoflex.core.model.operations.FactoryOutputOperations.java
 */
package de.fzi.wenpro.core.printing;

import java.text.DecimalFormat;
import java.util.Map;

import de.fzi.wenpro.core.calculations.RatioCapacity;
import de.fzi.wenpro.core.calculations.deprecated.PersonnelAnalysisCalculations;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.Product;

/**
 * @author aleksa
 *
 */
public class FactoryOutputOperations extends FactoryNodeOutputOperations{

	/**
	 * Constructor FactoryOutputOperations
	 * @param node
	 */
	public FactoryOutputOperations(Factory node) {
		super(node);		
	}
	
	public Factory getDO() {
		return (Factory)node;
	}
	
	//////////////////////////////////////////////////////////////////////////
	///////////////////////////// Output /////////////////////////////////////
	

	public void print(String indent){
		super.print(indent);
		System.out.println(indent+"\t"+"Produkte:");
		for (Product p : ((Factory)getDO()).getProducts())
			System.out.println(indent+"\t\t"+p.getName()+"\t"+p);
		System.out.println();
	}
	
	public void createCSVforPersonnelAnalysis(){
		Factory f = getDO();
		String[] modeNames = new String[]{"Keine Teilung","Eingeschr�nkte Teilung","Volle Teilung, ohne Wegezeiten"};
		
		DecimalFormat df = new DecimalFormat("#.000000");
		final double workplaceChangeTime = 4;
		String line;
		
		System.out.println(f.getName() + ";" + df.format(RatioCapacity.getRatioCapacity(f, f.getDefaultShiftplan()).getFinalComponentsVolume()) + " St�ck");
		System.out.print("St�ckzahl;Taktzeit;");
		for (int i=0; i<3; i++){
			System.out.print("Arbeitszeit - "+modeNames[i]+";");
			System.out.print("Personal - "+modeNames[i]+";");
			System.out.print("Springer - "+modeNames[i]+";");
		}
		System.out.println();
		for (int pieces = 1; ; pieces++){
			boolean alldone = true;
			double pace = 0;
			line = "";
			for (int mode = 0; mode < 3; mode++){
				PersonnelAnalysisCalculations p = new PersonnelAnalysisCalculations(f, f.getDefaultShiftplan(), pieces, workplaceChangeTime, mode);
				Map<FactoryNode, double[]> result = p.calculate();
				if (result != null){
					alldone = false;
					pace = p.calculatePace();
					double[] resultLine = result.get(f);
					line += df.format(resultLine[0]) + ";" + resultLine[1] + ";" + p.calculateStandByMen() + ";";
				}else{
					line += ";;;";
				}
			}
			if (alldone){
				break;
			}else{
				System.out.println(pieces + ";" + df.format(pace/60) + ";" + line);
			}
		}
		System.out.println("\n");
	}
	
	
}

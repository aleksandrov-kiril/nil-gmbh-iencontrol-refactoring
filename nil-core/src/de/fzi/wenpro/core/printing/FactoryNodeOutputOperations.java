/**
 * Ecoflex 2009
 * de.fzi.wenpro.core.model.operations.FactoryNodeOutputOperations.java
 */
package de.fzi.wenpro.core.printing;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import de.fzi.wenpro.core.calculations.CostCalculations;
import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.calculations.RatioCapacity;
import de.fzi.wenpro.core.calculations.deprecated.BreakEvenPoint;
import de.fzi.wenpro.core.calculations.deprecated.ExpansionFlexibility;
import de.fzi.wenpro.core.calculations.deprecated.MixFlexibility;
import de.fzi.wenpro.core.calculations.deprecated.OptimalProduction;
import de.fzi.wenpro.core.calculations.deprecated.VolumeFlexibility;
import de.fzi.wenpro.core.calculations.deprecated.ZeroProduction;
import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.ShiftSchedule;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.Numbers;
import de.fzi.wenpro.util.Vector;

/**
 * @author aleksa
 *
 */
public class FactoryNodeOutputOperations {

	protected FactoryNode node;
	
	public FactoryNodeOutputOperations(FactoryNode node){
		this.node=node;
	}
	
	
	//////////////////////////////////////////////////////////////////////////
	///////////////////////////// Output /////////////////////////////////////

	/**
	 * Print information about maximum, capacity and break even production.
	 *
	 */
	public void printInformation(){
		ShiftSchedule shiftplan = getFactory().getDefaultShiftplan();

		printHeadline();
		
		printProducesTableHeader();
		
		/*
		System.out.print("Malfunc\t\t");			
		for (Workplace w : getWorkplaceList())
			System.out.print(w.getMalfunctionTime(shiftplan) + "\t\t");
		System.out.println("\n");
		*/
		
		printCost(shiftplan);
		System.out.println("\n");			
		
		printProductPlans(shiftplan);
		System.out.println("\n");

		
		//factory.printProducesTableHeader();
		//printMixflexibilityInformation();
		//System.out.println("\n");

		//printProductTableHeader();
		//printRatioInformation(shiftplan);
		//System.out.println("\n");
		/*
		printMetrics();
		System.out.println("\n\n");
*/
		/*
		// Ausarbeitung
		if(this == factory){
			Simplex s = new Simplex(
					getTimeMatrix(), 
					getTimeLimitVector(shiftplan,false),
					getProfitByProduces(shiftplan,true),
					factory.getRatioMatrix(shiftplan),
					true, ""
				);
			double profit = getProfit(shiftplan, s.solve(), true);
			System.out.println("Gewinn:\t" + profit);
			}
		*/
	}

	public void printFinalProductInformation(){
		int i;
		
		printHeadline();
		ShiftSchedule shiftplan = getFactory().getDefaultShiftplan();
		System.out.print("\t\t");
		for (Product p : EntityList.getProductList(getDO())){
			if (p.isDisposable()){
				System.out.print(p.getName() + "\t");
			}
		}
		System.out.println();
		
		double[] r = RatioCapacity.getRatioCapacity(getDO(), shiftplan).getVolumeByProductTh();
		System.out.print("Ratio capacity\t");
		i=0;
		if (r==null){
			System.out.print("null");
		}else{
			for (Product p : EntityList.getProductList(getDO())){
				if (p.isDisposable()){
					System.out.print(Math.round(r[i]) + "\t");
				}
				i++;
			}
		}
		System.out.println();
		double[] be = BreakEvenPoint.getBreakEvenPoint(getDO(), shiftplan).getVolumeByProductTh();
		System.out.print("Break even\t");
		i=0;
		if (be==null){
			System.out.print("null");
		}else{
			for (Product p : EntityList.getProductList(getDO())){
				if (p.isDisposable()){
					System.out.print(Math.round(be[i]) + "\t");
				}
				i++;
			}

		}
		System.out.println();
	}
	
	
	public void printHeadline() {
		System.out.println("\n\n\n_____ "+getName()+" ________________________________________________________________________");
	}

	/**
	 * Prints the result of the main metrics.
	 */
	public void printMetrics(){
		System.out.println("Volume flexibility:\t" + 100*VolumeFlexibility.getVolumeFlexibility(getDO()) + " %");
		System.out.println("Mix flexibility:\t" + MixFlexibility.getMixFlexibility(getDO()) + " %");
		if(this.getDO() instanceof Factory)
			System.out.println("Expansion flexibility:\t" + 100*ExpansionFlexibility.getExpansionFlexibility((Factory)this.getDO()) + " %");
	}
	
	
	
	/**
	 * Prints relevant product plans.
	 */
	public void printProductPlans(ShiftSchedule shiftplan){
		RatioCapacity.getRatioCapacity(getDO(), shiftplan).getOO().print("ratioCapacity");
		Vector.print("load (cap)", RatioCapacity.getRatioCapacity(getFactory(), shiftplan).getLoad(), " %");
		System.out.println();
		BreakEvenPoint.getBreakEvenPoint(getDO(), shiftplan).getOO().print("breakEven");
		Vector.print("load (BE)", BreakEvenPoint.getBreakEvenPoint(getFactory(), shiftplan).getLoad(), " %");
		System.out.println();
		if (this.getDO() instanceof Factory){
			ProductPlan p = OptimalProduction.getOptimalProduction(getDO(), shiftplan);
			p.getOO().print("optimal\t");
			Vector.print("load (opt)", p.getLoad(), " %");
		}
	}
	
	
	
	/*
	 * Prints production costs and profits.
	 */
	
	public void printCost(ShiftSchedule shiftplan){
		double[] scrapRates = new double[EntityList.countOperations(getDO())];
		int i=0;
		double[] profit = CostCalculations.getProfitByProduces(getDO(), shiftplan);
		for (Operation op : EntityList.getOperationList(getDO())){
			if (!op.isPurchased())
				scrapRates[i] = 100 * ((InternalOperation)op).getScrapRate();
			else
				scrapRates[i] = 0;
			i++;
		}
		
		Vector.print("Wertsch�pfung", profit);
		Vector.print("Ausschuss", scrapRates, " %");
		Vector.print("Variable Kosten", CostCalculations.getVariableCosts(getDO(), shiftplan));
		Vector.print("Profit   ",  CostCalculations.getProfitByProduces(getDO(), shiftplan));
		Vector.print("ProfitTh ", CostCalculations.getTheoreticalProfitByProduces(getDO(), shiftplan));
		System.out.print("Zeit:\t\t");
		for(Operation op : EntityList.getOperationList(getDO()))
			System.out.print(op.getTime()+" s\t\t");		
		System.out.println();
		System.out.println("Constant costs:\t" + CostCalculations.getConstantCost(getDO(), shiftplan));		
	}
	
	/*
	 * Prints detailed information about the mix flexibility calculation.
	 */
	public void printMixflexibilityInformation() {
		
		System.out.println("Min mix flexibility:\t" + 100*MixFlexibility.getMinMixFlexibility(getDO()) + " %\t(" + MixFlexibility.getMinMixFlexibilityReason(getDO())+ ")");
		System.out.println("Avg mix flexibility:\t" + 100*MixFlexibility.getAvgMixFlexibility(getDO()) + " %\t(" + MixFlexibility.getAvgMixFlexibilityReason(getDO())+ ")");
		System.out.println("Sq mix flexibility:\t" + 100*MixFlexibility.getAvgMixFlexibility(getDO()) + " %");
		System.out.println();
//		getFactory().getOO().printProducesTableHeader(true);
		
		ProductPlan optimal = OptimalProduction.getHighestOptimalProduction(getFactory());
		Vector.print("Optimal\t", Vector.append(optimal.getVolume(), optimal.getProfit()));

		for (Product p : EntityList.getProductList(getDO())){
			Vector.print(p.getName() + " - zero", Vector.append(ZeroProduction.getZeroProduction(getDO(), p).getVolume(), ZeroProduction.getZeroProductionProfit(getDO(), p)));
		}
	}
	
	

	/**
	 * Prints a header for a table over all produces combinations in this node.
	 */
	
	public void printProducesTableHeader(boolean printProfitColumn){
		System.out.print("\t\t");
		for (Workplace w : EntityList.getWorkplaceList(getDO())){
			for (WorkplaceState ws: w.getStates()) {
				for (int i=0; i<ws.getOperations().size(); i++){
					if (i>0){
						System.out.print("________\t");
					}else if (w.getName().length() < 8){
						System.out.print(w.getName()+"\t\t");
					}else if (w.getName().length() < 16){
						System.out.print(w.getName()+"\t");
					}else{
						System.out.print(w.getName().substring(0, 14)+"\t");
					}
				}
			}
			
		}
		if (printProfitColumn)
			System.out.print("Profit");
		
		System.out.println();
		System.out.print("\t\t");
		for (Workplace w : EntityList.getWorkplaceList(getDO())) {
			for (WorkplaceState ws: w.getStates()) {
				for (Operation op : ws.getOperations()){
					System.out.print(productQuantityListToString(op.getOutputs()));
				}					
				System.out.println();
			}
		}
	}
	
	public static String productQuantityListToString(List<ProductQuantity> pqList){
		String result = "";
		int i = pqList.size();
		for (ProductQuantity pq : pqList){
			result += pq.getProduct();
			if (pq.getQuantity() != 1){
				result += " (" + pq.getQuantity() + ")";
			}
			if (--i > 0){
				result += ", ";
			}
		}
		return result;
	}
	
	/**
	 * Prints a header for a table over all produces combinations in this node.
	 */
	public void printProducesTableHeader(){
		printProducesTableHeader(false);
		
	}

	
	
	
	/**
	 * Prints a header for a table for all products.
	 */
	public void printProductTableHeader(){
		System.out.print("\t\t");
		for(Product p : getFactory().getProducts())
			System.out.print(p.getName()+"\t\t");
		System.out.println();
	}
	
	
	
	/**
	 * Print a table header for {@link # printQuickInformation()}.
	 */
	public void printQuickInformationHeader(){
		System.out.println(
				"Node"
				+ "\t\tVolume flex"
				+ "\tBreak even"
				+ "\tRatio capacity"
				+ "\tMix flexibility / reason"
				+ "\t\tExpansion flexibility"
		);
	}
	
	/**
	 * Print some important metric information in one line.
	 */
	public void printQuickInformation(){
		ShiftSchedule shiftplan = getFactory().getDefaultShiftplan();
		System.out.print(getName() + ":" + (getName().length()<7 ? "\t" : ""));
		System.out.print("\t" + (double)Math.round(10000*VolumeFlexibility.getVolumeFlexibility(getDO())));
		System.out.print("\t\t" + (BreakEvenPoint.getBreakEvenPoint(getDO(), shiftplan).isValid()  ?  (int)BreakEvenPoint.getBreakEvenPoint(getDO(), shiftplan).getTotalVolume()  :  "-"));
		System.out.print("\t\t" + (int)RatioCapacity.getRatioCapacity(getDO(), shiftplan).getTotalVolume());
		System.out.print("\t\t" + (double)Math.round(10000*MixFlexibility.getMixFlexibility(getDO()))/100 + " %");
		System.out.print("\t\t" + MixFlexibility.getMixFlexibilityReason(getDO()));
		if (this.getDO() instanceof Factory){
			System.out.print("\t\t" + (double)Math.round(10000*ExpansionFlexibility.getExpansionFlexibility((Factory)this.getDO()))/100 + " %");
		}
		System.out.println();
	}
	
	public void print(){
		print("");
	}
	
	public void print(String indent){
		System.out.println(indent+this.getName()+"\t"+this.toString());
//		for(FactoryNode t : getSubNodes())
//			t.getOO().print(indent+"\t");
	}
	
	
	
	//convenience methods
	
	/**
	 * @return the node
	 */
	public FactoryNode getDO() {
		return node;
	}


	/**
	 * @param node the node to set
	 */
	public void setNode(FactoryNode node) {
		this.node = node;
	}

	
	/**
	 * Convenience method for getDO().getName()
	 * @return
	 */
	private String getName(){
		return getDO().getName();
	}

	/**
	 * Convenience method for getDO().getFactory()
	 * @return
	 */
	protected Factory getFactory(){
		return getDO().getFactory();
	}

	/**
	 * Convenience method for getDO().getSubNodes()
	 * @return
	 */
	private List<FactoryNode> getSubNodes(){
		return getDO().getSubNodes();
	}
	
	public void createHtmlReportVolumeFlexibility(){
		String filename = "html/output.html";
		FileWriter outFile;
		try{
			outFile = new FileWriter(filename);
			PrintWriter out = new PrintWriter(outFile);
			writeHtmlBegin(out);
			out.println("<table>");
			writeTableHeader(out, "Factory node", "BE", null, "Max", null, "Volfex");
			for (FactoryNode f : EntityList.getSubnodeList(getDO())){
				out.println("<tr>");
				out.println("<td>" + f.getName() + "</td>");
				out.println("<td class=\"number\">" + Numbers.formatPieces(BreakEvenPoint.getLeastBreakEvenPoint(f).getTotalVolume()) + "</td>");
				out.println("<td>" + BreakEvenPoint.getLeastBreakEvenPoint(f).getShiftplan().getName() + "</td>");
				out.println("<td class=\"number\">" + Numbers.formatPieces(RatioCapacity.getHighestRatioCapacity(f).getTotalVolume()) + "</td>");
				out.println("<td>" + RatioCapacity.getHighestRatioCapacity(f).getShiftplan().getName() + "</td>");
				out.println("<td class=\"number\">" + Numbers.formatMetric(VolumeFlexibility.getVolumeFlexibility(f)) + "</td>");
				out.println("</tr>");
			}
			writeHtmlEnd(out);
			out.close();
			displayFile(filename);
		}catch (IOException e){
		}
	}
	
	public void createHtmlReportMixFlexibility(){
		String filename = "html/output.html";
		FileWriter outFile;
		try{
			outFile = new FileWriter(filename);
			PrintWriter out = new PrintWriter(outFile);
			writeHtmlBegin(out);
			out.println("<table>");
			writeTableHeader(out, "Factory node", "Mix flexibility", "Reason");
			for (FactoryNode f : EntityList.getSubnodeList(getDO())){
				out.println("<tr>");
				out.println("<td>" + f.getName() + "</td>");
				out.println("<td class=\"number\">" + Numbers.formatPercent(MixFlexibility.getMixFlexibility(f)) + "</td>");
				out.println("<td>" + MixFlexibility.getMixFlexibilityReason(f) + "</td>");
				out.println("</tr>");
			}
			writeHtmlEnd(out);
			out.close();
			displayFile(filename);
		}catch (IOException e){
		}
	}
	
	public void createHtmlReport(){
		String filename = "html/output.html";
		try {
			FileWriter outFile = new FileWriter(filename);
			PrintWriter out = new PrintWriter(outFile);
			writeHtmlBegin(out);
			

			ShiftSchedule shiftplan = ((Factory)node).getDefaultShiftplan();
			ProductPlan be = RatioCapacity.getRatioCapacity(node, shiftplan);
			if (be.isValid()){
				out.println("<table>");
				int i = 0;
				double[] variableCost = CostCalculations.getVariableCosts(node, be.getShiftplan());
				double varCostSum = 0;
				double finalPieces = be.getFinalComponentsVolume();
				writeTableHeader(out, "Arbeitsplatz", "Produkt", "Var", "Stk", "Var/Stk", "Stk/FC", "Var/FC");
				Workplace previousWorkplace = null;
				for (Operation op : EntityList.getOperationList(node)){
					if (op.getOutputs().get(0).getProduct().isDisposable()){
						out.println("<tr class=\"emph\">");
					}else if (be.getVolume()[i] == 0){
						out.println("<tr class=\"notproduced\">");
					}else{
						out.println("<tr>");
					}
					if (( (InternalOperation) op).getOwner().getWorkplace().equals(previousWorkplace)){
						out.println("<td></td>");
					}else if (( (InternalOperation) op).getOwner().getWorkplace() == null){
						out.println("<td>[Einkauf]</td>");
					}else{
						String name = ( (InternalOperation) op).getOwner().getWorkplace().getName();
						if (name.length() > 22){
							name = name.substring(0,22);
						}
						out.println("<td>" + name + "</td>");
					}
					out.println("<td>" + productQuantityListToString(op.getOutputs()) + "</td>");
					out.println("<td class=\"number\">" + Numbers.formatMoney(be.getVolume()[i] * variableCost[i]) + "</td>");
					out.println("<td class=\"number\">" + Numbers.formatPieces(be.getVolume()[i]) + "</td>");
					out.println("<td class=\"number\">" + Numbers.formatMoney(variableCost[i]) + "</td>");
					out.println("<td class=\"number\">" + Numbers.formatPieces(be.getVolume()[i] / finalPieces) + "</td>");
					out.println("<td class=\"number\">" + Numbers.formatMoney(be.getVolume()[i] * variableCost[i] / finalPieces) + "</td>");
					varCostSum += be.getVolume()[i] * variableCost[i];
					out.println("</tr>");
					
					previousWorkplace = ( (InternalOperation) op).getOwner().getWorkplace();
					i++;
				}
				out.println("<tr>");
				out.println("<th></th>");
				out.println("<th></th>");
				out.println("<th class=\"number\">"+Numbers.formatMoney(varCostSum)+"</th>");
				out.println("<th class=\"number\">"+Numbers.formatPieces(be.getTotalVolume())+"</th>");
				out.println("<th></th>");
				out.println("<th class=\"number\">"+Numbers.formatPieces(be.getTotalVolume() / finalPieces)+"</th>");
				out.println("<th class=\"number\">"+Numbers.formatMoney(varCostSum / finalPieces)+"</th>");
				out.println("</tr>");
				out.println("</table>");
			}else{
				out.println("<p>Ung�ltiger Produktplan</p>");
			}
			
			writeHtmlEnd(out);
			out.close();
			displayFile(filename);
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	private void writeHtmlBegin(PrintWriter out){
		out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
		out.println("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"de\" lang=\"de\">");
		out.println("<head>");
		out.println("  <title>Fabrikdaten</title>");
		out.println("  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />");
		out.println("  <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />");
		out.println("  </head>");
		out.println("<body>");
	}
	
	private void writeHtmlEnd(PrintWriter out){
		out.println("</body>");
		out.println("</html>");
	}
	
	public void writeTableHeader(PrintWriter out, String... columns){
		out.println("<tr>");
		for (int i=0; i<columns.length; i++){
			String title = columns[i];
			int colspan = 1;
			while (i < columns.length-1 && columns[i+1] == null){
				i++;
				colspan++;
			}
			if (colspan == 1){
				out.println("<th>" + title + "</th>");
			}else{
				out.println("<th colspan=\"" + colspan + "\">" + title + "</th>");
			}
		}
		out.println("</tr>");
	}
	
	private void displayFile(String filename) throws IOException{
//		if (Desktop.isDesktopSupported()){
//			Path p = new Path(filename);
//			Desktop.getDesktop().browse(p.toFile().toURI());
//		}else{
			System.out.println("Wrote output to " + filename);
//		}
//		String osName = System.getProperty("os.name");
//		String[] cmd = new String[3];
//		if (osName.indexOf("Windows") != -1){
//			if(osName.equals("Windows 95")){
//				cmd[0] = "command.com" ;
//			}else if (osName.indexOf("Windows") != -1){
//				cmd[0] = "cmd.exe";
//			}
//			cmd[1] = "/C" ;
//			cmd[2] = filename;
//			Runtime.getRuntime().exec(cmd);
//		}else{
//			System.out.println("Wrote output to " + filename);
//		}
	}
}

/**
 * Ecoflex 2009
 * de.fzi.wenpro.core.model.operations.ProductPlanOutputOperations.java
 */
package de.fzi.wenpro.core.printing;

import de.fzi.wenpro.core.model.deprecated.ProductPlan;
import de.fzi.wenpro.util.Vector;

/**
 * @author aleksa
 * 
 */
public class ProductPlanOutputOperations {
	
	private ProductPlan pp;
	
	public ProductPlanOutputOperations(ProductPlan pp){
		this.pp = pp;
	}
	
	/**
	 * Prints the product plan.
	 */
	public void print(){
		print("");
	}
	
	/**
	 * Prints the product plan.
	 */
	public void print(String label){
		Vector.print(label, getProductPlan().getVolume());
	}
	
	/**
	 * @return the pp
	 */
	public ProductPlan getProductPlan(){
		return pp;
	}
	
}

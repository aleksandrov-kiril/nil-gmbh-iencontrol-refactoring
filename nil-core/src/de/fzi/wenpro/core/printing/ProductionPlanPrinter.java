/**
 * 
 */
package de.fzi.wenpro.core.printing;

import java.util.List;
import java.util.Map;

import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.core.model.productionplan.Schedule;

/**
 * @author wicaksono
 *
 */
public class ProductionPlanPrinter {
	
	public static String productQuantityListToString(List<ProductQuantity> pqList){
		String result = "";
		int i = pqList.size();
		for (ProductQuantity pq : pqList){
			result += pq.getProduct();
			if (pq.getQuantity() != 1){
				result += " (" + pq.getQuantity() + ")";
			}
			if (--i > 0){
				result += ", ";
			}
		}
		return result;
	}
	
	public static void printProductionPlanEntry(ProductionPlanEntry entry){
		System.out.print(entry.toString() + " - " + productQuantityListToString(entry.getOperation().getOutputs()));
	}
	
	public static void printSchedule (Schedule<ProductionPlanEntry> schedule) {
		for (int i = 0; i < schedule.size(); i++){
			System.out.print(schedule.getTime(i) +  " : ");
			ProductionPlanEntry entry = schedule.get(i);
			printProductionPlanEntry(entry);
			System.out.println();
		}
	}
	
	public static void printProductionPlan (ProductionPlan plan) {
		System.out.println("===== Begin Print ProductionPlan =====");
		System.out.println("PP-ID : " + plan.toString());
		for ( Map.Entry<Workplace, Schedule<ProductionPlanEntry>> entry: plan.getEntries().entrySet()) {
			System.out.println(entry.getKey().getName());
			printSchedule(entry.getValue());
			System.out.println("--------------------");
		}
		System.out.println("===== End Print ProductionPlan =====");
	}
}

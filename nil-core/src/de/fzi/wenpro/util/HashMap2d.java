/**
 * ecoFLEXiP 2010
 * de.fzi.ecoflex.util.HashMap2d.java
 */
package de.fzi.wenpro.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Zerael
 *
 */
public class HashMap2d<K1, K2, V> extends HashMap<K1, Map<K2, V>>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2250668120161674529L;

	public V get(Object key1, Object key2){
		Map<K2, V> line = get(key1);
		if (line == null){
			return null;
		}else{
			return line.get(key2);
		}
	}
	
	public V put(K1 key1, K2 key2, V value){
		Map<K2, V> line = get(key1);
		if (line == null){
			line = new HashMap<K2, V>();
			put(key1, line);
		}
		return line.put(key2, value);
	}
	
    public boolean containsKey(Object key1, Object key2){
		Map<K2, V> line = get(key1);
    	return line != null && line.containsKey(key2); 
    }
    
    public boolean containsValue(Object value){
    	for (Map<K2, V> line : values()){
    		if (line.containsValue(value)){
    			return true;
    		}
    	}
    	return false;
    }
    
    public V remove(Object key1, Object key2){
		Map<K2, V> line = get(key1);
		if (line != null){
			return line.remove(key2);
		}else{
			return null;
		}
    }
    
    public void clear(Object key1){
		Map<K2, V> line = get(key1);
		if (line != null){
			line.clear();
		}
    }
}

package de.fzi.wenpro.util;

public class Matrix {
	
	public double[][] values;
	public final int height, width;
	
	public Matrix(int height, int width){
		values = new double[height][width];
		this.height = height;
		this.width = width;
	}
	
	public Matrix(int size){
		values = new double[size][size];
		this.height = size;
		this.width = size;
	}
	
	public Matrix(double[][] values){
		this.height = values.length;
		this.width = (values.length == 0) ? 0 : values[0].length;
		this.values = new double[height][];
		for (int i = 0; i < height; i++)
			this.values[i] = values[i].clone();
	}
	
	public Matrix(int[][] values){
		this.height = values.length;
		this.width = values[0].length;
		this.values = new double[height][width];
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				this.values[i][j] = values[i][j];
	}
	
	public Matrix(double[][] values, boolean cloneInput){
		if (cloneInput)
			this.values = values.clone();
		else
			this.values = values;
		this.height = values.length;
		this.width = values[0].length;
	}
	
	public Matrix(int height, int width, double[][] values){
		this.values = new double[height][width];
		for (int i = 0; i < height && i < values.length; i++)
			for (int j = 0; j < width && j < values[i].length; j++)
				this.values[i][j] = values[i][j];
		
		this.height = height;
		this.width = width;
	}
	
	public static Matrix getIdentityMatrix(int size){
		Matrix m = new Matrix(size);
		for (int i = 0; i < size; i++){
			m.values[i][i] = 1;
		}
		return m;
	}
	
	public void columnOperation(int from, int to, double factor){
		for (int i = 0; i < height; i++)
			values[i][to] += values[i][from] * factor;
	}
	
	public void setColumn(int column){
		for (int i = 0; i < height; i++)
			values[i][column] = 0;
	}
	
	public void setColumn(int column, double value){
		for (int i = 0; i < height; i++)
			values[i][column] = value;
	}
	
	public void setRow(int row){
		for (int i = 0; i < height; i++)
			values[i][row] = 0;
	}
	
	public void setRow(int row, double value){
		for (int i = 0; i < height; i++)
			values[i][row] = value;
	}
	
	public Matrix removeColumn(int column){
		if (column >= width)
			return new Matrix(values);
		else{
			Matrix result = new Matrix(height, width - 1);
			for (int i = 0; i < height; i++){
				for (int j = 0; j < column; j++)
					result.values[i][j] = values[i][j];
				for (int j = column; j < width - 1; j++)
					result.values[i][j] = values[i][j + 1];
			}
			return result;
		}
	}
	
	public Matrix removeColumn(boolean[] remove){
		int newWidth = 0;
		for (int i = 0; i < remove.length && i < width; i++)
			if (!remove[i])
				newWidth++;
		
		Matrix result = new Matrix(height, newWidth);
		int newj = 0;
		for (int j = 0; j < width; j++)
			if (!remove[j]){
				for (int i = 0; i < height; i++)
					result.values[i][newj] = values[i][j];
				newj++;
			}
		return result;
	}
	
	public Matrix mul(Matrix m){
		Matrix result = new Matrix(height, m.width);
		for (int i = 0; i < result.height; i++)
			for (int j = 0; j < result.width; j++)
				for (int k = 0; k < width; k++)
					result.values[i][j] += values[i][k] * m.values[k][j];
		return result;
	}
	
	public Matrix mulL(Matrix m){
		return m.mul(this);
	}
	
	public double[] transformVector(double[] vector){
		// return (this*vector) )
		double[] result = new double[height];
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				result[i] += values[i][j] * vector[j];
		return result;
	}
	
	public double[] transformVectorL(double[] vector){
		// return ((vector)T * this)
		// <=> return ((this)T * vector)T
		double[] result = new double[width];
		for (int j = 0; j < width; j++)
			for (int i = 0; i < height; i++)
				result[j] += values[i][j] * vector[i];
		return result;
	}
	
	public void print(){
		for (int i = 0; i < width + 1; i++)
			System.out.print("--------");
		System.out.println("-");
		for (int i = 0; i < height; i++){
			System.out.print("[\t");
			for (int j = 0; j < width; j++){
				System.out.print(values[i][j]);
				System.out.print("\t");
			}
			System.out.println("]");
		}
		for (int i = 0; i < width + 1; i++)
			System.out.print("--------");
		System.out.println("-");
	}
	
	public void print(String title){
		System.out.println(title);
		print();
	}
	
	// ////////// Static methods on double[][] matrices
	
	/**
	 * Returns the concatenation of the parameters. May return one of it's
	 * parameters if the other one is null. The source matrices' lines will not
	 * be copied, so the result contains references to the lines of the source
	 * matrices.
	 * @param m1: The first matrix to be concatenated.
	 * @param m2: The second matrix to be concatenated.
	 * @return: The concatenation of the two parameters.
	 */
	public static double[][] merge(double[][] m1, double[][] m2){
		if (m1 == null)
			return m2;
		else if (m2 == null)
			return m1;
		else{
			int height = m1.length + m2.length;
			double[][] result = new double[height][];
			
			int i, j = 0;
			for (i = 0; i < m1.length; i++)
				result[j++] = m1[i];
			for (i = 0; i < m2.length; i++)
				result[j++] = m2[i];
			
			return result;
		}
	}
	
	/**
	 * Returns the concatenation of the parameters. May return one of it's
	 * parameters if the others are null. The source matrices' lines will not
	 * be copied, so the result contains references to the lines of the source
	 * matrices.
	 * @param m1: The first matrix to be concatenated.
	 * @param m2: The second matrix to be concatenated.
	 * @return: The concatenation of the parameters.
	 */
	public static double[][] merge(double[][] m1, double[][] m2, double[][] m3){
		if (m1 == null)
			return merge(m2, m3);
		else if (m2 == null)
			return merge(m1, m3);
		else if (m3 == null)
			return merge(m1, m2);
		else{
			int height = m1.length + m2.length + m3.length;
			double[][] result = new double[height][];
			
			int i, j = 0;
			for (i = 0; i < m1.length; i++)
				result[j++] = m1[i];
			for (i = 0; i < m2.length; i++)
				result[j++] = m2[i];
			for (i = 0; i < m3.length; i++)
				result[j++] = m3[i];
			
			return result;
		}
	}
	
	/**
	 * Returns a copy of a matrix with has the specified height and width. The
	 * matrix or the matrix' lines are references to the matrix m if the size is
	 * correct.
	 */
	public static double[][] resize(double[][] m, int height, int width){
		if (m == null){
			return null;
		}else if (m.length == height && (height == 0 || m[0].length == width)){
			return m;
		}else{
			double[][] result = new double[height][];
			for (int i = 0; i < height; i++){
				result[i] = Vector.resize(m[i], width);
			}
			return result;
		}
	}
	
	/**
	 * Appends a vector as a new line on the bottom of the matrix. The result
	 * matrix contains references to the passed matrix and vector.
	 */
	public static double[][] append(double[][] m, double[] v){
		double[][] result = new double[m.length + 1][];
		System.arraycopy(m, 0, result, 0, m.length);
		result[m.length] = v;
		return result;
	}
	
	/**
	 * Sums up the colums of a matrix.
	 */
	public static double[] columnSum(double[][] m){
		double[] result = new double[m[0].length];
		for (int i = 0; i < m.length; i++){
			for (int j = 0; j < m[i].length; j++)
				result[j] += m[i][j];
		}
		return result;
	}
	
	/**
	 * Multiplies each element of a column with a factor so that the sum of the
	 * column is 1.
	 * @param matrix the matrix to be scaled
	 */
	public static void normalizeColumns(double[][] matrix){
		for (int j = 0; j < matrix[0].length; j++){
			double sum = 0;
			for (int i = 0; i < matrix.length; i++){
				sum += matrix[i][j];
			}
			if (sum != 0){
				for (int i = 0; i < matrix.length; i++){
					matrix[i][j] /= sum;
				}
			}
		}
	}
	
	/**
	 * Scales a matrix linewise by the elements of a vector, so that
	 * m[i,j] -> m[i,j]*v[j]
	 * @param matrix the source matrix
	 * @param vector the source vector
	 */
	public static void scaleColumns(double[][] matrix, double[] vector){
		for (int i = 0; i < matrix.length; i++){
			for (int j = 0; j < matrix[i].length; j++){
				matrix[i][j] *= vector[j];
			}
		}
	}
}

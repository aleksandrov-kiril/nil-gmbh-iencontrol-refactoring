package de.fzi.wenpro.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatePeriodConvertor {
	
	private static Calendar CALENDAR = Calendar.getInstance();
	private static DateFormat niceDf = new SimpleDateFormat("EEE. dd. MMMM yyyy");
	
	private final static int DAYS_IN_WEEKS = 7;
	private final static int MILISECONDS_IN_DAYS = 1000*60*60*24; // 1000 miliseconds per second
																	// 60 seconds per minute
																	// 60 minutes per hour
																	// 24 hours per day
																	// 7 days per week

	
	/**
	 * Returns the number of Ecoflex periods (weeks) to the
	 * <code>givenDate</code>, starting from the next monday 
	 * (next start of a period). The periods are rounded down.
	 * 
	 * @param givenDate
	 * @return
	 * @throws PeriodException
	 */
	public static int getPeriodForDate(Date givenDate) throws PeriodException{

		Date nextWeekStart = computeStartOfNextWeekDate();
		
		// chec
		if(nextWeekStart.after(givenDate))
			throw new PeriodException("second date: "+niceDf.format(givenDate)+" is earlier than next week start date: "+niceDf.format(nextWeekStart));
		long timems = givenDate.getTime() - nextWeekStart.getTime();
		long time = timems/MILISECONDS_IN_DAYS;
		time = time - time % DAYS_IN_WEEKS;
		time = time/DAYS_IN_WEEKS;
		return (int)time;
		
	}
	
	public static Date computeStartOfNextWeekDate(){

		long time = getNextDay(Calendar.getInstance().getTimeInMillis(),
				Calendar.SUNDAY);
		Calendar cal = CALENDAR;
		cal.setTimeInMillis(time);

		return cal.getTime();
	}
	
	public static Calendar computeStartOfNextWeekCalendar(){

		long time = getNextDay(Calendar.getInstance().getTimeInMillis(),
				Calendar.SUNDAY);
		Calendar cal = CALENDAR;
		cal.setTimeInMillis(time);

		return cal;
	}
	
	private static long getDay(long date, int startOfWeek, int increment) {
	        Calendar calendar = CALENDAR;
	        synchronized(calendar) {
	            calendar.setTimeInMillis(date);
	            int day = calendar.get(Calendar.DAY_OF_WEEK);
	            // Normalize the view starting date to a week starting day
	            while (day != startOfWeek) {
	                calendar.add(Calendar.DATE, increment);
	                day = calendar.get(Calendar.DAY_OF_WEEK);
	            }
	            return startOfDayInMillis(calendar.getTimeInMillis());
	        }
	}

	public static long getNextDay(long date, int startOfWeek) {
		return getDay(date, startOfWeek, 1);
	}

	public static long startOfDayInMillis(long date) {
		Calendar calendar = CALENDAR;
		synchronized (calendar) {
			calendar.setTimeInMillis(date);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MINUTE, 0);
			return calendar.getTimeInMillis();
		}
	}
	
	public static Date getDateFromDateTime(int year, int month, int day){
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, day);
		return cal.getTime();
	}
	
	
	public static Calendar getCalendarFromDateTime(int year, int month, int day){
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, day);
		return cal;
	}

}
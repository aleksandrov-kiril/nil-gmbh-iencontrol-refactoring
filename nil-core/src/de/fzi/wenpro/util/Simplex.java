package de.fzi.wenpro.util;


public class Simplex {
	public boolean debugprint = false;
	public String debugprefix = "";
	
	private final double[][] A;
	private final double[] a;
	private final double[] c;
	private final Matrix conversion;
	final int nVariable, nConstraint;
	
	// tableau variables
	private double[][] tA;
	private double[] ta, tc;
	private boolean[] base;
	private int[] baseIndex;
	
	// result variables
	public static final int UNKNOWN=0;
	public static final int SOLVABLE=1;
	public static final int INFEASIBLE=2;
	public static final int UNBOUNDED=3;
	private int type = UNKNOWN;
	
	
	public Simplex(int nVariable, int nConstraint, double[][] A, double[] a, double[] c){
		this(nVariable, nConstraint, A, a, c, null, false, "");
	}
	
	public Simplex(int nVariable, int nConstraint, double[][] A, double[] a, double[] c, boolean debugprint, String debugprefix){
		this(nVariable, nConstraint, A, a, c, null, debugprint, debugprefix);
	}
	
	public Simplex(int nVariable, int nConstraint, double[][] A, double[] a, double[] c, double[][] condition){
		this(nVariable, nConstraint, A, a, c, condition, false, "");
	}
	
	public Simplex(int nVariable, int nConstraint, double[][] A, double[] a, double[] c, double[][] condition, boolean debugprint, String debugprefix){		
		this(
				Matrix.resize(A, nConstraint, nVariable),
				Vector.resize(a, nConstraint),
				Vector.resize(c, nVariable),
				Matrix.resize(condition, condition.length, nVariable),
				debugprint, debugprefix
		);
	}
	
	
	
	public Simplex(double[][] A, double[] a, double[] c){
		this(A, a, c, null, false, "");
	}
	public Simplex(double[][] A, double[] a, double[] c, boolean debugprint, String debugprefix){
		this(A, a, c, null, debugprint, debugprefix);
	}
	
	public Simplex(double[][] A, double[] a, double[] c, double[][] condition){
		this(A, a, c, condition, false, "");
	}
	public Simplex(double[][] A, double[] a, double[] c, double[][] condition, boolean debugprint, String debugprefix){
		this.debugprint = debugprint;
		this.debugprefix = debugprefix;
		printMatrix(A, "A");
		printVector(a, "a");
		printVector(c, "c");
		
		if(A.length!=a.length)
			throw new IllegalArgumentException("A and a have different heights: " + A.length + ", " + a.length);
		if(A.length > 0  &&  A[0].length != c.length)
			throw new IllegalArgumentException("A and c have different widths: " + A[0].length + ", " + c.length);
		
		if(condition == null){
			
			this.A = A;
			this.a = a;
			this.c = c;
			nVariable = c.length;
			nConstraint = a.length;
			conversion = null;
			
		}else{
			
			printMatrix(condition, "condition");
			
			if(condition.length > 0  &&  A.length > 0  &&  condition[0].length != A[0].length)
				throw new IllegalArgumentException("A and condition have different widths: " + A[0].length + ", " + condition.length);
			
			
			
			// Find x with Ax<=a && x>=0 && (condition)*x=0 && c*x maximal
			
			final int nInputVariable = c.length;
			int nEliminated = 0;
			
			Matrix E = Matrix.getIdentityMatrix(nInputVariable);
			Matrix C = new Matrix(condition);
			boolean[] eliminated = new boolean[nInputVariable];
			
			for (int i = 0; i<condition.length; i++){
				printMatrix(E, "E" + String.valueOf(i));
				int j; // := Index of the first non-zero entry in C[i]
				for(j=0; j<nInputVariable  &&  C.values[i][j] == 0; j++);
				
				if(j<nInputVariable){
					// Eliminate variable x[j] using the condition C[i]*x==0
					for (int l=0; l<nInputVariable; l++)
						if (l != j){
							E.columnOperation(j, l, -C.values[i][l]/C.values[i][j]);
							C.columnOperation(j, l, -C.values[i][l]/C.values[i][j]);
						}
					C.setColumn(j, 0);
					E.setColumn(j, 0);
					nEliminated++;
					eliminated[j] = true;
				}
			}
			nVariable = nInputVariable-nEliminated;
			
			printMatrix(E, "E");
			printVector(eliminated, "eliminated");
			
			
			
			
			// Find Ex with AEx<=a && EX>=0 && c(Ex) maximal
			
			// Some constraints in EX>=0 are not implicitly regarded
			// by the basic algorithm (i. e. the don't have the form
			// xi>=0 after the the transformation by E). These
			// conditions have to be added to A and a.
			
			conversion = E.removeColumn(eliminated);
			
			double[][] newConstrictions = new double[nInputVariable][nVariable];
			int nNewConstrictions = 0;
			for (int i=0; i<nInputVariable; i++){
				int nonZeroCount = 0;
				for (int j=0; j<nVariable; j++)
					if (conversion.values[i][j] != 0)
						nonZeroCount++;
				if(nonZeroCount>1){
					for (int j=0; j<nVariable; j++)
						newConstrictions[nNewConstrictions][j] = -conversion.values[i][j];
					nNewConstrictions ++;
				}
				println("Non-Zero: " + String.valueOf(nonZeroCount));
			}
			println("Zus�tzliche Beschr�nkungen: " + String.valueOf(nNewConstrictions));
			
			Matrix Atmp = (new Matrix(A)).mul(conversion);
			this.A = new double[a.length+nNewConstrictions][nVariable];
			for(int i=0; i<a.length; i++)
				this.A[i] = Atmp.values[i];
			for(int i=0; i<nNewConstrictions; i++)
				this.A[a.length+i] = newConstrictions[i];
			
			this.a = new double[a.length+nNewConstrictions];
			for(int i=0; i<a.length; i++)
				this.a[i] = a[i];
			
			this.c = conversion.transformVectorL(c);
			
			nConstraint = a.length + nNewConstrictions;
			
			// Find Ex with AEx<=a && X>=0 && c(Ex) maximal
			
			println("\n::::::::::: Nach der Transformation:");
			printMatrix(this.A, "A");
			printVector(this.a, "a");
			printVector(this.c, "c");
			printMatrix(conversion, "conversion");
			
		}
		initializeTableau();
	}
	
	
	/**
	 * Initializes the tableau variables tA, Ta, tc, base, and baseIndex.
	 * All slack variables are set to be basic variables (even is this
	 * makes some of them negative).
	 */
	private void initializeTableau(){
		tA = new double[nConstraint][nVariable+nConstraint];
		ta = this.a.clone();
		tc = new double[nVariable+nConstraint];
		base = new boolean[nVariable+nConstraint];
		baseIndex = new int[nConstraint];
		
		for (int j=0; j<nVariable; j++){
			tc[j]=c[j];
			for(int i=0; i<nConstraint; i++)
				tA[i][j] = this.A[i][j];
			base[j] = false;
		}
		for (int j=nVariable; j<nVariable+nConstraint; j++){
			tc[j]=0;
			for(int i=0; i<nConstraint; i++)
				tA[i][j] = (i==j-nVariable)?1:0;
			base[j] = true;
			baseIndex[j-nVariable] = j;
		}
		println();
		println("Initial tableau");
		printCurrentTableau();
	}
	
	/**
	 * Replaces a basic variable by another
	 * @param entering: The index of the variable to be added to the base. This variable mustn't be a basic.
	 * @param leaving: The index of the variable to be removed from the base. Thist variable must be basic.
	 */
	private void changeBase(int entering, int leaving){
		double factor;
		
		print(leaving<nVariable?"x"+(leaving+1):"y"+(leaving+1-nVariable));
		print(" => ");
		println(entering<nVariable?"x"+(entering+1):"y"+(entering+1-nVariable));
		
		// I. Find the leaving variable's line
		int line = getVariablesLine(leaving);
		
		if(line==-1)
			throw new IllegalArgumentException("Leaving variable isn't basic.");
		if(base[entering])
			throw new IllegalArgumentException("Entering variable is already basic.");
		if(tA[line][entering]==0)			
			throw new IllegalArgumentException("Can't switch these variables in the current tableau.");
		
		
		
		// II. Zero the tA column of the entering variable
		for (int i=0; i<nConstraint; i++)
			if(i!=line){
				factor = -tA[i][entering]/tA[line][entering];
				for(int j=0; j<nVariable+nConstraint; j++)
					tA[i][j] += tA[line][j]*factor;
				tA[i][entering] = 0; // Should already be 0 if not for numeric reasons
				ta[i] += ta[line]*factor;
			}
		
		// III. Zero the tc entry of the entering variable
		factor = -tc[entering]/tA[line][entering];
		for(int j=0; j<nVariable+nConstraint; j++)
			tc[j] += tA[line][j]*factor;
		tc[entering] = 0; // Should already be 0 if not for numeric reasons
		
		// IV. Scale the constraint
		factor = 1/tA[line][entering];
		for(int j=0; j<nVariable+nConstraint; j++)
			tA[line][j] *= factor;
		tA[line][entering] = 1; // Should already be 1 if not for numeric reasons
		ta[line] *= factor;
		
		// V. Update base and baseIndex
		base[entering] = true;
		base[leaving] = false;
		baseIndex[line] = entering;
		
		
		printCurrentTableau();
	}
	
	
	
	
	/**
	 * Finds the line in the tA matrix which corredponds to the given basic variable.
	 * @param iVariable: The index of the basic variable.
	 * @return: The index of the tA line, or -1 if the given variable isn't basic.
	 */
	private int getVariablesLine(int iVariable){
		for(int i=0; i<nConstraint; i++)
			if(baseIndex[i] == iVariable)
				return i;
		return -1;
	}
	
	
	
	/**
	 * Change the base of the current tableau.
	 * @param newBase: The new base.
	 */
	public void setBase(boolean[] newBase){
		boolean debugprintBackup = debugprint;
		debugprint &= true;
		
		println("SetBase");
		print("old base: ");
		for(int j=0; j<nVariable+nConstraint; j++)
			print(base[j]+"\t");
		println();
		print("new base: ");
		for(int j=0; j<nVariable+nConstraint; j++)
			print(newBase[j]+"\t");
		println();
		println("Initial Tableau:");
		printCurrentTableau();
		
		int iLine = 0;
		for(int entering=0; entering<nVariable+nConstraint; entering++)
			if(newBase[entering]){
				println("~~~~~~~~~~~~ Enter: "+ entering + " ~~~~~~~~~~~~~");
				// Find a line where the entering variable's coefficent isn't 0. iLine2 may not be smaller than iLine,
				// because these lines are already processed and may not be swapped.
				/*
				int iLine2 = iLine;
				while (iLine2<nConstraint && tA[iLine2][entering]==0)
					iLine2++;
				if(iLine2==nConstraint)
					// There is no line that can be used to make the variable enter. Looks like the matrix is singulary.
					throw new IllegalArgumentException("The condition matrix is singulary for the selected variables. The base cannot be set.");
				 */
				// Find a line where the entering variable's coefficent is not 0. To
				// avoid numerical instability the coefficent is chosen to be close to 1
				// or -1. iLine2 may not be smaller than iLine, because these lines are
				// already processed and may not be swapped.
				int iLine2=iLine;
				double d = Double.POSITIVE_INFINITY;
				for (int i=iLine; i<nConstraint; i++){
					double val = Math.abs(tA[i][entering]);
					val = Math.max(val, 1/val);
					if (val<d){
						d = val;
						iLine2 = i;
					}
				}
				if(d == Double.POSITIVE_INFINITY)
					// There is no line that can be used to make the variable enter. Looks like the matrix is singulary.
					throw new IllegalArgumentException("The condition matrix is singulary for the selected variables. The base cannot be set.");
				
				
				
				if(iLine != iLine2){
					println("Swapping " + iLine + " and " + iLine2 + ":");
					// Swap line and line2.
					double[] tmpLine = tA[iLine];
					tA[iLine] = tA[iLine2];
					tA[iLine2] = tmpLine;
					double tmp = ta[iLine];
					ta[iLine] = ta[iLine2];
					ta[iLine2] = tmp;
					printCurrentTableau();
				}
				
				// Zero the column exept for the selected line
				for(int i=0; i<nConstraint; i++)
					if(i != iLine  &&  tA[i][entering] != 0){
						for (int j=0; j<nVariable+nConstraint; j++)
							if(j!=entering)
								tA[i][j] -= tA[iLine][j] * tA[i][entering] / tA[iLine][entering];
						ta[i] -= ta[iLine] * tA[i][entering] / tA[iLine][entering];
						
						tA[i][entering] = 0;
					}
				
				// Normalize the selected line
				if(tA[iLine][entering]!=1){
					for (int j=0; j<nVariable+nConstraint; j++)
						if(j!=entering)
							tA[iLine][j] /= tA[iLine][entering];
					ta[iLine] /=  tA[iLine][entering];
					tA[iLine][entering] = 1;
				}
				
				// Zero the cost vector entry for the entering variable
				if(tc[entering] != 0){
					for (int j=0; j<nVariable+nConstraint; j++)
						if(j!=entering)
							tc[j] -= tA[iLine][j]*tc[entering];
					tc[entering] = 0;
				}
				
				
				baseIndex[iLine] = entering;
				println(entering + " has entered on line " + iLine + ":");
				printCurrentTableau();
				println();
				iLine++;
			}
		base = newBase.clone();
		println("SetBase is done! Result:");
		printCurrentTableau();
		println();
		println();
		
		debugprint = debugprintBackup;
	}
	
	
	
	/**
	 * 
	 * @return: A copy of the current tableau base.
	 */
	public boolean[] getBase(){
		return base.clone();
	}
	
	/**
	 * Returns a value indicating if the problem is solvable.
	 * @return One of the following constants:
	 *    <ul>
	 * 		<li>{@link #UNKNOWN}: Not yet determined</li>
	 * 		<li>{@link #SOLVABLE}: The problem has an optimal solution</li>
	 *      <li>{@link #INFEASIBLE}: The problem has no solution</li>
	 *      <li>{@link #UNBOUNDED}: The problem is unbounded, infinitely good solutions are possible</li>
	 *      </ul>
	 */
	public int getType(){
		return type;
	}
	/**
	 * Transforms the tableau to make the base valid, i.e. make the current
	 * solution valid, i.e. make all components of ta non-negative.
	 *
	 */
	private void setInitialBase(){
		println("setInitialBase");
		
		int taMaxi;
		do{
			double taMax = 0;
			taMaxi = -1;
			for (int i=0; i<nConstraint; i++){
				if (ta[i] < taMax){
					taMax = ta[i];
					taMaxi = i;
				}
			}
			if (taMaxi >= 0){
				double max = Double.NEGATIVE_INFINITY;
				int jMax = -1;
				for (int j=0; j<nVariable+nConstraint; j++){
					if (tA[taMaxi][j] < 0){
						double val = tc[j] / tA[taMaxi][j];
						if (val > max){
							max = val;
							jMax = j;
						}
					}
				}
				if (jMax >= 0){
					changeBase(jMax, baseIndex[taMaxi]);
				}else{
					type = INFEASIBLE;
					return;
				}
			}
		}while (taMaxi >= 0);
	}
	
	public double[] solve(){
		println("Solving ...");
		setInitialBase();
		if(type==INFEASIBLE){
			println("nicht l�sbar");
			return null;
		}
		
		println("Still solving ...");
		double highestImprovement;
		do{
			// find the best entering and leaving variable
			int entering=-1,leaving=-1;
			highestImprovement = Double.NEGATIVE_INFINITY;
			for(int j=0; j<nVariable+nConstraint; j++)
				// ai=>0   (set by SetInitialBase)
				// ai/Aij*cj>0   (because the profit must be increased each step)
				// ai/Aij=>0   (else the right side would become negative)
				//   => Aij>0
				//   => cj>0
				//   => ai>0
				if(!base[j] && tc[j]>0){
					// find j with the smallest ai/Aij with Aij>0
					double minval = Double.POSITIVE_INFINITY;
					int minIndex = -1;
					for(int i=0; i<nConstraint; i++)
						if(tA[i][j]>0){
							double val = ta[i]/tA[i][j];
							if(0 <= val && val < minval){
								minval = val;
								minIndex = i;
							}
						}
					if(minval==Double.POSITIVE_INFINITY){
						this.type = UNBOUNDED;
						return null;
					}else if(tc[j]*minval>highestImprovement){
						highestImprovement = tc[j]*minval;
						entering = j;
						leaving = baseIndex[minIndex];
					}
				}
			
			
			if(highestImprovement>Double.NEGATIVE_INFINITY)
				changeBase(entering, leaving);
		}while(highestImprovement>Double.NEGATIVE_INFINITY);
		
		
		double[] result = new double[nVariable];
		for(int i=0; i<nConstraint; i++){
			if(baseIndex[i]<nVariable)
				result[baseIndex[i]] = ta[i];
		}
		print("L�sung: ");
		for(int i=0; i<nVariable; i++)
			print(result[i]+"\t");
		println();
		
		type = SOLVABLE;
		if(conversion==null)
			return result;
		else
			return conversion.transformVector(result);
		
	}
	
	
	/**
	 * Finds a solution that still solves the original problem optimally, but
	 * among these possible solutions selects one that is maximal on the passed
	 * vector.  
	 * @param c2: The vector whose product with the solution is to be improved.
	 * @return: The new, improved, solution.
	 */
	public double[] improve(double[] c2){
		double[] tc2 = new double[nVariable+nConstraint];
		if(conversion == null){
			if(c2.length != c.length)
				throw new IllegalArgumentException("c2 and c have different widths: " + c2.length + ", " + c.length);
			System.arraycopy(c2,0,tc2,0,c2.length);
		}else{
			if(c2.length != conversion.height)
				throw new IllegalArgumentException("c2 and c have different widths: " + c2.length + ", " + conversion.height);
			System.arraycopy(conversion.transformVectorL(c2),0,tc2,0,conversion.width);
		}
		
		if(type == UNKNOWN)
			solve();
		if(type != SOLVABLE)
			return null;
		
		
		// Adjust c2 to the current base
		for (int i=0; i<nConstraint; i++)
			if(tc2[baseIndex[i]] != 0){
				for (int j=0; j<nVariable+nConstraint; j++)
					if(j!=baseIndex[i])
						tc2[j] -= tA[i][j]*tc2[baseIndex[i]];
				tc2[baseIndex[i]] = 0;
			}
		
		// Improve the result for c2
		
		double highestImprovement;
		do{
			// find the best entering and leaving variable
			int entering=-1,leaving=-1;
			highestImprovement = Double.NEGATIVE_INFINITY;
			for(int j=0; j<nVariable+nConstraint; j++)
				// improve c2: 
				//   => Aij>0
				//   => c2j>0
				//   => ai>0
				// keep c:
				// ai/Aij*cj=0   (because the profit is already optimal but may not decrease)
				
				//   => ai>0
				//   => Aij>0
				//   => cj=0
				//   => c2j>0
				if(!base[j] && Math.abs(tc[j])<1e-12 && tc2[j]>0){ //TODO: check numeric stuff
					// find j with the smallest ai/Aij with Aij>0
					double minval = Double.POSITIVE_INFINITY;
					int minIndex = -1;
					for(int i=0; i<nConstraint; i++)
						if(tA[i][j]>0){
							double val = ta[i]/tA[i][j];
							if(0 <= val && val < minval){
								minval = val;
								minIndex = i;
							}
						}
					if(minval==Double.POSITIVE_INFINITY){
						this.type = UNBOUNDED;
						return null;
					}else if(tc2[j]*minval>highestImprovement){
						highestImprovement = tc2[j]*minval;
						entering = j;
						leaving = baseIndex[minIndex];
					}
				}
			
			
			if(highestImprovement>Double.NEGATIVE_INFINITY){
				changeBase(entering, leaving);
				int line = getVariablesLine(entering);
				if(tc2[entering] != 0){
					for (int j=0; j<nVariable+nConstraint; j++)
						if(j!=entering)
							tc2[j] -= tA[line][j]*tc2[entering];
					tc2[entering] = 0;
				}
			}
			if(highestImprovement>Double.NEGATIVE_INFINITY)printCurrentTableau();
		}while(highestImprovement>Double.NEGATIVE_INFINITY);
		
		
		double[] result = new double[nVariable];
		for(int i=0; i<nConstraint; i++){
			if(baseIndex[i]<nVariable)
				result[baseIndex[i]] = ta[i];
		}
		print("L�sung: ");
		for(int i=0; i<nVariable; i++)
			print(result[i]+"\t");
		println();
		
		type = SOLVABLE;
		if(conversion==null)
			return result;
		else
			return conversion.transformVector(result);
		
	}
	
	
	
	
	private void print(String s){
		if (debugprint) System.out.print(s);
	}
	private void println(String s){
		if (debugprint){
			System.out.println(s);
			System.out.print(debugprefix);
		}
	}
	private void println(){
		if (debugprint){
			System.out.println();
			System.out.print(debugprefix);
		}
	}
	private void printVector(double[] vector, String title){
		if (debugprint){
			println(title + " = [");
			for (int i=0; i<vector.length; i++)
				println("\t"+String.valueOf(vector[i])+",");
			println("]");
		}
	}
	private void printVector(boolean[] vector, String title){
		if (debugprint){
			println(title + " = [");
			for (int i=0; i<vector.length; i++)
				println("\t"+String.valueOf(vector[i])+",");
			println("]");
		}
	}
	
	private void printMatrix(Matrix matrix, String title){
		if (debugprint)
			matrix.print(title + " = ");
	}
	private void printMatrix(double[][] matrix, String title){
		if (debugprint)
			(new Matrix(matrix)).print(title + " = ");
	}
	
	private void printCurrentTableau(){
		if (debugprint){
			for(int j=0; j<nVariable; j++)
				print("x"+(j+1)+(base[j]?"�":"")+"\t");
			for(int j=0; j<nConstraint; j++)
				print("y"+(j+1)+(base[j+nVariable]?"�":"")+"\t");
			println();
			for(int j=0; j<nVariable+nConstraint; j++)
				print((double)Math.round(1000*tc[j])/1000+"\t");
			println();
			for(int j=0; j<nVariable+nConstraint; j++)
				print("--------");
			println();
			for(int i=0; i<nConstraint; i++){
				for(int j=0; j<nVariable+nConstraint; j++)
					print((double)Math.round(1000*tA[i][j])/1000+"\t");
				println("| "+ta[i]);
			}
			println();
			for(int j=0; j<nVariable+nConstraint; j++)
				print("--------");
			println();
		}
	}
}

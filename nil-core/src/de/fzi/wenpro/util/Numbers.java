/**
 * Ecoflex 2009
 * de.fzi.ecoflex.util.Numbers.java
 */
package de.fzi.wenpro.util;

import static de.nilgmbh.i18n.Messagesi18n.getString;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * @author siebel
 *
 */
public class Numbers {
	private static NumberFormat dGeneral = null;
	private static NumberFormat dPercent = null;
	private static NumberFormat dMetric = null;
	private static NumberFormat dWorkers = null;
	private static NumberFormat dWorkersInt = null;
	private static NumberFormat dMoney = null;
	private static NumberFormat dPieces = null;
	private static NumberFormat dRatio = null;
	private static NumberFormat dSeconds = null;
	private static NumberFormat dMinutes = null;
	private static NumberFormat dScrapRate = null;
	private static NumberFormat dEnergyConsumptionPerHour = null;
	private static NumberFormat dEnergyConsumption = null;
	private static NumberFormat dPowerRate = null;
	
	
	private static void createFormatObjects(){
		if (dGeneral == null){
			dGeneral = new DecimalFormat("0.0");
			dGeneral.setRoundingMode(RoundingMode.HALF_UP);

			dPercent = new DecimalFormat("0.0 %");
			dPercent.setRoundingMode(RoundingMode.HALF_UP);

			dMetric = new DecimalFormat("0.00 %");
			dMetric.setRoundingMode(RoundingMode.HALF_UP);

			dWorkers = new DecimalFormat("0.000");
			dWorkers.setRoundingMode(RoundingMode.HALF_UP);

			dWorkersInt = new DecimalFormat("0");
			dWorkersInt.setRoundingMode(RoundingMode.UP);
			
			dPieces = new DecimalFormat("0");
			dPieces.setRoundingMode(RoundingMode.HALF_UP);

			dRatio = new DecimalFormat("0.0 %");
			dRatio.setRoundingMode(RoundingMode.HALF_UP);

			dSeconds = new DecimalFormat("0.0 s");
			dSeconds.setRoundingMode(RoundingMode.HALF_UP);

			dMinutes = new DecimalFormat("0.00 min");
			dMinutes.setRoundingMode(RoundingMode.HALF_UP);

			dScrapRate = new DecimalFormat("0.0 %");
			dScrapRate.setRoundingMode(RoundingMode.HALF_UP);
			
			dEnergyConsumptionPerHour = new DecimalFormat("0.0 kWh");
			dEnergyConsumptionPerHour.setRoundingMode(RoundingMode.HALF_UP);
			
			dEnergyConsumption = new DecimalFormat("0.0 kWs");
			dEnergyConsumption.setRoundingMode(RoundingMode.HALF_UP);
			
			dPowerRate = new DecimalFormat("0.0 kW");
			dPowerRate.setRoundingMode(RoundingMode.HALF_UP);
			
			dMoney = new DecimalFormat("0.00 " + getString("currency"));
			dMoney.setRoundingMode(RoundingMode.HALF_UP);
		}
	}
	
	public static String format(double d){
		createFormatObjects();
		return dGeneral.format(d);
	}
	
	public static String formatPercent(double d){
		createFormatObjects();
		return dPercent.format(d);
	}
	
	public static String formatMetric(double d){
		createFormatObjects();
		return dMetric.format(d);
	}
	
	public static String formatWorkers(double d){
		createFormatObjects();
		return dWorkers.format(d);
	}
	
	public static String formatMoney(double d){
		createFormatObjects();
		return dMoney.format(d);
	}
	
	public static String formatPieces(double d){
		createFormatObjects();
		return dPieces.format(d);
	}
	
	public static String formatPiecesUnit(double d){
		createFormatObjects();
		String result = dPieces.format(d);
		return result + " " + (result.equals("1") ? getString("piecesSingular") : getString("pieces"));
	}
	
	public static String formatRatio(double d){
		createFormatObjects();
		return dRatio.format(d);
	}

	public static String formatSeconds(double d) {
		createFormatObjects();
		return dSeconds.format(d);
	}

	public static String formatMinutes(double d) {
		createFormatObjects();
		return dMinutes.format(d);
	}

	public static String formatScrapRate(double d) {
		createFormatObjects();
		return dScrapRate.format(d);
	}

	/**
	 * @param d
	 * @return
	 */
	public static String formatWorkersInt(double d) {
		createFormatObjects();
		return dWorkersInt.format(d);
	}
	
	public static String formatEnergyConsumptionPerHour(double d) {
		createFormatObjects();
		return dEnergyConsumptionPerHour.format(d/3600);
	}
	public static String formatEnergyConsumption(double d) {
		createFormatObjects();
		return dEnergyConsumption.format(d);
	}
	
	public static String formatPowerRate(double d) {
		createFormatObjects();
		return dPowerRate.format(d);
	}
	
}

package de.fzi.wenpro.util;

import java.util.Comparator;

import de.fzi.wenpro.core.model.Order;

public class EndDateComparator implements Comparator<Order>{
	
	public int compare(Order ord1, Order ord2){
		if(ord1.getEndDate().after(ord2.getEndDate()))
            return 1;
        else if(ord2.getEndDate().after(ord1.getEndDate()))
            return -1;
        else
            return 0;  
	}
	
}
/**
 * Ecoflex 2009
 * de.fzi.ecoflex.util.CalculationInterruptedException.java
 */
package de.fzi.wenpro.util;

/**
 * @author siebel
 *
 */
public class CalculationInterruptedException extends Exception{
	private static final long serialVersionUID = 1L;
}

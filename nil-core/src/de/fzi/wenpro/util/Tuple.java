package de.fzi.wenpro.util;

public class Tuple<S, T> {
	private S data1;
	private T data2;
	
	public Tuple(S data1, T data2) {
		this.data1 = data1;
		this.data2 = data2;
	}
	
	public S getFirst() {
		return data1;
	}
	
	public T getSecond() {
		return data2;
	}

	@Override
	public String toString() {
		return "Tuple [" + data1 + ", " + data2 + "]";
	}
	
	
}

package de.fzi.wenpro.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Helper class that contains convenience methods for manipulating {@link Calendar} objects
 * 
 * @author Kiril Aleksandrov
 *
 */
public class CalendarUtil {

	/**
	 * {@link DateFormat} for displaying in GUI elements
	 */
	private static DateFormat niceDf = new SimpleDateFormat("EEE. dd. MMMM yyyy");
	

	/**
	 * {@link DateFormat} for displaying date and time in GUI elements
	 */
	private static DateFormat niceDTf = new SimpleDateFormat("EEE. dd.MM.yy, hh:mm");
	
	/**
	 * This method returns the String representation of a given {@link Calendar}-entry. The format of the dates used is "EEE. dd. MMMM yyyy".
	 * This method should be used in GUI elements, that display formats
	 * 
	 * @param date
	 * @return the String representation of a Calendar
	 */
	public static String getDateTextNice(Calendar date){
		return niceDf.format(date.getTime());
	}
	
	
	/**
	 * This method returns the String representation of a given {@link Calendar}-entry. The format of the dates used is "EEE. dd.MM.YYYY, hh:mm".
	 * This method should be used in GUI elements, that display formats
	 * 
	 * @param date
	 * @return the String representation of a Calendar
	 */
	public static String getDateTimeTextNice(Calendar date){
		return niceDTf.format(date.getTime());
	}
	
}

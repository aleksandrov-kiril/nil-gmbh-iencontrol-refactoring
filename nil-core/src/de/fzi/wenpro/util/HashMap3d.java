/**
 * ecoFLEXiP 2010
 * de.fzi.ecoflex.util.HashMap2d.java
 */
package de.fzi.wenpro.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author siebel
 *
 */
public class HashMap3d<K1, K2, K3, V> extends HashMap2d<K1, K2, Map<K3, V>>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2250668120161674529L;

	public V get(Object key1, Object key2, Object key3){
		Map<K3, V> line = get(key1, key2);
		if (line == null){
			return null;
		}else{
			return line.get(key3);
		}
	}
	
	public V put(K1 key1, K2 key2, K3 key3, V value){
		Map<K3, V> line = get(key1, key2);
		if (line == null){
			line = new HashMap<K3, V>();
			put(key1, key2, line);
			line.put(key3, value);
		}
		return line.put(key3, value);
	}
	
    public boolean containsKey(Object key1, Object key2, Object key3){
		Map<K3, V> line = get(key1, key2);
		return line != null && line.containsKey(key3); 
    }
    
    public boolean containsValue(Object value){
    	for (Map<K2, Map<K3, V>> line : values()){
    		for (Map<K3, V> line2 : line.values()){
    			if (line2.containsValue(value)){
    				return true;
    			}
    		}
    	}
    	return false;
    }
    
    public Map<K3, V> remove(Object key1, Object key2){
		Map<K2, Map<K3, V>> line = get(key1);
		if (line != null){
			return line.remove(key2);
		}
		return null;
    }
    
    public V remove(Object key1, Object key2, Object key3){
		Map<K3, V> line = get(key1, key2);
		if (line != null){
			return line.remove(key3);
		}
		return null;
    }
    
    public void clear(Object key1){
		Map<K2, Map<K3, V>> line = get(key1);
		if (line != null){
			line.clear();
		}
    }
    
    public void clear(Object key1, Object key2){
		Map<K3, V> line = get(key1, key2);
		if (line != null){
			line.clear();
		}
    }
}

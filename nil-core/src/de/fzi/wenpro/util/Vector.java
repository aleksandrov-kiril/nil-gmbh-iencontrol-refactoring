package de.fzi.wenpro.util;


public class Vector {

	/**
	 * Returns a scaled copy of a vector; every component is multiplied by the
	 * same factor.
	 */
	public static double[] scale(double[] vector, double factor){
		double[] result = new double[vector.length];
		for (int i=0; i<vector.length; i++)
			result[i] = vector[i]*factor;
		return result;
		
	}

	
	
	/**
	 * Returns a copy of a vector with has the specified width. Returns the
	 * vector itself if it's size is correct.
	 */
	public static double[] resize(double[] v, int size){
		if(v == null  ||  v.length == size){
			return v;
		}else{
			double[] result = new double[size];
			System.arraycopy(v, 0, result, 0, Math.min(size, v.length));
			return result;
		}
	}
	
	
	
	/**
	 * Returns a sub vector.
	 */
	public static double[] sub(double[] v, int start, int size){
		double[] result = new double[size];
		System.arraycopy(v, start, result, 0, size);
		return result;
	}

	
	
	/**
	 * Concatenates two vectors.
	 */
	public static double[] append(double[] v1, double v2){
		double[] result = new double[v1.length+1];
		System.arraycopy(v1, 0, result, 0, v1.length);
		result[v1.length] = v2;
		return result;
	}

	
	
	/**
	 * Returns a vector filled with equal components
	 */
	public static double[] constant(int size, double value){
		double[] result = new double[size];
		java.util.Arrays.fill(result, value);
		return result;
	}

	
	
	/**
	 * Returns a vector of two blocks, of which each is filled with equal
	 * components.
	 */
	public static double[] block(int size1, double value1, int size2, double value2){
		double[] result = new double[size1+size2];
		java.util.Arrays.fill(result, 0, size1, value1);
		java.util.Arrays.fill(result, size1, size1+size2, value2);
		return result;
	}

	
	
	/**
	 * Calculates the sum of all components in a vector.
	 */
	public static double sum(double[] vector){
		double result = 0;
		if (vector != null)
			for (double d : vector)
				result += d;
		return result;
	}
	
	
	
	/**
	 * Returns the scalar product of the two given vectors.
	 */
	public static double sprod(double[] vector1, double[] vector2){
		double result = 0;
		for (int i=0; i<Math.min(vector1.length, vector2.length); i++)
			result += vector1[i]*vector2[i];
		return result;
	}


	
	/**
	 * Returns true if the given vector contains only zeros.
	 */
	public static boolean isZeroVector(double[] vector){
		for (int i=0; i<vector.length; i++)
			if (vector[i] != 0)
				return false;
		return true;
	}
	

	/**
	 * Prints the given vector, and adds a unit to each entry.
	 */
	public static void print(double[] a, String unit){
		Vector.print("", a, unit);
	}



	/**
	 * Prints the given vector.
	 */
	public static void print(double[] a){
		Vector.print("", a, "");
	}



	/**
	 * Prints the given vector with a label.
	 */
	public static void print(String label, double[] a){
		Vector.print(label, a, "");
	}


	
	/**
	 * Prints the given vector with a label, and adds a unit to each entry.
	 */
	public static void print(String label, double[] a, String unit){
		System.out.print(label+"\t");
		if(a == null)
			System.out.print("(null)");
		else
			for(double d : a){
				String s;
				if(Double.isNaN(d))
					s = "NaN";
				else
					s = Double.toString((double)Math.round(1000*d)/1000) + unit;
				if(s.length()<8)
					System.out.print(s + "\t\t");
				else
					System.out.print(s + "\t");
			}
		System.out.println();
	}
	
}

/**
 * 
 */
package de.nilgmbh.app.updatemechanism;

/**
 * @author wicaksono
 *
 */
public enum UpdateType {
	PROJECT_LOADED,
	PRODUCTIONPLAN_VIEW_CHANGED,
	PRODUCTIONPLAN_CHANGED, // FIXME whats the difference?
	PRODUCTIONPLAN_CALCULATED,
	NEW_OPERATION_CREATED;
	
}

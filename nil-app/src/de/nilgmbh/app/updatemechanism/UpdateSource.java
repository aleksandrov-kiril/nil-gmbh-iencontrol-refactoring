/**
 * 
 */
package de.nilgmbh.app.updatemechanism;

/**
 * Interface used by the {@link WorkingSession} to register and unregister
 * UpdateListeners to the states of the {@link WorkingSession} and to notify
 * them about the state changes in {@link WorkingSession}
 * 
 * @author FZI Forschungszentrum Informatik, Haid-und-Neu-Str. 10-14, 76131
 *         Karlsruhe, Intelligent Systems and Production Engineering (ISPE)
 * 
 * @author Kiril Aleksandrov
 * 
 */
public interface UpdateSource {
	public void registerUpdateListener(UpdateListener listener);

	public void unregisterUpdateListener(UpdateListener listener);
	
	/**
	 * Notify all registered listeners about the changes in the state
	 */
	public void notifyAllListener(UpdateType type);
}

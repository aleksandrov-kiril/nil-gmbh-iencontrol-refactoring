/**
 * 
 */
package de.nilgmbh.app.updatemechanism;

/**
 * Interface to be implemented by all Views which have to listen to changes
 * @author FZI Forschungszentrum Informatik, Haid-und-Neu-Str. 10-14, 76131
 *         Karlsruhe, Intelligent Systems and Production Engineering (ISPE)
 *
 *
 */
public interface UpdateListener {
	public void update(UpdateType type);
}

package de.nilgmbh.app.commands;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.internalmodel.WertpronetProjectPlan;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarModel;
import de.nilgmbh.app.ui.views.FeinScheduleView;
import de.nilgmbh.app.updatemechanism.UpdateType;

public class LoadProductionPlanProject extends AbstractHandler implements IHandler {
	private static DateFormat formatter = new SimpleDateFormat("yyyyMMdd_hhmmss");
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		FileDialog fd = new FileDialog(HandlerUtil.getActiveShell(event),SWT.OPEN);
		fd.setText(getString("LoadEcoflexProjectAction.dialogText"));
		fd.setFilterPath("./");
		fd.setFilterExtensions(new String[] {"*.wpplan"});
		String filename = fd.open();
		
		WertpronetProjectPlan wpp = null;
		try{
			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream in = new ObjectInputStream(fis);
			wpp = (WertpronetProjectPlan)in.readObject();
			in.close();
		}
		catch(IOException e){
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Project.setCurrentProject(wpp.getWertpronetProject());
		ProductionPlanProject ppp = ProductionPlanProject.getCurrentProductionPlanProject();
		
		for(ProductionPlan pp:wpp.getProducionPlans()){
			ProductionPlanTimeBarModel pptbm = new ProductionPlanTimeBarModel(pp);
			String ppName = "Plan "+ formatter.format(Calendar.getInstance().getTime());
			pptbm.setName(ppName);
			ppp.getProductionPlanTimeBarModels().add(pptbm);
			ppp.setCurrentProductionPlanTimeBarModel(pptbm);
			
		}
		
		for(ProductionPlanTimeBarModel pptbm:ppp.getProductionPlanTimeBarModels()){
			ppp.getProductionPlanTimeBarModels().remove(pptbm);
			ProductionPlanTimeBarModel newpptbm = new ProductionPlanTimeBarModel(pptbm.getProductionPlan());
			newpptbm.setName(pptbm.getName());
			ppp.getProductionPlanTimeBarModels().add(newpptbm);
			ppp.setCurrentProductionPlanTimeBarModel(newpptbm);
			try {
				IViewPart vp = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().showView(FeinScheduleView.ID, Integer.toString(AlternativeProductionPlanHandler.instanceNum++), IWorkbenchPage.VIEW_ACTIVATE);
				((FeinScheduleView)vp).setPartName(ppp.getCurrentProductionPlanTimeBarModel().getName());
//				((FeinScheduleView)vp).setInput(ppp.getCurrentProductionPlanTimeBarModel());
			} catch (PartInitException e) { 	
				MessageDialog.openError(HandlerUtil.getActiveWorkbenchWindow(event).getShell(), "Error", "Error opening view:" + e.getMessage());
			}
		}
		ProductionPlanProject.setCurrentProductionPlanProject(ppp);
		
		// notify all views
		Activator.getDefault().notifyAllListener(UpdateType.PROJECT_LOADED);
		Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CALCULATED); // TODO open all feinschedule-view for all PP
		

		return null;
	}


}

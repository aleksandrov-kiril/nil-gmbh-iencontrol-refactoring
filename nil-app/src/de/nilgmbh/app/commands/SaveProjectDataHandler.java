package de.nilgmbh.app.commands;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.handlers.HandlerUtil;

import de.fzi.wenpro.core.model.Project;

public class SaveProjectDataHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		
		Project outputObject = Project.getCurrentProject();
		
		// choose file for saving the WertpronetProjectPlan
		FileDialog fd = new FileDialog(HandlerUtil.getActiveShell(event),SWT.SAVE);
		fd.setText("Save Project Data");
		fd.setFilterPath("./");
		fd.setFilterExtensions(new String[] {"*.wpr"});
		String filename = fd.open();
		
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(outputObject);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}

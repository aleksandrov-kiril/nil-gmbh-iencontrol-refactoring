package de.nilgmbh.app.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import de.nilgmbh.app.ui.views.FactoryView;

public class OpenFactoryCheckView extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			//						IViewPart vp = window.getActivePage().showView(FeinScheduleView.ID, Integer.toString(AlternativeProductionPlanHandler.instanceNum++), IWorkbenchPage.VIEW_ACTIVATE);
			IViewPart vp = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().showView(FactoryView.ID);

		} catch (PartInitException e) {
			MessageDialog.openError(HandlerUtil.getActiveWorkbenchWindow(event).getShell(), "Error", "Error opening view:" + e.getMessage());
		}
		return null;
	}

}

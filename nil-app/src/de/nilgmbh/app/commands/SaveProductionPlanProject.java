package de.nilgmbh.app.commands;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.handlers.HandlerUtil;

import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.internalmodel.WertpronetProjectPlan;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarModel;

public class SaveProductionPlanProject extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		List<ProductionPlan> pplist = new ArrayList<ProductionPlan>();
		for( ProductionPlanTimeBarModel pptbm :ProductionPlanProject.getCurrentProductionPlanProject().getProductionPlanTimeBarModels()){
			pplist.add(pptbm.getProductionPlan());
		}
		
		// Create the WertpronetProjectPlan
		WertpronetProjectPlan wpp = new WertpronetProjectPlan(Project.getCurrentProject(), pplist);
		
		
		// choose file for saving the WertpronetProjectPlan
		FileDialog fd = new FileDialog(HandlerUtil.getActiveShell(event),SWT.SAVE);
		fd.setText("Speichern Sie den aktuellen Planungszustand");
		fd.setFilterPath("./");
		fd.setFilterExtensions(new String[] {"*.wpplan"});
		String filename = fd.open();
		
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(wpp);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}


}

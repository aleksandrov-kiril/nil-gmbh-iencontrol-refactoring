package de.nilgmbh.app.commands;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.handlers.HandlerUtil;

import de.fzi.wenpro.core.model.Project;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.Application;
import de.nilgmbh.app.updatemechanism.UpdateType;

public class LoadProjectDataHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		FileDialog fd = new FileDialog(HandlerUtil.getActiveShell(event),SWT.OPEN);
		fd.setText("Load Project Data");
		fd.setFilterPath("./");
		fd.setFilterExtensions(new String[] {"*.wpr"});
		String filename = fd.open();
		
		Project ep = null;
		try{
			
			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream ois = new ObjectInputStream(fis);
			ep = (Project) ois.readObject();
			ois.close();

		}
		catch(IOException e){
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(ep.getOriginalFactory().getName());
		Project.setCurrentProject(ep);
		Activator.getNewOrders().addAll(ep.getOriginalFactory().getOrders());

		// notify all views
		Activator.getDefault().notifyAllListener(UpdateType.PROJECT_LOADED);
		Application.fileLoaded=true;
		Application.SCENARIO = ep.getOriginalFactory().getName();
		return null;
	}

}

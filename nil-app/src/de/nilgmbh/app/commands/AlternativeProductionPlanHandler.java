/**
 * 
 */
package de.nilgmbh.app.commands;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.jaret.util.date.JaretDate;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.ui.dialogs.AlternativeWizard;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarModel;
import de.nilgmbh.app.ui.views.FeinScheduleView;

/**
 * @author Ardhi
 *
 */
public class AlternativeProductionPlanHandler extends AbstractHandler{

	public static int instanceNum = 0;
	private Display d;
	private Shell s;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IWorkbenchPage page = window.getActivePage();
		d = PlatformUI.getWorkbench().getDisplay();
		s = new Shell (d);		
		AlternativeWizard aw = new AlternativeWizard();
		WizardDialog wd = new WizardDialog(s, aw);
		Date currentDate = new Date();
		
		if(wd.open()== Window.OK){			
			if(aw.isNewPlan()){
				ProductionPlan productionPlan = new ProductionPlan();
				ProductionPlanTimeBarModel newTimeBarModel = new ProductionPlanTimeBarModel(productionPlan);
				newTimeBarModel.setName(aw.getTitleName() + "_" + currentDate.toString());
				ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel(newTimeBarModel);
				ProductionPlanProject.getCurrentProductionPlanProject().getProductionPlanTimeBarModels().add(newTimeBarModel);
				

				try {

					IViewPart vp = page.showView(FeinScheduleView.ID, Integer.toString(instanceNum++), IWorkbenchPage.VIEW_ACTIVATE);
					Calendar startDate = aw.getStartingDate();
					System.out.println(startDate.getTime().toString());
					((FeinScheduleView)vp).setPartName(aw.getTitleName());
					((FeinScheduleView)vp).get_tbv().setStartDate(new JaretDate(startDate.getTime()));
					


				} 
				catch (PartInitException e) {
					MessageDialog.openError(window.getShell(), "Error", "Error opening view:" + e.getMessage());
				}
			}
			else{
				ProductionPlanTimeBarModel copyTimeBarModel= ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().clone();
				copyTimeBarModel.setName(aw.getTitleName() + "_" + currentDate.toString());
				ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel(copyTimeBarModel);
				ProductionPlanProject.getCurrentProductionPlanProject().getProductionPlanTimeBarModels().add(copyTimeBarModel);


				
				try {

					IViewPart vp = page.showView(FeinScheduleView.ID, Integer.toString(instanceNum++), IWorkbenchPage.VIEW_ACTIVATE);
					((FeinScheduleView)vp).setPartName(aw.getTitleName());


				} 
				catch (PartInitException e) {
					e.printStackTrace();
				}
				
			}
			
		}
		return null;
	} 




}

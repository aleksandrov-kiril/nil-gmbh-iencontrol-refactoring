package de.nilgmbh.app.commands;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;

import de.nilgmbh.app.internalmodel.PropertyRow;

public class ValueColumnEditingSupport extends EditingSupport {
	private TableViewer tableViewer;

	public ValueColumnEditingSupport(TableViewer tableViewer) {
		super(tableViewer);
		this.tableViewer = tableViewer;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return new TextCellEditor(tableViewer.getTable());
	}

	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	
	@Override
	protected Object getValue(Object element) {
		return ((PropertyRow)element).getValue();
	}

	@Override
	protected void setValue(Object element, Object value) {
		((PropertyRow)element).setValue(value);
		tableViewer.update(element, null);
	}

}

package de.nilgmbh.app.commands;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.jaret.util.date.JaretDate;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.ui.dialogs.CalculateWizard;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarModel;
import de.nilgmbh.app.ui.views.FeinScheduleView;
import de.nilgmbh.app.ui.views.ProductionPlanPropertiesView;
import de.nilgmbh.app.updatemechanism.UpdateType;
import de.nilgmbh.energyhyperheuristic.hyper.PlanCreator;

public class CalculateHandler extends AbstractHandler implements IHandler {

	public static int instanceNum = 0;
	private static DateFormat formatter = new SimpleDateFormat("yyyyMMdd_hhmmss");
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();

		CalculateWizard cw = new CalculateWizard();

		WizardDialog wd = new WizardDialog(window.getShell(), cw){
			
			@Override
			protected Button createButton(Composite parent, int id, String label,
					boolean defaultButton) {
				if(id == IDialogConstants.FINISH_ID){
					Button finish = super.createButton(parent, id, label, defaultButton);
					finish.setText("OK");
					return finish;
				}
				else{
					return super.createButton(parent, id, label, defaultButton);
				}
			}
		};
		if(wd.open()== Window.OK){	

			if(cw.isNewCalculation()){
				
				PlanCreator creator = new PlanCreator();

				Calendar begin = cw.getCalculatingDate();
				Calendar now = Calendar.getInstance();
				

				
//				ProductionPlan productionPlan = null;
//				if(begin.before(now)){
//					productionPlan = new ProductionPlan(now);
//				}else if(begin.after(now)){
//					productionPlan = new ProductionPlan(begin);
//				}
				ProductionPlan productionPlan = new ProductionPlan(begin);
				
				ProductionPlan productionPlan2 = new ProductionPlan(creator.createPlan(productionPlan, 
																					  Project.getCurrentProject().getOriginalFactory().getOrders(), 
																					  begin, now,
																					  cw.getCalculationTime(), 
																					  cw.getMaxEnergyLevel(),
																					  cw.getMaxEnergyLevelWeight(),
																					  cw.getNightShiftAvoidanceWeight())); 
				
				// graphical stuff
				ProductionPlanTimeBarModel newProductionPlanTimeBarModel = 
						new ProductionPlanTimeBarModel(productionPlan2);
				ProductionPlanProject.setCurrentDate(new JaretDate(begin.getTime()));			
							
				
				String ppName = "Plan "+ formatter.format(Calendar.getInstance().getTime());
				newProductionPlanTimeBarModel.setName(ppName);
				//TODO: the result is still stored in original production plan time bar model
				//					ProductionPlanProject.getCurrentProductionPlanProject().setOriginalProductionPlanTimeBarModel(newProductionPlanTimeBarModel);
				ProductionPlanProject.getCurrentProductionPlanProject().getProductionPlanTimeBarModels().add(newProductionPlanTimeBarModel);
				ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel(newProductionPlanTimeBarModel);
//				Activator.clearNewOrderList();

				try {
					//						IViewPart vp = window.getActivePage().showView(FeinScheduleView.ID, Integer.toString(AlternativeProductionPlanHandler.instanceNum++), IWorkbenchPage.VIEW_ACTIVATE);
					IViewPart vp = window.getActivePage().showView(FeinScheduleView.ID, ppName, IWorkbenchPage.VIEW_ACTIVATE);

					((FeinScheduleView)vp).setPartName(newProductionPlanTimeBarModel.getName());
					((FeinScheduleView)vp).setInput(newProductionPlanTimeBarModel);
				} catch (PartInitException e) {
					MessageDialog.openError(window.getShell(), "Error", "Error opening view:" + e.getMessage());
				}

				try {
					window.getActivePage().showView(ProductionPlanPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
				} catch (PartInitException e) {
					e.printStackTrace();
				}	

				// TODO: notify all views
				Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CALCULATED);
				Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);

			} else if(!cw.isNewCalculation()){
				System.out.println("RECALCULATION");

				if (Project.getCurrentProductionPlan()!= null){
					PlanCreator creator = new PlanCreator();
	
					Calendar begin = cw.getCalculatingDate();
					Calendar now = Calendar.getInstance();
		
					//FIXME: Get selected Plan from DropDownMenue
					ProductionPlan productionPlan = new ProductionPlan(Project.getCurrentProductionPlan());
					
					ProductionPlan productionPlan2 = new ProductionPlan(creator.createPlan(productionPlan, 
																						  Project.getCurrentProject().getOriginalFactory().getOrders(), 
																						  begin, now,
																						  cw.getCalculationTime(), 
																						  cw.getMaxEnergyLevel(),
																						  cw.getMaxEnergyLevelWeight(),
																						  cw.getNightShiftAvoidanceWeight())); 
					ProductionPlanTimeBarModel newProductionPlanTimeBarModel = 
							new ProductionPlanTimeBarModel(productionPlan2);
//					Project.setCurrentProductionPlan(productionPlan2);
//					
//					ProductionPlan plan;
//					plan = Project.getCurrentProductionPlan();
								
					
					String ppName = "Plan "+ formatter.format(Calendar.getInstance().getTime());
					newProductionPlanTimeBarModel.setName(ppName);
					//TODO: the result is still stored in original production plan time bar model
					//					ProductionPlanProject.getCurrentProductionPlanProject().setOriginalProductionPlanTimeBarModel(newProductionPlanTimeBarModel);
					ProductionPlanProject.getCurrentProductionPlanProject().getProductionPlanTimeBarModels().add(newProductionPlanTimeBarModel);
					ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel(newProductionPlanTimeBarModel);
	//				Activator.clearNewOrderList();
	
					try {
						//						IViewPart vp = window.getActivePage().showView(FeinScheduleView.ID, Integer.toString(AlternativeProductionPlanHandler.instanceNum++), IWorkbenchPage.VIEW_ACTIVATE);
						IViewPart vp = window.getActivePage().showView(FeinScheduleView.ID, ppName, IWorkbenchPage.VIEW_ACTIVATE);
	
						((FeinScheduleView)vp).setPartName(newProductionPlanTimeBarModel.getName());
						((FeinScheduleView)vp).setInput(newProductionPlanTimeBarModel);
					} catch (PartInitException e) {
						MessageDialog.openError(window.getShell(), "Error", "Error opening view:" + e.getMessage());
					}
	
					try {
						window.getActivePage().showView(ProductionPlanPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
					} catch (PartInitException e) {
						e.printStackTrace();
					}	
	
					// TODO: notify all views
					Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CALCULATED);
					Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
				} else {
					System.out.println("No Plan available");
				}
			}
		}	
		return null;
	}

}

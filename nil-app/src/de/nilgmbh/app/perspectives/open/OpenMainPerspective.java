package de.nilgmbh.app.perspectives.open;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveRegistry;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.handlers.HandlerUtil;

import de.nilgmbh.app.perspectives.Perspective;

public class OpenMainPerspective extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IPerspectiveRegistry perspectiveRegistry = HandlerUtil.getActiveWorkbenchWindow(event).getWorkbench().getPerspectiveRegistry();
		IPerspectiveDescriptor pd = perspectiveRegistry.findPerspectiveWithId(Perspective.ID);
		IWorkbenchPage page = HandlerUtil.getActiveSite(event).getPage();
		page.setPerspective(pd);
		
		IEditorReference[] alleditors = page.getEditorReferences();
		for (IEditorReference editorRef:alleditors) {
			page.hideEditor(editorRef);
		}
		return null;
	}

}

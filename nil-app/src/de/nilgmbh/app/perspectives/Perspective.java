package de.nilgmbh.app.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.nilgmbh.app.ui.views.FactoryView;
import de.nilgmbh.app.ui.views.FeinScheduleView;
import de.nilgmbh.app.ui.views.OrdersView;
import de.nilgmbh.app.ui.views.PPEntryEnergyInformationView;
import de.nilgmbh.app.ui.views.PPEntryPropertiesView;
import de.nilgmbh.app.ui.views.ProductTreeView;
import de.nilgmbh.app.ui.views.ProductionPlanPropertiesView;
import de.nilgmbh.app.ui.views.WarningView;

public class Perspective implements IPerspectiveFactory {
	
	public static final String ID = Perspective.class.getName();	
	

	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);
		layout.setFixed(false);				
	
		IFolderLayout topLeft = layout.createFolder("TopLeft", IPageLayout.TOP, 0.65f, editorArea);
		topLeft.addPlaceholder(FeinScheduleView.ID + ":*");
		topLeft.addPlaceholder(FactoryView.ID);
		topLeft.addView(OrdersView.ID);
		topLeft.addView(FeinScheduleView.ID);
		topLeft.addView(ProductTreeView.ID);

	
		IFolderLayout bottomLeft = layout.createFolder("BottomLeft", IPageLayout.BOTTOM, 0.3f,editorArea);
		bottomLeft.addView(PPEntryPropertiesView.ID);		
		bottomLeft.addView(ProductionPlanPropertiesView.ID);
		bottomLeft.addView(PPEntryEnergyInformationView.ID);
	
		IFolderLayout bottomRight = layout.createFolder("BottomRight", IPageLayout.RIGHT, 0.3f,"BottomLeft");
		bottomRight.addView(WarningView.ID);
		
		layout.getViewLayout(ProductTreeView.ID).setCloseable(false);
		layout.getViewLayout(PPEntryPropertiesView.ID).setCloseable(false);
		layout.getViewLayout(WarningView.ID).setCloseable(false);
		layout.getViewLayout(OrdersView.ID).setCloseable(false);
		layout.getViewLayout(ProductionPlanPropertiesView.ID).setCloseable(false);
		layout.getViewLayout(PPEntryEnergyInformationView.ID).setCloseable(false);
	}

}

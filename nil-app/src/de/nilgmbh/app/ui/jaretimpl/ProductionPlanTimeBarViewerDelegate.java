/**
 * 
 */
package de.nilgmbh.app.ui.jaretimpl;

import java.awt.Cursor;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import de.jaret.util.date.Interval;
import de.jaret.util.date.JaretDate;
import de.jaret.util.ui.timebars.TimeBarMarker;
import de.jaret.util.ui.timebars.TimeBarViewerDelegate;
import de.jaret.util.ui.timebars.TimeBarViewerInterface;
import de.jaret.util.ui.timebars.TimeBarViewerInterface.Orientation;
import de.jaret.util.ui.timebars.mod.IntervalModificator;
import de.jaret.util.ui.timebars.model.TimeBarRow;

/**
 * @author Ardhi
 *
 */
public class ProductionPlanTimeBarViewerDelegate extends TimeBarViewerDelegate {

	List<TimeBarRow> _highlightedRows;
    /** default delta for modification snap/detection. */
    private static final int DEFAULT_SEL_DELTA = 4;
	
    /** delta for detecting modification clicks and drags. */
    private int _selectionDelta = DEFAULT_SEL_DELTA;
	
	public ProductionPlanTimeBarViewerDelegate(TimeBarViewerInterface tbvi) {
		super(tbvi);
		_highlightedRows = new ArrayList<TimeBarRow>();
	
	}
	
	
	
    public List<TimeBarRow> getHighlightedRows() {
		return _highlightedRows;
	}



	public void setHighlightedRows(List<TimeBarRow> _highlightedRows) {
		this._highlightedRows = _highlightedRows;
	}



	/**
     * Highlight the given rows.
     * 
     * @param row the row to be highlighted
     */
    public void highlightRows(List<TimeBarRow> rows) {
        List<TimeBarRow> oldRows = new ArrayList<TimeBarRow>();
        for (TimeBarRow oldRow : _highlightedRows) 
        	oldRows.add(oldRow);
        
        _highlightedRows.clear();
        for (TimeBarRow row : rows) {
        	_highlightedRows.add(row);
        }
        
        for (TimeBarRow row : _highlightedRows) 
        	_tbvi.repaint(getRowBounds(row));
        
        for (TimeBarRow oldRow : oldRows) 
        	_tbvi.repaint(getRowBounds(oldRow));

    }
    /**
     * Check whether the hierarchy delimiting line is hit by the given location.
     * 
     * @param x x coordinate
     * @param y y coordinate
     * @return true if the location is above or in the range of the selection delta
     */
    private boolean hierarchyLineHit(int x, int y) {
        if (_orientation == Orientation.HORIZONTAL) {
            return (_hierarchyWidth > 0 && Math.abs(_hierarchyWidth - x) < _selectionDelta);
        } else {
            return (_hierarchyWidth > 0 && Math.abs(_hierarchyWidth - y) < _selectionDelta);
        }
    }
    
    /**
     * Check whether the header delimiting line is hit by the given location.
     * 
     * @param x x coordinate
     * @param y y coordinate
     * @return true if the location is above or in the range of the selection delta
     */
    private boolean headerLineHit(int x, int y) {
        if (_orientation == Orientation.HORIZONTAL) {
            return (_yAxisWidth > 0 && Math.abs(_hierarchyWidth + _yAxisWidth - x) < _selectionDelta);
        } else {
            return (_yAxisWidth > 0 && Math.abs(_hierarchyWidth + _yAxisWidth - y) < _selectionDelta);
        }
    }
    
    /**
     * Retrieve the interval that has a bound near to the given x coordinate in a given row. If more than one interval
     * might be hit, the exact coordinates are checked. if the location checked is in beetween two intervals in reach,
     * the first interval will be returned.
     * 
     * @param row row in question
     * @param x x coordinate to be checked for intervals
     * @param y y coordinate
     * @return nearest interval with bound inside the area around x or <code>null</code>
     */
    private Interval getTouchedInterval(TimeBarRow row, int x, int y) {
        // check only intervals currently displayed
        List<Interval> intervals = row.getIntervals(getStartDate(), getEndDate());
        List<Interval> candidates = new ArrayList<Interval>(5);
        List<Rectangle> candidateRects = new ArrayList<Rectangle>(5);
        for (Interval interval : intervals) {
            Rectangle intervalRect = getIntervalBounds(row, interval);
            if (_orientation == Orientation.HORIZONTAL) {
                if (y >= intervalRect.y && y <= intervalRect.y + intervalRect.height) {
                    if (Math.abs(x - intervalRect.x) <= _selectionDelta) {
                        candidates.add(interval);
                        candidateRects.add(intervalRect);
                    } else if (Math.abs(intervalRect.x + intervalRect.width - x) <= _selectionDelta) {
                        candidates.add(interval);
                        candidateRects.add(intervalRect);
                    }
                }
            } else {
                if (x >= intervalRect.x && x <= intervalRect.x + intervalRect.width) {
                    if (Math.abs(y - intervalRect.y) <= _selectionDelta) {
                        candidates.add(interval);
                        candidateRects.add(intervalRect);
                    } else if (Math.abs(intervalRect.y + intervalRect.height - y) <= _selectionDelta) {
                        candidates.add(interval);
                        candidateRects.add(intervalRect);
                    }
                }
            }
        }
        // check candidates
        if (candidates.size() == 0) {
            return null;
        }
        if (candidates.size() == 1) {
            return candidates.get(0);
        }

        for (int i = 0; i < candidates.size(); i++) {
            Interval interval = candidates.get(i);
            Rectangle rect = candidateRects.get(i);
            if (rect.contains(x, y)) {
                return interval;
            }
        }
        // there might be the case that no inteval is a direct hit ...
        // then just use the first one
        return candidates.get(0);

    }

    /**
     * Check whether resizing is allowed for a given Interval.
     * 
     * @param row row of the interval
     * @param interval interval to check resize allowance for
     * @return true if interval is allowed to be resized
     */
    private boolean isResizingAllowed(TimeBarRow row, Interval interval) {
        if (_intervalModificators == null || _intervalModificators.size() == 0) {
            return false;
        }
        boolean result = true;
        if (_intervalModificators != null) {
            for (IntervalModificator modificator : _intervalModificators) {
                result = result
                        && (!modificator.isApplicable(row, interval) || modificator.isSizingAllowed(row, interval));
            }
        }
        return result;
    }
    
    @Override
    public void mouseMoved(int x, int y) {
        boolean horizontal = _orientation == Orientation.HORIZONTAL;
        boolean nothingHitInDiagramArea = true;
        if (_lineDraggingAllowed && (hierarchyLineHit(x, y) || headerLineHit(x, y))) {
        	if(y < 60){
        		_tbvi.setCursor(Cursor.W_RESIZE_CURSOR);
        	}
        	else{
        		_tbvi.setCursor(Cursor.DEFAULT_CURSOR);
        	}
            nothingHitInDiagramArea = false;
        } else if (_rowHeightDraggingAllowed && rowLineHit(x, y)) {
            _tbvi.setCursor(Cursor.HAND_CURSOR);
            nothingHitInDiagramArea = false;
        } else if (_diagramRect != null && _diagramRect.contains(x, y)) {
            // in the diagram area check for interval bounds and change cursor
            // if an interval modificator
            // is set and resizing is allowed
            TimeBarRow row = rowForXY(x, y);
            if (row != null) {
                Interval interval = getTouchedInterval(row, x, y);
                if (interval != null && isResizingAllowed(row, interval)) {
                    JaretDate d = dateForXY(x, y);
                    long eastDiff = Math.abs(d.diffMilliSeconds(interval.getBegin()));
                    long westDiff = Math.abs(d.diffMilliSeconds(interval.getEnd()));
                    if (eastDiff < westDiff) {
                        _tbvi.setCursor(horizontal ? Cursor.E_RESIZE_CURSOR : Cursor.N_RESIZE_CURSOR);
                    } else {
                        _tbvi.setCursor(horizontal ? Cursor.W_RESIZE_CURSOR : Cursor.S_RESIZE_CURSOR);
                    }
                    nothingHitInDiagramArea = false;
                } else {
                    _tbvi.setCursor(Cursor.DEFAULT_CURSOR);
                }
            }
        } else {
            _tbvi.setCursor(Cursor.DEFAULT_CURSOR);
        }
        // if the mouse is in the axis area and hits a marker change the
        // cursor (or if dragging in the area is allowed)
        if (nothingHitInDiagramArea
                && ((_xAxisRect != null && _xAxisRect.contains(x, y)) || _markerDraggingInDiagramArea)) {
            TimeBarMarker marker = getMarkerForXY(x, y);
            if (marker != null) {
                _tbvi.setCursor(Cursor.HAND_CURSOR);
            } else {
                _tbvi.setCursor(Cursor.DEFAULT_CURSOR);
            }
        }

    }

}

/**
 * 
 */
package de.nilgmbh.app.ui.jaretimpl;

import java.util.ArrayList;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.core.model.productionplan.Schedule;
import de.fzi.wenpro.core.printing.ProductionPlanPrinter;
import de.jaret.util.ui.timebars.model.DefaultRowHeader;
import de.jaret.util.ui.timebars.model.DefaultTimeBarModel;
import de.jaret.util.ui.timebars.model.TimeBarModelListener;
import de.jaret.util.ui.timebars.model.TimeBarRow;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;

/**
 * @author wicaksono
 *
 */
public class ProductionPlanTimeBarModel extends DefaultTimeBarModel {
	private ProductionPlan productionPlan;
	private String name;

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductionPlanTimeBarModel() {
		super();
		
	}
	
	public ProductionPlanTimeBarModel(ProductionPlan productionPlan) {
		this();
		this.productionPlan = productionPlan;
		buildModel();
	}

	public ProductionPlan getProductionPlan() {
		return productionPlan;
	}

	public void setProductionPlan(ProductionPlan productionPlan) {
		this.productionPlan = productionPlan;
		clearModel();
		buildModel();
	}
	
	public ProductionPlanTimeBarModel(ProductionPlanTimeBarModel src) {
		this._listenerList = new ArrayList<TimeBarModelListener>();
		for (TimeBarModelListener listener : src._listenerList) {
			this._listenerList.add(listener);
		}
		this._maxDate = src._maxDate.copy();
		this._minDate = src._minDate.copy();
		this.productionPlan = src.productionPlan.clone();
		this._rows = new ArrayList<TimeBarRow>();
		for (TimeBarRow row : src._rows) {
			if (row instanceof WorkplaceTimeBarRowModel) {
				WorkplaceTimeBarRowModel workplaceRow = new WorkplaceTimeBarRowModel ( (WorkplaceTimeBarRowModel) row, this.productionPlan);
				_rows.add(workplaceRow);
				Schedule<ProductionPlanEntry> schedule = this.productionPlan.getEntries().get(workplaceRow.getWorkplace());
				
				if (schedule != null) {
					for (int i = 0; i < schedule.size(); i++) {
						ProductionPlanEntry productionPlanEntry = schedule.get(i);
						double begin = schedule.getTime(i);
						ProductionPlanEntryIntervalImpl interval = new ProductionPlanEntryIntervalImpl(productionPlanEntry);
						interval.setBegin(ProductionPlanProject.getCurrentDate().copy().advanceSeconds(begin));
						double end = begin + productionPlanEntry.getDuration();
						interval.setEnd(ProductionPlanProject.getCurrentDate().copy().advanceSeconds(end));
						workplaceRow.addInterval(interval);
					}
				}
			}
		}
	}
	
	public ProductionPlanTimeBarModel clone() {
		return new ProductionPlanTimeBarModel(this);
		
	}
	
	private void buildModel() {
		for (Workplace wp : EntityList.getWorkplaceList(Project.getCurrentProject().getOriginalFactory())) {
			WorkplaceTimeBarRowModel timerBarRowModel = new WorkplaceTimeBarRowModel(new DefaultRowHeader(wp.getName()), wp, productionPlan);
			this.addRow(timerBarRowModel);
			Schedule<ProductionPlanEntry> schedule = productionPlan.getEntries().get(wp);
			
			if (schedule != null) {
				for (int i = 0; i < schedule.size(); i++) {
					ProductionPlanEntry productionPlanEntry = schedule.get(i);
					double begin = schedule.getTime(i);
					timerBarRowModel.addInterval(begin, productionPlanEntry);
				}
			}
			
		}
		ProductionPlanPrinter.printProductionPlan(productionPlan);		
	}
	
	
	public TimeBarRow getTimeBarRow(Workplace workplace) {
		for (int i = 0; i < this.getRowCount(); i++) {
			if (this.getRow(i).getRowHeader().getLabel().equals(workplace.getName())) {
				return this.getRow(i);
			}
		}
		return null;
	}
	
	
	private void clearModel() {
		for (TimeBarRow row : this._rows) {
			row.getIntervals().clear();
		}
		this._rows.clear();
	}
}

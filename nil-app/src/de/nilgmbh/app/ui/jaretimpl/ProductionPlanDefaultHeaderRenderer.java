package de.nilgmbh.app.ui.jaretimpl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import de.jaret.util.swt.SwtGraphicsHelper;
import de.jaret.util.ui.timebars.TimeBarViewerDelegate;
import de.jaret.util.ui.timebars.model.TimeBarRowHeader;
import de.jaret.util.ui.timebars.swt.renderer.DefaultHeaderRenderer;

public class ProductionPlanDefaultHeaderRenderer extends DefaultHeaderRenderer {
	public ProductionPlanDefaultHeaderRenderer(){
        super(null);
	}
	
	@Override
    public void draw(GC gc, Rectangle drawingArea, TimeBarViewerDelegate delegate, TimeBarRowHeader header,
            boolean selected, boolean printing) {

        String str = header.toString();
        Color bg = gc.getBackground();
        Color fg = gc.getForeground();
        if (selected && !printing) {
            gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
            gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
        }
        gc.fillRectangle(drawingArea);
//        SwtGraphicsHelper.drawStringCenteredVCenter(gc, str, drawingArea.x, drawingArea.x + drawingArea.width,
//                drawingArea.y + drawingArea.height / 2);
        SwtGraphicsHelper.drawStringLeftAlignedVCenter(gc, str, drawingArea.x, drawingArea.y + drawingArea.height / 2);

        gc.setBackground(bg);
        gc.setForeground(fg);
        if (delegate.getDrawRowGrid()) {
            if (printing) {
                gc.setLineWidth(getDefaultLineWidth());
            }
             gc.drawLine(drawingArea.x, drawingArea.y + drawingArea.height - 1, drawingArea.x + drawingArea.width, drawingArea.y + drawingArea.height - 1);

            gc.setLineWidth(1);
        }
    }

}

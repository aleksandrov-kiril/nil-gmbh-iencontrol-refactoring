/**
 * 
 */
package de.nilgmbh.app.ui.jaretimpl;

import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.jaret.util.date.Interval;
import de.jaret.util.date.IntervalImpl;
import de.jaret.util.date.JaretDate;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.updatemechanism.UpdateType;

/**
 * @author wicaksono
 *
 */
public class ProductionPlanEntryIntervalImpl  extends IntervalImpl {
	private ProductionPlanEntry entry;
	public ProductionPlanEntryIntervalImpl() {
		super();
	}
	
	public ProductionPlanEntryIntervalImpl(ProductionPlanEntry entry) {
		super();
		this.entry = entry;
		
	}
	public ProductionPlanEntryIntervalImpl(JaretDate begin, JaretDate end) {
		super(begin, end);
	}

	public ProductionPlanEntryIntervalImpl(JaretDate begin, JaretDate end, ProductionPlanEntry productionPlanEntry) {
		super(begin, end);
		this.entry = productionPlanEntry;
	}
	
	public ProductionPlanEntryIntervalImpl(ProductionPlanEntryIntervalImpl src) {
		this (src._begin.copy(), src._end.copy(), src.entry.clone());	
	}
	
	public ProductionPlanEntryIntervalImpl (ProductionPlanEntryIntervalImpl src, ProductionPlanEntry ppEntry) {
		this (src._begin.copy(), src._end.copy(), ppEntry);		
	}
	
	public ProductionPlanEntryIntervalImpl clone() {
		return new ProductionPlanEntryIntervalImpl(this);
	}
	
	public ProductionPlanEntry getEntry() {
		return entry;
	}
	public void setEntry(ProductionPlanEntry entry) {
		this.entry = entry;
	}
	
	/**
	 * The percentage of time in this interval in which there are higher costs.
	 * The costs are higher on weekends (Saturday, 0:00 until Sunday, 23:59:59)
	 * and every day from 22:00 until 6:00.
	 * @return the percentage of expensive time
	 */
	public double getExpensiveTimeRatio(){
		final int standardWorkHourBegin = 6;
		final int standardWorkHourEnd = 22;
		
		double sumHours = 0;
		double expensiveHours = 0;

		JaretDate now = getBegin().copy();
		double hoursInDay = (double)now.getMillisInDay()/JaretDate.MILLIS_PER_HOUR;
		sumHours -= hoursInDay;
		if (now.isWeekendDay()){
			expensiveHours -= hoursInDay;
		}else if (hoursInDay < standardWorkHourBegin){
			expensiveHours -= hoursInDay;
		}else if (hoursInDay < standardWorkHourEnd){
			expensiveHours -= standardWorkHourBegin;
		}else{
			expensiveHours -= hoursInDay - (standardWorkHourEnd-standardWorkHourBegin);
		}
		now.clearTime();
		
		JaretDate end = getEnd().copy();
		hoursInDay = (double)end.getMillisInDay()/JaretDate.MILLIS_PER_HOUR;
		sumHours += hoursInDay;
		if (end.isWeekendDay()){
			expensiveHours += hoursInDay;
		}else if (hoursInDay < standardWorkHourBegin){
			expensiveHours += hoursInDay;
		}else if (hoursInDay < standardWorkHourEnd){
			expensiveHours += standardWorkHourBegin;
		}else{
			expensiveHours += hoursInDay - (standardWorkHourEnd-standardWorkHourBegin);
		}
		end.clearTime();
		
		while (now.compareDateTo(end) < 0){
			sumHours += 24;
			if (now.isWeekendDay()){
				expensiveHours += 24;
			}else{
				expensiveHours += 24 - (standardWorkHourEnd-standardWorkHourBegin);
			}
			now.advanceDays(1);
		}
		return sumHours==0 ? 0 : expensiveHours/sumHours;
	}
	
	/**
	 * The factor by that production in the given interval is more expensive
	 * than a production interval of the same length in standard working time.
	 * The variable cost for this interval is <code>getCostFactor() * c</code>,
	 * where <code>c</code> is the cost in standard working time.
	 * @return the cost factor for this interval
	 */
	public double getCostFactor(){
		// the factor by which the variable costs outside of the standard
		// working time is increased
		final double costFactor = 1.25;
		return 1 + (costFactor - 1) * getExpensiveTimeRatio();
	}

	/**
	 * Sets the variable cost caused by the {@link ProductionPlanEntry} in this
	 * interval, including retooling costs. The extra costs caused by working
	 * time outside of the standard working time are included.
	 * NOTE: The calculation required correct values in the begin and end fields.
	 * @return the calculated costs
	 */
	public double calculateCost(){
		double result = entry.calculateCost() * getCostFactor();
		entry.setCost(result);
		return result;
	}
	
	@Override
	public String toString() {
		double productionTime = this.entry.getProductionTime();
		StringBuffer sb = new StringBuffer();
		
		if (entry == null){
			return "NULL";
		}else{
			boolean first = true;
			for (ProductQuantity pq : entry.getOperation().getOutputs()){
				if (!first){
					sb.append("\n");
				}
				int quantity = (int) Math.round(productionTime * pq.getQuantity()/(entry.getOperation().getTime()));
				sb.append(pq.getProduct().getName() + " � " + quantity);
				first = false;
			}
		}
		return sb.toString();
	}
	
	 /**
     * {@inheritDoc}
     */
	public boolean intersect(Interval interval) {
		return IntervalImpl.intersectNonIncluding(this, interval);
	}
	
	 public void setBegin(JaretDate begin) {
		 super.setBegin(begin);
		 Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
	 }
	 public void setEnd(JaretDate end) {
		 super.setEnd(end);
		 Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
	 }
	
}

/**
 * 
 */
package de.nilgmbh.app.ui.jaretimpl;

import java.util.Calendar;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.printing.Printer;

import de.jaret.util.date.DateUtils;
import de.jaret.util.date.JaretDate;
import de.jaret.util.date.holidayenumerator.HolidayEnumerator;
import de.jaret.util.ui.timebars.TickScaler;
import de.jaret.util.ui.timebars.TickScaler.Range;
import de.jaret.util.ui.timebars.TimeBarViewerDelegate;
import de.jaret.util.ui.timebars.TimeBarViewerInterface;
import de.jaret.util.ui.timebars.model.TimeBarRow;
import de.jaret.util.ui.timebars.strategy.ITickProvider;
import de.jaret.util.ui.timebars.swt.renderer.DefaultGridRenderer;

/**
 * @author Ardhi
 *
 */
public class ProductionPlanGridRenderer extends DefaultGridRenderer {

	/** default value (pps) for coloring days. */
	private static final double DEFAULT_UPPERPPSMARKLIMIT = 0.1;
	
	private static final int BEGIN_WORKING_TIME_HOUR = 6;
	
	private static final int BEGIN_WORKING_TIME_MINUTE = 59;
	
	private static final int BEGIN_WORKING_TIME_SECOND = 59;
	
	private static final int END_WORKING_TIME = 22 ;


	/** default color for the major grid (r,g,b). */
	private static final RGB MAJORGRID_COLOR = new RGB(200, 200, 200);
	/** default color for the minor grid (r,g,b). */
	private static final RGB MINORGRID_COLOR = new RGB(230, 230, 230);
	/** default color for saturdays (r,g,b). */
	private static final RGB SATURDAY_COLOR = new RGB(255, 230, 230);
	/** default color for sundays (r,g,b). */
	private static final RGB SUNDAY_COLOR = new RGB(255, 200, 200);
	/** default color for special days (r,g,b). */
	private static final RGB SPECIALDAY_COLOR = new RGB(255, 255, 207);
	/** default color for non-work shift*/
	private static final RGB NONWORKSHIFT_COLOR = new RGB(220, 220, 220);

	/** color of the major grid. */
	private Color _colorMajorGrid;

	/** color of the minor grid. */
	private Color _colorMinorGrid;

	/** color for saturdays. */
	private Color _colorSaturday;

	/** color for sundays. */
	private Color _colorSunday;

	/** color for special days. */
	private Color _colorSpecialDay;

	/** color for non-work shift. */
	private Color _colorNonWorkShiftColor;

	/** holiday enumerator for coloring days. */
	private HolidayEnumerator _holidayEnumerator;

	/** if true weekend days are colored. */
	private boolean _markWeekends = true;

	/** if true, special day according to the holiday enumerator are marked. */
	private boolean _markSpecialdays = true;

	/** if true, holidays according to the holiday enumerator are marked. */
	private boolean _markHolidays = true;

	/** limit for coloring days. */
	private double _upperPPSMarkLimit = DEFAULT_UPPERPPSMARKLIMIT;


	private int _beginWorkHour = 0;
	private int _endWorkHour = END_WORKING_TIME;


	/**
	 * Create a DefaultGridRenderer for a printer.
	 * 
	 * @param printer printer device
	 */
	public ProductionPlanGridRenderer(Printer printer) {
		super(printer);  
	}

	/**
	 * Create a DefaultGridRenderer for the screen.
	 */
	public ProductionPlanGridRenderer() {
		super(null);
	}

	private void initializeColors(GC gc) {
		_colorMajorGrid = new Color(gc.getDevice(), MAJORGRID_COLOR);
		_colorMinorGrid = new Color(gc.getDevice(), MINORGRID_COLOR);
		_colorSaturday = new Color(gc.getDevice(), SATURDAY_COLOR);
		_colorSunday = new Color(gc.getDevice(), SUNDAY_COLOR);
		_colorSpecialDay = new Color(gc.getDevice(), SPECIALDAY_COLOR);
		_colorNonWorkShiftColor = new Color (gc.getDevice(), NONWORKSHIFT_COLOR);

	}

	/**
	/*
	 * {@inheritDoc}
	 */
	 public void draw(GC gc, TimeBarViewerDelegate delegate, Rectangle drawingArea, boolean printing) {
		 // lazy initialize the used colors
		 if (_colorMajorGrid == null) {
			 initializeColors(gc);
		 }
		 boolean horizontal = delegate.getOrientation() == TimeBarViewerInterface.Orientation.HORIZONTAL;

		 if (_tickProvider == null) {
			 drawIntern(gc, delegate, drawingArea, printing, horizontal);
		 } else {
			 drawIntern(gc, delegate, drawingArea, printing, horizontal, _tickProvider);
		 }

	 }

	 public void drawIntern(GC gc, TimeBarViewerDelegate delegate, Rectangle drawingArea, boolean printing,
			 boolean horizontal, ITickProvider tickProvider) {
		 int ox = drawingArea.x;
		 int oy = drawingArea.y;
		 int width = drawingArea.width;
		 int height = drawingArea.height;

		 Color fg = gc.getForeground();

		 if (printing) {
			 _upperPPSMarkLimit = _upperPPSMarkLimit * getScaleX();
		 }

		 // first date
		 JaretDate date = delegate.getStartDate().copy();
		 date.setTime(0, 0, 0);


		 int max = horizontal ? (ox + width) : (oy + height);

		 // draw day backgroud if configured
		 if (_markHolidays || _markSpecialdays || _markWeekends) {
			 Color bg = gc.getBackground();
			 if (delegate.getPixelPerSecond() < _upperPPSMarkLimit) {
				 // only valid if no variable xaxis is used, see below
				 int daywidth = (int) (delegate.getPixelPerSecond() * 24.0 * 60.0 * 60.0);
				 while (delegate.xForDate(date) < max) {
					 int x = delegate.xForDate(date);
					 Color mark = null;
					 if (_holidayEnumerator != null) {
						 if (_markHolidays && _holidayEnumerator.isHoliday(date.getDate())) {
							 mark = _colorSunday;
						 } else if (_markSpecialdays && _holidayEnumerator.isSpecialDay(date.getDate())) {
							 mark = _colorSpecialDay;
						 }
					 }
					 if (mark == null && _markWeekends) {
						 if (date.getDayOfWeek() == Calendar.SATURDAY) {
							 mark = _colorSaturday;
						 } else if (date.getDayOfWeek() == Calendar.SUNDAY) {
							 mark = _colorSunday;
						 }
					 }

					 if (mark == null) {


						 int begin = delegate.xForDate(date.setTime(_beginWorkHour, 0, 0));
						 int begin2 = delegate.xForDate(date.setTime(_endWorkHour, 0, 0));
						 int end = delegate.xForDate(date.setTime(BEGIN_WORKING_TIME_HOUR, BEGIN_WORKING_TIME_MINUTE,BEGIN_WORKING_TIME_SECOND));
						 int end2 = delegate.xForDate(date.setTime(23, 59, 59));	        	            

						 gc.setBackground(_colorNonWorkShiftColor);
						 gc.fillRectangle(begin, drawingArea.y, end - begin, drawingArea.height);
						 gc.fillRectangle(begin2, drawingArea.y, end2 - begin2, drawingArea.height);

						 date.setMinutes(0);
						 date.setHours(0);
						 date.setSeconds(0);   	            

					 }
					 if (mark != null) {
						 gc.setBackground(mark);
						 // if the viewer uses a variable x scale, calculate the width of the day dynamically
						 if (delegate.hasVariableXScale()) {
							 int x2 = delegate.xForDate(date.copy().advanceDays(1));
							 daywidth = x2 - x;
						 }
						 if (horizontal) {
							 gc.fillRectangle(x, oy, daywidth, height);
						 } else {
							 // TODO CHECK
							 gc.fillRectangle(ox, x, width, daywidth);
						 }
					 }
					 date.advanceDays(1);
				 }

			 }

			 gc.setBackground(bg);
		 }

		 // correct line width for printing
		 if (printing) {
			 gc.setLineWidth(getDefaultLineWidth());
		 }
		 // draw the minor grid
		 gc.setForeground(_colorMinorGrid);
		 for (JaretDate d : tickProvider.getMinorTicks(delegate)) {
			 int x = delegate.xForDate(d);
			 if (horizontal) {
				 gc.drawLine(x, oy, x, oy + height);
			 } else {
				 gc.drawLine(ox, x, ox + width, x);
			 }
		 }

		 // draw the major grid
		 gc.setForeground(_colorMajorGrid);
		 for (JaretDate d : tickProvider.getMajorTicks(delegate)) {
			 int x = delegate.xForDate(d);
			 if (horizontal) {
				 gc.drawLine(x, oy, x, oy + height);
			 } else {
				 gc.drawLine(ox, x, ox + width, x);
			 }
		 }
		 gc.setLineWidth(1);
		 gc.setForeground(fg);
	 }

	 public void drawIntern(GC gc, TimeBarViewerDelegate delegate, Rectangle drawingArea, boolean printing,
			 boolean horizontal) {

		 int ox = drawingArea.x;
		 int oy = drawingArea.y;
		 int width = drawingArea.width;
		 int height = drawingArea.height;

		 Color fg = gc.getForeground();

		 int idx;
		 if (!printing) {
			 idx = TickScaler.getTickIdx(delegate.getPixelPerSecond() / getScaleX());
		 } else {
			 idx = TickScaler.getTickIdx(delegate.getPixelPerSecond() / getScaleX());
			 _upperPPSMarkLimit = _upperPPSMarkLimit * getScaleX();
		 }
		 int majTick = TickScaler.getMajorTickMinutes(idx);
		 int minTick = TickScaler.getMinorTickMinutes(idx);
		 Range range = TickScaler.getRange(idx);

		 // first date
		 JaretDate date = delegate.getStartDate().copy();

		 // clean starting date on a major tick minute position (starting with a
		 // day)
		 date.setMinutes(0);
		 date.setHours(0);
		 date.setSeconds(0);
		 // if range is week take a week starting point
		 if (range == Range.WEEK) {
			 while (date.getDayOfWeek() != DateUtils.getFirstDayOfWeek()) {
				 date.backDays(1);
			 }
		 } else if (range == Range.MONTH) {
			 // month -> month starting point
			 date.setDay(1);
		 }
		 JaretDate save = date.copy();

		 int max = horizontal ? (ox + width) : (oy + height);

		 // draw day backgroud if configured
		 if (_markHolidays || _markSpecialdays || _markWeekends) {
			 Color bg = gc.getBackground();   
			 if (range == Range.DAY || range == Range.WEEK
					 || (range == Range.HOUR && delegate.getPixelPerSecond() < _upperPPSMarkLimit)) {
				 // only valid if no variable xaxis is used, see below
				 int daywidth = (int) (delegate.getPixelPerSecond() * 24.0 * 60.0 * 60.0);
				 while (delegate.xForDate(date) < max) {
					 int x = delegate.xForDate(date);
					 Color mark = null;
					 if (_holidayEnumerator != null) {
						 if (_markHolidays && _holidayEnumerator.isHoliday(date.getDate())) {
							 mark = _colorSunday;
						 } else if (_markSpecialdays && _holidayEnumerator.isSpecialDay(date.getDate())) {
							 mark = _colorSpecialDay;
						 }
					 }
					 if (mark == null && _markWeekends) {
						 if (date.getDayOfWeek() == Calendar.SATURDAY) {
							 mark = _colorSaturday;
						 } else if (date.getDayOfWeek() == Calendar.SUNDAY) {
							 mark = _colorSunday;
						 }
					 }
					 if (mark == null) {


						 int begin = delegate.xForDate(date.setTime(_beginWorkHour, 0, 0));
						 int begin2 = delegate.xForDate(date.setTime(_endWorkHour, 0, 0));
						 int end = delegate.xForDate(date.setTime(BEGIN_WORKING_TIME_HOUR, BEGIN_WORKING_TIME_MINUTE,BEGIN_WORKING_TIME_SECOND));
						 int end2 = delegate.xForDate(date.setTime(23, 59, 59));	        	            

						 gc.setBackground(_colorNonWorkShiftColor);
						 gc.fillRectangle(begin, drawingArea.y, end - begin, drawingArea.height);
						 gc.fillRectangle(begin2, drawingArea.y, end2 - begin2, drawingArea.height);

						 date.setMinutes(0);
						 date.setHours(0);
						 date.setSeconds(0);   	            

					 }
					 if (mark != null) {
						 gc.setBackground(mark);
						 // if the viewer uses a variable x scale, calculate the width of the day dynamically
						 if (delegate.hasVariableXScale()) {
							 int x2 = delegate.xForDate(date.copy().advanceDays(1));
							 daywidth = x2 - x;
						 }
						 if (horizontal) {
							 gc.fillRectangle(x, oy, daywidth, height);
						 } else {
							 // TODO CHECK
							 gc.fillRectangle(ox, x, width, daywidth);
						 }
					 }
					 date.advanceDays(1);
				 }

			 }




			 gc.setBackground(bg);

		 }

		 date = save.copy();
		 // draw the minor grid
		 if (printing) {
			 gc.setLineWidth(getDefaultLineWidth());
		 }
		 gc.setForeground(_colorMinorGrid);
		 while (delegate.xForDate(date) < max) {
			 int x = delegate.xForDate(date);
			 if (horizontal) {
				 gc.drawLine(x, oy, x, oy + height);
			 } else {
				 gc.drawLine(ox, x, ox + width, x);
			 }

			 if (range == Range.MONTH) {
				 int adv = Math.round((float) minTick / (float) (24 * 60 * 7 * 4));
				 if (adv == 0) {
					 adv = 1;
				 }
				 date.advanceMonths(adv);
			 } else {
				 date.advanceMinutes(minTick);
			 }
		 }

		 date = save.copy();
		 // draw the major grid
		 gc.setForeground(_colorMajorGrid);
		 while (delegate.xForDate(date) < ox + width) {
			 int x = delegate.xForDate(date);
			 if (horizontal) {
				 gc.drawLine(x, oy, x, oy + height);
			 } else {
				 gc.drawLine(ox, x, ox + width, x);
			 }
			 if (range == Range.MONTH) {
				 int adv = Math.round((float) majTick / (float) (24 * 60 * 7 * 4));
				 if (adv == 0) {
					 adv = 1;
				 }
				 date.advanceMonths(adv);
			 } else {
				 date.advanceMinutes(majTick);
			 }
		 }
		 gc.setLineWidth(1);
		 gc.setForeground(fg);
	 }
	 //	    /**
	 //	     * {@inheritDoc}
	 //	     */
	 //	    public void dispose() {
	 //	        _colorMajorGrid.dispose();
	 //	        _colorMinorGrid.dispose();
	 //	        _colorWork.dispose();
	 //	        _colorNonWork.dispose();
	 //	    }
	 //
	 //	    /**
	 //	     * {@inheritDoc}
	 //	     */
	 //	    public GridRenderer createPrintRenderer(Printer printer) {
	 //	        CalendarGridRenderer renderer = new CalendarGridRenderer(printer);
	 //	        // TODO
	 //	        return renderer;
	 //	    }

	 public int getBeginWorkHour() {
		 return _beginWorkHour;
	 }

	 public void setBeginWorkHour(int beginWorkHour) {
		 _beginWorkHour = beginWorkHour;
	 }

	 public int getEndWorkHour() {
		 return _endWorkHour;
	 }

	 public void setEndWorkHour(int endWorkHour) {
		 _endWorkHour = endWorkHour;
	 }

	 public void setTickProvider(ITickProvider tickProvider) {

	 }

	 /**
	  * {@inheritDoc}
	  */
	  @Override
	  public void drawRowBeforeIntervals(GC gc, TimeBarViewerDelegate delegate,
			  Rectangle drawingArea, TimeBarRow row, boolean selected,
			  boolean printing) {
		  boolean highlighted = false;
		  for (TimeBarRow tbr : ((ProductionPlanTimeBarViewerDelegate) delegate).getHighlightedRows()) {
			  if (tbr.equals(row))
				  highlighted = true;
		  }
		  // draw a background if selected or highlighted
		  // highlighting is at higher priority than selection
		  if (selected || highlighted) {
			  Color bg = gc.getBackground();
			  int alpha = gc.getAlpha();
			  if (highlighted) {
				  gc.setBackground(_highlightColor);
				  gc.setAlpha(_highlightAlpha);
			  } else {
				  gc.setBackground(_rowSelectColor);
				  gc.setAlpha(_rowSelectAlpha);
			  }
			  gc.fillRectangle(drawingArea);
			  gc.setAlpha(alpha);
			  gc.setBackground(bg);
		  }

	  }

}

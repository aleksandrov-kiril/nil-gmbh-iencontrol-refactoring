/**
 * 
 */
package de.nilgmbh.app.ui.jaretimpl;

import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

//import de.fzi.wenpro.core.model.operation.ProductionPlanPrinter;
import de.fzi.wenpro.core.printing.ProductionPlanPrinter;
import de.jaret.util.date.Interval;
import de.jaret.util.date.JaretDate;
import de.jaret.util.ui.timebars.mod.IntervalModificator;
import de.jaret.util.ui.timebars.model.TimeBarRow;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.ui.views.PPEntryPropertiesView;
import de.nilgmbh.app.ui.views.WarningView;
import de.nilgmbh.app.updatemechanism.UpdateType;

/**
 * @author Ardhi
 *
 */
public class ProductionPlanIntervalModificator implements IntervalModificator {

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable(TimeBarRow row, Interval interval) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSizingAllowed(TimeBarRow row, Interval interval) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public boolean newBeginAllowed(TimeBarRow row, Interval interval, JaretDate newBegin) {
    	WorkplaceTimeBarRowModel wtbrm = (WorkplaceTimeBarRowModel) row;
    	Interval prevInterval = wtbrm.getPrevInterval(interval); 
    	if(prevInterval != null){
    		if(interval.getEnd().compareTo(newBegin) <= 0 || newBegin.compareTo(prevInterval.getEnd()) <=0){
        		return false;
        	}    		
    	}
    	else{
    		if(interval.getEnd().compareTo(newBegin) <= 0 ){
        		return false;
    		}
    	}
    
    	wtbrm.removeAndSaveInterval(interval);
    	wtbrm.addAndSaveInterval(interval);
    	if (wtbrm.getProductionPlan().getWarningList().size() > 0){
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(WarningView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
				Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}
		else{
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(PPEntryPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}
		ProductionPlanPrinter.printProductionPlan(ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().getProductionPlan());

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public boolean newEndAllowed(TimeBarRow row, Interval interval, JaretDate newEnd) {
    	WorkplaceTimeBarRowModel wtbrm = (WorkplaceTimeBarRowModel) row;
    	Interval nextInterval = wtbrm.getNextInterval(interval); 
    	if(nextInterval != null){
    		if(interval.getBegin().compareTo(newEnd) >= 0 || newEnd.compareTo(nextInterval.getBegin()) >=0){
    			return false;
    		}
    	}
    	else {
    		if(interval.getBegin().compareTo(newEnd) >= 0 ){
    			return false;
    		}
    	}
    	
    	wtbrm.removeAndSaveInterval(interval);
    	wtbrm.addAndSaveInterval(interval);    
    	if (wtbrm.getProductionPlan().getWarningList().size() > 0){
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(WarningView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
				Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}
		else{
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(PPEntryPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}
		ProductionPlanPrinter.printProductionPlan(ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().getProductionPlan());

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isShiftingAllowed(TimeBarRow row, Interval interval) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public boolean shiftAllowed(TimeBarRow row, Interval interval, JaretDate newBegin) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public double getSecondGridSnap() {
        return -1;
    }

    /**
     * {@inheritDoc}
     */
	public double getSecondGridSnap(TimeBarRow row, Interval interval) {
        return -1;
	}

}

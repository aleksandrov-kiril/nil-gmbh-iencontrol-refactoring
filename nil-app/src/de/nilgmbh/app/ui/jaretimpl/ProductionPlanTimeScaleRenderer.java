package de.nilgmbh.app.ui.jaretimpl;

import org.eclipse.swt.printing.Printer;

import de.jaret.util.ui.timebars.swt.renderer.BoxTimeScaleRenderer;

public class ProductionPlanTimeScaleRenderer extends BoxTimeScaleRenderer {
	
	public ProductionPlanTimeScaleRenderer(){
		super();
	}
	
	public ProductionPlanTimeScaleRenderer(Printer printer){
		super(printer);
	}

}

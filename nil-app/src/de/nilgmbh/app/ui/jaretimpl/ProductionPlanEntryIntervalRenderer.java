package de.nilgmbh.app.ui.jaretimpl;

import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import de.fzi.wenpro.core.energy.model.productionplan.EntryState;
import de.jaret.util.date.Interval;
import de.jaret.util.swt.SwtGraphicsHelper;
import de.jaret.util.ui.timebars.TimeBarViewerDelegate;
import de.jaret.util.ui.timebars.swt.renderer.DefaultRenderer;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanEntryIntervalImpl;

/**
 * Renderer for the TimeBarViewer widget. It renders intervals containing retooling and production time 
 * as two colored bars.
 * 
 * @author wicaksono
 */
public class ProductionPlanEntryIntervalRenderer extends DefaultRenderer {
	

	 /**
    * Construct renderer for screen use.
    * 
    */
   public ProductionPlanEntryIntervalRenderer() {
   	super();
   }
	
	
   /**
    * {@inheritDoc}
    */
	@Override
	public void draw(GC gc, Rectangle drawingArea,
			TimeBarViewerDelegate delegate, Interval interval,
			boolean selected, boolean printing, boolean overlap) {
		_delegate = delegate;
		drawRetoolingInterval(gc, drawingArea, delegate, interval, selected, printing, overlap);
		drawStartingProductionInterval(gc, drawingArea, delegate, interval, selected, printing, overlap);
		drawRunningProductionInterval(gc, drawingArea, delegate, interval, selected, printing, overlap);
		drawStoppingProductionInterval(gc, drawingArea, delegate, interval, selected, printing, overlap);

	}


	
	
	
   /**
    * Drawing method for starting phase in production interval.
    * 
    * @param gc GC
    * @param drawingArea drawingArea
    * @param delegate delegate
    * @param interval interval to draw
    * @param selected true for selected drawing
    * @param printing true for printing
    * @param overlap true if the interval overlaps with other intervals
    */
   private void drawStartingProductionInterval(GC gc, Rectangle drawingArea, TimeBarViewerDelegate delegate, Interval interval,
           boolean selected, boolean printing, boolean overlap) {
       
   	// draw focus
   	
       drawFocus(gc, drawingArea, delegate, interval, selected, printing, overlap);
       Rectangle productionRect = getStartingProductionRect(drawingArea, (ProductionPlanEntryIntervalImpl) interval, overlap);
       Color bg = gc.getBackground();    
    
       if (!selected) {
           gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
       } else {
           gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_CYAN));
       }
       gc.fillRectangle(productionRect);
       gc.drawRectangle(productionRect);
       gc.setBackground(bg);
   }

   /**
    * Drawing method for running phase in production interval.
    * 
    * @param gc GC
    * @param drawingArea drawingArea
    * @param delegate delegate
    * @param interval interval to draw
    * @param selected true for selected drawing
    * @param printing true for printing
    * @param overlap true if the interval overlaps with other intervals
    */
   private void drawRunningProductionInterval(GC gc, Rectangle drawingArea, TimeBarViewerDelegate delegate, Interval interval,
           boolean selected, boolean printing, boolean overlap) {
       
   	// draw focus
   	
       drawFocus(gc, drawingArea, delegate, interval, selected, printing, overlap);
       Rectangle productionRect = getRunningProductionRect(drawingArea, (ProductionPlanEntryIntervalImpl) interval, overlap);
       Color bg = gc.getBackground();
       String str = interval.toString();        
    
       if (!selected) {
           gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
       } else {
           gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_CYAN));
       }
       gc.fillRectangle(productionRect);
       gc.drawRectangle(productionRect);
       SwtGraphicsHelper.drawStringCentered(gc, str, productionRect);
       gc.setBackground(bg);
   }
   
   /**
    * Drawing method for stopping phase in production interval.
    * 
    * @param gc GC
    * @param drawingArea drawingArea
    * @param delegate delegate
    * @param interval interval to draw
    * @param selected true for selected drawing
    * @param printing true for printing
    * @param overlap true if the interval overlaps with other intervals
    */
   private void drawStoppingProductionInterval(GC gc, Rectangle drawingArea, TimeBarViewerDelegate delegate, Interval interval,
           boolean selected, boolean printing, boolean overlap) {
       
   	// draw focus
   	
       drawFocus(gc, drawingArea, delegate, interval, selected, printing, overlap);
       Rectangle productionRect = getStoppingProductionRect(drawingArea, (ProductionPlanEntryIntervalImpl) interval, overlap);
       Color bg = gc.getBackground();     
    
       if (!selected) {
           gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
       } else {
           gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_CYAN));
       }
       gc.fillRectangle(productionRect);
       gc.drawRectangle(productionRect);
       gc.setBackground(bg);
   }
	
   
	/**
    * Drawing method for default rendering.
    * 
    * @param gc GC
    * @param drawingArea drawingArea
    * @param delegate delegate
    * @param interval interval to draw
    * @param selected true for selected drawing
    * @param printing true for printing
    * @param overlap true if the interval overlaps with other intervals
    */
   private void drawRetoolingInterval(GC gc, Rectangle drawingArea, TimeBarViewerDelegate delegate, Interval interval,
           boolean selected, boolean printing, boolean overlap) {
   	
   	// draw focus
       drawFocus(gc, drawingArea, delegate, interval, selected, printing, overlap);
       Rectangle retoolingRect = getRetoolingRect(drawingArea, (ProductionPlanEntryIntervalImpl) interval, overlap);

       Color bg = gc.getBackground();

       if (!selected) {
           gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_YELLOW));
       } else {
           gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_CYAN));
       }
       gc.fillRectangle(retoolingRect);
       gc.drawRectangle(retoolingRect);
       gc.setBackground(bg);
   }
	
   /**
    * Calculate the actual drawing rectangle for the interval usig the BORDERFACTOR to determine the border.
    * 
    * @param horizontal true for horizontal false for vertical
    * @param drawingArea drawingArea
    * @param overlap true if it is an overlapping interval
    * @return the actual drawing rectangle
    */
   protected Rectangle getIRect(boolean horizontal, Rectangle drawingArea, boolean overlap) {
       if (horizontal) {
           int borderHeight = (int) (drawingArea.height * BORDERFACTOR / 2);
           int height = drawingArea.height - (overlap ? 0 : 2 * borderHeight);
           int y = drawingArea.y + (overlap ? 0 : borderHeight);
           return new Rectangle(drawingArea.x, y, drawingArea.width - 1, height - 1);
       } else {
           int borderWidth = (int) (drawingArea.width * BORDERFACTOR / 2);
           int width = drawingArea.width - (overlap ? 0 : 2 * borderWidth);
           int x = drawingArea.x + (overlap ? 0 : borderWidth);
           return new Rectangle(x, drawingArea.y, width - 1, drawingArea.height - 1);
       }
   }
   
   /**
    * Calculates the actual drawing rectangle for the retooling part of the interval.
    * 
    * @param drawingArea drawingArea
    * @param interval interval to draw
    * @return the actual drawing rectangle
    */
   private Rectangle getRetoolingRect(Rectangle drawingArea, ProductionPlanEntryIntervalImpl interval, boolean overlap) {
       int borderHeight = (int) (drawingArea.height * BORDERFACTOR / 2);
       int y = drawingArea.y + (overlap ? 0 : borderHeight);
       int height = drawingArea.height - (overlap ? 0 : 2 * borderHeight);
       int retooling = (int) Math.round(interval.getEntry().getRetoolingTime());
   	int width = (int) ((retooling / (interval.getEntry().getDuration())) * drawingArea.width);
   	return new Rectangle(drawingArea.x, y, width, height - 1);
   }
   
   /**
    * Calculates the actual drawing rectangle for the production part of the interval.
    * 
    * @param drawingArea drawingArea
    * @param interval interval to draw
    * @return the actual drawing rectangle
    */

   
   protected Rectangle getStartingProductionRect(Rectangle drawingArea, ProductionPlanEntryIntervalImpl interval, boolean overlap) {
  
   	Rectangle retoolingRect = getRetoolingRect(drawingArea, interval, overlap);
   	int x = drawingArea.x + retoolingRect.width ;
   	Set<EntryState> states = (interval.getEntry()).getEntryStates();
   	double startingDuration = 0;
   	for(EntryState es : states ){
   		if(es.getName().equalsIgnoreCase("STARTING")){
   			startingDuration = es.getDuration();
   		}  
   	}
   	int startingWidth = (int) Math.round(((startingDuration/interval.getEntry().getDuration())*drawingArea.width));    	
   	return new Rectangle(x, retoolingRect.y, startingWidth, retoolingRect.height);
   }
   

   protected Rectangle getRunningProductionRect(Rectangle drawingArea, ProductionPlanEntryIntervalImpl interval, boolean overlap) {
   	   
	Rectangle retoolingRect = getRetoolingRect(drawingArea, interval, overlap);
	Rectangle startingRect = getStartingProductionRect(drawingArea, interval, overlap);
   	Set<EntryState> states = (interval.getEntry()).getEntryStates();
   	double runningDuration = 0;
   	for(EntryState es : states ){
   		if(es.getName().equalsIgnoreCase("RUNNING")){
   			runningDuration = es.getDuration();
   		} 
   	}	
   	int x = drawingArea.x + retoolingRect.width + startingRect.width;
   	int runningWidth = (int) Math.round(((runningDuration/interval.getEntry().getDuration())*drawingArea.width));   
   	return new Rectangle(x, retoolingRect.y, runningWidth, retoolingRect.height);
   }
   
   protected Rectangle getStoppingProductionRect(Rectangle drawingArea, ProductionPlanEntryIntervalImpl interval, boolean overlap) {
	   
   	Rectangle retoolingRect = getRetoolingRect(drawingArea, interval, overlap);
   	Rectangle startingRect = getStartingProductionRect(drawingArea, interval, overlap);
   	Rectangle runningRect = getRunningProductionRect(drawingArea, interval, overlap);
   	Set<EntryState> states = (interval.getEntry()).getEntryStates();

   	double stoppingDuration = 0;  	
   	for(EntryState es : states ){
   		if(es.getName().equalsIgnoreCase("STOPPING")){
   			stoppingDuration = es.getDuration();
   		} 
   	}
   	int x = drawingArea.x + retoolingRect.width + startingRect.width + runningRect.width;
   	int stoppingWidth = (int) Math.round(((stoppingDuration/interval.getEntry().getDuration())*drawingArea.width ));   
   	return new Rectangle(x, retoolingRect.y, stoppingWidth, retoolingRect.height);
   }

}


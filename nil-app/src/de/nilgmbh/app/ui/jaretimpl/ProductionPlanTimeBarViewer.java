/**
 * 
 */
package de.nilgmbh.app.ui.jaretimpl;

import java.util.List;

import org.eclipse.swt.widgets.Composite;

import de.jaret.util.ui.timebars.model.TimeBarRow;
import de.jaret.util.ui.timebars.swt.TimeBarViewer;
import de.jaret.util.ui.timebars.swt.renderer.HeaderRenderer;

/**
 * @author Ardhi
 *
 */
public class ProductionPlanTimeBarViewer extends TimeBarViewer {
	

	public ProductionPlanTimeBarViewer(Composite parent, int style) {
		super(parent, style);
		this._delegate  = new ProductionPlanTimeBarViewerDelegate(this);
		this. _gridRenderer = new ProductionPlanGridRenderer();
		this._headerRenderer = new ProductionPlanDefaultHeaderRenderer();
	}
	
	
    /**
     * {@inheritDoc}
     */
    public void highlightRows(List<TimeBarRow> timeBarRows) {
        ((ProductionPlanTimeBarViewerDelegate) _delegate).highlightRows(timeBarRows);
    }
    /**
     * @return Returns the headerRenderer.
     */
    public HeaderRenderer getHeaderRenderer() {
        return _headerRenderer;
    }

    /**
     * @param headerRenderer The headerRenderer to set.
     */
    public void setHeaderRenderer(ProductionPlanDefaultHeaderRenderer headerRenderer) {
        _headerRenderer = headerRenderer;
        repaint();
    }
    
 

}

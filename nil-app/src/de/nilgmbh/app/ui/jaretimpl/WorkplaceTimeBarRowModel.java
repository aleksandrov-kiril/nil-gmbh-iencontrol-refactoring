/**
 * 
 */
package de.nilgmbh.app.ui.jaretimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.core.model.productionplan.Schedule;
import de.jaret.util.date.Interval;
import de.jaret.util.date.IntervalImpl;
import de.jaret.util.date.JaretDate;
import de.jaret.util.ui.timebars.model.DefaultTimeBarRowModel;
import de.jaret.util.ui.timebars.model.TimeBarRowHeader;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.util.ProductionPlanerConstants;

/**
 * @author wicaksono
 *
 */
public class WorkplaceTimeBarRowModel extends DefaultTimeBarRowModel {

	private Workplace workplace;
	private ProductionPlan productionPlan;

	public WorkplaceTimeBarRowModel() {
		super();
	}

	public WorkplaceTimeBarRowModel(TimeBarRowHeader header, Workplace workplace) {
		super(header);
		this.workplace = workplace;
	}

	public WorkplaceTimeBarRowModel(Workplace workplace) {
		super();
		this.workplace = workplace;
	}
	
	public WorkplaceTimeBarRowModel(TimeBarRowHeader header, Workplace workplace,
			ProductionPlan productionPlan) {
		this(header,  workplace);
		this.productionPlan = productionPlan;
	}
	
	public WorkplaceTimeBarRowModel(WorkplaceTimeBarRowModel src) {
		this (src._header, src.getWorkplace(), src.productionPlan);
		this._intervals = new ArrayList<Interval>();
//		this._intervals = new ArrayList<ProductionPlanEntryIntervalImpl>();
		for (Interval intv : src.getIntervals()) {
			if (intv instanceof ProductionPlanEntryIntervalImpl)
			this._intervals.add(((ProductionPlanEntryIntervalImpl)intv).clone());
		}
		this._maxDate = src._maxDate==null ? null : src._maxDate.copy();
		this._minDate = src._minDate==null ? null : src._minDate.copy();
		// TODO: should be changed
		this._listenerList = src._listenerList;
	}
	
	public WorkplaceTimeBarRowModel(WorkplaceTimeBarRowModel src, ProductionPlan productionPlan) {
		this (src._header, src.getWorkplace(), productionPlan);
		this._intervals = new ArrayList<Interval>();

		for (Interval intv : src.getIntervals()) {
			if (intv instanceof ProductionPlanEntryIntervalImpl)
			this._intervals.add(((ProductionPlanEntryIntervalImpl)intv).clone());
		}
		this._maxDate = src._maxDate==null ? null : src._maxDate.copy();
		this._minDate = src._minDate==null ? null : src._minDate.copy();
		// TODO: should be changed
		this._listenerList = src._listenerList;
	}
	
	public WorkplaceTimeBarRowModel clone() {
		return new WorkplaceTimeBarRowModel(this);
	}

	public Workplace getWorkplace() {
		return workplace;
	}

	public void setWorkplace(Workplace workplace) {
		this.workplace = workplace;
	}
	
	

	public ProductionPlan getProductionPlan() {
		return productionPlan;
	}

	public void setProductionPlan(ProductionPlan productionPlan) {
		this.productionPlan = productionPlan;
	}

	/**
	 * Gets an {@link Interval} whose begin date exactly the previous of the given interval's begin date
	 * @param interval whose previous interval to find
	 * @return next {@link Interval}
	 */
	public Interval getPrevInterval(Interval interval) {
		if (_intervals.isEmpty()) 
			return null;
		if (_intervals.get(0).getBegin().compareTo(interval.getBegin()) > 0 ||
				((ProductionPlanEntryIntervalImpl) _intervals.get(0)).getEntry().equals( ((ProductionPlanEntryIntervalImpl) interval).getEntry() )) {
			return null;
		}
		Interval maxInterval = _intervals.isEmpty()? null : _intervals.get(0);
		for (Interval intv : this._intervals) {
			if (! ((ProductionPlanEntryIntervalImpl) intv).getEntry().equals(((ProductionPlanEntryIntervalImpl) interval).getEntry())  
					&& intv.getBegin().compareTo(maxInterval.getBegin()) > 0
					&& intv.getBegin().compareTo(interval.getBegin()) < 0

			) {
				maxInterval = intv;
			}
		}
		if ( ( (ProductionPlanEntryIntervalImpl) maxInterval).getEntry().equals(  (ProductionPlanEntryIntervalImpl) interval ) ) {
			return null;
		}
		return maxInterval;
	}
	
	/**
	 * Gets an {@link Interval} whose begin date exactly the next of given interval's begin date
	 * @param interval whose next interval to find
	 * @return next {@link Interval}
	 */
	public Interval getNextInterval(Interval interval) {
		if (_intervals.isEmpty()) 
			return null;
		for (Interval intv : this._intervals) {
			if (! ((ProductionPlanEntryIntervalImpl) intv).getEntry().equals(((ProductionPlanEntryIntervalImpl) interval).getEntry())  
					&& intv.getBegin().compareTo(interval.getBegin()) >= 0
					

			) {
				return intv;
			}
		}
		return null;	
	}
	/**
	 * Checks whether there is enough space , when the given interval is added to the row
	 * @param interval, which the check is based on
	 * @return <code>true</code> if there is enough space in the row
	 */
	public boolean isEnoughPlaceToAdd(Interval interval) {
		Interval prev = getPrevInterval(interval);
		Interval next = getNextInterval(interval);
		if (next == null) {
			return true;
		}
		if (prev != null) {
			if (next.getBegin().diffSeconds(prev.getEnd()) < interval.getEnd().diffSeconds(interval.getBegin())) {
				return false;
			}	
		}
		else {
			JaretDate currentDate = new JaretDate();
			if ( next.getBegin().diffSeconds(currentDate) < interval.getEnd().diffSeconds(interval.getBegin()))
				return false;
		}
		return true;
	}
	

	
	/**
	 * Adds an interval to  current object and saves it in {@link ProductionPlan}.
	 * @param interval the interval to add
	 */
	public void addAndSaveInterval (Interval interval){
		
		super.addInterval(interval);
		ProductionPlanEntry productionPlanEntry = ((ProductionPlanEntryIntervalImpl) interval).getEntry();
		productionPlanEntry.setWorkplace(this.workplace);
		int duration = interval.getEnd().diffSeconds(interval.getBegin());
		productionPlanEntry.setDuration(duration);
		JaretDate currentDate = ProductionPlanProject.getCurrentDate();
		int relativeBegin = interval.getBegin().diffSeconds(currentDate);
		productionPlan.addWithRetooling(productionPlanEntry, relativeBegin);
//		if (productionPlanEntry.doRetooling()) {
//			JaretDate endDate =  interval.getEnd().copy().advanceSeconds(productionPlanEntry.getRetoolingTime());
//			interval.setEnd(endDate);
//			interval.setBegin(endDate.copy().backSeconds(productionPlanEntry.getRetoolingTime() + productionPlanEntry.getDuration()));
//		}
		correctIntervals();
	}
	
	
	/**
	 * Only adds interval for drawing. It will be not saved in  {@link ProductionPlan}
	 * @param begin
	 * @param productionPlanEntry
	 */
	public void addInterval(double begin, ProductionPlanEntry productionPlanEntry) {
		productionPlanEntry.setWorkplace(this.workplace);
		Interval interval = new ProductionPlanEntryIntervalImpl(productionPlanEntry);
		//TODO: Check old vs new:
		//Old:
		//JaretDate currentDate = ProductionPlanProject.getCurrentDate();
		//New:
		JaretDate currentDate = new JaretDate(productionPlan.getPlanStartDate().getTime());
		interval.setBegin(currentDate.copy().advanceSeconds(begin));
		double end = begin + productionPlanEntry.getDuration();
		interval.setEnd(currentDate.copy().advanceSeconds(end));
		this.addInterval(interval);
		
	}
	
	/**
	 * Removes the given interval from the current object. The corresponding {@link ProductionPlanEntry} is also removed from
	 * current {@link ProductionPlan} 
	 * @param interval interval to remove
	 */
	public boolean removeAndSaveInterval(Interval interval) {
		this.remInterval(interval);
		return productionPlan.remove( ((ProductionPlanEntryIntervalImpl) interval).getEntry() );
	}
	
	
	/**
	 * Adds and saves the given interval to the left-most possible place in the current row
	 * @param interval interval to add
	 */
	public void addAndSaveIntervalToMinPos(Interval interval) {
		int duration = interval.getEnd().diffSeconds(interval.getBegin());
		if (isEnoughPlaceToAdd(interval)) {
			addAndSaveInterval(interval);
		}
		else {
			for (Interval intv : this._intervals) {
				interval.setBegin(intv.getEnd().copy());
				interval.setEnd(intv.getEnd().copy().advanceSeconds(duration));
				System.out.println("-------> " + intv.toString());
				if (isEnoughPlaceToAdd(interval)) {
					addAndSaveInterval(interval);
					break;
				}
			}
		}		
	}
	
	/**
	 * Creates an {@link Interval} and then adds and saves it to the left-most 
	 * possible place in the current row
	 * @param workPlace
	 * @param product
	 * @param amount
	 * @param begin
	 */
	public void addAndSaveIntervalToMinPos(Workplace workPlace, Product product, int amount, Date begin) {
		this.workplace = workPlace;
		ProductionPlanEntry productionPlanEntry = new ProductionPlanEntry(workPlace, workPlace.getProducingWorkplaceState(product).get(0), workPlace.getOperation(product).get(0), ProductionPlanerConstants.DEFAULT_DURATION, 5000);
		IntervalImpl interval = new ProductionPlanEntryIntervalImpl(productionPlanEntry);
		JaretDate beginJaretDate = new JaretDate (begin);
		interval.setBegin(beginJaretDate);
		double duration = this.workplace.getOperation(product).get(0).getTime() * amount;
		interval.setEnd(beginJaretDate.copy().advanceSeconds(duration));
		this.addAndSaveIntervalToMinPos(interval);	
	}
	/**
	 * Adds an interval to  current object and saves it in {@link ProductionPlan}. This Methode is called when using initialwizard 
	 * and when dragging from tree to feinscheduleview
	 */

	
	public void addAndSaveInterval2(Workplace workPlace, Product product, int amount, Date begin) {		
		this.workplace = workPlace;
		//TODO: refactoring: result should be tested
		Operation newOperation = workPlace.getOperation(product).get(0);
		ProductionPlanEntry productionPlanEntry = new ProductionPlanEntry(workPlace, workPlace.getProducingWorkplaceState(product).get(0), newOperation,amount* newOperation.getTime(), 0);
		IntervalImpl newInterval = new ProductionPlanEntryIntervalImpl(productionPlanEntry);
		JaretDate beginJaretDate = new JaretDate (begin);
		newInterval.setBegin(beginJaretDate);
		double productionTime = newOperation.getTime() * amount;
		double draggedIntervalRetoolingTime = workPlace.getProducingWorkplaceState(product).get(0).getRetoolingTime();
		newInterval.setEnd(beginJaretDate.copy().advanceSeconds(productionTime + draggedIntervalRetoolingTime));
		((ProductionPlanEntryIntervalImpl)newInterval).calculateCost();
		
		List<Interval> intervals = this.getIntervals();
		boolean overlap = false;	
		for(Interval i : intervals){
			if(newInterval.intersects(i)){
				overlap = true;
			}
		}
		if(!overlap){
			this.addAndSaveInterval(newInterval);
		}else{	
			if(this.getIntervals(beginJaretDate).size() == 0){
				List<Interval> oldIntervalInRow = this.getIntervals();			
				for (int i =0 ; i<oldIntervalInRow.size();i++){	
					if(oldIntervalInRow.get(i).getBegin().copy().compareTo(newInterval.getBegin().copy()) > 0){
						this.moveAndSaveInterval(oldIntervalInRow.get(i), oldIntervalInRow.get(i).getBegin().copy().advanceSeconds(productionTime + draggedIntervalRetoolingTime));
					}
				}
				this.addAndSaveInterval(newInterval);				
			}
			else{
				ProductionPlanEntryIntervalImpl oldIntervalAtDrop = (ProductionPlanEntryIntervalImpl) this.getIntervals(new JaretDate(begin)).get(0);
				//TODO: refactoring result: should be tested
				Operation destOperation = oldIntervalAtDrop.getEntry().getOperation();	
				
				// the same operation added to the bar, the bar length should be extended and all of following intervals should be shifted
				if (destOperation.equals(newOperation)) {				
					JaretDate newBegin = oldIntervalAtDrop.getBegin().copy();
					JaretDate newEnd = oldIntervalAtDrop.getBegin().copy().advanceSeconds(oldIntervalAtDrop.getSeconds()+productionPlanEntry.getDuration());
					newInterval.setBegin(newBegin);
					newInterval.setEnd(newEnd);	
					List<Interval> oldIntervalInRow = this.getIntervals();			
					for (int i =0 ; i<oldIntervalInRow.size();i++){	
						if(oldIntervalInRow.get(i).getBegin().copy().compareTo(oldIntervalAtDrop.getBegin().copy()) > 0){
							ProductionPlanEntryIntervalImpl next = (ProductionPlanEntryIntervalImpl) this.getNextInterval(oldIntervalAtDrop);
							//TODO: refactoring result: should be tested
							if (next != null && next.getBegin().diffSeconds(oldIntervalAtDrop.getEnd()) < (amount* newOperation.getTime())){
								this.moveAndSaveInterval(oldIntervalInRow.get(i), oldIntervalInRow.get(i).getBegin().copy().advanceSeconds(((ProductionPlanEntryIntervalImpl)newInterval).getEntry().getDuration()));
							}
						}
					}
					((ProductionPlanEntryIntervalImpl)newInterval).getEntry().setDuration(oldIntervalAtDrop.getSeconds()+((ProductionPlanEntryIntervalImpl)newInterval).getEntry().getDuration());
					this.removeAndSaveInterval(oldIntervalAtDrop);				
					this.addAndSaveInterval(newInterval);
					//FIXME: retooling does not work				
				}
				// different product added to the bar, the bar should be fragmented
				else {
					
					ProductionPlanEntryIntervalImpl newIntervalAtDrop = (ProductionPlanEntryIntervalImpl) this.getIntervals(new JaretDate(begin)).get(0);
					double newIntervalAtDropRetoolingTime = newIntervalAtDrop.getEntry().getRetoolingTime();
					double oldIntervalAtDropDuration = oldIntervalAtDrop.getBegin().diffSeconds(oldIntervalAtDrop.getEnd());
					double newIntervalAtDropDuration = newIntervalAtDrop.getBegin().diffSeconds(new JaretDate(begin));
					double rest = Math.abs(oldIntervalAtDropDuration - newIntervalAtDropDuration);
					newIntervalAtDrop.setBegin(oldIntervalAtDrop.getBegin());
					newIntervalAtDrop.setEnd(new JaretDate(begin));
					newInterval.setBegin(new JaretDate(begin));
					//TODO: refactoring result: should be tested
					newInterval.setEnd(new JaretDate(begin).copy().advanceSeconds((amount* newOperation.getTime())+draggedIntervalRetoolingTime));
					//TODO: refactoring result: should be tested
					//FIXME: the cost of operation should be used
					double value = 0;
					for (Product p: destOperation.getOutputProducts()) {
						value = value + p.getValue();
					}
					ProductionPlanEntry newIntervalAtDropEntry = new ProductionPlanEntry(this.workplace, this.workplace.getWorkplaceStateByOperation(destOperation), destOperation, (amount* workplace.getOperation(product).get(0).getTime()), value);
					//TODO: refactoring result: should be tested
					ProductionPlanEntry newIntervalAtDrop2Entry =  new ProductionPlanEntry(this.workplace, this.workplace.getWorkplaceStateByOperation(destOperation), destOperation, (amount* workplace.getOperation(product).get(0).getTime()), value);
					ProductionPlanEntryIntervalImpl newIntervalAtDrop2 = new ProductionPlanEntryIntervalImpl(newInterval.getEnd(), newInterval.getEnd().copy().advanceSeconds(rest+newIntervalAtDropRetoolingTime), newIntervalAtDrop2Entry);
					
					List<Interval> oldIntervalInRow = this.getIntervals();
					for (int i =0 ; i<oldIntervalInRow.size();i++){					
						if(oldIntervalInRow.get(i).getBegin().copy().compareTo(oldIntervalAtDrop.getBegin().copy()) > 0){
							ProductionPlanEntryIntervalImpl next = (ProductionPlanEntryIntervalImpl) this.getNextInterval(newInterval);
							//TODO: refactoring result: should be tested
							if (next != null && next.getBegin().diffSeconds(new JaretDate(begin).advanceSeconds(rest)) < (amount* workplace.getOperation(product).get(0).getTime())){
								this.moveAndSaveInterval(oldIntervalInRow.get(i), oldIntervalInRow.get(i).getBegin().copy().advanceSeconds(((ProductionPlanEntryIntervalImpl)newInterval).getEntry().getDuration()));
							}
						}
					}				
					newIntervalAtDropEntry.setDuration(newIntervalAtDropDuration);
					newIntervalAtDrop2Entry.setDuration(rest+newIntervalAtDropRetoolingTime);
					this.removeAndSaveInterval(oldIntervalAtDrop);
					this.addAndSaveInterval(newIntervalAtDrop);
					this.addAndSaveInterval(newInterval);
					this.addAndSaveInterval(newIntervalAtDrop2);
				}
			}
		}
	}
	
	/**
	 * Adds an interval to  current object and saves it in {@link ProductionPlan}. This Methode is called when using initialwizard editmode
	 * @param interval the interval to add
	 */
	

	public void addAndSaveInterval3(Workplace workPlace, Product product, int amount, Date begin) {
		this.workplace = workPlace;
		//TODO: refactoring result: should be tested
		Operation newOperation = workPlace.getOperation(product).get(0);
		ProductionPlanEntry productionPlanEntry = new ProductionPlanEntry(workPlace, workPlace.getProducingWorkplaceState(product).get(0), newOperation,(amount* workplace.getOperation(product).get(0).getTime()), 5000);
		IntervalImpl newInterval = new ProductionPlanEntryIntervalImpl(productionPlanEntry);
		JaretDate beginJaretDate = new JaretDate (begin);
		newInterval.setBegin(beginJaretDate);
		//TODO: refactoring result: should be tested
		double productionTime = newOperation.getTime() * amount;
		double retoolingTime = workPlace.getProducingWorkplaceState(product).get(0).getRetoolingTime();
		newInterval.setEnd(beginJaretDate.copy().advanceSeconds(productionTime + retoolingTime));
//		double duration = this.workplace.getOperation(product).get(0).getTime() * amount;
//		newInterval.setEnd(beginJaretDate.copy().advanceSeconds(duration));
		List<Interval> intervals = this.getIntervals();
		boolean overlap = false;
		for(Interval i : intervals){
			if(newInterval.intersects(i)){
				overlap = true;
			}
		}
		if(!overlap){
			this.addAndSaveInterval(newInterval);
		}
		else{		
			//TODO: refactoring result: should be tested
			ProductionPlanEntryIntervalImpl oldIntervalAtDrop = (ProductionPlanEntryIntervalImpl) this.getIntervals(new JaretDate(begin).advanceMinutes(1)).get(0);

			List<Interval> oldIntervalInRow = this.getIntervals();			
			for (int i =0 ; i<oldIntervalInRow.size();i++){	
				if(oldIntervalInRow.get(i).getBegin().copy().compareTo(oldIntervalAtDrop.getBegin().copy()) > 0){
					ProductionPlanEntryIntervalImpl next = (ProductionPlanEntryIntervalImpl) this.getNextInterval(oldIntervalAtDrop);
					//TODO: refactoring result: should be tested
					if (next != null && next.getBegin().diffSeconds(oldIntervalAtDrop.getEnd()) < (amount* workplace.getOperation(product).get(0).getTime())-oldIntervalAtDrop.getSeconds()){
						//TODO: refactoring result: should be tested
						this.moveAndSaveInterval(oldIntervalInRow.get(i), oldIntervalInRow.get(i).getBegin().copy().advanceSeconds((amount* workplace.getOperation(product).get(0).getTime())-oldIntervalAtDrop.getSeconds()));

						
					}
				}
			}
			((ProductionPlanEntryIntervalImpl)newInterval).getEntry().setDuration(((ProductionPlanEntryIntervalImpl)newInterval).getEntry().getDuration()+retoolingTime);
			this.removeAndSaveInterval(oldIntervalAtDrop);				
			this.addAndSaveInterval(newInterval);
		}
	}

	public void moveAndSaveInterval(Interval interval, JaretDate destBegin) {
		double duration = ((ProductionPlanEntryIntervalImpl) interval).getEntry().getDuration();
		int time = Math.abs(interval.getBegin().diffSeconds(destBegin));
		interval.setBegin(destBegin);
		interval.setEnd(destBegin.copy().advanceSeconds(duration));
		productionPlan. move(((ProductionPlanEntryIntervalImpl)interval).getEntry(), getTimeByEntry(((ProductionPlanEntryIntervalImpl)interval).getEntry())+time);
		((ProductionPlanEntryIntervalImpl)interval).calculateCost();

	}
	
	
	private double getTimeByEntry(ProductionPlanEntry productionPlanEntry){
		Schedule<ProductionPlanEntry> schedule = productionPlan.getEntries().get(this.workplace);
		for (int i = 0; i < schedule.size(); i++) {
			if (productionPlanEntry.equals(schedule.get(i))) {
				return schedule.getTime(i);
			}
		}
		return 0;
	}
	
	public void correctIntervals() {
		JaretDate earliestDate = this.getMinDate();
		double earliestTime = this.productionPlan.getEntries().get(this.workplace).getTime(0);
		this.getIntervals().clear();
		for (int i = 0; i < this.productionPlan.getEntries().get(this.workplace).size(); i++) {
			JaretDate begin = earliestDate.copy().advanceSeconds(this.productionPlan.getEntries().get(this.workplace).getTime(i)-earliestTime);
			JaretDate end = earliestDate.copy().advanceSeconds(( 
					this.productionPlan.getEntries().get(this.workplace).getTime(i) +
					this.productionPlan.getEntries().get(this.workplace).get(i).getDuration()) - earliestTime);
			Interval interval = new ProductionPlanEntryIntervalImpl(this.productionPlan.getEntries().get(this.workplace).get(i));
			interval.setBegin(begin);
			interval.setEnd(end);
			((ProductionPlanEntryIntervalImpl)interval).calculateCost();

			this.addInterval(interval);
			
		}
	}
}

/**
 * 
 */
package de.nilgmbh.app.ui.views;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.views.IViewDescriptor;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanWarning;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.internalmodel.WarningRow;
import de.nilgmbh.app.internalmodel.WarningViewerComparator;
import de.nilgmbh.app.internalmodel.WarningViewerFilter;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarModel;
import de.nilgmbh.app.updatemechanism.UpdateListener;
import de.nilgmbh.app.updatemechanism.UpdateType;

/**
 * @author Ardhi
 *
 */
public class WarningView extends ViewPart implements UpdateListener, IViewDescriptor {

	public static final String ID = WarningView.class.getName();

	

	private TableViewer tableViewer;
	private WarningViewerComparator comparator;
	private WarningViewerFilter filter ;
	


	@Override
	public void createPartControl(Composite parent) {
		GridLayout layout = new GridLayout(2,false);
		parent.setLayout(layout);
		Label searchLabel = new Label (parent,SWT.NONE);
		searchLabel.setText(getString("WarningView.searchLabel")+" : ");
		final Text searchText = new Text(parent, SWT.BORDER | SWT.SEARCH);
		searchText.setLayoutData(new GridData (GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		searchText.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent ke) {
				filter.setSearchText(searchText.getText());
				tableViewer.refresh();
			}

		});		

		createViewer(parent);
	
	
		
		
		
		Activator.getDefault().registerUpdateListener(this);
//		getSite().getPage().addPartListener(new IPartListener2() {
//		
//
//			@Override
//			public void partActivated(IWorkbenchPartReference partRef ) {
//
//				ProductionPlan plan1 = ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().getProductionPlan();
//
//				if (plan1.getWarningList().size() >= 1){
//					try {
//						partRef.getPage().showView(WarningView.ID,null,IWorkbenchPage.VIEW_VISIBLE);
//					} catch (PartInitException e) {
//						e.printStackTrace();
//					}
//			
////					clearData();
////					Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
////					ProductionPlanPrinter.printProductionPlan(productionPlanTimeBarModel.getProductionPlan());				
//				}
//
//
//
//			}
//
//			@Override
//			public void partBroughtToTop(IWorkbenchPartReference arg0) {
//
//			}
//
//			@Override
//			public void partClosed(IWorkbenchPartReference arg0) {
//
//			}
//
//			@Override
//			public void partDeactivated(IWorkbenchPartReference arg0) {
//
//			}
//
//			@Override
//			public void partHidden(IWorkbenchPartReference arg0) {
//
//			}
//
//			@Override
//			public void partInputChanged(IWorkbenchPartReference arg0) {
//
//			}
//
//			@Override
//			public void partOpened(IWorkbenchPartReference arg0) {
//
//			}
//
//			@Override
//			public void partVisible(IWorkbenchPartReference arg0) {
//
//			}
//
//		});

		
		comparator = new WarningViewerComparator();
		tableViewer.setComparator(comparator);

		filter = new WarningViewerFilter();
		tableViewer.addFilter(filter);

	}

	private void createViewer(Composite parent) {
		tableViewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL| SWT.V_SCROLL | SWT.FULL_SELECTION);
		createColumns(parent, tableViewer);
		Table table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);		
		tableViewer.setContentProvider(new ArrayContentProvider());

		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 2);		
		tableViewer.getControl().setLayoutData(gridData);

	}

	private void createColumns(final Composite parent, final TableViewer viewer) {
		String[] titles = {getString("WarningView.tableHeader.name"),
							getString("WarningView.tableHeader.type"),
							getString("WarningView.tableHeader.workplace"),
							getString("WarningView.tableHeader.message")};
		int [] bounds = {200,200,200,600};

		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0],0);	
		col.setLabelProvider(new ColumnLabelProvider(){
			public String getText(Object element) {
				WarningRow warningRow = (WarningRow) element;
				return (String) warningRow.getName();

			}
		});

		col = createTableViewerColumn(titles[1], bounds[1],1);
		col.setLabelProvider(new ColumnLabelProvider(){
			public String getText(Object element) {
				WarningRow warningRow = (WarningRow) element;
				return (String) warningRow.getType();
			}
		});	

		col = createTableViewerColumn(titles[2], bounds[2],2);
		col.setLabelProvider(new ColumnLabelProvider(){
			public String getText(Object element) {
				WarningRow warningRow = (WarningRow) element;
				return (String) warningRow.getWorkplace();
			}
		});	

		col = createTableViewerColumn(titles[3], bounds[3],3);
		col.setLabelProvider(new ColumnLabelProvider(){
			public String getText(Object element) {
				WarningRow warningRow = (WarningRow) element;
				return (String) warningRow.getMessage();
			}
		});	
	}

	private TableViewerColumn createTableViewerColumn(String title, int bound, int colNumber) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer,SWT.NONE);
		viewerColumn.getColumn().setText(title);
		viewerColumn.getColumn().setWidth(bound);
		viewerColumn.getColumn().setResizable(true);
		viewerColumn.getColumn().setMoveable(true);
		viewerColumn.getColumn().addSelectionListener(getSelectionAdapter(viewerColumn.getColumn(), colNumber));
		return viewerColumn;
	}

	private SelectionAdapter getSelectionAdapter(final TableColumn column,
			final int index) {
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				comparator.setColumn(index);
				int dir = tableViewer.getTable().getSortDirection();
				if (tableViewer.getTable().getSortColumn() == column) {
					dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
				} else {

					dir = SWT.DOWN;
				}
				tableViewer.getTable().setSortDirection(dir);
				tableViewer.getTable().setSortColumn(column);
				tableViewer.refresh();
			}
		};
		return selectionAdapter;
	}

	public void setInputData(ProductionPlan plan) {

		List <WarningRow> wr = new ArrayList<WarningRow>();
		List <ProductionPlanWarning> warning = plan.getWarningList();
		
		for (int i=0; i<warning.size();i++){	
			String workplaceName = warning.get(i).workplace == null? null : warning.get(i).workplace.getName();
			if (workplaceName == null && warning.get(i).product != null) {
				List<Workplace> workplaces = EntityList.getProducingWorkplaceList(warning.get(i).product);
				List<String> workplaceNames = new ArrayList<String>();
				for (int j = 0 ; j < workplaces.size(); j++) {
					workplaceNames.add(workplaces.get(j).getName());
				}
				workplaceName = workplaceNames.toString();					
			}
			
			String productName = warning.get(i).product == null? "" : warning.get(i).product.getName();
			wr.add(new WarningRow(productName,warning.get(i).getWarningTypeString(),workplaceName,warning.get(i).getErrorMessage()));
		}
		tableViewer.setInput(wr);
	}


	@Override
	public void setFocus() {
		
	}

	@Override
	public void update(UpdateType type) {
		ProductionPlan currentPlan;
		switch (type) {
//		case PROJECT_LOADED: 
//			currentPlan = ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().getProductionPlan();
//			setInputData(currentPlan);
//			break;
		case PRODUCTIONPLAN_CHANGED:
			ProductionPlanTimeBarModel currentProductionPlanTimeBarModel = ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel();
			if (currentProductionPlanTimeBarModel != null){
				currentPlan = currentProductionPlanTimeBarModel.getProductionPlan();
				setInputData(currentPlan);
			}
			break;
		default:
			break;
		}

	}

	@Override
	public IViewPart createView() throws CoreException {
		return null;
	}

	@Override
	public String[] getCategoryPath() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public String getId() {
		return null;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	@Override
	public String getLabel() {
		
		return getString("WarningView.tabTitle");
	}

	@Override
	public float getFastViewWidthRatio() {
		return 0;
	}

	@Override
	public boolean getAllowMultiple() {
		return false;
	}

	@Override
	public boolean isRestorable() {
		return false;
	}


}

/**
 * 
 */
package de.nilgmbh.app.ui.views;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import de.fzi.wenpro.core.energy.model.productionplan.EntryState;
import de.fzi.wenpro.util.Numbers;
import de.nilgmbh.app.internalmodel.EnergyPropertyRow;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanEntryIntervalImpl;
import de.nilgmbh.app.updatemechanism.UpdateListener;
import de.nilgmbh.app.updatemechanism.UpdateType;

/**
 * @author Kiril Aleksandrov
 * @version 22.01.2015
 */
public class PPEntryEnergyInformationView extends ViewPart implements ISelectionListener, UpdateListener {
	
	public static final String ID = PPEntryEnergyInformationView.class.getName();
	private TreeViewer treeViewer;
	private static ProductionPlanEntryIntervalImpl ppeii;
	
	@Override
	public void createPartControl(Composite parent) {
		treeViewer = new TreeViewer(parent, SWT.BORDER);
		createColumns(treeViewer);
		treeViewer.setContentProvider(new EntryStateContentProvider());
		treeViewer.setLabelProvider(new EntryStateLabelProvider());
		treeViewer.setUseHashlookup(true);
		treeViewer.expandAll();

		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);
	}
	
	private void createColumns(TreeViewer viewer){
		TreeViewerColumn column = new TreeViewerColumn(viewer, SWT.LEFT);
		column.getColumn().setText("Name");
		column.getColumn().setWidth(200);
		column.getColumn().setResizable(true);
		column.getColumn().setMoveable(false);

		column = new TreeViewerColumn(viewer, SWT.LEFT);
		column.getColumn().setText("Value");
		column.getColumn().setWidth(200);
		column.getColumn().setResizable(true);
		column.getColumn().setMoveable(false);
		//		column.setEditingSupport(new PropEditingSupport(viewer));
		Tree tree = viewer.getTree();
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);
	}

	private class EntryStateContentProvider implements ITreeContentProvider {
		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		@Override
		public Object[] getElements(Object inputElement) {
			return ((TreeSet<?>) inputElement).toArray();
		}

		@Override
		public Object[] getChildren(Object parentElement) {
			if(parentElement instanceof EntryState){
				List<Object> propertyState = new ArrayList<Object>();
				propertyState.add(new EnergyPropertyRow("Start", ((EntryState)parentElement).getStartingTime()));
				propertyState.add(new EnergyPropertyRow("Stopp", (((EntryState)parentElement).getStartingTime()+((EntryState)parentElement).getDuration())));
				propertyState.add(new EnergyPropertyRow("Duration", ((EntryState)parentElement).getDuration()));
				propertyState.add(new EnergyPropertyRow("Energieverbrauch", ((EntryState)parentElement).getEnergyLevel()));

				return propertyState.toArray();
			}
			return null;
		}

		@Override
		public Object getParent(Object element) {

			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
			if(element instanceof EntryState){
				return true;
			}
			return false;
		}
	}
	
	private class EntryStateLabelProvider extends LabelProvider implements ITableLabelProvider {

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			switch (columnIndex) {
			case 0: // TODO
				if (element instanceof EntryState ){
					return getText(element);
				}else if(element instanceof EnergyPropertyRow && ((EnergyPropertyRow)element).getName().equals("Start")){
					return (String) ((EnergyPropertyRow)element).getName();
				}else if(element instanceof EnergyPropertyRow && ((EnergyPropertyRow)element).getName().equals("Stopp")){
					return (String) ((EnergyPropertyRow)element).getName();
				}else if(element instanceof EnergyPropertyRow && ((EnergyPropertyRow)element).getName().equals("Duration")){
					return (String) ((EnergyPropertyRow)element).getName();
				}else if(element instanceof EnergyPropertyRow && ((EnergyPropertyRow)element).getName().equals("Energieverbrauch")){
					return (String) ((EnergyPropertyRow)element).getName();
				}
				
			case 1: // TODO

				SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss ");
				if(element instanceof EntryState){
					return "";
				}else if(element instanceof EnergyPropertyRow && ((EnergyPropertyRow)element).getName().equals("Start")){
					int value = (int) ((EnergyPropertyRow)element).getValue();
					Date start = (Date) ppeii.getBegin().getDate().clone();
					Calendar newCal = Calendar.getInstance();
					newCal.setTime(start);
					newCal.add(Calendar.SECOND,value);
					return dateFormatter.format(newCal.getTime());
				}else if(element instanceof EnergyPropertyRow && ((EnergyPropertyRow)element).getName().equals("Stopp")){
					int value = (int) ((EnergyPropertyRow)element).getValue();
					Date start = (Date) ppeii.getBegin().getDate().clone();
					Calendar newCal = Calendar.getInstance();
					newCal.setTime(start);
					newCal.add(Calendar.SECOND,value);
					return dateFormatter.format(newCal.getTime());
				}else if(element instanceof EnergyPropertyRow && ((EnergyPropertyRow)element).getName().equals("Duration")){
					return Numbers.formatSeconds(((EnergyPropertyRow)element).getValue());
				}else if(element instanceof EnergyPropertyRow && ((EnergyPropertyRow)element).getName().equals("Energieverbrauch")){
					return Numbers.formatEnergyConsumption(((EnergyPropertyRow)element).getValue());
				}
			default:
				return "default";
			}
		}

		@Override
		public String getText(Object element) {
			if (element instanceof EntryState) {
				return ((EntryState) element).getName();
			} else{
				return "";
			}
		}
	}

	@Override
	public void update(UpdateType type) {
		
	}
	
	@Override
	public void setFocus() {
		
	}


	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {

		if (part instanceof FeinScheduleView ){
			ppeii = (ProductionPlanEntryIntervalImpl) ((IStructuredSelection) selection).getFirstElement();
			if (ppeii != null ) {
				treeViewer.setInput(ppeii.getEntry().getEntryStates());
				treeViewer.expandAll();	
			}			
		}		
	}

}

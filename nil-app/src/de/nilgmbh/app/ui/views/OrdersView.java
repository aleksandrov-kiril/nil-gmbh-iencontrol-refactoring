package de.nilgmbh.app.ui.views;


import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import de.fzi.wenpro.core.model.Order;
import de.fzi.wenpro.core.model.Project;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.ui.common.OrderViewer;
import de.nilgmbh.app.ui.dialogs.NewOrderDialog;
import de.nilgmbh.app.updatemechanism.UpdateListener;
import de.nilgmbh.app.updatemechanism.UpdateType;

public class OrdersView extends ViewPart implements UpdateListener {
	
	public static final String ID = OrdersView.class.getName();

	OrderViewer orderViewer;


	public OrdersView() {
		Activator.getDefault().getImageRegistry().get("productEnd");
		Activator.getDefault().getImageRegistry().get("productIntermediate");
	}
	
	@Override
	public void createPartControl(Composite parent) {
		Composite top = new Composite(parent, SWT.BORDER);
		top.setLayout(new FillLayout());
		this.orderViewer = new OrderViewer(top, true);
		initDND(orderViewer, parent);
		Activator.getDefault().registerUpdateListener(this);
	}
	private void initDND(final OrderViewer orderViewer, Composite parent) {

		int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT | DND.DROP_NONE;
		final DropTarget target = new DropTarget(orderViewer, operations);

		// Receive data in Text
		final TextTransfer textTransfer = TextTransfer.getInstance();
		Transfer[] types = new Transfer[] { textTransfer };
		target.setTransfer(types);
		target.addDropListener(new DropTargetListener() {
			
			@Override
			public void dropAccept(DropTargetEvent event) {
				
			}
			
			@Override
			public void drop(DropTargetEvent event) {
				if(textTransfer.isSupportedType(event.currentDataType)){				
					NewOrderDialog dialog = new NewOrderDialog(event.display.getActiveShell(),getString("OrderView.newOrderDialog"));
					int returnCode = dialog.open();
					if (returnCode == Dialog.OK) {
						Order newOrder = dialog.getOrder();
						Project.getCurrentProject().getOriginalFactory().addOrder(newOrder);
						Activator.getNewOrders().add(newOrder);
						orderViewer.refresh();
					}
				}
					
			}
			
			@Override
			public void dragOver(DropTargetEvent event) {
				
			}
			
			@Override
			public void dragOperationChanged(DropTargetEvent event) {
				
			}
			
			@Override
			public void dragLeave(DropTargetEvent event) {
				
				
			}
			
			@Override
			public void dragEnter(DropTargetEvent event) {
				
			}
		});
		
	}
	
	@Override
	public void setFocus() {
		orderViewer.setFocus();
	}

	@Override
	public void update(UpdateType type) {
		switch (type) {
		case PROJECT_LOADED:
			orderViewer.setInput(Project.getCurrentProject().getOriginalFactory());
			orderViewer.refresh();
			break;
		case PRODUCTIONPLAN_CALCULATED:
			orderViewer.refresh();
			break;
		default:
			break;
		}
		
	}





}

package de.nilgmbh.app.ui.views;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.FactoryNode;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.nilgmbh.app.Activator;

public class FactoryView extends ViewPart implements ISelectionListener{

	// constants
	
	//ui variables
	private TreeViewer factoryViewer;

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = FactoryView.class.getName();

	public FactoryView() {
	}

	@Override
	public void createPartControl(Composite parent) {
		factoryViewer = new TreeViewer(parent);
		factoryViewer.setLabelProvider(new LabelProvider(){
			
			Image factoryimage = Activator.getImageDescriptor("icons/factory.png").createImage();
			Image segmentimage = Activator.getImageDescriptor("icons/segment.png").createImage();
			Image lineimage =	 Activator.getImageDescriptor("icons/line.png").createImage();
			Image workplaceimage = Activator.getImageDescriptor("icons/workplace.png").createImage();
			Image stateimage =	 Activator.getImageDescriptor("icons/state.png").createImage();
			Image operationimage = Activator.getImageDescriptor("icons/operation.png").createImage();
			Image productimage =	 Activator.getImageDescriptor("icons/product.png").createImage();
			
			@Override
			public String getText(Object element) {
				if(element instanceof FactoryNode)
					return ((FactoryNode) element).getName();
				else
					return super.getText(element);
			}
			
			@Override
			public Image getImage(Object element) {
				if(element instanceof FactoryNode){
					switch (((FactoryNode) element).getLevel()) {
					case 0:
						return factoryimage;
					case 1:
						return segmentimage;
					case 2:
						return lineimage;
					case 3:
						return workplaceimage;
					default:
						return null;
					}
				}
				else if(element instanceof Operation){
					return operationimage;
				}
				else if(element instanceof WorkplaceState){
					return stateimage;
				}
				else if(element instanceof Product){
					return productimage;
				}
				return null;
			}
		});
		factoryViewer.setContentProvider(new ITreeContentProvider() {
			
			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}
			
			@Override
			public void dispose() {
			}
			
			@Override
			public Object getParent(Object element) {
				if(element instanceof FactoryNode){
					return ((FactoryNode) element).getParent();
				}
				if(element instanceof WorkplaceState){
					return ((WorkplaceState) element).getWorkplace();
				}
				if(element instanceof Operation){
				}
				return null;
			}
			
			
			@Override
			public boolean hasChildren(Object element) {
				if(element instanceof Workplace){
					return ((Workplace) element).getStates().size()>0;
				}
				if(element instanceof FactoryNode){
					return ((FactoryNode) element).getSubNodes().size()>0;
				}
				if(element instanceof WorkplaceState){
					return ((WorkplaceState) element).getOperations().size()>0;
				}
				if(element instanceof Operation){
					return ((Operation) element).getOutputs().size()>0;
				}
				return false;
			}
			
			@Override
			public Object[] getElements(Object element) {
				if(element instanceof FactoryWrapper){
					return new Object[]{((FactoryWrapper) element).getF()};
				}
				return new Object[]{};
			}
			
			@Override
			public Object[] getChildren(Object element) {
				if(element instanceof Workplace){
					return ((Workplace) element).getStates().toArray();
				}
				if(element instanceof FactoryNode){
					return ((FactoryNode) element).getSubNodes().toArray();
				}
				if(element instanceof WorkplaceState){
					return ((WorkplaceState) element).getOperations().toArray();
				}
				if(element instanceof Operation){
					return ((Operation) element).getOutputProducts().toArray();
				}
				return null;
			}
		});
		factoryViewer.setInput(new FactoryWrapper(Project.getCurrentProject().getOriginalFactory()));
		factoryViewer.addDoubleClickListener(new IDoubleClickListener() {
			
			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object selection = ((IStructuredSelection)event.getSelection()).getFirstElement();
				if(selection instanceof Workplace){
					EditDialog editWorkplace = new EditDialog(getViewSite().getShell(),((Workplace)selection));
					editWorkplace.open();
					((Workplace) selection).setWorkerLimit(editWorkplace.workerLimit);
				}
				else if(selection instanceof InternalOperation){	
					EditDialog editOperation = new EditDialog(getViewSite().getShell(),((InternalOperation)selection));
					editOperation.open();
					((InternalOperation) selection).setTime(editOperation.productionTime);
					((InternalOperation) selection).setScrapRate(editOperation.scraprate);
//					((InternalOperation) selection).setLotSize(editOperation.lotsize);
				}
				else if(selection instanceof WorkplaceState){
					EditDialog editWorkplaceState = new EditDialog(getViewSite().getShell(),((WorkplaceState)selection));
					editWorkplaceState.open();
					((WorkplaceState) selection).setRetoolingCost(editWorkplaceState.retoolingcost);
					((WorkplaceState) selection).setRetoolingTime(editWorkplaceState.retoolingtime);
				}
				else if(selection instanceof Product){
					EditDialog editWorkplaceState = new EditDialog(getViewSite().getShell(),((Product)selection));
					editWorkplaceState.open();
					((Product) selection).setName(editWorkplaceState.productname);
				}
				
				factoryViewer.refresh(true);
				
			}
		});
		
	}

	@Override
	public void setFocus() {
	}
	
	private class FactoryWrapper{
		private Factory f;
		public FactoryWrapper(Factory f) {
			this.f=f;
		}
		
		public Factory getF() {
			return f;
		}
	}

	private class EditDialog extends Dialog{

		private INPUTTYPE inputtype;
		private Workplace wp;
		private WorkplaceState wps;
		private InternalOperation op;
		private double productionTime;
		private double scraprate;
		private int lotsize;
		private int workerLimit;
		private double retoolingcost;
		private double retoolingtime;
		protected String productname;
		
		private EditDialog(Shell parentShell) {
			super(parentShell);
		}
		
		protected EditDialog(Shell shell, Workplace wp){
			this(shell);
			inputtype = INPUTTYPE.WP;
			workerLimit = wp.getWorkerLimit();
			this.wp=wp;
		}
		
		protected EditDialog(Shell shell, WorkplaceState wps){
			this(shell);
			inputtype = INPUTTYPE.WPS;
			retoolingcost = wps.getRetoolingCost();
			retoolingtime = wps.getRetoolingTime();
			this.wps=wps;
		}
		
		protected EditDialog(Shell shell, InternalOperation op){
			this(shell);
			lotsize = op.getLotSize();
			scraprate = op.getScrapRate();
			productionTime = op.getTime();
			this.op=op;
			inputtype = INPUTTYPE.OP;
		}
		
		public EditDialog(Shell shell, Product product) {
			this(shell);
			
		}

		@Override
		protected void configureShell(Shell newShell) {
			super.configureShell(newShell);
			switch (inputtype) {
			case OP:
				newShell.setText("Operationseigenschaften");
			case WP:
				newShell.setText("Arbeitsplatzeigenschaften");
			case WPS:
				newShell.setText("Zustandseigenschaften");
				
			}
		}
		
		@Override
		protected Control createDialogArea(Composite parent) {
			Composite top;
			switch (inputtype) {
			case WP:
				top = new Composite(parent, SWT.NONE);
				top.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).create());
				top.setLayout(GridLayoutFactory.swtDefaults().numColumns(2)
						.equalWidth(false).create());
//				Label labelRetoolingWorkplaceTime = new Label(top, SWT.NONE);
//				labelRetoolingWorkplaceTime.setLayoutData(GridDataFactory.swtDefaults().create());
//				labelRetoolingWorkplaceTime.setText(("Umr�stzeit"));
//				
//				textRetoolingWorkplaceTime = new Text(top, SWT.BORDER);
//				textRetoolingWorkplaceTime.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
//				textRetoolingWorkplaceTime.setText(String.valueOf(wp.getActiveState()!=null?wp.getActiveState().getRetoolingTime():0));
//				
//				Label labelRetoolingWorkplaceCost = new Label(top, SWT.NONE);
//				labelRetoolingWorkplaceCost.setLayoutData(GridDataFactory.swtDefaults().create());
//				labelRetoolingWorkplaceCost.setText(("Umr�stkosten"));
//				
//				textRetoolingWorkplaceCost = new Text(top, SWT.BORDER);
//				textRetoolingWorkplaceCost.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
//				textRetoolingWorkplaceCost.setText(String.valueOf(wp.getActiveState()!=null?wp.getActiveState().getRetoolingCost():0));
				
				Label labelWorkerCount = new Label(top, SWT.NONE);
				labelWorkerCount.setLayoutData(GridDataFactory.swtDefaults().create());
				labelWorkerCount.setText(("Arbeiteranzahl"));
				
				final Text textWorkerCount = new Text(top, SWT.BORDER);
				textWorkerCount.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
				textWorkerCount.setText(String.valueOf(wp.getWorkerLimit()));
				textWorkerCount.addModifyListener(new ModifyListener() {
					
					@Override
					public void modifyText(ModifyEvent e) {
							try {
							workerLimit = Integer.parseInt(textWorkerCount.getText());
						} catch (Exception e2) {
							MessageBox mb = new MessageBox(getShell(), OK);
							mb.setMessage("Arbeiteranzahl muss eine ganze Zahl sein!");
							textWorkerCount.setText(String.valueOf(wp.getWorkerLimit()));
						}
					}
				});
				
				wp.getPersonnelAttendanceRate();
				
				break;
			case WPS:
				top = new Composite(parent, SWT.NONE);
				top.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).create());
				top.setLayout(GridLayoutFactory.swtDefaults().numColumns(3)
						.equalWidth(false).create());
				Label labelRetoolingTime = new Label(top, SWT.NONE);
				labelRetoolingTime.setLayoutData(GridDataFactory.swtDefaults().create());
				labelRetoolingTime.setText(("Umr�stzeit"));
				
				final Text textRetoolingTime = new Text(top, SWT.BORDER);
				textRetoolingTime.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
				textRetoolingTime.setText(String.valueOf(wps.getRetoolingTime()));
				textRetoolingTime.addModifyListener(new ModifyListener() {
					
					@Override
					public void modifyText(ModifyEvent e) {
							try {
							retoolingtime = Double.parseDouble(textRetoolingTime.getText());
						} catch (Exception e2) {
							MessageBox mb = new MessageBox(getShell(), OK);
							mb.setMessage("Umr�stzeit muss eine Zahl sein!");
							textRetoolingTime.setText(String.valueOf(wps.getRetoolingTime()));
						}
					}
				});
				
				Label label1 = new Label(top, SWT.NONE);
				label1.setLayoutData(GridDataFactory.swtDefaults().align(SWT.CENTER, SWT.CENTER).grab(false, false).create());
				label1.setText(String.valueOf("sec"));
				
				Label labelRetoolingCost= new Label(top, SWT.NONE);
				labelRetoolingCost.setLayoutData(GridDataFactory.swtDefaults().create());
				labelRetoolingCost.setText(("Umr�stkosten"));
				
				final Text textRetoolingCost = new Text(top, SWT.BORDER);
				textRetoolingCost.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
				textRetoolingCost.setText(String.valueOf(wps.getRetoolingCost()));
				textRetoolingCost.addModifyListener(new ModifyListener() {
					@Override
					public void modifyText(ModifyEvent e) {
						try {
							retoolingcost = Double.parseDouble(textRetoolingCost.getText());
						} catch (Exception e2) {
							MessageBox mb = new MessageBox(getShell(), OK);
							mb.setMessage("Umr�stkosten m�ssen eine Zahl sein!");
							textRetoolingCost.setText(String.valueOf(wps.getRetoolingCost()));
						}
					}
				});
				
				label1 = new Label(top, SWT.NONE);
				label1.setLayoutData(GridDataFactory.swtDefaults().align(SWT.CENTER, SWT.CENTER).grab(false, false).create());
				label1.setText(String.valueOf("Euro"));
				break;
			case OP:
				top = new Composite(parent, SWT.NONE);
				top.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).create());
				top.setLayout(GridLayoutFactory.swtDefaults().numColumns(3)
						.equalWidth(false).create());
				
				Label labelProductionTime = new Label(top, SWT.NONE);
				labelProductionTime.setLayoutData(GridDataFactory.swtDefaults().create());
				labelProductionTime.setText(("Produktionszeit"));
				
				final Text textProductionTime = new Text(top, SWT.BORDER);
				textProductionTime.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
				textProductionTime.setText(String.valueOf(op.getTime()));
				textProductionTime.addModifyListener(new ModifyListener() {
					@Override
					public void modifyText(ModifyEvent e) {
						try {
							productionTime = Double.parseDouble(textProductionTime.getText());
						} catch (Exception e2) {
							MessageBox mb = new MessageBox(getShell(), OK);
							mb.setMessage("Produkitonszeit muss eine Zahl sein!");
							textProductionTime.setText(String.valueOf(op.getTime()));
						}
					}
				});
				
				Label label = new Label(top, SWT.NONE);
				label.setLayoutData(GridDataFactory.swtDefaults().align(SWT.CENTER, SWT.CENTER).grab(false, false).create());
				label.setText(String.valueOf("sec"));
				
//				Label labelLotSize = new Label(top, SWT.NONE);
//				labelLotSize.setLayoutData(GridDataFactory.swtDefaults().create());
//				labelLotSize.setText(("Schrittgr��e"));
//				
//				final Text textLotsize = new Text(top, SWT.BORDER);
//				textLotsize.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
//				textLotsize.setText(String.valueOf(op.getLotSize()));
//				textLotsize.addModifyListener(new ModifyListener() {
//					@Override
//					public void modifyText(ModifyEvent e) {
//						try {
//							lotsize = Integer.parseInt(textLotsize.getText());
//						} catch (Exception e2) {
//							MessageBox mb = new MessageBox(getShell(), OK);
//							mb.setMessage("Produkitonszeit muss eine ganze Zahl sein!");
//							textLotsize.setText(String.valueOf(op.getLotSize()));
//						}
//					}
//				});
//				
//				label = new Label(top, SWT.NONE);
//				label.setLayoutData(GridDataFactory.swtDefaults().align(SWT.CENTER, SWT.CENTER).grab(false, false).create());
//				label.setText(String.valueOf("St�ck"));
				
				Label labelScrapRate = new Label(top, SWT.NONE);
				labelScrapRate.setLayoutData(GridDataFactory.swtDefaults().create());
				labelScrapRate.setText(("Ausschussrate"));
				
				final Text textScrapRate = new Text(top, SWT.BORDER);
				textScrapRate.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
				textScrapRate.setText(String.valueOf(op.getScrapRate()));
				textScrapRate.addModifyListener(new ModifyListener() {
					@Override
					public void modifyText(ModifyEvent e) {
						try {
							scraprate = Double.parseDouble(textScrapRate.getText());
						} catch (Exception e2) {
							MessageBox mb = new MessageBox(getShell(), OK);
							mb.setMessage("Ausschussrate muss eine Zahl sein!");
							textScrapRate.setText(String.valueOf(op.getScrapRate()));
						}
					}
				});
				
				label = new Label(top, SWT.NONE);
				label.setLayoutData(GridDataFactory.swtDefaults().align(SWT.CENTER, SWT.CENTER).grab(false, false).create());
				label.setText(String.valueOf("St�ck"));
				break;
			case PROD:
				top = new Composite(parent, SWT.NONE);
				top.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).create());
				top.setLayout(GridLayoutFactory.swtDefaults().numColumns(2)
						.equalWidth(false).create());
				Label labelProduct = new Label(top, SWT.NONE);
				labelProduct.setLayoutData(GridDataFactory.swtDefaults().align(SWT.CENTER, SWT.CENTER).grab(false, false).create());
				labelProduct.setText(String.valueOf("Produktname"));
				
				final Text textProduct = new Text(top, SWT.BORDER);
				textProduct.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
				textProduct.setText(String.valueOf(op.getName()));
				textProduct.addModifyListener(new ModifyListener() {
					@Override
					public void modifyText(ModifyEvent e) {
						try {
							productname = textProduct.getText();
						} catch (Exception e2) {
							e2.printStackTrace();
						}
					}
				});
				
				break;
			default:
				top = new Composite(parent, SWT.NONE);
				break;
			}
			return top;
		}
		
	}
	enum INPUTTYPE {
		WP,WPS,OP,PROD
	}
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
	}
}

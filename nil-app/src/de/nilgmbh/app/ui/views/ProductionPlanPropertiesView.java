package de.nilgmbh.app.ui.views;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import de.fzi.wenpro.util.Numbers;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.internalmodel.PropertyRow;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarModel;
import de.nilgmbh.app.updatemechanism.UpdateListener;
import de.nilgmbh.app.updatemechanism.UpdateType;

public class ProductionPlanPropertiesView extends ViewPart implements ISelectionListener, UpdateListener {
	
	public static final String ID = ProductionPlanPropertiesView.class.getName();

	public ProductionPlanPropertiesView() {
	}

	
	private TableViewer tableViewer;
	@Override
	public void createPartControl(Composite parent) {
		createViewer(parent);
		Activator.getDefault().registerUpdateListener(this);

	}
	
	private void createViewer(Composite parent) {
		tableViewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL| SWT.V_SCROLL | SWT.FULL_SELECTION);
		createColumns(tableViewer);
		Table table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);		
		tableViewer.setContentProvider(new ArrayContentProvider());

	}
	
	private void createColumns(final TableViewer viewer) {
		String[] titles = {getString("ProductionPlanPropertiesView.name"),getString("ProductionPlanPropertiesView.value")};
		int [] bounds = {100,300};
		
		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0]);	
		col.setLabelProvider(new ColumnLabelProvider(){
			public String getText(Object element) {
				PropertyRow propertyRow =  (PropertyRow) element;
				return (String) propertyRow.getName();
			}
		});
		
		col = createTableViewerColumn(titles[1], bounds[1]);
		col.setLabelProvider(new ColumnLabelProvider(){
			public String getText(Object element) {
				PropertyRow propertyRow =  (PropertyRow) element;
				return (String) propertyRow.getValue();
			}
		});		
	}
	
	private TableViewerColumn createTableViewerColumn(String title, int bound) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer,SWT.NONE);
		viewerColumn.getColumn().setText(title);
		viewerColumn.getColumn().setWidth(bound);
		viewerColumn.getColumn().setResizable(true);
		viewerColumn.getColumn().setMoveable(true);
		return viewerColumn;
	}
	public void setInput(ProductionPlanTimeBarModel pptbm) {
		List <PropertyRow> pr = new ArrayList<PropertyRow>();
		if (pptbm == null) {
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.totalcost"),""));
			pr.add(new PropertyRow("Energiekosten",""));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.duration"),""));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.begin"),""));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.end"),""));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.totalenergyconsumption"),""));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.epi"),""));
			
		}
		else {			
						
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.totalcost"),Numbers.formatMoney( pptbm.getProductionPlan().getCost())));
			pr.add(new PropertyRow("Energiekosten",Numbers.formatMoney(pptbm.getProductionPlan().getEnergyCost())));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.duration"),Numbers.formatMinutes(pptbm.getMaxDate().diffSeconds(pptbm.getMinDate())/60)));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.begin"),pptbm.getMinDate().toDisplayString()));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.end"),pptbm.getMaxDate().toDisplayString()));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.totalenergyconsumption"), Numbers.formatEnergyConsumptionPerHour(pptbm.getProductionPlan().getEnergyConsumption())));
			pr.add(new PropertyRow(getString("ProductionPlanPropertiesView.epi"),Numbers.format(pptbm.getProductionPlan().getEPI())));

		}
		
		
		tableViewer.setInput(pr);
	}

	@Override
	public void setFocus() {

	}


	@Override
	public void update(UpdateType type) {
		ProductionPlanTimeBarModel productionPlanTimeBarModel;
		switch (type) {
		case PRODUCTIONPLAN_CHANGED:
			productionPlanTimeBarModel = ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel();
			setInput(productionPlanTimeBarModel);	
			break;

		default:
			break;
		}
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
	}

}

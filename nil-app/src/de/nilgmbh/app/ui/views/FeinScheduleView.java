package de.nilgmbh.app.ui.views;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.fzi.wenpro.core.model.productionplan.ProductionPlanEntry;
import de.fzi.wenpro.core.printing.ProductionPlanPrinter;
import de.jaret.util.date.Interval;
import de.jaret.util.date.IntervalImpl;
import de.jaret.util.date.JaretDate;
import de.jaret.util.ui.timebars.TimeBarMarker;
import de.jaret.util.ui.timebars.TimeBarMarkerImpl;
import de.jaret.util.ui.timebars.TimeBarViewerInterface;
import de.jaret.util.ui.timebars.model.DefaultRowHeader;
import de.jaret.util.ui.timebars.model.DefaultTimeBarModel;
import de.jaret.util.ui.timebars.model.DefaultTimeBarRowModel;
import de.jaret.util.ui.timebars.model.ITimeBarChangeListener;
import de.jaret.util.ui.timebars.model.TimeBarRow;
import de.jaret.util.ui.timebars.model.TimeBarSelectionListener;
import de.jaret.util.ui.timebars.model.TimeBarSelectionModel;
import de.jaret.util.ui.timebars.strategy.DefaultOverlapStrategy;
import de.jaret.util.ui.timebars.swt.TimeBarViewer;
import de.jaret.util.ui.timebars.swt.renderer.DefaultTitleRenderer;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.ui.common.ScalePanel;
import de.nilgmbh.app.ui.common.dnd.DNDContainer;
import de.nilgmbh.app.ui.dialogs.InitialWizard;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanEntryIntervalImpl;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanEntryIntervalRenderer;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanGridRenderer;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanIntervalModificator;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarModel;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarViewer;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeScaleRenderer;
import de.nilgmbh.app.ui.jaretimpl.WorkplaceTimeBarRowModel;
import de.nilgmbh.app.updatemechanism.UpdateListener;
import de.nilgmbh.app.updatemechanism.UpdateType;

/**
 * @author Ardhi
 *
 */

public class FeinScheduleView extends ViewPart implements ISelectionListener, UpdateListener {
	public static final String ID = FeinScheduleView.class.getName();
	Display display = Display.getCurrent();

	protected  ProductionPlanTimeBarViewer _tbv;

	private static final boolean MONITORINTERVALCHANGES = false;

	private ProductionPlanTimeBarModel productionPlanTimeBarModel;

	public FeinScheduleView() {	

	}


	public  ProductionPlanTimeBarViewer get_tbv() {
		return _tbv;
	}

	public void set_tbv(ProductionPlanTimeBarViewer _tbv) {
		this._tbv = _tbv;
	}

	@Override
	public void setPartName(String partName) {
		super.setPartName(partName);
	}
	
	public void registerTimeBarRenderer() {
		_tbv.registerTimeBarRenderer(ProductionPlanEntryIntervalImpl.class, new
				ProductionPlanEntryIntervalRenderer());
	}

	@Override
	public void createPartControl(final Composite parent) {
		
		this.productionPlanTimeBarModel = ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel();
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		parent.setLayout(gridLayout);
		GridData gd = new GridData(SWT.FILL,SWT.FILL, true, true);

		_tbv = new ProductionPlanTimeBarViewer(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		_tbv.setLayoutData(gd);
		_tbv.setTimeScalePosition(TimeBarViewer.TIMESCALE_POSITION_TOP);
		_tbv.setModel(productionPlanTimeBarModel);
		_tbv.setDrawOverlapping(true);
		_tbv.addMarker(new TimeBarMarkerImpl(true, new JaretDate(Activator.getCurrentTime().getTime())));
		// add a context menu on intervals
		// commented to not introduce a jface dependencay in this example
		// MenuManager ctxMM = new MenuManager();
		// ctxMM.add(new Action() {
		// @Override
		// public String getText() {
		// return "Test";
		// }
		// });
		// Menu intervalContextMenu = ctxMM.createContextMenu(_tbv);
		// _tbv.setIntervalContextMenu(intervalContextMenu);

		// add two interval modificator using different grid snaps
		// the last row (r3) will be filled with other intevals using the other
		// grid snap
		_tbv.addIntervalModificator(new ProductionPlanIntervalModificator(){
			@Override
			public double getSecondGridSnap() {
				return 100;
			}


			@Override
			public boolean isApplicable(TimeBarRow row, Interval interval) {
				return !(interval instanceof ProductionPlanEntryIntervalImpl);
			}

		});

		_tbv.addIntervalModificator(new ProductionPlanIntervalModificator() {
			@Override
			public double getSecondGridSnap() {
				return 	1000;
			}

			@Override
			public boolean isApplicable(TimeBarRow row, Interval interval) {
				return (interval instanceof ProductionPlanEntryIntervalImpl);
			}

		});


		// optional other interval renderer

		registerTimeBarRenderer();
		_tbv.setPixelPerSecond(0.05);
		_tbv.setDrawRowGrid(true);
		_tbv.setSelectionDelta(6);

		// do not assume sorted intervals
		// --> tell the default strategy to do the sorting
		((DefaultOverlapStrategy) _tbv.getOverlapStrategy())
		.setAssumeSortedIntervals(false);

		// configure the title renderer with a background image and set the
		// title
		DefaultTitleRenderer titleRenderer = new DefaultTitleRenderer();
		titleRenderer.setBackgroundRscName("/de/jaret/examples/timebars/hierarchy/swt/titlebg.png");
		// _tbv.setTitleRenderer(titleRenderer);
		// _tbv.setTitle("SwtOverlap");

		//		BoxTimeScaleRenderer btsr =  new BoxTimeScaleRenderer();
		ProductionPlanTimeScaleRenderer pptscr = new ProductionPlanTimeScaleRenderer();	

		// enable DST correction
		pptscr.setCorrectDST(true);
		_tbv.setTimeScaleRenderer(pptscr);
		_tbv.setGridRenderer(new ProductionPlanGridRenderer());


		//		 for controlpanel

		//				SwtControlPanel ctrl = new SwtControlPanel(parent, SWT.NULL, _tbv,null);
		//				gd = new GridData(GridData.FILL_HORIZONTAL);
		//				ctrl.setLayoutData(gd);		
		ScalePanel sp = new ScalePanel(parent, SWT.NULL, _tbv);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		sp.setLayoutData(gd);


		//		 OverlapControlPanel ctrl2 = new OverlapControlPanel(parent, SWT.NULL,
		//		 _tbv);
		//		 gd = new GridData(GridData.FILL_HORIZONTAL);
		//		 ctrl2.setLayoutData(gd);

		// add a listener for doubleclicks (and some test code for selecting
		// specially rendered intervals)
		// shows how to get intervals from a point


		_tbv.addMenuDetectListener(new MenuDetectListener(){

			@Override
			public void menuDetected(MenuDetectEvent e) {
				final List<Interval> intervals = _tbv.getSelectionModel().getSelectedIntervals();
				if (intervals != null ) {	
					Menu menu = new Menu(parent);					
					MenuItem initialWizardItem = new MenuItem (menu, SWT.PUSH);
					initialWizardItem.setText ("Operation Modifizieren");
					initialWizardItem.addListener (SWT.Selection, new Listener () {
						@Override
						public void handleEvent(Event event) {
							ProductionPlanEntryIntervalImpl ppeiiSelection = new ProductionPlanEntryIntervalImpl(intervals.get(0).getBegin(), intervals.get(0).getEnd(), ((ProductionPlanEntryIntervalImpl) intervals.get(0)).getEntry());
							Object object = ppeiiSelection;		
							InitialWizard initialWizard = new InitialWizard(object, true);
							WizardDialog dialog = new WizardDialog(event.display.getActiveShell(), initialWizard);
							if (dialog.open() == Window.OK){
								//								row.removeAndSaveInterval(intervals.get(0));
							}
						}						
					});

					menu.setLocation(e.x, e.y);
					menu.setVisible (true);
					while (!menu.isDisposed () && menu.isVisible ()) {
						if (!parent.getShell().getDisplay().readAndDispatch ()){
							parent.getShell().getDisplay().sleep ();
						}
					}
					menu.dispose ();
				}

			}

		});
		// add update listener
		Activator.getDefault().registerUpdateListener(this);

		_tbv.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.DEL && !_tbv.getSelection().isEmpty()){
					for (Object obj : ((IStructuredSelection)_tbv.getSelection()).toList()){
						Interval in = (Interval)obj;
						WorkplaceTimeBarRowModel row = (WorkplaceTimeBarRowModel) _tbv
						.getModel().getRowForInterval(in);
						row.removeAndSaveInterval(in);

						Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);

						ProductionPlanPrinter.printProductionPlan(productionPlanTimeBarModel.getProductionPlan());
						if (productionPlanTimeBarModel.getProductionPlan().getWarningList().size() > 0){
							try {
								getSite().getWorkbenchWindow().getActivePage().showView(WarningView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
								Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);


							} catch (PartInitException e1) {
								e1.printStackTrace();
							}
						}
						else{
							try {
								getSite().getWorkbenchWindow().getActivePage().showView(PPEntryPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);

							} catch (PartInitException e1) {
								e1.printStackTrace();
							}
						}

					}						
				}
			}

			@Override

			public void keyPressed(KeyEvent e) {

			}
		});



		//		_tbv.addDragDetectListener(new DragDetectListener() {
		//
		//			@Override
		//			public void dragDetected(DragDetectEvent e) {
		//				ProductionPlanPrinter.printProductionPlan(productionPlanTimeBarModel.getProductionPlan());	
		//				
		//			}


		//            public void mouseMove(MouseEvent me) {
		//                if ((me.stateMask & SWT.BUTTON1) != 0) {
		//                	_tbv.getDelegate().mouseDragged(me.x, me.y, convertModifierMaskToSwing(me.stateMask));
		//                } else {
		//                    _tbv.getDelegate().mouseMoved(me.x, me.y);
		//                }
		//                ProductionPlanPrinter.printProductionPlan(productionPlanTimeBarModel.getProductionPlan());				
		//            }
		//        });
		//		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);

		// adds listener for changing tabs
		getSite().getPage().addPartListener(new IPartListener2() {

			@Override
			public void partActivated(IWorkbenchPartReference partRef ) {

				if (partRef.getPart(false) == FeinScheduleView.this){
					clearData();
					ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel((ProductionPlanTimeBarModel) _tbv.getModel());
					Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
					
//										_tbv.setModel(ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel());
//					ProductionPlanPrinter.printProductionPlan(productionPlanTimeBarModel.getProductionPlan());				
				}



			}

			@Override
			public void partBroughtToTop(IWorkbenchPartReference arg0) {

			}

			@Override
			public void partClosed(IWorkbenchPartReference arg0) {
			}

			@Override
			public void partDeactivated(IWorkbenchPartReference arg0) {

			}

			@Override
			public void partHidden(IWorkbenchPartReference arg0) {

			}

			@Override
			public void partInputChanged(IWorkbenchPartReference arg0) {

			}

			@Override
			public void partOpened(IWorkbenchPartReference arg0) {

			}

			@Override
			public void partVisible(IWorkbenchPartReference arg0) {

			}

		});

		initDND(_tbv, parent);

		if (MONITORINTERVALCHANGES) {
			registerChangeListener(_tbv);
		}

		// additional controls
		Composite addPanel = new Composite(parent, SWT.NULL);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		addPanel.setLayoutData(gd);
		addPanel.setLayout(new RowLayout());


		_tbv.getSelectionModel().addTimeBarSelectionListener(new
				TimeBarSelectionListener() {

			public void elementAddedToSelection(TimeBarSelectionModel
					selectionModel, Object element) {
			}

			public void elementRemovedFromSelection(TimeBarSelectionModel
					selectionModel, Object element) {
			}

			public void selectionChanged(TimeBarSelectionModel selectionModel) {

			}

		});





		DefaultOverlapStrategy os = (DefaultOverlapStrategy) _tbv
		.getOverlapStrategy();
		os.setAssumeSortedIntervals(false);


		// for registering  to be listened

		getSite().setSelectionProvider(_tbv);
		clearData();
	}

	private Point _dragStart;
	private int _startOffsetX;
	private int _startOffsetY;
	private JaretDate _dragStartDate;
	private List<Interval> _draggedIntervals;
	private List<Integer> _yOffsets;
	private List<Interval> _origIntervals;
	private TimeBarRow _draggedRow;
	private boolean _isRowDrag;
	


	private void clearData() {
		_tbv.deHighlightRow();
		_tbv.setGhostIntervals(null, null);
		_tbv.setGhostRows(null, null);
		_draggedIntervals = null;
		_draggedRow = null;
		_isRowDrag = false;
		_origIntervals = null;
	}

	private void initDND(final TimeBarViewer tbv, Composite parent) {

		// support move and copy
		int operations = DND.DROP_COPY | DND.DROP_MOVE;
		final DragSource source = new DragSource(tbv, operations);

		// Provide data in Text format
		Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
		source.setTransfer(types);

		source.addDragListener(new DragSourceListener() {
			public void dragStart(DragSourceEvent event) {

				// check whether drag occured on header or hierarchy area
				if (_tbv.isInRowAxis(event.x, event.y)) {
					// possible row drag

					TimeBarRow row = _tbv.getRowForXY(event.x, event.y);
					if (row != null && !_tbv.rowLineHit(event.x, event.y)) {
						// row hit, start row drag
						_isRowDrag = true;
						_dragStart = new Point(event.x, event.y);

						// capture the data for internal use
						// row drag: use row at starting position
						_draggedRow = row;

					} else {
						event.doit = false;
					}

				} 
				// drag happened outside the header or hierarchy area (Interval area)
				else {

					// Only start the drag if there is an interval at hand
					// and the point of the beginning drag is not near the
					// border of the interval, allowing the
					// timebarviewers implementation of dragging for moving
					// inside a row and for resizing to be usable
					// even with an installed drag source (The internal drag
					// feature can be disabled when using
					// the systems drag and drop support)
					//					ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel((ProductionPlanTimeBarModel) _tbv.getModel());
					List<Interval> l = tbv.getIntervalsAt(event.x, event.y);
					if (l.size() > 0) {

						Interval interval = l.get(0);

						JaretDate date = tbv.dateForXY(event.x, event.y);
						// int secondsForMargin =
						// (int)(50.0/_tbv.getPixelPerSecond()); // 50 pixel
						// margin
						int secondsForMargin = 100; // 100 second margin
						if (Math.abs(date.diffSeconds(interval.getBegin())) < secondsForMargin	|| Math.abs(date.diffSeconds(interval.getEnd())) < secondsForMargin) {

							System.out.println("Near the border of an interval -> no drag");
							event.doit = true;
						}

						else {
							// remember the point the drag started
							_dragStart = new Point(event.x, event.y);
							_dragStartDate = _tbv.dateForXY(event.x, event.y);
							_isRowDrag = false;

							// make sure the interval is selected
							if (_tbv.getSelectionModel().getSelectedIntervals().size() == 1) {
								_tbv.getSelectionModel().clearSelection();
								_tbv.getSelectionModel().addSelectedInterval(interval);
							}

							// capture the data of the dragged intervals
							// drag all selected intervals
							// to allow changing the time of the intervals and
							// drawing ghost intervals a copy of the
							// intervals will be made
							_draggedIntervals = new ArrayList<Interval>();
							_origIntervals = new ArrayList<Interval>();
							_yOffsets = new ArrayList<Integer>();

							for (Interval i : _tbv.getSelectionModel().getSelectedIntervals()) {

								_draggedIntervals.add(new ProductionPlanEntryIntervalImpl(i
										.getBegin().copy(), i.getEnd().copy(), ((ProductionPlanEntryIntervalImpl) i).getEntry()));

								_origIntervals.add(i);
								TimeBarRow row = _tbv.getModel().getRowForInterval(i);
								if (row != null) {
									int yOffset;
									if (_tbv.getTBOrientation().equals( TimeBarViewerInterface.Orientation.HORIZONTAL)) {
										yOffset = _tbv.getYForRow(row)
										- _dragStart.y;
									} else {
										yOffset = _tbv.getYForRow(row)
										- _dragStart.x;
									}
									_yOffsets.add(yOffset);
								}

							}

							TimeBarRow row = _tbv.getRowForXY(event.x, event.y);
							// store both x and y offsets since the orientation
							// may be vertical or horizontal
							_startOffsetY = event.y - tbv.getYForRow(row);
							_startOffsetX = event.x - tbv.getYForRow(row);
						}
					} 
					else {
						// when there is no interval in a row
						event.doit = false;

					}
				}
			}

			public void dragSetData(DragSourceEvent event) {
				// Provide the data of the requested type.
				if (TextTransfer.getInstance().isSupportedType(event.dataType)) {
					// unfortenatly the coordinates in the event are always 0,0
					// -> use the coordinates transferred in
					// startDrag to get the interval (a possibly is to use the
					// selection, especially for dragging
					// multiple intervals)
					if (_isRowDrag) {
						event.data = "row: "
							+ _draggedRow.getRowHeader().getLabel();
					} else {
						StringBuffer buf = new StringBuffer();
						buf.append("intervals: ");
						for (Interval interval : _origIntervals) {
							buf.append(interval);							
						}
						event.data = buf.toString();
					}
				}
			}

			public void dragFinished(DragSourceEvent event) {
				// drag finished -> if it is a move operation remove the
				// original intervals
				// from their rows
				if (event.detail == DND.DROP_MOVE) {
					System.out.println("Perform move");
					//					System.out.println(ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().getProductionPlan());
					if (_isRowDrag) {
						DefaultTimeBarModel model = (DefaultTimeBarModel) _tbv
								.getModel();
						model.remRow(_draggedRow);
					} else {
						for (Interval origInterval : _origIntervals) {
							WorkplaceTimeBarRowModel row = (WorkplaceTimeBarRowModel) _tbv
									.getModel().getRowForInterval(origInterval);
							if (row != null)
								row.remInterval(origInterval);

						}
					}
				}
				// clear the dragged data
				// and the ghosts in the viewer
				_tbv.deHighlightRow();
				_tbv.setGhostIntervals(null, null);
				_tbv.setGhostRows(null, null);
				_draggedIntervals = null;
				_draggedRow = null;
				_isRowDrag = false;
				_origIntervals = null;

				//				ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel((ProductionPlanTimeBarModel) _tbv.getModel());
				ProductionPlanPrinter.printProductionPlan(productionPlanTimeBarModel.getProductionPlan());

			}
		});

		// ////////////////////
		// Drop target

		// Allow data to be copied or moved to the drop target
		operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT | DND.DROP_NONE;
		final DropTarget target = new DropTarget(tbv, operations);

		// Receive data in Text
		final TextTransfer textTransfer = TextTransfer.getInstance();
		types = new Transfer[] { textTransfer };
		target.setTransfer(types);

		target.addDropListener(new DropTargetListener() {
			final static int DELAY = 5;
			int _verticalAutoscrollDelay = DELAY;

			public void dragEnter(DropTargetEvent event) {
			}

			public void dragOver(DropTargetEvent event) {
				// event.feedback = DND.FEEDBACK_SELECT | DND.FEEDBACK_SCROLL;

				if (_isRowDrag && _draggedRow != null) {
					// row drag
					int destY = Display.getCurrent().map(null, tbv, event.x,
							event.y).y;
					int destX = Display.getCurrent().map(null, tbv, event.x,
							event.y).x;
					tbv.setGhostRow(_draggedRow, 0);
					tbv.setGhostOrigin(destX, destY);
					TimeBarRow overRow = tbv.getRowForXY(destX, destY);
					if (overRow != null) {
						tbv.highlightRow(overRow);
					} else {
						tbv.deHighlightRow();
					}

				} 

				else if (!_isRowDrag && _draggedIntervals != null
						&& _draggedIntervals.size() > 0) {
					// map the coordinates from the event to the timebar viewer
					// widget
					int destY = Display.getCurrent().map(null, tbv, event.x,
							event.y).y;
					int destX = Display.getCurrent().map(null, tbv, event.x,
							event.y).x;

					JaretDate curDate = _tbv.dateForXY(destX, destY);
					long diffSeconds = _dragStartDate.diffSeconds(curDate);
					// correct the dragged interval bounds
					for (int i = 0; i < _draggedIntervals.size(); i++) {
						Interval orig = _origIntervals.get(i);
						Interval interval = _draggedIntervals.get(i);

						interval.setBegin(orig.getBegin().copy()
								.advanceSeconds(-diffSeconds));
						interval.setEnd(orig.getEnd().copy().advanceSeconds(
								-diffSeconds));
					}


					// check if we are over a row and highlight it
					// this will only highlight the row for the mouse pointer
					// if multiple intervals are dragged their respective rows
					// will not be highlighted
					TimeBarRow overRow = tbv.getRowForXY(destX, destY);
					if (overRow != null) {
						tbv.highlightRow(overRow);
						ProductionPlanEntryIntervalImpl ppeiiDrag;
						for(int i =0; i < _draggedIntervals.size(); i++){
							ppeiiDrag = new ProductionPlanEntryIntervalImpl(_draggedIntervals.get(i).getBegin(),_draggedIntervals.get(i).getEnd(),((ProductionPlanEntryIntervalImpl)_draggedIntervals.get(i)).getEntry());					
							List<ProductQuantity> products = ppeiiDrag.getEntry().getOperation().getOutputs();

							Workplace workplace = ((WorkplaceTimeBarRowModel) overRow).getWorkplace();
							if(workplace.areProduced(products)){
								event.detail = DND.DROP_MOVE;
							} else {
								event.detail = DND.DROP_NONE;
							}
						}
					} 

					else {
						System.out.println("Row is null x,y:" + event.x + ","
								+ event.y);
						tbv.deHighlightRow();
					}
					// tell the timebar viewer
					tbv.setGhostIntervals(_draggedIntervals, _yOffsets);
					tbv.setGhostOrigin(destX, destY);

					// vertical autoscroll
					// the delay is initialized at the top of the drop target!
					int range = 15;
					Rectangle diagramRect = _tbv.getDelegate().getDiagramRect();
					if (destY <= diagramRect.y) {
						_verticalAutoscrollDelay--;
						if (_verticalAutoscrollDelay < 0) {
							_verticalAutoscrollDelay = DELAY;
							int ridx = _tbv.getFirstRowDisplayed();
							if (ridx > 0) {
								_tbv.setFirstRowDisplayed(ridx - 1);
								_tbv.setFirstRowOffset(0);
							} else {
								_tbv.setFirstRowOffset(0);
							}
						}
					} else if (destY >= diagramRect.y + diagramRect.height
							- range) {
						_verticalAutoscrollDelay--;
						if (_verticalAutoscrollDelay < 0) {
							_verticalAutoscrollDelay = DELAY;

							TimeBarRow row = _tbv.rowForY(destY);
							int ridx = _tbv.getDelegate().getRowIndex(row);
							System.out.println("ridx " + ridx);
							if (ridx + 1 < _tbv.getDelegate().getRowCount()) {
								System.out.println("try ridx " + (ridx + 1));
								TimeBarRow nextRow = _tbv.getDelegate().getRow(
										ridx + 1);
								_tbv.setLastRow(nextRow);
								// _tbv.scrollRowToVisible(nextRow);
							}
						}
					} else {
						_verticalAutoscrollDelay = DELAY;
					}
				}
				else{
					int destY = Display.getCurrent().map(null, tbv,
							event.x, event.y).y;
					int destX = Display.getCurrent().map(null, tbv,
							event.x, event.y).x;
					TimeBarRow overRow = tbv.getRowForXY(destX, destY);

					if (overRow != null) {						
						tbv.highlightRow(overRow);
						Product product = (Product) DNDContainer.data;
						Workplace workplace = ((WorkplaceTimeBarRowModel) overRow).getWorkplace() ;						
						if(workplace.isProduced(product)){
							event.detail = DND.DROP_MOVE;
						}
						else{
							event.detail = DND.DROP_NONE;							
							tbv.deHighlightRow();
						}	

					} else {

						tbv.deHighlightRow();
					}
				}
			}

			public void dragOperationChanged(DropTargetEvent event) {
			}

			public void dragLeave(DropTargetEvent event) {
				// leaving: do not leave a row highlighted
				tbv.deHighlightRow();
				tbv.setGhostIntervals(null, null);
			}

			public void dropAccept(DropTargetEvent event) {

			}

			public void drop(DropTargetEvent event) {
				// drop operation. the drop operation places the dragged
				// intervals in the destination rows.
				// if a row can not be determined (no row under the dragged
				// interval) the interval will be discarded
				// The dragged intervals are copies of the original intervals,
				// in a real application it might be better
				// to move the originals in case of a move operation.
				// The dragged row is not modified by the drag operation. So in
				// case of a drop it is copied and
				// inserted.
				// ATTENTION: the implementation herein does only work properly
				// if no row sorters or row filters are
				// applied.
				// If that is the case it is up to the implementation to take of
				// that.
				if (textTransfer.isSupportedType(event.currentDataType)) {
					String text = (String) event.data;
					System.out.println("DROP: " + text);

					if (_isRowDrag && _draggedRow != null) {
						int destY = Display.getCurrent().map(null, tbv,
								event.x, event.y).y;
						int destX = Display.getCurrent().map(null, tbv,
								event.x, event.y).x;

						TimeBarRow overRow = tbv.getRowForXY(destX, destY);
						System.out
						.println("over row " + overRow.getRowHeader());
						if (overRow != null) {
							DefaultTimeBarRowModel row = copyDraggedRow();
							DefaultTimeBarModel model = (DefaultTimeBarModel) _tbv
							.getModel();

							int index = model.getIndexForRow(overRow);
							System.out.println("index " + index);
							model.addRow(index, row);
						}
					}

					else if (_draggedIntervals != null) {
						Interval destInterval = null;
						for (int i = 0; i < _draggedIntervals.size(); i++) {
							int destY = Display.getCurrent().map(null, tbv,
									event.x, event.y).y;
							int destX = Display.getCurrent().map(null, tbv,
									event.x, event.y).x;
							int offY = _yOffsets.get(i);
							TimeBarRow overRow = null;
							if (_tbv.getTBOrientation().equals(TimeBarViewerInterface.Orientation.HORIZONTAL)) {
								overRow = tbv.rowForY(destY + offY+ _startOffsetY);
							} else {
								overRow = tbv.rowForY(destX + offY
										+ _startOffsetX);
							}

							WorkplaceTimeBarRowModel workplaceTimeBarRowModel = (WorkplaceTimeBarRowModel) overRow;

							if (workplaceTimeBarRowModel != null) {
								ProductionPlanEntryIntervalImpl draggedInterval = new ProductionPlanEntryIntervalImpl(_draggedIntervals.get(i).getBegin(),_draggedIntervals.get(i).getEnd(),((ProductionPlanEntryIntervalImpl)_draggedIntervals.get(i)).getEntry());

								System.out.println("orig: "+_origIntervals.get(i).getBegin().toDisplayString()+" and "+draggedInterval.getEntry().getWorkplace() + " and "+_origIntervals.get(i).getEnd().toDisplayString());
								System.out.println("drag: "+_draggedIntervals.get(i).getBegin().toDisplayString()+" and "+workplaceTimeBarRowModel.getWorkplace() + " and " + _draggedIntervals.get(i).getEnd().toDisplayString());


								if (workplaceTimeBarRowModel.getWorkplace().areProduced((draggedInterval.getEntry().getOperation().getOutputs()))){
									boolean overlap = false;
									for (Interval rowInterval : workplaceTimeBarRowModel.getIntervals()){
										if (_draggedIntervals.get(i).intersects(rowInterval) && !((ProductionPlanEntryIntervalImpl) _draggedIntervals.get(i)).getEntry().equals( ((ProductionPlanEntryIntervalImpl) rowInterval).getEntry() )){
											overlap = true;
											destInterval = rowInterval;
										}			
									}	
									//									if (workplaceTimeBarRowModel.isEnoughPlaceToAdd(_draggedIntervals.get(i))) {

									if (!overlap){	

										workplaceTimeBarRowModel.removeAndSaveInterval(_origIntervals.get(i));
										workplaceTimeBarRowModel.addAndSaveInterval(_draggedIntervals.get(i));

										Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
										//											ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel((ProductionPlanTimeBarModel) _tbv.getModel());
										if (productionPlanTimeBarModel.getProductionPlan().getWarningList().size() > 0){
											try {
												getSite().getWorkbenchWindow().getActivePage().showView(WarningView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
												Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
											} catch (PartInitException e) {
												e.printStackTrace();
											}
										}
										else{
											try {
												getSite().getWorkbenchWindow().getActivePage().showView(PPEntryPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);

											} catch (PartInitException e) {
												e.printStackTrace();
											}
										}											
										ProductionPlanPrinter.printProductionPlan(productionPlanTimeBarModel.getProductionPlan());

									}
								
									else{ 
										// if there is no Interval at drop
										if(workplaceTimeBarRowModel.getIntervals(_draggedIntervals.get(i).getBegin()).size() == 0 && workplaceTimeBarRowModel.getIntervals(_draggedIntervals.get(i).getEnd()).size() == 0 ){
											List<Interval> oldIntervalInRow = workplaceTimeBarRowModel.getIntervals();			
											for (int j =0 ; j<oldIntervalInRow.size();j++){	
												if(oldIntervalInRow.get(j).getBegin().copy().compareTo(_draggedIntervals.get(i).getBegin().copy()) > 0){

													if(((ProductionPlanEntryIntervalImpl) _draggedIntervals.get(i)).getEntry().equals(((ProductionPlanEntryIntervalImpl) oldIntervalInRow.get(j)).getEntry())){
														workplaceTimeBarRowModel.removeAndSaveInterval(oldIntervalInRow.get(j));
													}
													else{
														workplaceTimeBarRowModel.moveAndSaveInterval(oldIntervalInRow.get(j), oldIntervalInRow.get(j).getBegin().copy().advanceSeconds(((ProductionPlanEntryIntervalImpl)_draggedIntervals.get(i)).getEntry().getDuration()));
//														workplaceTimeBarRowModel.moveAndSaveInterval(oldIntervalInRow.get(j), oldIntervalInRow.get(j).getBegin().copy().advanceSeconds(((ProductionPlanEntryIntervalImpl)_draggedIntervals.get(i)).getEntry().getProductionTime()));
													}
												}
											}
											workplaceTimeBarRowModel.addAndSaveInterval(_draggedIntervals.get(i));				
										}
										// there is an interval at drop (overlapping)
										else{
											boolean dragFromLeft = false;
											
											// drag from left
											if (draggedInterval.getBegin().compareTo(destInterval.getBegin()) < 0 && draggedInterval.getEnd().compareTo(destInterval.getBegin()) > 0){
												dragFromLeft = true;
											}
											else if (draggedInterval.getBegin().compareTo(destInterval.getEnd()) < 0 && draggedInterval.getEnd().compareTo(destInterval.getEnd()) > 0){
												dragFromLeft = false;
											}
											else if (draggedInterval.getBegin().compareTo(destInterval.getBegin()) > 0 && draggedInterval.getEnd().compareTo(destInterval.getEnd()) < 0){
												dragFromLeft = false;
											}
											
											JaretDate dropTime = tbv.dateForXY(destX, destY);	
											ProductionPlanEntryIntervalImpl oldIntervalAtDrop = null;
											//Difference from dropTime to the begin of the drag interval, so when the begin of the interval intersect with another interval, it will do something
											double differenceFromBegin = dropTime.diffSeconds(_draggedIntervals.get(i).getBegin());											
											JaretDate dragIntervalBegin = dropTime.copy().backSeconds(differenceFromBegin);	
											double differenceFromEnd = Math.abs(dropTime.diffSeconds(_draggedIntervals.get(i).getEnd()));
											JaretDate dragIntervalEnd = dropTime.copy().advanceSeconds(differenceFromEnd);	
											if(!dragFromLeft){		
												oldIntervalAtDrop = (ProductionPlanEntryIntervalImpl) workplaceTimeBarRowModel.getIntervals(dragIntervalBegin).get(0);
																							}
											else if(dragFromLeft){	
												oldIntervalAtDrop = (ProductionPlanEntryIntervalImpl) workplaceTimeBarRowModel.getIntervals(dragIntervalEnd).get(0);
											}

											List<ProductQuantity> draggedOutputs = draggedInterval.getEntry().getOperation().getOutputs();
											List<ProductQuantity> outputsAtDropLocation = oldIntervalAtDrop.getEntry().getOperation().getOutputs();
											
											// the same product added to the bar, the bar length should be extended and all of following intervals should be shifted //
											// first check the lengts of both lists
											if(draggedOutputs.size() == outputsAtDropLocation.size()){
												boolean productsEqual = false;
												
												// check if all products from the first list have a counterpart in the other list
												for(ProductQuantity p:draggedOutputs){
													if(outputsAtDropLocation.contains(p)){
														productsEqual = true;
													}
													else {
														productsEqual=false;
														break;
													}
												}
												
												if(productsEqual){ // in case the lists are isomorphic
													double retoolingTime = draggedInterval.getEntry().getRetoolingTime();
													ProductionPlanEntry newIntervalEntry = oldIntervalAtDrop.getEntry();
													ProductionPlanEntryIntervalImpl newInterval = new ProductionPlanEntryIntervalImpl(newIntervalEntry);
													newInterval.setBegin(oldIntervalAtDrop.getBegin());
													newInterval.setEnd(oldIntervalAtDrop.getBegin().copy().advanceSeconds(oldIntervalAtDrop.getSeconds()+draggedInterval.getSeconds()-retoolingTime));
													((ProductionPlanEntryIntervalImpl)newInterval).calculateCost();
													List<Interval> oldIntervalInRow = workplaceTimeBarRowModel.getIntervals();			
													for (int j =0 ; j<oldIntervalInRow.size();j++){		
														if(oldIntervalInRow.get(j).getBegin().copy().compareTo(oldIntervalAtDrop.getBegin().copy()) > 0){
															ProductionPlanEntryIntervalImpl next = (ProductionPlanEntryIntervalImpl) workplaceTimeBarRowModel.getNextInterval(oldIntervalAtDrop);
															if (next != null && next.getBegin().diffSeconds(oldIntervalAtDrop.getEnd()) < draggedInterval.getSeconds() ){
																workplaceTimeBarRowModel.moveAndSaveInterval(oldIntervalInRow.get(j), oldIntervalInRow.get(j).getBegin().copy().advanceSeconds(((ProductionPlanEntryIntervalImpl)newInterval).getEntry().getDuration()));
															}
														}
													}												
													newIntervalEntry.setDuration(oldIntervalAtDrop.getSeconds()+draggedInterval.getSeconds()-retoolingTime);
													workplaceTimeBarRowModel.removeAndSaveInterval(oldIntervalAtDrop);
													workplaceTimeBarRowModel.removeAndSaveInterval(_origIntervals.get(i));
													workplaceTimeBarRowModel.removeAndSaveInterval(draggedInterval);
													workplaceTimeBarRowModel.addAndSaveInterval(newInterval);	
												}

												else{ // different product added to the bar, the bar should be fragmented
													if(!dragFromLeft){
														ProductionPlanEntryIntervalImpl newIntervalAtDrop = (ProductionPlanEntryIntervalImpl) workplaceTimeBarRowModel.getIntervals(dragIntervalBegin).get(0);											
														double oldIntervalAtDropDuration = workplaceTimeBarRowModel.getIntervals(dragIntervalBegin).get(0).getSeconds();
														double newIntervalAtDropDuration = dragIntervalBegin.diffSeconds(newIntervalAtDrop.getBegin());
														double rest = Math.abs(oldIntervalAtDropDuration-newIntervalAtDropDuration);
														double retoolingTime = workplaceTimeBarRowModel.getWorkplace().getProducingWorkplaceState(outputsAtDropLocation.get(0).getProduct()).get(0).getRetoolingTime();
														newIntervalAtDrop.setBegin(oldIntervalAtDrop.getBegin().copy());
														newIntervalAtDrop.setEnd(dragIntervalBegin);	
														draggedInterval.setBegin(dragIntervalBegin);		
														draggedInterval.setEnd(dragIntervalBegin.copy().advanceSeconds(draggedInterval.getEntry().getDuration()));
														ProductionPlanEntryIntervalImpl newIntervalAtDrop2 = new ProductionPlanEntryIntervalImpl(_draggedIntervals.get(i).getEnd(), _draggedIntervals.get(i).getEnd().copy().advanceSeconds(rest+retoolingTime), newIntervalAtDrop.getEntry().clone());
														List<Interval> oldIntervalInRow = workplaceTimeBarRowModel.getIntervals();			
														for (int j =0 ; j<oldIntervalInRow.size();j++){					
															if(oldIntervalInRow.get(j).getBegin().copy().compareTo(oldIntervalAtDrop.getBegin().copy()) > 0){
																ProductionPlanEntryIntervalImpl next = (ProductionPlanEntryIntervalImpl) workplaceTimeBarRowModel.getNextInterval(draggedInterval);
																// dragIntervalBegin.copy().advanceSeconds(rest) = oldIntervalAtDrop.getEnd()
																if (next != null && next.getBegin().diffSeconds(dragIntervalBegin.copy().advanceSeconds(rest+retoolingTime)) < draggedInterval.getSeconds() ){
																	workplaceTimeBarRowModel.moveAndSaveInterval(oldIntervalInRow.get(j), oldIntervalInRow.get(j).getBegin().copy().advanceSeconds(((ProductionPlanEntryIntervalImpl)draggedInterval).getEntry().getDuration()));
																}
															}
														}
														newIntervalAtDrop.getEntry().setDuration(newIntervalAtDrop.getSeconds());	
														newIntervalAtDrop2.getEntry().setDuration(rest+retoolingTime);								
														workplaceTimeBarRowModel.removeAndSaveInterval(oldIntervalAtDrop);											
														workplaceTimeBarRowModel.removeAndSaveInterval(_origIntervals.get(i));
														workplaceTimeBarRowModel.addAndSaveInterval(newIntervalAtDrop);
														workplaceTimeBarRowModel.addAndSaveInterval(_draggedIntervals.get(i));
														workplaceTimeBarRowModel.addAndSaveInterval(newIntervalAtDrop2);
													}
													else if(dragFromLeft){
														List<Interval> oldIntervalInRow = workplaceTimeBarRowModel.getIntervals();			
														for (int j =0 ; j<oldIntervalInRow.size();j++){	
															if(oldIntervalInRow.get(j).getEnd().copy().compareTo(_draggedIntervals.get(i).getEnd().copy()) > 0){
																workplaceTimeBarRowModel.moveAndSaveInterval(oldIntervalInRow.get(j), oldIntervalInRow.get(j).getBegin().copy().advanceSeconds(((ProductionPlanEntryIntervalImpl)_draggedIntervals.get(i)).getEntry().getDuration()));
															}
														}
														for(int k=0 ;k < oldIntervalInRow.size();k++){
															if(((ProductionPlanEntryIntervalImpl)oldIntervalInRow.get(k)).getEntry().equals(((ProductionPlanEntryIntervalImpl) _draggedIntervals.get(i)).getEntry())){
																workplaceTimeBarRowModel.removeAndSaveInterval(oldIntervalInRow.get(k));
															}
														}
														workplaceTimeBarRowModel.addAndSaveInterval(_draggedIntervals.get(i));				
													}

												}
											}

											
//											// the same product added to the bar, the bar length should be extended and all of following intervals should be shifted
//											if(draggedIntervalProduct.equals(IntervalAtDropProduct)){
//												ProductionPlanEntry newIntervalEntry = oldIntervalAtDrop.getEntry();
//												ProductionPlanEntryIntervalImpl newInterval = new ProductionPlanEntryIntervalImpl(newIntervalEntry);
//												newInterval.setBegin(oldIntervalAtDrop.getBegin());
//												newInterval.setEnd(oldIntervalAtDrop.getBegin().copy().advanceSeconds(oldIntervalAtDrop.getSeconds()+draggedInterval.getSeconds()));							
//												List<Interval> oldIntervalInRow = workplaceTimeBarRowModel.getIntervals();			
//												for (int j =0 ; j<oldIntervalInRow.size();j++){		
//													if(oldIntervalInRow.get(j).getBegin().copy().compareTo(oldIntervalAtDrop.getBegin().copy()) > 0){
//														ProductionPlanEntryIntervalImpl next = (ProductionPlanEntryIntervalImpl) workplaceTimeBarRowModel.getNextInterval(oldIntervalAtDrop);
//														if (next != null && next.getBegin().diffSeconds(oldIntervalAtDrop.getEnd()) < draggedInterval.getSeconds() ){
//															workplaceTimeBarRowModel.moveAndSaveInterval(oldIntervalInRow.get(j), oldIntervalInRow.get(j).getBegin().copy().advanceSeconds(((ProductionPlanEntryIntervalImpl)newInterval).getEntry().getDuration()));
//														}
//													}
//												}												
//												newIntervalEntry.setDuration(oldIntervalAtDrop.getSeconds()+draggedInterval.getSeconds());
//												workplaceTimeBarRowModel.removeAndSaveInterval(oldIntervalAtDrop);
//												workplaceTimeBarRowModel.removeAndSaveInterval(_origIntervals.get(i));
//												workplaceTimeBarRowModel.removeAndSaveInterval(draggedInterval);
//												workplaceTimeBarRowModel.addAndSaveInterval(newInterval);	
//											}
//
//											// different product added to the bar, the bar should be fragmented
//											else{
//
//												ProductionPlanEntryIntervalImpl newIntervalAtDrop = (ProductionPlanEntryIntervalImpl) workplaceTimeBarRowModel.getIntervals(dropTime).get(0);											
//												double oldIntervalAtDropDuration = workplaceTimeBarRowModel.getIntervals(dropTime).get(0).getSeconds();
//												double newIntervalAtDropDuration = dragIntervalBegin.diffSeconds(newIntervalAtDrop.getBegin());
//												double rest = Math.abs(oldIntervalAtDropDuration-newIntervalAtDropDuration);
//												newIntervalAtDrop.setBegin(oldIntervalAtDrop.getBegin().copy());
//												newIntervalAtDrop.setEnd(dragIntervalBegin);	
//												draggedInterval.setBegin(dragIntervalBegin);		
//												draggedInterval.setEnd(dragIntervalBegin.copy().advanceSeconds(draggedInterval.getEntry().getDuration()));
//												ProductionPlanEntryIntervalImpl newIntervalAtDrop2 = new ProductionPlanEntryIntervalImpl(_draggedIntervals.get(i).getEnd(), _draggedIntervals.get(i).getEnd().copy().advanceSeconds(rest), newIntervalAtDrop.getEntry().clone());
//												List<Interval> oldIntervalInRow = workplaceTimeBarRowModel.getIntervals();			
//												for (int j =0 ; j<oldIntervalInRow.size();j++){					
//													if(oldIntervalInRow.get(j).getBegin().copy().compareTo(oldIntervalAtDrop.getBegin().copy()) > 0){
//														ProductionPlanEntryIntervalImpl next = (ProductionPlanEntryIntervalImpl) workplaceTimeBarRowModel.getNextInterval(draggedInterval);
//														// dragIntervalBegin.copy().advanceSeconds(rest) = oldIntervalAtDrop.getEnd()
//														if (next != null && next.getBegin().diffSeconds(dragIntervalBegin.copy().advanceSeconds(rest)) < draggedInterval.getSeconds() ){
//															workplaceTimeBarRowModel.moveAndSaveInterval(oldIntervalInRow.get(j), oldIntervalInRow.get(j).getBegin().copy().advanceSeconds(((ProductionPlanEntryIntervalImpl)draggedInterval).getEntry().getDuration()));
//														}
//													}
//												}
//												newIntervalAtDrop.getEntry().setDuration(newIntervalAtDrop.getSeconds());	
//												newIntervalAtDrop2.getEntry().setDuration(rest);								
//												workplaceTimeBarRowModel.removeAndSaveInterval(oldIntervalAtDrop);											
//												workplaceTimeBarRowModel.removeAndSaveInterval(_origIntervals.get(i));
//												workplaceTimeBarRowModel.addAndSaveInterval(newIntervalAtDrop);
//												workplaceTimeBarRowModel.addAndSaveInterval(_draggedIntervals.get(i));
//												workplaceTimeBarRowModel.addAndSaveInterval(newIntervalAtDrop2);
//											}
											draggedInterval.getEntry().setWorkplace(workplaceTimeBarRowModel.getWorkplace());

											Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
											//											ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel((ProductionPlanTimeBarModel) _tbv.getModel());

											if (productionPlanTimeBarModel.getProductionPlan().getWarningList().size() > 0){
												try {
													getSite().getWorkbenchWindow().getActivePage().showView(WarningView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
													Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
												} catch (PartInitException e) {
													e.printStackTrace();
												}
											}
											else{
												try {
													getSite().getWorkbenchWindow().getActivePage().showView(PPEntryPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);

												} catch (PartInitException e) {
													e.printStackTrace();
												}
											}
											ProductionPlanPrinter.printProductionPlan(productionPlanTimeBarModel.getProductionPlan());
										}
									}
								}
								//								}
							}
						}	

					}
					// for dragging from ProductTreeView
					else {

						int destY = Display.getCurrent().map(null,tbv,event.x, event.y).y;
						int destX = Display.getCurrent().map(null,tbv,event.x, event.y).x;
						TimeBarRow overRow = tbv.getRowForXY(destX, destY);
						JaretDate dropJaretDate = tbv.dateForXY(destX, destY);						
						if (overRow != null) {
							tbv.highlightRow(overRow);
						} else {
							System.out.println("Row is null x,y:" + event.x+ "," + event.y);
							tbv.deHighlightRow();
						}

						Product product = (Product) DNDContainer.data;						
						Workplace workplace = ((WorkplaceTimeBarRowModel) overRow).getWorkplace() ;
						List<WorkplaceState> states = workplace.getProducingWorkplaceState(product);
						int amount = 0;
						for(WorkplaceState ws : states){ // change after refactoring TODO: does it make sense
							
							Operation leastComplexOperation = null;
							List<Operation> appliedOperations = ws.getOperation(product);
							if(appliedOperations.size()>1){
								leastComplexOperation = appliedOperations.get(0);
								// searching for the operation with the least number of outputs
								Iterator<Operation> it = appliedOperations.iterator();
								while(it.hasNext()){
									Operation nextOperation = it.next();
									leastComplexOperation = leastComplexOperation.getOutputs().size()>nextOperation.getOutputs().size()?nextOperation:leastComplexOperation;
								}
							}
							else{
								leastComplexOperation = appliedOperations.get(0);
							}
							for(ProductQuantity pq: leastComplexOperation.getOutputs()){
								if(pq.getProduct().equals(product)){
									amount = leastComplexOperation.getLotSize(); // lot size only for one product?
								}
							}
						}

						

						((WorkplaceTimeBarRowModel) overRow).addAndSaveInterval2(workplace, product,amount, dropJaretDate.getDate());
						Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
						ProductionPlanProject.getCurrentProductionPlanProject().setCurrentProductionPlanTimeBarModel((ProductionPlanTimeBarModel) _tbv.getModel());
						try {
							getSite().getWorkbenchWindow().getActivePage().showView(ProductionPlanPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
						} catch (PartInitException e) {
							e.printStackTrace();
						}

						if (productionPlanTimeBarModel.getProductionPlan().getWarningList().size() > 0){
							try {
								getSite().getWorkbenchWindow().getActivePage().showView(WarningView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
							
								Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);

							} catch (PartInitException e) {
								e.printStackTrace();
							}
						}
						else{
							try {
								getSite().getWorkbenchWindow().getActivePage().showView(PPEntryPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);

							} catch (PartInitException e) {
								e.printStackTrace();
							}
						}
						ProductionPlanPrinter.printProductionPlan(productionPlanTimeBarModel.getProductionPlan());
					}
				}
			}

			private DefaultTimeBarRowModel copyDraggedRow() {
				DefaultRowHeader header = (DefaultRowHeader) _draggedRow
				.getRowHeader();
				DefaultTimeBarRowModel row = new DefaultTimeBarRowModel(
						new DefaultRowHeader(header.getLabel() + "(copy)"));
				for (Interval interval : _draggedRow.getIntervals()) {
					row.addInterval(new IntervalImpl(
							interval.getBegin().copy(), interval.getEnd()
							.copy()));
				}
				return row;
			}
		});

		// Dispose listener on parent of timebar viewer to dispose the
		// dragsource and dragtarget BEFORE the timebar
		// viewer
		// this prevents an exception beeing thrown by SWT
		parent.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				source.dispose();
				target.dispose();

			}
		});

	}

	private void registerChangeListener(TimeBarViewer tbv) {
		tbv.addTimeBarChangeListener(new ITimeBarChangeListener() {

			public void intervalChangeCancelled(TimeBarRow row,
					Interval interval) {
				System.out.println("CHANGE CANCELLED " + row + " " + interval);
			}

			public void intervalChangeStarted(TimeBarRow row, Interval interval) {
				System.out.println("CHANGE STARTED " + row + " " + interval);
			}

			public void intervalChanged(TimeBarRow row, Interval interval,
					JaretDate oldBegin, JaretDate oldEnd) {
				System.out.println("CHANGE DONE " + row + " " + interval);
			}

			public void intervalIntermediateChange(TimeBarRow row,
					Interval interval, JaretDate oldBegin, JaretDate oldEnd) {
				System.out.println("CHANGE INTERMEDIATE " + row + " "
						+ interval);
			}

			public void markerDragStarted(TimeBarMarker marker) {
				System.out.println("Marker drag started " + marker);
			}

			public void markerDragStopped(TimeBarMarker marker) {
				System.out.println("Marker drag stopped " + marker);
			}

		});

	}

	@Override
	public void setFocus() {

	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if(part instanceof ProductTreeView){

			Product product = null;
			if (((ITreeSelection) selection).getFirstElement() instanceof Product)
				product = (Product) ((ITreeSelection) selection).getFirstElement();
			else if (((ITreeSelection) selection).getFirstElement() instanceof ProductQuantity) 
				product = ((ProductQuantity) ((ITreeSelection) selection).getFirstElement()).getProduct();

			if (product != null) {
				// Highlight multiple rows
				List<Workplace> workplaces = EntityList.getProducingWorkplaceList(product);
				List<TimeBarRow> wpTimeBarRows = new ArrayList<TimeBarRow>();
				for(Workplace wp : workplaces) {
					wpTimeBarRows.add(productionPlanTimeBarModel.getTimeBarRow(wp));
				}
				this.get_tbv().highlightRows(wpTimeBarRows);				
			}
			//TODO: if an operation is selected, the corresponding workplace should be highlighted
		}
	}


	public void setInput(ProductionPlanTimeBarModel productionPlanTimeBarModel){
		_tbv.setModel(productionPlanTimeBarModel);
		_tbv.update();
		
	}
	public void setStartDate(Date startDate){		
		_tbv.setMinDate(new JaretDate(startDate));
		_tbv.update();
	}
	
	@Override
	public void update(UpdateType type) {
		switch (type) {
		case PRODUCTIONPLAN_CALCULATED:
			//TODO: the model is still set with original time bar model
//			_tbv.setModel(ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel());
//			this.setInput(ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel());
//			_tbv.update();
			
			break;

		default:
			break;
		}

	}



}

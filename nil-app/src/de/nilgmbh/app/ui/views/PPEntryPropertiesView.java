package de.nilgmbh.app.ui.views;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.FactoryResource;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.util.Numbers;
import de.nilgmbh.app.internalmodel.OperationProduct;
import de.nilgmbh.app.internalmodel.PropertyRow;
import de.nilgmbh.app.internalmodel.WorkplaceStateProduct;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanEntryIntervalImpl;


/**
 * @author Ardhi
 *
 */

public class PPEntryPropertiesView extends ViewPart implements ISelectionListener {
	public static final String ID = PPEntryPropertiesView.class.getName();
	
	private TableViewer tableViewer;
	@Override
	public void createPartControl(Composite parent) {
		createViewer(parent);
//		setPartName(getString("PropertiesView.tabTitle"));
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);

	}
	
	private void createViewer(Composite parent) {
		tableViewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL| SWT.V_SCROLL | SWT.FULL_SELECTION);
		createColumns(tableViewer);
		Table table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);		
		tableViewer.setContentProvider(new ArrayContentProvider());
//		setInputData(productionPlanEntryIntervalImpl);

	}
	
	private void createColumns(final TableViewer viewer) {
		String[] titles = {getString("PropertiesView.name"), getString("PropertiesView.value")};
		int [] bounds = {100,250};
		
		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0]);	
		col.setLabelProvider(new ColumnLabelProvider(){
			public String getText(Object element) {
				PropertyRow propertyRow =  (PropertyRow) element;
				return (String) propertyRow.getName();
			}
		});
		
		col = createTableViewerColumn(titles[1], bounds[1]);
		col.setLabelProvider(new ColumnLabelProvider(){
			public String getText(Object element) {
				PropertyRow propertyRow =  (PropertyRow) element;
				return (String) propertyRow.getValue();
			}
		});		
	}
	
	private TableViewerColumn createTableViewerColumn(String title, int bound) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer,SWT.NONE);
		viewerColumn.getColumn().setText(title);
		viewerColumn.getColumn().setWidth(bound);
		viewerColumn.getColumn().setResizable(true);
		viewerColumn.getColumn().setMoveable(true);
		return viewerColumn;
	}

	
	public void setInput(ProductionPlanEntryIntervalImpl entryInterval) {
		List <PropertyRow> pr = new ArrayList<PropertyRow>();
		if (entryInterval == null) {
			pr.add(new PropertyRow(getString("PropertiesView.begin"),""));
			pr.add(new PropertyRow(getString("PropertiesView.end"), ""));
			pr.add(new PropertyRow(getString("PropertiesView.duration"), ""));
			pr.add(new PropertyRow(getString("PropertiesView.productName"),  ""));
			pr.add(new PropertyRow(getString("PropertiesView.quantity"),""));
			pr.add(new PropertyRow(getString("PropertiesView.productionTime"),""));
			pr.add(new PropertyRow(getString("PropertiesView.retoolingTime"),""));
			pr.add(new PropertyRow(getString("PropertiesView.cost"),""));
			pr.add(new PropertyRow("K�uflich", ""));
		}
		else {		
			int quantity = (int) Math.round(entryInterval.getEntry().getProductionTime()/(entryInterval.getEntry().getOperation().getTime()));			
			pr.add(new PropertyRow(getString("PropertiesView.begin"),entryInterval.getBegin().toDisplayString()));
			pr.add(new PropertyRow(getString("PropertiesView.end"), entryInterval.getEnd().toDisplayString()));
			pr.add(new PropertyRow(getString("PropertiesView.duration"), Numbers.formatMinutes((entryInterval.getEntry().getDuration()/60))));			
			pr.add(new PropertyRow(getString("PropertiesView.productName"), entryInterval.getEntry().getOperation().getOutputs().get(0).getProduct().getName()));
			pr.add(new PropertyRow(getString("PropertiesView.quantity"), new Integer(quantity).toString()));
			pr.add(new PropertyRow(getString("PropertiesView.productionTime"), new Double(entryInterval.getEntry().getProductionTime())==null? Numbers.formatMinutes(0): Numbers.formatMinutes(((entryInterval.getEntry().getDuration()-entryInterval.getEntry().getRetoolingTime())/60))));
			pr.add(new PropertyRow(getString("PropertiesView.retoolingTime"), new Double(entryInterval.getEntry().getRetoolingTime())==null? Numbers.formatMinutes(0): Numbers.formatMinutes((entryInterval.getEntry().getRetoolingTime()/60))));
			pr.add(new PropertyRow(getString("PropertiesView.cost"), Numbers.formatMoney(entryInterval.getEntry().getCost())));
			for (Product p : entryInterval.getEntry().getOperation().getOutputProducts() ){			
				pr.add(new PropertyRow("K�uflich", p.isPurchasable()? "Ja" : "Nein"));
			}
		}
		
		
		tableViewer.setInput(pr);
	}
	
	public void setInput(Product product) {
		
		List <PropertyRow> pr = new ArrayList<PropertyRow>();		
		if (product == null) {	
			pr.add(new PropertyRow(getString("PropertiesView.productName"),  ""));
			pr.add(new PropertyRow(getString("PropertiesView.workplaceName"),""));
			pr.add(new PropertyRow(getString("PropertiesView.cost"),""));
			pr.add(new PropertyRow("K�uflich", ""));
		}
		else {				
			List<Workplace > workplaceList = EntityList.getProducingWorkplaceList(product);
			pr.add(new PropertyRow(getString("PropertiesView.productName"),product.getName()));
			List <String> workplaceNames = new ArrayList<String>();
			for (int i = 0; i<workplaceList.size(); i++){
				workplaceNames.add(workplaceList.get(i).getName());
			}
			pr.add(new PropertyRow(getString("PropertiesView.workplaceName"), workplaceNames.toString()));
			pr.add(new PropertyRow(getString("PropertiesView.cost"), Numbers.formatMoney(product.isPurchasable() ? product.getPurchasePrice() : product.getValue())));			
			pr.add(new PropertyRow("K�uflich", product.isPurchasable()? "Ja" : "Nein"));
			
		}		
		tableViewer.setInput(pr);
	}
	
	public void setInput(WorkplaceStateProduct wsp) {
		
		List <PropertyRow> pr = new ArrayList<PropertyRow>();		
		if (wsp == null) {	
			pr.add(new PropertyRow(getString("PropertiesView.productName"),  ""));
			pr.add(new PropertyRow(getString("PropertiesView.workplaceName"),""));
			pr.add(new PropertyRow(getString("PropertiesView.retoolingTime"),""));
			pr.add(new PropertyRow(getString("PropertiesView.retoolingCost"),""));
			pr.add(new PropertyRow(getString("PropertiesView.quantity"),""));
			pr.add(new PropertyRow(getString("PropertiesView.cost"),""));
		}
		else {
			pr.add(new PropertyRow(getString("PropertiesView.productName"),  wsp.getProduct().getName()));
			pr.add(new PropertyRow(getString("PropertiesView.workplaceName"),wsp.getWorkplaceState().getWorkplace().getName()));
			pr.add(new PropertyRow(getString("PropertiesView.retoolingTime"), Numbers.formatSeconds(wsp.getWorkplaceState().getRetoolingTime())));
			pr.add(new PropertyRow(getString("PropertiesView.retoolingCost"), Numbers.formatMoney(wsp.getWorkplaceState().getRetoolingCost())));
			// must be tested
			for(Operation op : wsp.getProduct().getOperations()){				
				for (int i=0; i < op.getCosts().size(); i++){
					pr.add(new PropertyRow(((FactoryResource) wsp.getOperation()).getCosts().get(i).getName(), Numbers.formatMoney(((FactoryResource) wsp.getOperation()).getCosts().get(i).getValue())));
				}
			}

		}		
		tableViewer.setInput(pr);
	}
	public void setInput(OperationProduct op) {
		
		List <PropertyRow> pr = new ArrayList<PropertyRow>();		
		if (op == null) {	
			pr.add(new PropertyRow(getString("PropertiesView.productName"),  ""));
			pr.add(new PropertyRow(getString("PropertiesView.workplaceName"),""));
			pr.add(new PropertyRow(getString("PropertiesView.retoolingTime"),""));
			pr.add(new PropertyRow(getString("PropertiesView.retoolingCost"),""));
			pr.add(new PropertyRow(getString("PropertiesView.quantity"),""));
			pr.add(new PropertyRow(getString("PropertiesView.cost"),""));
		}
		else {
			for (int i=0; i< op.getOperation().getInputs().size(); i++){
				pr.add(new PropertyRow(getString("PropertiesView.input")+(i+1),  op.getOperation().getInputs().get(i).getProduct().getName()));
			}
			for (int i=0; i< op.getOperation().getOutputs().size(); i++){
				pr.add(new PropertyRow(getString("PropertiesView.output")+(i+1),  op.getOperation().getOutputs().get(i).getProduct().getName()));
			}			
			pr.add(new PropertyRow(getString("PropertiesView.time"),Numbers.formatSeconds(op.getOperation().getTime())));
			pr.add(new PropertyRow(getString("PropertiesView.lotSize"), Numbers.formatPieces(op.getOperation().getLotSize())));
			pr.add(new PropertyRow(getString("PropertiesView.mmf"), Numbers.format(((InternalOperation) op.getOperation()).getMmf())));
			pr.add(new PropertyRow(getString("PropertiesView.componentFactor"), Numbers.format(((InternalOperation) op.getOperation()).getComponentFactor())));

		}		
		tableViewer.setInput(pr);
	}
	
	@Override
	public void setFocus() {
//		tableViewer.getControl().setFocus();	
		}
	
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		
		
		
		if (part instanceof FeinScheduleView ){
			
//				try {
//					getSite().getWorkbenchWindow().getActivePage().showView(PropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
//				} catch (PartInitException e) {
//					e.printStackTrace();
//				}
			
			ProductionPlanEntryIntervalImpl ppeii = (ProductionPlanEntryIntervalImpl) ((IStructuredSelection) selection).getFirstElement();
			setInput(ppeii);	
		}		
		else if(part instanceof ProductTreeView){
			if (((ITreeSelection) selection).getFirstElement() instanceof Product){
				setInput((Product) ((ITreeSelection) selection).getFirstElement());
			}
			else if (((ITreeSelection) selection).getFirstElement() instanceof ProductQuantity) {
				setInput(((ProductQuantity) ((ITreeSelection) selection).getFirstElement()).getProduct());
			}
			else if (((ITreeSelection) selection).getFirstElement() instanceof WorkplaceStateProduct){
				setInput((WorkplaceStateProduct) ((ITreeSelection) selection).getFirstElement());
			}
			else if (((ITreeSelection) selection).getFirstElement() instanceof OperationProduct){
				setInput((OperationProduct) ((ITreeSelection) selection).getFirstElement());
			}
			
		}
	}
}



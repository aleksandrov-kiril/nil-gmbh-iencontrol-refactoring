package de.nilgmbh.app.ui.views;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.internalmodel.OperationProduct;
import de.nilgmbh.app.internalmodel.WorkplaceStateProduct;
import de.nilgmbh.app.ui.common.dnd.TreeDragListener;
import de.nilgmbh.app.ui.dialogs.InitialWizard;
import de.nilgmbh.app.ui.dialogs.NewOperationWizard;
import de.nilgmbh.app.updatemechanism.UpdateListener;
import de.nilgmbh.app.updatemechanism.UpdateType;

/**
 * @author Ardhi
 * 
 */

public class ProductTreeView extends ViewPart implements UpdateListener {
	public static final String ID = ProductTreeView.class.getName();
	public TreeViewer tree;

	public ProductTreeView() {
		Activator.getDefault().getImageRegistry().get("productEnd");
		Activator.getDefault().getImageRegistry().get("productIntermediate");
	}

	@Override
	public void createPartControl(final Composite parent) {

		tree = new TreeViewer(parent, SWT.BORDER);
		createColumns(tree);
		tree.setContentProvider(new ProductTreeContentProvider());
		tree.setLabelProvider(new ProductTreeLabelProvider(tree));
		tree.setUseHashlookup(true);

		int operations = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[] { TextTransfer.getInstance() };
		tree.addDragSupport(operations, transferTypes, new TreeDragListener(tree));
		tree.getTree().addMenuDetectListener(new MenuDetectListener() {

			@Override
			public void menuDetected(MenuDetectEvent e) {
				if (tree.getSelection() != null) {
					Object selectedObject = ((IStructuredSelection) tree.getSelection()).getFirstElement();
					Product selectedProduct = null;
					if (selectedObject instanceof Product || selectedObject instanceof ProductQuantity) {
						if (selectedObject instanceof ProductQuantity) {
							selectedProduct = ((ProductQuantity) selectedObject).getProduct();
						} else if (selectedObject instanceof Product) {
							selectedProduct = (Product) selectedObject;
						}
						final Product finalSelectedProduct = selectedProduct;
						Menu menu = new Menu(parent);
						MenuItem initialWizardItem = new MenuItem(menu, SWT.PUSH);
						initialWizardItem.setText(getString("ProductTreeView.newEntry"));
						initialWizardItem.addListener(SWT.Selection, new Listener() {

							@Override
							public void handleEvent(Event event) {
								InitialWizard initialWizard = new InitialWizard(finalSelectedProduct, false);
								WizardDialog dialog = new WizardDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(), initialWizard);
								dialog.open();
							}
						});
						MenuItem newWizard  = new MenuItem(menu, SWT.PUSH);
						newWizard.setText(getString("ProductTreeView.newOperation"));
						newWizard.addListener(SWT.Selection,new Listener() {

							@Override
							public void handleEvent(Event event) {
								NewOperationWizard newOperationWizard = new NewOperationWizard(finalSelectedProduct);
								WizardDialog dialog = new WizardDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(), newOperationWizard);				
								dialog.open();
							}
						});

						menu.setLocation(e.x, e.y);
						menu.setVisible(true);
						while (!menu.isDisposed() && menu.isVisible()) {
							if (!parent.getShell().getDisplay().readAndDispatch())
								parent.getShell().getDisplay().sleep();
						}
						menu.dispose();
					}
				}
			}
		});



		// for registering object to be listened by another View
		getSite().setSelectionProvider(tree);
		Activator.getDefault().registerUpdateListener(this);
	}
	//	 initDragAndDrop(tree);


	// protected void initDragAndDrop(final TreeViewer viewer) {
	// int operations = DND.DROP_COPY;
	// Transfer[] transferTypes = new Transfer[] {TemplateTransfer
	// .getInstance() };
	// DragSourceListener listener = new DragSourceAdapter() {
	// @Override
	// public void dragSetData(final DragSourceEvent event) {
	// if (EditorInputTransfer.getInstance().isSupportedType(
	// event.dataType)) {
	// event.data = ((IStructuredSelection) viewer.getSelection())
	// .getFirstElement();
	// }
	// }
	//
	// @SuppressWarnings("synthetic-access")
	// @Override
	// public void dragStart(DragSourceEvent event) {
	// DragHelper.data = ((IStructuredSelection) viewer.getSelection())
	// .getFirstElement();
	// DragHelper.sameObject = false;
	// }
	// };
	// viewer.addDragSupport(operations, transferTypes, listener);
	// }

	@Override
	public void setFocus() {

	}

	private void createInput() {
		tree.setInput(EntityList.getDisposableProductList(Project.getCurrentProject().getOriginalFactory()));
	}

	private void createColumns(TreeViewer viewer){
		TreeViewerColumn column = new TreeViewerColumn(viewer, SWT.LEFT);
		column.getColumn().setText(getString("ProductTreeView.product"));
		column.getColumn().setWidth(200);
		column.getColumn().setResizable(true);
		column.getColumn().setMoveable(false);

		column = new TreeViewerColumn(viewer, SWT.LEFT);
		column.getColumn().setText(getString("ProductTreeView.quantity"));
		column.getColumn().setWidth(100);
		column.getColumn().setResizable(true);
		column.getColumn().setMoveable(false);
		//		column.setEditingSupport(new PropEditingSupport(viewer));

		Tree tree = viewer.getTree();
		tree.setHeaderVisible(true);

		tree.setLinesVisible(true);

	}

	private class ProductTreeContentProvider implements ITreeContentProvider {

		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		@Override
		public Object[] getElements(Object inputElement) {
			if (inputElement instanceof List<?>) {
				return ((List<?>) inputElement).toArray();
			} else {
				return new Object[] {};
			}
		}

		@Override
		public Object[] getChildren(Object parentElement) {
			// 4 cases: Product, WorkplaceStateProduct, Operation, ProductQuantity

			// case Product
			if (parentElement instanceof Product){
				Product product = (Product) parentElement;
				List <WorkplaceStateProduct> workplaceStates = new ArrayList<WorkplaceStateProduct>();

				for(Workplace producingWorkplace: EntityList.getProducingWorkplaceList(product)){
					for(WorkplaceState ws: producingWorkplace.getProducingWorkplaceState(product)){
						workplaceStates.add(new WorkplaceStateProduct(product, ws));
					}
				}
				return workplaceStates.toArray();
			}

			// case WorkplaceStateProduct
			else if(parentElement instanceof WorkplaceStateProduct){
				WorkplaceStateProduct wsp = (WorkplaceStateProduct) parentElement;
				List<Operation> producingOperations = wsp.getWorkplaceState().getOperation(wsp.getProduct());
				List<OperationProduct> ops = new ArrayList<OperationProduct>();
				for(Operation op : producingOperations){
					ops.add(new OperationProduct(wsp.getProduct(), op));
				}
				return ops.toArray();
			}

			// Case OperationProduct
			else if (parentElement instanceof OperationProduct) {
				OperationProduct op = (OperationProduct) parentElement;
				return op.getOperation().getInputs().toArray();
			}

			// case ProductQuantity
			else if(parentElement instanceof ProductQuantity){
				return getChildren(((ProductQuantity) parentElement).getProduct());
			}

			// this case should not happen
			return new Object[] {};
		}

		@Override
		public Object getParent(Object element) {
			// 4 cases: Product, WorkplaceStateProduct, Operation, ProductQuantity

			// case Product
			if (element instanceof Product) {
			}
			// case WorkplaceStateProduct
			else if (element instanceof WorkplaceStateProduct) {
			}
			// case OperationProdut
			else if (element instanceof OperationProduct) {
			}
			// case ProductQuantity
			else if (element instanceof ProductQuantity) {
			}

			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
			// 4 cases: Product, WorkplaceStateProduct, Operation, ProductQuantity

			// case Product
			if(element instanceof Product){
				return !EntityList.getProducingWorkplaceList((Product) element).isEmpty();
			}
			// case WorkplaceStateProduct
			if(element instanceof WorkplaceStateProduct){
				WorkplaceStateProduct wsp = (WorkplaceStateProduct) element;
				List<Operation> producingOperations = wsp.getWorkplaceState().getOperation(wsp.getProduct());
				return producingOperations.size()>0;
			}
			// case OperationProdut
			else if (element instanceof OperationProduct) {
				OperationProduct op = (OperationProduct) element;
				return op.getOperation().getInputs().size()>0;
			}
			// case ProductQuantity
			else if (element instanceof ProductQuantity) {
				return hasChildren(((ProductQuantity) element).getProduct());
			} 

			return false;
		}

	}

	private class ProductTreeLabelProvider extends LabelProvider implements ITableLabelProvider {

		private ProductTreeLabelProvider(TreeViewer viewer){
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			switch (columnIndex) {
			case 0: // TODO
				return getImage(element);
			case 1: // TODO
			default:
				return null;
			}
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			switch (columnIndex) {
			case 0: // TODO
				return getText(element);
			case 1: // TODO
				if(element instanceof ProductQuantity){
					return String.valueOf(((ProductQuantity) element).getQuantity());
				}
			default:
				return "";
			}
		}

		@Override
		public String getText(Object element) {
			// 4 cases: Product, WorkplaceStateProduct, Operation, ProductQuantity

			// case Product
			if (element instanceof Product) {
				return ((Product) element).getName();
			} 
			// case WorkplaceStateProduct
			else if (element instanceof WorkplaceStateProduct) {
				return ((WorkplaceStateProduct) element).getWorkplaceState().getWorkplace().getName();
			}
			// case OperationProdut
			else if (element instanceof OperationProduct) {
				return "Operation: "+ ((OperationProduct)element).getProduct().getName();
			}
			// case ProductQuantity
			else if (element instanceof ProductQuantity) {
				return ((ProductQuantity) element).getProduct().getName();
			}

			return "";
		}

		@Override
		public Image getImage(Object element) {
			if (element instanceof Product) {
				if (((Product) element).isDisposable()) {
					return Activator.getDefault().getImageRegistry().get("productEnd");
				} else {

					return Activator.getDefault().getImageRegistry().get("productIntermediate");
				}
			}
			else if(element instanceof Operation){
				return null;
			}
			else if(element instanceof ProductQuantity){
				if (((ProductQuantity) element).getProduct().isDisposable()) {
					return Activator.getDefault().getImageRegistry().get("productEnd");
				} else {
					return Activator.getDefault().getImageRegistry().get("productIntermediate");
				}
			}
			return null;
		}

	}

	@Override
	public void update(UpdateType type) {
		switch (type) {
		case PROJECT_LOADED:
			createInput();
			tree.refresh(true);
			break;
		case NEW_OPERATION_CREATED:
			tree.refresh(true);
			break;
		default:
			break;
		}

	}


}

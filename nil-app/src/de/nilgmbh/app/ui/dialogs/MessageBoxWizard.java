package de.nilgmbh.app.ui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class MessageBoxWizard {
	public MessageBoxWizard(String message) {
		final Display d = PlatformUI.getWorkbench().getDisplay();
		final Shell s = new Shell (d);
		
		int style = SWT.ICON_ERROR | SWT.OK;

		MessageBox messageBox = new MessageBox(s, style);

		messageBox.setText("ERROR");

		messageBox.setMessage(message);

		if (messageBox.open() == SWT.OK) {
			
			s.close();
			
		}

		while (!s.isDisposed()){
			if(!d.readAndDispatch())
				d.sleep();

		}
	

	
	}
}

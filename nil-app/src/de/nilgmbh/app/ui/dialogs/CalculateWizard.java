package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.Calendar;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;

import de.fzi.wenpro.util.DatePeriodConvertor;

public class CalculateWizard extends Wizard {

	private static CalculateWizardPage cwp;
	private boolean newCalculation;
	private int calculationDuration;
	private int calculationTime;
	private Calendar calculatingDate;
	private double maxEnergyLevel;
	private int maxEnergyLevelWeight;
	private int nightShiftAvoidanceWeight;

	private Object comboSelected = null;


	public CalculateWizard(){
		super();
		super.setWindowTitle((getString("CalculateWizard.title")));
	}

	@Override
	public void addPages() {
		cwp = new CalculateWizardPage("name");
		addPage(cwp);
	}

	@Override
	public boolean performFinish() {

		// set the date to state calculation and the calculation duration //
		calculatingDate = DatePeriodConvertor.getCalendarFromDateTime(cwp.getCalculateDate().getYear(), cwp.getCalculateDate().getMonth(), cwp.getCalculateDate().getDay());
		System.out.println("Planinng start data - Calendar : "+calculatingDate.getTime().toString());
		
		calculationDuration = cwp.getCalculationDurationText().getText().isEmpty()? 0: Integer.parseInt(cwp.getCalculationDurationText().getText());
		calculationTime = cwp.isSecondsSelected() ? calculationDuration : (calculationDuration * 60);

		// set the calculation constraints: night shift avoidance and energy peak avoidance
		maxEnergyLevel = cwp.getMaxAllowedEnergyText().getText().isEmpty()? 0 : Double.parseDouble(cwp.getMaxAllowedEnergyText().getText());
		maxEnergyLevelWeight = cwp.shouldAvoidPeaks()?1:0; //if peak avoidance selected weigt is 1, 0 otherwise
		
		nightShiftAvoidanceWeight = cwp.shouldPlanFulltime()?0:1; // if full time planing is selected weight is 0, 1 otherwise

		
		if (cwp.getNewPlanButton().getSelection() && calculationTime > 0 && maxEnergyLevel > 0 ) {
			newCalculation = true;
		} else if (cwp.getExistingPlanButton().getSelection() && calculationTime > 0 && maxEnergyLevel > 0) {
			newCalculation = false;
			IStructuredSelection selection = (IStructuredSelection) cwp.getComboViewer().getSelection();
			comboSelected = selection.getFirstElement();
		} else{
			new MessageBoxWizard("Bitte fuellen Sie alle Felder aus!");
			return false;
		}
		return true;
	}

	public Calendar getCalculatingDate() {
		return calculatingDate;
	}

	public void setCalculatingDate(Calendar calculatingDate) {
		this.calculatingDate = calculatingDate;
	}

	public int getCalculationTime() {
		return calculationTime;
	}

	public Object getComboSelected() {
		return comboSelected;
	}

	public boolean isNewCalculation() {
		return newCalculation;
	}
	public double getMaxEnergyLevel(){
		return maxEnergyLevel;
	}

	public int getMaxEnergyLevelWeight() {
		return maxEnergyLevelWeight;
	}

	public int getNightShiftAvoidanceWeight() {
		return nightShiftAvoidanceWeight;
	}
	
	



}

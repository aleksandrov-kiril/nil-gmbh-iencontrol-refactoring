package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.security.acl.Group;
import java.util.List;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;

import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarModel;

public class CalculateWizardPage extends WizardPage {
	
	private Button newPlanButton, existingPlanButton, radioMin, radioSec;
	private ComboViewer comboViewer;
	private Text calculationTimeText, maxAllowedEnergyText;
	private DateTime calculateDate;
	private Button peakAvoidanceButton;
	private Button fulltimeButton;
	
	private boolean peaksAvoidanceOptionSelected = true;
	private boolean planFulltimeOptionSelected = true;
	



	public CalculateWizardPage(String pageName) {
		super(pageName);
	}
	
	@Override
	public void createControl(Composite parent) {
		
		GridLayoutFactory trippleGridLayout = GridLayoutFactory.swtDefaults().numColumns(4).equalWidth(false);
		GridDataFactory singSpan = GridDataFactory.swtDefaults().span(1,1).grab(true, false).align(SWT.FILL, SWT.CENTER);
		GridDataFactory doubSpan = GridDataFactory.swtDefaults().span(2,1).grab(true, false).align(SWT.FILL, SWT.CENTER);
		GridDataFactory tripSpan = GridDataFactory.swtDefaults().span(3,1).grab(true, false).align(SWT.FILL, SWT.CENTER);
		GridDataFactory quadSpan = GridDataFactory.swtDefaults().span(4,1).grab(true, false).align(SWT.FILL, SWT.CENTER);
		
		Composite baseComposite = new Composite(parent, SWT.NULL);
		baseComposite.setLayout(trippleGridLayout.create());
		
		newPlanButton = new Button(baseComposite, SWT.RADIO);
		newPlanButton.setLayoutData(quadSpan.create());
		newPlanButton.setText((getString("CalculateWizardPage.newPlanButton")));
		newPlanButton.setSelection(true);
		
		existingPlanButton = new Button(baseComposite,SWT.RADIO);
		existingPlanButton.setLayoutData(doubSpan.create());
		existingPlanButton.setText((getString("CalculateWizardPage.existingPlanButton")));

		comboViewer = new ComboViewer(baseComposite, SWT.READ_ONLY | SWT.FILL);
		comboViewer.getControl().setLayoutData(doubSpan.create());
		comboViewer.setContentProvider(new ArrayContentProvider());
		comboViewer.setLabelProvider(new LabelProvider(){
			public String getText(Object element) {

				if (element instanceof ProductionPlanTimeBarModel){
					ProductionPlanTimeBarModel productionPlanTimeBarModel = (ProductionPlanTimeBarModel) element;
					return productionPlanTimeBarModel.getName();			
				}					
				return super.getText(element);
			}
		});
		List<ProductionPlanTimeBarModel> productionPlanTimeBarModels = ProductionPlanProject.getCurrentProductionPlanProject().getProductionPlanTimeBarModels();
		comboViewer.setInput(productionPlanTimeBarModels);
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				event.getSelection();

			}
		});
		comboViewer.getControl().setEnabled(false);

		Label separatorLabel = new Label(baseComposite, SWT.SEPARATOR|SWT.HORIZONTAL);
		separatorLabel.setLayoutData(quadSpan.create());
		
		//start date
		Label calculateDateLabel = new Label(baseComposite, SWT.NONE);
		calculateDateLabel.setLayoutData(doubSpan.create());
		calculateDateLabel.setText((getString("CalculateWizardPage.begin")));
		
		calculateDate = new DateTime(baseComposite, SWT.DROP_DOWN);	
		calculateDate.setLayoutData(doubSpan.create());
		calculateDate.setDate(2012, 8, 12); // FIXME: default date for demo
		
		separatorLabel = new Label(baseComposite, SWT.SEPARATOR|SWT.HORIZONTAL);
		separatorLabel.setLayoutData(quadSpan.create());
		separatorLabel = new Label(baseComposite, SWT.NONE);
		separatorLabel.setLayoutData(quadSpan.create());
		separatorLabel.setText("Planungsrestriktionen");
		
		fulltimeButton = new Button(baseComposite, SWT.CHECK);
		fulltimeButton.setLayoutData(doubSpan.create());
		fulltimeButton.setText((getString("CalculateWizardPage.NightShiftAvoidance")));
		fulltimeButton.setSelection(planFulltimeOptionSelected);
		
		Label fulltimeLabel = new Label(baseComposite, SWT.NONE);
		fulltimeLabel.setLayoutData(doubSpan.create());
		
		peakAvoidanceButton = new Button(baseComposite, SWT.CHECK);
		peakAvoidanceButton.setLayoutData(singSpan.create());
		peakAvoidanceButton.setText((getString("CalculateWizardPage.PeakLoadAvoidance")));
		peakAvoidanceButton.setSelection(peaksAvoidanceOptionSelected);
		
		final Label peakAvoLabel = new Label(baseComposite, SWT.NONE);
		peakAvoLabel.setLayoutData(doubSpan.copy().align(SWT.END, SWT.CENTER).create());
		peakAvoLabel.setText("Grenze (kWh)");
		
		maxAllowedEnergyText = new Text (baseComposite, SWT.BORDER);
		maxAllowedEnergyText.setLayoutData(singSpan.create());
		maxAllowedEnergyText.setEnabled(true);
		maxAllowedEnergyText.setText("30");
		
		separatorLabel = new Label(baseComposite, SWT.SEPARATOR|SWT.HORIZONTAL);
		separatorLabel.setLayoutData(quadSpan.create());
		
		Label calculationTimeLabel = new Label(baseComposite, SWT.NONE);
		calculationTimeLabel.setText((getString("CalculateWizardPage.calculationTime"))+" : ");
		
		calculationTimeText = new Text (baseComposite, SWT.BORDER);
		calculationTimeText.setLayoutData(doubSpan.create());
		calculationTimeText.setText("2");
		
		final Label timeUnit = new Label(baseComposite, SWT.NONE);
		timeUnit.setLayoutData(singSpan.create());
		timeUnit.setFont(new Font(Display.getCurrent(),"Segoe UI", 9, SWT.ITALIC));
		
		Label emptySlot = new Label(baseComposite, SWT.NONE);
		emptySlot.setLayoutData(singSpan.create());
		emptySlot.setVisible(false);
		
		radioMin = new Button(baseComposite, SWT.RADIO);
		radioMin.setText((getString("CalculateWizardPage.minutes")));
		radioMin.setLayoutData(singSpan.create());
		radioMin.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				timeUnit.setText((getString("CalculateWizardPage.minutes")));
			}
		});
		
		radioSec = new Button(baseComposite, SWT.RADIO);
		radioSec.setText((getString("CalculateWizardPage.seconds")));
		radioSec.setLayoutData(GridDataFactory.swtDefaults().span(1,1).grab(true, false).align(SWT.END, SWT.CENTER).create());
		radioSec.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				timeUnit.setText((getString("CalculateWizardPage.seconds")));
			}
		});
		
		timeUnit.setText((getString("CalculateWizardPage.minutes")));
		radioMin.setSelection(true);
		radioSec.setSelection(false);
		
		
		//hier
//		Label maxAllowedEnergyLabel = new Label(baseComposite, SWT.NONE);
//		maxAllowedEnergyLabel.setText((getString("CalculateWizardPage.maxEnergy")));
//		maxAllowedEnergyLabel.setLayoutData(singleSpan.create());
		
			
	
		
		// listeners

		newPlanButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				newPlanButton.setSelection(true);
				comboViewer.getControl().setEnabled(false);
			}
		});
		
		existingPlanButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				newPlanButton.setSelection(false);
				comboViewer.getControl().setEnabled(true);
			}
		});
		
		peakAvoidanceButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				peakAvoLabel.setVisible(peakAvoidanceButton.getSelection());
				peaksAvoidanceOptionSelected = peakAvoidanceButton.getSelection();
				maxAllowedEnergyText.setVisible(peaksAvoidanceOptionSelected);
			}
		});
		
		fulltimeButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				planFulltimeOptionSelected = fulltimeButton.getSelection();
			}
		});
		
//		Composite peakLoadComposite = new Composite(baseComposite, SWT.NONE);
//		GridLayout peakLoadCompositeLayout= new GridLayout(4, true);
//		peakLoadComposite.setLayout(peakLoadCompositeLayout);
//		peakLoadComposite.setLayoutData(GridDataFactory.swtDefaults().span(3,1).grab(true, false).align(SWT.FILL, SWT.CENTER).create());
//		
//		Label peakLoadLabel = new Label(peakLoadComposite, SWT.NONE);
//		peakLoadLabel.setText((getString("CalculateWizardPage.PeakLoadAvoidance")));
//		peakLoadLabel.setLayoutData(singleSpan.create());
//		
//		peakLoadSlider = new Slider(peakLoadComposite, SWT.NONE);
//		peakLoadSlider.setLayoutData(GridDataFactory.swtDefaults().span(2,1).grab(true, false).align(SWT.FILL,SWT.CENTER).create());
//		peakLoadSlider.setMaximum(100 + peakLoadSlider.getThumb());
//		peakLoadSlider.setMinimum(0);
//		
//		final Label peakLoadNumberLabel = new Label (peakLoadComposite, SWT.NONE);
//		peakLoadNumberLabel.setLayoutData(GridDataFactory.swtDefaults().align(SWT.LEFT, SWT.CENTER).grab(true, false).span(1,1).hint(131, SWT.DEFAULT).create());
//		peakLoadNumberLabel.setText("0");
//		
//		Label emptyLabel = new Label(peakLoadComposite, SWT.NONE);
//		emptyLabel.setVisible(false);
//		
//		Label peakLoadZeroLabel = new Label(peakLoadComposite, SWT.NONE);
//		peakLoadZeroLabel.setText("0");
//		peakLoadZeroLabel.setLayoutData(singleSpan.create());
//		
//		Label peakLoadHundredLabel = new Label(peakLoadComposite, SWT.NONE);
//		peakLoadHundredLabel.setText("100");
//		peakLoadHundredLabel.setLayoutData(GridDataFactory.swtDefaults().span(1,1).grab(true, false).align(SWT.END, SWT.CENTER).create());
//		
//		peakLoadSlider.addSelectionListener(new SelectionListener() {
//			
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				peakLoadNumberLabel.setText(String.valueOf(peakLoadSlider.getSelection()));
//			}
//			
//			@Override
//			public void widgetDefaultSelected(SelectionEvent e) {
//			}
//		});
//		
//		Composite nightShiftComposite = new Composite(baseComposite, SWT.NONE);
//		GridLayout nightShiftCompositeLayout= new GridLayout(4, true);
//		nightShiftComposite.setLayout(nightShiftCompositeLayout);
//		nightShiftComposite.setLayoutData(GridDataFactory.swtDefaults().span(3,1).grab(true, false).align(SWT.FILL, SWT.CENTER).create());
//		
//		Label nightShiftLabel = new Label(nightShiftComposite, SWT.NONE);
//		nightShiftLabel.setText((getString("CalculateWizardPage.NightShiftAvoidance")));
//		nightShiftLabel.setLayoutData(singleSpan.create());
//		
//		nightShiftSlider = new Slider(nightShiftComposite, SWT.NONE);
//		nightShiftSlider.setLayoutData(GridDataFactory.swtDefaults().span(2,1).grab(true, false).align(SWT.FILL,SWT.CENTER).create());
//		nightShiftSlider.setMaximum(100 + nightShiftSlider.getThumb());
//		nightShiftSlider.setMinimum(0);
//		
//		final Label nightShiftNumberLabel = new Label (nightShiftComposite, SWT.NONE);
//		nightShiftNumberLabel.setLayoutData(GridDataFactory.swtDefaults().align(SWT.LEFT, SWT.CENTER).grab(true, false).span(1,1).hint(131, SWT.DEFAULT).create());
//		nightShiftNumberLabel.setText("0");
//		
//		Label emptyLabel2 = new Label(nightShiftComposite, SWT.NONE);
//		emptyLabel2.setVisible(false);
//		
//		Label nightShiftZeroLabel = new Label(nightShiftComposite, SWT.NONE);
//		nightShiftZeroLabel.setText("0");
//		nightShiftZeroLabel.setLayoutData(singleSpan.create());
//		
//		Label nightShiftHundredLabel = new Label(nightShiftComposite, SWT.NONE);
//		nightShiftHundredLabel.setText("100");
//		nightShiftHundredLabel.setLayoutData(GridDataFactory.swtDefaults().span(1,1).grab(true, false).align(SWT.END, SWT.CENTER).create());
//		
//		nightShiftSlider.addSelectionListener(new SelectionListener() {
//			
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				nightShiftNumberLabel.setText(String.valueOf(nightShiftSlider.getSelection()));
//			}
//			
//			@Override
//			public void widgetDefaultSelected(SelectionEvent e) {
//			}
//		});
//		

		
//		final Composite strategySelectionComposite = new Composite(baseComposite, SWT.NONE);
//		GridLayout strategySelectionLayout= new GridLayout(2, true);
//		strategySelectionComposite.setLayout(strategySelectionLayout);
//		strategySelectionComposite.setLayoutData(data);
//		Label strategySelectionCompositeLabel = new Label(strategySelectionComposite, SWT.NONE);
//		strategySelectionCompositeLabel.setText("Berechnungsstrategie: ");
//		
//		strategyCombo = new ComboViewer(strategySelectionComposite, SWT.READ_ONLY | SWT.BORDER);
//		strategyCombo.setContentProvider(new ArrayContentProvider());
//		strategyCombo.setLabelProvider(new LabelProvider(){
//			public String getText(Object element) {
//				if (element instanceof StrategyOption){
//					return ((StrategyOption) element).getStrategyName();
//				}		
//				return "NULL";
//				
//			}
//		});
//		List<StrategyOption> strategyOptions = getStrategyOptions();
//		strategyCombo.setInput(strategyOptions);
//		strategyCombo.addSelectionChangedListener(new ISelectionChangedListener() {
//			@Override
//			public void selectionChanged(SelectionChangedEvent event) {
//				event.getSelection();
//
//			}
//		});
		
		
		setControl(baseComposite);	
	}
	

	public ComboViewer getComboViewer() {
		return comboViewer;
	}

	public Button getNewPlanButton() {
		return newPlanButton;
	}

	public Button getExistingPlanButton() {
		return existingPlanButton;
	}

	public Text getCalculationDurationText() {
		return calculationTimeText;
	}
	
	public Text getMaxAllowedEnergyText() {
		return maxAllowedEnergyText;
	}

	public boolean isSecondsSelected(){
		return radioSec.getSelection();
	}
	public DateTime getCalculateDate() {
		return calculateDate;
	}

	public void setCalculateDate(DateTime calculateDate) {
		this.calculateDate = calculateDate;
	}
	

	public boolean shouldAvoidPeaks() {
		return peaksAvoidanceOptionSelected;
	}
	
	public boolean shouldPlanFulltime() {
		return planFulltimeOptionSelected;
	}
	
//	private List<StrategyOption> getStrategyOptions() {
//		List<StrategyOption> result = new ArrayList<StrategyOption>();
//		result.add(new StrategyOption(0, "alle Strategien"));
//		result.add(new StrategyOption(1, "ohne Nachtschicht"));
//		result.add(new StrategyOption(2, "keine Lastspitzenvermeidung, alle Schichten"));
//		result.add(new StrategyOption(3, "erzwungene Lastspitzenvermeidung, alle Schichten"));
//		return result;
//	}
	


}

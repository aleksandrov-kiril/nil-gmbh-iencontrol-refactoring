/**
 * 
 */
package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.Calendar;

import org.eclipse.jface.wizard.Wizard;

import de.fzi.wenpro.util.DatePeriodConvertor;

/**
 * @author Ardhi
 *
 */
public class AlternativeWizard extends Wizard {
	
	private String titleName;
	private boolean newPlan;
	private AlternativeWizardPage awp;
	private Calendar startingDate;
	



	public AlternativeWizardPage getTitleWizardPage() {
		return awp;
	}



	public void setTitleWizardPage(AlternativeWizardPage alternativeWizardPage) {
		this.awp = alternativeWizardPage;
	}
	
	public String getTitleName() {
		return titleName;
	}


	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	
	public boolean isNewPlan() {
		return newPlan;
	}


	public void setNewPlan(boolean newPlan) {
		this.newPlan = newPlan;
	}



	public AlternativeWizard(){
		super();
		super.setWindowTitle((getString("AlternativeWizard.title")));
	}


	
	@Override
	public void addPages() {
		awp = new AlternativeWizardPage("name");
		addPage(awp);
	
	}
	
	
	@Override
	public boolean performFinish() {
		
		if(!awp.getTitleText().getText().equals("") ){
		
			titleName = awp.getTitleText().getText();
			if(awp.getNewButton().getSelection()){
				newPlan = true;
				startingDate = DatePeriodConvertor.getCalendarFromDateTime(awp.getStartDate().getYear(), awp.getStartDate().getMonth(), awp.getStartDate().getDay());
			}
			else{
				newPlan = false;
			}
			
			
			
			
			
			
			return true;
			
		}
		else{
			new MessageBoxWizard("Bitte F�llen Sie alle Felder aus!");
			return false;
		}

	}



	public Calendar getStartingDate() {
		return startingDate;
	}



	public void setStartingDate(Calendar startingDate) {
		this.startingDate = startingDate;
	}




}

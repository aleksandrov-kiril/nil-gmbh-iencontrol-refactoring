package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.MessageBox;

public class PanameraWizard extends Wizard{
	
	private static PanameraWizardPage pwp;
	private int nPalettes = 0;
	
	public PanameraWizard(){
		super();
		super.setWindowTitle((getString("PanameraWizard.title")));
	}
	
	@Override
	public void addPages(){
		pwp = new PanameraWizardPage("name");
		addPage(pwp);
	}

	@Override
	public boolean performFinish(){
		try{
			nPalettes = Integer.valueOf(pwp.getTextPalettes().getText());
		}catch (NumberFormatException e){
			nPalettes = 0;
		}
		try{
			nPalettes = Integer.valueOf(pwp.getTextPaletteSize().getText());
		}catch (NumberFormatException e){
			nPalettes = 0;
		}
		if (nPalettes <= 0){
			MessageBox m = new MessageBox(getShell());
			m.setMessage(getString("PanameraWizard.InvalidNumber"));
			m.open();
			return false;
		}
		return true;
	}

	public int getNPalettes(){
		return nPalettes;
	}

	public int getPaletteSize(){
		return nPalettes;
	}
}

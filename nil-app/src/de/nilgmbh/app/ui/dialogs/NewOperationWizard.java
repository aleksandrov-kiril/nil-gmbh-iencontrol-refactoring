/**
 * 
 */
package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import org.eclipse.jface.wizard.Wizard;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.InternalOperation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.updatemechanism.UpdateType;

/**
 * @author Ardhi
 *
 */
public class NewOperationWizard extends Wizard {

	private NewOperationWizardFirstPage one;
	private Object object;

		
	public NewOperationWizard (Object object){
	super();
	super.setWindowTitle(getString("ProductTreeView.newOperation"));
	this.object = object;

	}
	@Override
	public void addPages() {
		one = new NewOperationWizardFirstPage(this.object);
		addPage(one);
	}

	@Override
	public boolean performFinish() { 
		System.out.println(EntityList.getProducingWorkplaceList((Product) object));
		one.assignSelectedComponents();
		InternalOperation operation = new InternalOperation();
		operation.setTime(one.getProductionTime());
		operation.setLotSize(one.getLotSize());
		operation.setInputs(one.getInputs());
		operation.setOutput(one.getOutputs());
		operation.setScrapRate(one.getScrapRate());
		WorkplaceState workplaceState = one.getWorkplaceState();
		workplaceState.getOperations().add(operation);
		operation.setOwner(workplaceState);
		operation.setFactory(Project.getCurrentProject().getOriginalFactory());
		Activator.getDefault().notifyAllListener(UpdateType.NEW_OPERATION_CREATED);
		System.out.println(EntityList.getProducingWorkplaceList((Product) object));
		
		return true;	
		
	}

}

package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class PanameraWizardPage extends WizardPage{
	
	private Text textPalettes, textPaletteSize;
	
	public PanameraWizardPage(String pageName){
		super(pageName);
	}
	
	@Override
	public void createControl(Composite parent){
		Composite top = new Composite(parent, SWT.NULL);
		GridLayout gl = new GridLayout(2, false);
		top.setLayout(gl);
		
		Label nPalettesLabel = new Label(top, SWT.NONE);
		nPalettesLabel.setText(getString("PanameraWizard.nPalettes"));
		nPalettesLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		
		textPalettes = new Text(top, SWT.BORDER);
		textPalettes.setText("20");
		textPalettes.setLayoutData(new GridData(160, SWT.DEFAULT));

		
		Label paletteSizeLabel = new Label(top, SWT.NONE);
		paletteSizeLabel.setText(getString("PanameraWizard.paletteSize"));
		paletteSizeLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		
		textPaletteSize = new Text(top, SWT.BORDER);
		textPaletteSize.setText("160");
		textPaletteSize.setLayoutData(new GridData(160, SWT.DEFAULT));
		
		setControl(top);
	}
	
	public Text getTextPalettes(){
		return textPalettes;
	}
	
	public Text getTextPaletteSize(){
		return textPaletteSize;
	}
}

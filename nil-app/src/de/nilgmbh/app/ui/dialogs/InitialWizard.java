 package de.nilgmbh.app.ui.dialogs;


import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.printing.ProductionPlanPrinter;
import de.jaret.util.date.Interval;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.internalmodel.ProductionPlanProject;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanEntryIntervalImpl;
import de.nilgmbh.app.ui.jaretimpl.WorkplaceTimeBarRowModel;
import de.nilgmbh.app.ui.views.PPEntryPropertiesView;
import de.nilgmbh.app.ui.views.WarningView;
import de.nilgmbh.app.updatemechanism.UpdateType;

/**
 * @author Ardhi
 *
 */

public class InitialWizard extends Wizard {
	private InitialWizardPage one;
	private Object object;
	private Workplace wp;
	private boolean editMode;
//	private ModelCreator modelCreator;
	
	
	public InitialWizard (Object object, boolean editMode){
	super();
	super.setWindowTitle((getString("InitialWizard.title")));
	setNeedsProgressMonitor(false);
	this.object = object;
	this.editMode = editMode;

	}
	@Override
	public void addPages() {
		one = new InitialWizardPage(this.object, editMode);
		addPage(one);
	}

	@Override
	public boolean performFinish() { 
		
		if (editMode){
									
			IStructuredSelection selection = (IStructuredSelection)one.getComboViewer().getSelection();
			Object comboSelected = selection.getFirstElement();
			
			ProductionPlanEntryIntervalImpl ppeiiSelected = (ProductionPlanEntryIntervalImpl) object;

			Interval intervalSelected = (Interval) object;	
			wp =  (Workplace) comboSelected;
			int quantityInt = Integer.parseInt(one.getQuantityText().getText());
//			Product selectedProduct = ppeiiSelected.getEntry().getOperation().getOutput();
			Product selectedProduct = null;
			for (Product p : ppeiiSelected.getEntry().getOperation().getOutputProducts()){
				selectedProduct = p;
			}
			WorkplaceTimeBarRowModel  workplaceTimeBarRowModel = (WorkplaceTimeBarRowModel) ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().getTimeBarRow(wp);
			if (!one.getQuantityText().getText().equals("") && one.getTimeChooser().getDate() != null && comboSelected != null ){
						
//				boolean overlap = false;
//				for (Interval rowInterval : workplaceTimeBarRowModel.getIntervals()){
//					if (_draggedIntervals.get(i).intersects(rowInterval) && !((ProductionPlanEntryIntervalImpl) _draggedIntervals.get(i)).getEntry().equals( ((ProductionPlanEntryIntervalImpl) rowInterval).getEntry() )){
//						overlap = true;
//					}			
//				}		
				workplaceTimeBarRowModel.removeAndSaveInterval(intervalSelected);								
				workplaceTimeBarRowModel.addAndSaveInterval3(wp, selectedProduct ,quantityInt, one.getTimeChooser().getDate());
				if (workplaceTimeBarRowModel.getProductionPlan().getWarningList().size() > 0){
					try {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(WarningView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
						Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
					} catch (PartInitException e) {
						e.printStackTrace();
					}
				}
				else{
					try {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(PPEntryPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
					} catch (PartInitException e) {
						e.printStackTrace();
					}
				}
				ProductionPlanPrinter.printProductionPlan(ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().getProductionPlan());
			}
			else{
				new MessageBoxWizard("Bitte fuellen Sie alle Felder aus!");
			}
			
			return true;
		}
		
		else{

			IStructuredSelection selection = (IStructuredSelection)one.getComboViewer().getSelection();
			Object comboSelected = selection.getFirstElement();		
			wp =  (Workplace) comboSelected;
			int quantityInt = Integer.parseInt(one.getQuantityText().getText());
			WorkplaceTimeBarRowModel  workplaceTimeBarRowModel = (WorkplaceTimeBarRowModel) ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().getTimeBarRow(wp);
			if (!one.getQuantityText().getText().equals("") && one.getTimeChooser().getDate() != null  && comboSelected != null ){
						
//				boolean overlap = false;
//				for (Interval rowInterval : workplaceTimeBarRowModel.getIntervals()){
//					if (_draggedIntervals.get(i).intersects(rowInterval) && !((ProductionPlanEntryIntervalImpl) _draggedIntervals.get(i)).getEntry().equals( ((ProductionPlanEntryIntervalImpl) rowInterval).getEntry() )){
//						overlap = true;
//					}			//				}					
				
//				workplaceTimeBarRowModel.addAndSaveIntervalToMinPos(wp, (Product) object	,quantityInt, one.getTimeChooser().getDate());
				workplaceTimeBarRowModel.addAndSaveInterval2(wp, (Product) object	,quantityInt, one.getTimeChooser().getDate());
				
				Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
				if (workplaceTimeBarRowModel.getProductionPlan().getWarningList().size() > 0){
					try {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(WarningView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
						Activator.getDefault().notifyAllListener(UpdateType.PRODUCTIONPLAN_CHANGED);
					} catch (PartInitException e) {
						e.printStackTrace();
					}
				}
				else{
					try {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(PPEntryPropertiesView.ID, null, IWorkbenchPage.VIEW_VISIBLE);
					} catch (PartInitException e) {
						e.printStackTrace();
					}
				}						
				ProductionPlanPrinter.printProductionPlan(ProductionPlanProject.getCurrentProductionPlanProject().getCurrentProductionPlanTimeBarModel().getProductionPlan());			}
			else{
				new MessageBoxWizard("Bitte fuellen Sie alle Felder aus!");
			}		

			return true;
		}		
		
	}
	
	

}

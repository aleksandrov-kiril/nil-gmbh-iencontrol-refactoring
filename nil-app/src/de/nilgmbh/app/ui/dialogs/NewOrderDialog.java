package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.Calendar;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Order;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.util.DatePeriodConvertor;
import de.nilgmbh.app.ui.common.dnd.DNDContainer;

/**
 * {@link Dialog} for creating new {@link Order} elements
 * 
 * @author Kiril Aleksandrov
 * 
 */
public class NewOrderDialog extends Dialog {


	private String title;

	private Product product;
	private int quantity;
	private Calendar startingDate;
	private Calendar finishingDate;

	public ComboViewer productCombo;
	private Text quantityText;
	private DateTime startDate;
	private DateTime endDate;

	private Order orderToEdit;
	/**
	 * Constructor NewOrderDialog
	 * 
	 * @param activeShell
	 * @param string
	 */
	public NewOrderDialog(Shell activeShell, String string) {
		super(activeShell);
		this.title = string;
	}

	/**
	 * @param firstElement
	 */
	public void setInput(Order firstElement) {
		this.orderToEdit = firstElement;
	}
	


	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (this.title != null) {
			shell.setText(this.title);
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		
		Group top = new Group(parent, SWT.NONE);
		top.setLayout(GridLayoutFactory.swtDefaults().numColumns(2)
				.equalWidth(false).create());

		Label label = new Label(top, SWT.NONE);
		label.setLayoutData(GridDataFactory.swtDefaults().create());
		label.setText(getString("OrderView.productLabel"));

		productCombo = new ComboViewer(top, SWT.READ_ONLY);
		productCombo.getCombo().setLayoutData(
				GridDataFactory.swtDefaults().grab(true, false).minSize(100,SWT.DEFAULT).hint(150, SWT.DEFAULT).create());
		productCombo.setContentProvider(new ArrayContentProvider());
		productCombo.setLabelProvider(new LabelProvider(){
			@Override
			public String getText(Object element) {
				return ((Product)element).getName();
			}
		});
		productCombo.setInput(EntityList.getDisposableProductList(Project
				.getCurrentProject().getOriginalFactory()));
		
		

		label = new Label(top, SWT.NONE);
		label.setLayoutData(GridDataFactory.swtDefaults().create());
		label.setText(getString("OrderView.quantityLabel"));

		quantityText = new Text(top, SWT.BORDER | SWT.WRAP | SWT.SINGLE);
		quantityText.setToolTipText(getString("quantityTooltip"));
		quantityText.setLayoutData(GridDataFactory.swtDefaults()
				.grab(true, false).align(SWT.FILL, SWT.BEGINNING).create());

		label = new Label(top, SWT.NONE);
		label.setLayoutData(GridDataFactory.swtDefaults().create());
		label.setText(getString("OrderView.startPeriodLabel"));

		
		startDate = new DateTime(top, SWT.DROP_DOWN);
		startDate.setLayoutData(GridDataFactory.swtDefaults()
				.grab(true, false).align(SWT.FILL, SWT.BEGINNING).create());
		Calendar cal = DatePeriodConvertor.computeStartOfNextWeekCalendar(); 
		startDate.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

		label = new Label(top, SWT.NONE);
		label.setLayoutData(GridDataFactory.swtDefaults().create());
		label.setText(getString("OrderView.finishingPeriodLabel"));

		endDate = new DateTime(top, SWT.DROP_DOWN);
		endDate.setLayoutData(GridDataFactory.swtDefaults()
				.grab(true, false).align(SWT.FILL, SWT.BEGINNING).create());
		endDate.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));


		// set input if editing
		if(orderToEdit!=null){
			productCombo.setSelection(new StructuredSelection(orderToEdit.getProduct()), true);
			productCombo.getCombo().setEnabled(false);

			quantityText.setText(String.valueOf(orderToEdit.getQuantity()));
			
			cal = Calendar.getInstance();
			cal.setTime(orderToEdit.getStartDate().getTime());
			startDate.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			cal.setTime(orderToEdit.getEndDate().getTime());
			endDate.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			
		}
		else if(DNDContainer.data != null){
			Product product = (Product) DNDContainer.data;
			productCombo.setSelection(new StructuredSelection(product), true);
			productCombo.getCombo().setEnabled(false);
			DNDContainer.data = null;
			
		}


		return top;
		
	}

	@Override
	protected void okPressed() {
		if ((((IStructuredSelection) productCombo.getSelection())
				.getFirstElement() instanceof Product)) {
			product = ((Product) ((IStructuredSelection) productCombo
					.getSelection()).getFirstElement());
		} else {
			MessageBox warning = new MessageBox(getShell());
			warning.setMessage(getString("OrderView.noProductSelected"));
			warning.open();
			return;
		}
		try {
			quantity = Integer.valueOf(quantityText.getText());
		} catch (NumberFormatException e) {
			MessageBox warning = new MessageBox(getShell());
			warning.setMessage(getString("OrderView.quantityUnsuportedType"));
			warning.open();
			return;
		}
		if(DatePeriodConvertor.getDateFromDateTime(startDate.getYear(), startDate.getMonth(), startDate.getDay()).after(DatePeriodConvertor.getDateFromDateTime(endDate.getYear(),endDate.getMonth(),endDate.getDay()))){
			MessageBox warning = new MessageBox(getShell());
			warning.setMessage(getString("finishingPeriodBeforStartPeriod"));
			warning.open();
			return;
		} else {
			startingDate = DatePeriodConvertor.getCalendarFromDateTime(startDate.getYear(), startDate.getMonth(), startDate.getDay());
			finishingDate = DatePeriodConvertor.getCalendarFromDateTime(endDate.getYear(),endDate.getMonth(),endDate.getDay());
			
		}
		setReturnCode(Project.getCurrentProject()
				.getCurrentAlternativeFactory() != null ? 1 : 0);
		super.okPressed();
	}

	/**
	 * Returns the newly created Order
	 */
	public Order getOrder() {
		if(orderToEdit==null){		
			return new Order(product, quantity, startingDate, finishingDate);
		}
		else {
			orderToEdit.setQuantity(quantity);
			orderToEdit.setStartDate(startingDate);
			orderToEdit.setEndDate(finishingDate);
			return orderToEdit;
		}

	}
}

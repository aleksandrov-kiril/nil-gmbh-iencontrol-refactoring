package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.Workplace;
import de.jaret.util.ui.datechooser.DateChooser;
import de.jaret.util.ui.datechooser.TimeChooser;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanEntryIntervalImpl;

/**
 * @author Ardhi
 *
 */

public class InitialWizardPage extends WizardPage {
	
	private Composite c;
	private Object object ;
	private Text quantityText;
	private DateChooser dateChooser;
	private TimeChooser timeChooser;
	private ComboViewer comboViewer;
	private boolean editMode;
	
//	public InitialWizardPage(Object object ){
//		super("Initial Wizard");
//		this.object = object;
//	}
	
	public InitialWizardPage(Object object, boolean editMode){
		super("Initial Wizard");
		this.object = object;
		this.editMode = editMode;

	}

	public Text getQuantityText() {
		return quantityText;
	}

	public void setQuantityText(Text quantityText) {
		this.quantityText = quantityText;
	}

	public DateChooser getDateChooser() {
		return dateChooser;
	}

	public void setDateChooser(DateChooser dateChooser) {
		this.dateChooser = dateChooser;
	}

	public TimeChooser getTimeChooser() {
		return timeChooser;
	}

	public void setTimeChooser(TimeChooser timeChooser) {
		this.timeChooser = timeChooser;
	}

	public ComboViewer getComboViewer() {
		return comboViewer;
	}

	public void setComboViewer(ComboViewer comboViewer) {
		this.comboViewer = comboViewer;
	}
	


	@Override
	public void createControl(Composite parent) {
		if(editMode){
			ProductionPlanEntryIntervalImpl entryInterval = (ProductionPlanEntryIntervalImpl) object;
			double producingTime = entryInterval.getEntry().getProductionTime();
			int quantity = (int) Math.round(producingTime/(entryInterval.getEntry().getOperation().getTime()));
			
			c = new Composite(parent, SWT.NULL);
			GridLayout gridLayout = new GridLayout (2, true);
			c.setLayout(gridLayout);
			//		c.setSize(350, 150);
			//		GridData data1 = new GridData (SWT.BEGINNING, SWT.CENTER, true, false);
			GridData data2 = new GridData (SWT.FILL, SWT.CENTER, true, false);

			final Label  quantityLabel = new Label (c, SWT.NONE);
			quantityLabel.setText (getString("InitialWizardPage.quantity"));

			quantityText = new Text (c, SWT.BORDER);
			quantityText.setLayoutData(data2);
			quantityText.setText( new Integer(quantity).toString());

			final Label startDateLabel = new Label (c, SWT.NONE);
			startDateLabel.setText (getString("InitialWizardPage.startingDate"));

			dateChooser = new DateChooser(c, SWT.NONE);
			dateChooser.setLayoutData(data2);
			dateChooser.setText(entryInterval.getBegin().toDisplayStringDate());

			final Label startTimeLabel = new Label (c, SWT.NONE);
			startTimeLabel.setText (getString("InitialWizardPage.startingTime"));

			timeChooser = new TimeChooser(c, SWT.NONE);
			timeChooser.setLayoutData(data2);
			timeChooser.setDateChooser(dateChooser);
			timeChooser.setText(entryInterval.getBegin().toDisplayStringTime(false));
			timeChooser.setDate(entryInterval.getBegin().getDate());
			System.out.println("timechooser : "+ entryInterval.getBegin().toDisplayString());

			final Label workplaceLabel = new Label (c, SWT.NONE);
			workplaceLabel.setText (getString("InitialWizardPage.workplace"));
			
			comboViewer = new ComboViewer(c, SWT.READ_ONLY);
			comboViewer.setContentProvider(new ArrayContentProvider());
			comboViewer.setLabelProvider(new LabelProvider() {
				@Override
				public String getText(Object element) {

					if (element instanceof Workplace){
						Workplace workplace = (Workplace) element;
						return workplace.getName();						
					}					
					return super.getText(element);
					
				}

			});
//			Product productSelected = entryInterval.getEntry().getOperation().getOutput(); 
			Product selectedProduct = null;
			for (Product p : entryInterval.getEntry().getOperation().getOutputProducts()){
				selectedProduct = p;
			}
			List <Workplace> workplaces = 	 EntityList.getProducingWorkplaceList(selectedProduct);
			comboViewer.setInput(workplaces);
			comboViewer.setSelection(new StructuredSelection(entryInterval.getEntry().getWorkplace()));		
			comboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					event.getSelection();

				}
			});
		}
		else{
			c = new Composite(parent, SWT.NULL);
			GridLayout gridLayout = new GridLayout (2, true);
			c.setLayout(gridLayout);
			//		c.setSize(350, 150);
			//		GridData data1 = new GridData (SWT.BEGINNING, SWT.CENTER, true, false);
			GridData data2 = new GridData (SWT.FILL, SWT.CENTER, true, false);

			final Label  quantityLabel = new Label (c, SWT.NONE);
			quantityLabel.setText ("Stueckzahl : ");

			quantityText = new Text (c, SWT.BORDER);
			quantityText.setLayoutData(data2);

			final Label startDateLabel = new Label (c, SWT.NONE);
			startDateLabel.setText ("Startdatum :");

			dateChooser = new DateChooser(c, SWT.NONE);
			dateChooser.setLayoutData(data2);

			final Label startTimeLabel = new Label (c, SWT.NONE);
			startTimeLabel.setText ("Startzeit :");

			timeChooser = new TimeChooser(c, SWT.NONE);
			timeChooser.setLayoutData(data2);
			timeChooser.setDateChooser(dateChooser);

			final Label workplaceLabel = new Label (c, SWT.NONE);
			workplaceLabel.setText ("Arbeitsplatz :");


			comboViewer = new ComboViewer(c, SWT.READ_ONLY);
			comboViewer.setContentProvider(new ArrayContentProvider());
			comboViewer.setLabelProvider(new LabelProvider() {
				@Override
				public String getText(Object element) {

					if (element instanceof Workplace){
						Workplace workplace = (Workplace) element;
						return workplace.getName();
					}
					return super.getText(element);
				}

			});
			List <Workplace> workplaces = 	 EntityList.getProducingWorkplaceList((Product) object);
			comboViewer.setInput(workplaces);

			comboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					event.getSelection();
				}
			});
		}
	

		setControl(c);

	}
}

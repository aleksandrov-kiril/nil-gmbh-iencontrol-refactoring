/**
 * 
 */
package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * @author Ardhi
 *
 */
public class AlternativeWizardPage extends WizardPage {
	
	


	private Composite c, radioButtonComposite;
	private Text titleText;
	private Button newButton, copyButton;
	private DateTime startDate;

	
	protected AlternativeWizardPage(String pageName) {
		super(pageName);
	}
	


	@Override
	public void createControl(Composite parent) {
		c = new Composite(parent, SWT.NULL);
		GridLayout gl = new GridLayout(2, true);
		c.setLayout(gl);
		
		GridData data = new GridData(SWT.FILL, SWT.CENTER, true, false);		
		Label titleLabel = new Label(c, SWT.NONE);
		titleLabel.setText((getString("AlternativeWizardPage.name"))+" : ");
				 
	    titleText = new Text(c, SWT.BORDER);
		titleText.setLayoutData(data);
		
		Label typeLabel = new Label (c,SWT.NONE);
		typeLabel.setText((getString("AlternativeWizardPage.type"))+" : ");
		
		radioButtonComposite = new Composite(c, SWT.NONE);
		GridLayout rbGridLayout = new GridLayout(1, false);
		radioButtonComposite.setLayout(rbGridLayout);
		
		newButton = new Button(radioButtonComposite, SWT.RADIO);
		newButton.setText((getString("AlternativeWizardPage.newRadioButton")));
		newButton.setSelection(true);
		
		final Composite dateComposite = new Composite(radioButtonComposite, SWT.NONE);
		GridLayout dateLayout = new GridLayout(1, true);
		dateComposite.setLayout(dateLayout);
		dateComposite.setLayoutData(data);	
		
		startDate = new DateTime(dateComposite, SWT.DROP_DOWN);	
		
		copyButton = new Button(radioButtonComposite,SWT.RADIO);
		copyButton.setText((getString("AlternativeWizardPage.copyRadioButton")));
		
		newButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				newButton.setSelection(true);
				dateComposite.setVisible(true);				
			}
		});
		copyButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				newButton.setSelection(false);
				dateComposite.setVisible(false);				
			}
		});
		
		setControl(c);		
	}

	public DateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}
	public Button getNewButton() {
		return newButton;
	}

	public void setNewButton(Button newButton) {
		this.newButton = newButton;
	}

	public Button getCopyButton() {
		return copyButton;
	}

	public void setCopyButton(Button copyButton) {
		this.copyButton = copyButton;
	}

	public Text getTitleText() {
		return titleText;
	}
	
	public void setTitleText(Text titleText) {
		this.titleText = titleText;
	}





}

package de.nilgmbh.app.ui.dialogs;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import de.fzi.wenpro.core.calculations.EntityList;
import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.WorkplaceState;
import de.fzi.wenpro.util.Numbers;


public class NewOperationWizardFirstPage extends WizardPage implements SelectionListener{

	private Composite composite, newWorkplaceStateComposite;
	private Object object ;
	private ComboViewer workplaceComboViewer, workplaceStateComboViewer, outputProductComboViewer, inputProductComboViewer;
	private Button newWPSRadio, existingWPSRadio, addInputButton, removeOutputButton, addOutputButton, removeInputButton;
	private TableViewer outputTableViewer, inputTableViewer;
	private Text outputQuantityText, inputQuantityText, retoolingTimeText, retoolingCostText, productionTimeText, scrapRateText, lotSizeText;
	private Label retoolingTimeLabel, retoolingCostLabel;

	private Workplace workplace;
	private WorkplaceState workplaceState;
	private List<ProductQuantity> outputs;
	private List<ProductQuantity> inputs;
	private double productionTime;
	private int lotSize;
	private double scrapRate;


	public NewOperationWizardFirstPage(Object object) {
		super("First");
		this.object = object;
		outputs = new ArrayList<ProductQuantity>();
		inputs = new ArrayList<ProductQuantity>();
		productionTime = 0;
	}

	@Override
	public void createControl(Composite parent) {
		Workplace selectedWorkplace = EntityList.getProducingWorkplaceList((Product) object).isEmpty()? null : EntityList.getProducingWorkplaceList((Product) object).get(0);

		composite = new Composite(parent, SWT.NULL);
		GridLayout gridLayout = new GridLayout(3, false);
		composite.setLayout(gridLayout);

		// -------------- Workplace Part ----------------//
		final Label workplaceLabel = new Label (composite, SWT.NONE);
		workplaceLabel.setText (getString("NewOperationWizardFirstPage.workplace"));


		workplaceComboViewer = new ComboViewer(composite, SWT.READ_ONLY);
		GridData data = new GridData (SWT.FILL, SWT.CENTER, true, false);
		data.horizontalSpan = 2;
		workplaceComboViewer.getCombo().setLayoutData(data);
		workplaceComboViewer.setContentProvider(new ArrayContentProvider());
		workplaceComboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {

				if (element instanceof Workplace){
					Workplace workplace = (Workplace) element;
					return workplace.getName();
				}
				return super.getText(element);
			}

		});
		workplaceComboViewer.setInput(EntityList.getWorkplaceList(Project.getCurrentProject().getOriginalFactory()));
		workplaceComboViewer.setSorter(new ViewerSorter(){
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				return ((Workplace) e1).getName().compareTo( ((Workplace) e2).getName());
			}
		}) ;
		if (selectedWorkplace != null)
			workplaceComboViewer.setSelection(new StructuredSelection(selectedWorkplace));
		workplaceComboViewer.getCombo().addSelectionListener(this);


		// -------------- Workplace State Part ----------------//
		Group workplaceStateGroup = new Group(composite, SWT.NONE);
		workplaceStateGroup.setText(getString("NewOperationWizardFirstPage.workplaceState"));
		gridLayout = new GridLayout(4, false);
		workplaceStateGroup.setLayout(gridLayout);
		data = new GridData(GridData.FILL, GridData.CENTER, true, false);
		data.horizontalSpan = 3;
		workplaceStateGroup.setLayoutData(data);

		newWPSRadio = new Button(workplaceStateGroup, SWT.RADIO);
		data = new GridData (SWT.BEGINNING, SWT.BEGINNING, false, false);
		newWPSRadio.setLayoutData(data);
		newWPSRadio.setText(getString("NewOperationWizardFirstPage.newWorkplaceState"));
		newWPSRadio.addSelectionListener(this);
		newWPSRadio.setSelection(true);

		newWorkplaceStateComposite = new Composite(workplaceStateGroup, SWT.NULL);
		newWorkplaceStateComposite.setLayout(new GridLayout(3, false));
		data = new GridData (SWT.BEGINNING, SWT.BEGINNING, true, false);
		data.horizontalSpan = 3;
		newWorkplaceStateComposite.setLayoutData(data);

		//---- retooling time -----//
		retoolingTimeLabel = new Label (newWorkplaceStateComposite, SWT.NONE);
		retoolingTimeLabel.setText (getString("NewOperationWizardFirstPage.retoolingTime"));
		retoolingTimeLabel.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));


		retoolingTimeText = new Text(newWorkplaceStateComposite, SWT.BORDER);
		retoolingTimeText.setText(Numbers.format(0));
		data = new GridData (SWT.FILL, SWT.CENTER, true, false);
		int width = retoolingTimeText.getBorderWidth() * 10;
		Rectangle trim = retoolingTimeText.computeTrim(0, 0, width, 0);
		data.widthHint = trim.width;
		retoolingTimeText.setLayoutData(data);



		final Label retoolingSecond = new Label(newWorkplaceStateComposite, SWT.NONE);
		retoolingSecond.setText(getString("NewOperationWizardFirstPage.seconds"));
		retoolingSecond.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));

		//---- retooling cost -----//
		retoolingCostLabel = new Label (newWorkplaceStateComposite, SWT.NONE);
		retoolingCostLabel.setText (getString("NewOperationWizardFirstPage.retoolingCost"));
		retoolingCostLabel.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));


		retoolingCostText = new Text(newWorkplaceStateComposite, SWT.BORDER);
		retoolingCostText.setText(Numbers.format(0));
		data = new GridData (SWT.FILL, SWT.CENTER, true, false);
		width = retoolingCostText.getBorderWidth() * 10;
		trim = retoolingCostText.computeTrim(0, 0, width, 0);
		data.widthHint = trim.width;
		retoolingCostText.setLayoutData(data);


		final Label retoolingEuro = new Label(newWorkplaceStateComposite, SWT.NONE);
		retoolingEuro.setText("Euro");
		retoolingEuro.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));


		//--- end retoolings -----//
		existingWPSRadio = new Button(workplaceStateGroup, SWT.RADIO);
		data = new GridData (SWT.FILL, SWT.CENTER, true, false);
		newWPSRadio.setLayoutData(data);
		existingWPSRadio.setText(getString("NewOperationWizardFirstPage.existingWorkplaceState"));
		existingWPSRadio.addSelectionListener(this);
		existingWPSRadio.setLayoutData(data);


		workplaceStateComboViewer = new ComboViewer(workplaceStateGroup, SWT.READ_ONLY);
		data = new GridData (SWT.FILL, SWT.CENTER, true, false);
		data.horizontalSpan = 2;
		workplaceStateComboViewer.getCombo().setLayoutData(data);
		workplaceStateComboViewer.setContentProvider(new ArrayContentProvider());
		workplaceStateComboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				String text = "";
				if (element instanceof WorkplaceState){
					WorkplaceState workplaceState = (WorkplaceState) element;
					for (Operation operation: workplaceState.getOperations()) {
						for (Product product : operation.getOutputProducts()) {
							text = text +  " - " + product.getName();
						}
					}
					return text;

				}
				return super.getText(element);
			}

		});
		if (selectedWorkplace != null) {
			workplaceStateComboViewer.setInput(selectedWorkplace.getStates());
			workplaceStateComboViewer.setSelection(new StructuredSelection(getFirstSelectedWorkplaceState(selectedWorkplace)));
		}
		workplaceStateComboViewer.getCombo().setEnabled(false);

		// -------------- Output Part ----------------//
		Group outputsGroup = new Group(composite, SWT.NONE);
		outputsGroup.setText(getString("NewOperationWizardFirstPage.outputs"));
		gridLayout = new GridLayout(3, false);
		outputsGroup.setLayout(gridLayout);
		data = new GridData(GridData.FILL, GridData.CENTER, true, false);
		data.horizontalSpan = 3;
		outputsGroup.setLayoutData(data);


		outputProductComboViewer = new ComboViewer(outputsGroup, SWT.READ_ONLY);
		data = new GridData (SWT.FILL, SWT.CENTER, true, false);
		outputProductComboViewer.getCombo().setLayoutData(data);
		outputProductComboViewer.setContentProvider(new ArrayContentProvider());
		outputProductComboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Product){
					Product product =  (Product) element;
					return product.getName();
				}
				return super.getText(element);
			}

		});
		outputProductComboViewer.setInput(EntityList.getProductList(Project.getCurrentProject().getOriginalFactory()));
		outputProductComboViewer.setSorter(new ViewerSorter(){
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				return ((Product) e1).getName().compareTo( ((Product) e2).getName());
			}
		}) ;
		outputProductComboViewer.setSelection(new StructuredSelection((Product) object));

		outputQuantityText = new Text(outputsGroup, SWT.BORDER);
		data = new GridData(SWT.FILL, SWT.CENTER, true, false);
		outputQuantityText.setLayoutData(data);

		Label itemsLabel = new Label(outputsGroup, SWT.NONE);
		itemsLabel.setText("St�ck");
		itemsLabel.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));

		outputTableViewer = new TableViewer(outputsGroup, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		data = new GridData (SWT.FILL, SWT.FILL, true, false);
		int tableHeight = outputTableViewer.getTable().getItemHeight() * 5;
		trim = outputTableViewer.getTable().computeTrim(0, 0, 0, tableHeight);
		data.heightHint = trim.height;
		outputTableViewer.getTable().setLayoutData(data);
		createProductQuantityColumns(outputsGroup, outputTableViewer);
		outputTableViewer.getTable().setHeaderVisible(true);
		outputTableViewer.getTable().setLinesVisible(true);
		outputTableViewer.setContentProvider(new IStructuredContentProvider() {

			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
				viewer.refresh();
			}

			@Override
			public void dispose() {
				// TODO Auto-generated method stub

			}

			@Override
			public Object[] getElements(Object inputElement) {
				return ((List<?>) inputElement).toArray();
			}
		});
		outputTableViewer.setInput(outputs);
		outputTableViewer.setSorter(new ViewerSorter(){
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				return ((Product) e1).getName().compareTo( ((Product) e2).getName());
			}
		}) ;

		Composite outputButtonComposite = new Composite(outputsGroup, SWT.NONE);
		gridLayout = new GridLayout(1, false);
		outputButtonComposite.setLayout(gridLayout);
		data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		data.horizontalSpan = 2;
		outputButtonComposite.setLayoutData(data);

		addOutputButton = new Button(outputButtonComposite, SWT.PUSH);
		addOutputButton.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));
		addOutputButton.setText(getString("NewOperationWizardFirstPage.add"));
		addOutputButton.addSelectionListener(this);

		removeOutputButton = new Button(outputButtonComposite, SWT.PUSH);
		removeOutputButton.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));
		removeOutputButton.setText(getString("NewOperationWizardFirstPage.remove"));
		removeOutputButton.addSelectionListener(this);


		// -------------- Input Part ----------------//
		Group inputsGroup = new Group(composite, SWT.NONE);
		inputsGroup.setText(getString("NewOperationWizardFirstPage.inputs"));
		gridLayout = new GridLayout(3, false);
		inputsGroup.setLayout(gridLayout);
		data = new GridData(GridData.FILL, GridData.CENTER, true, false);
		data.horizontalSpan = 3;
		inputsGroup.setLayoutData(data);



		inputProductComboViewer = new ComboViewer(inputsGroup, SWT.READ_ONLY);
		inputProductComboViewer.getCombo().setLayoutData( new GridData (SWT.FILL, SWT.CENTER, true, false));
		inputProductComboViewer.setContentProvider(new ArrayContentProvider());
		inputProductComboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Product){
					return ((Product) element).getName();
				}
				return super.getText(element);
			}

		});
		inputProductComboViewer.setInput(EntityList.getProductList(Project.getCurrentProject().getOriginalFactory()));
		inputProductComboViewer.setSorter(new ViewerSorter(){
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				return ((Product) e1).getName().compareTo( ((Product) e2).getName());
			}
		}) ;
		inputProductComboViewer.setSelection(new StructuredSelection((Product) object));

		inputQuantityText = new Text(inputsGroup, SWT.BORDER);
		data = new GridData(SWT.FILL, SWT.CENTER, true, false);
		inputQuantityText.setLayoutData(data);

		itemsLabel = new Label(inputsGroup, SWT.NONE);
		itemsLabel.setText("St�ck");
		itemsLabel.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));

		inputTableViewer = new TableViewer(inputsGroup, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		data = new GridData (SWT.FILL, SWT.FILL, true, false);
		tableHeight = inputTableViewer.getTable().getItemHeight() * 5;
		trim = inputTableViewer.getTable().computeTrim(0, 0, 0, tableHeight);
		data.heightHint = trim.height;
		inputTableViewer.getTable().setLayoutData(data);
		createProductQuantityColumns(inputsGroup, inputTableViewer);
		inputTableViewer.getTable().setHeaderVisible(true);
		inputTableViewer.getTable().setLinesVisible(true);
		inputTableViewer.setContentProvider(new IStructuredContentProvider() {

			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
				viewer.refresh();
			}

			@Override
			public void dispose() {
				// TODO Auto-generated method stub

			}

			@Override
			public Object[] getElements(Object inputElement) {
				return ((List<?>) inputElement).toArray();
			}
		});
		inputTableViewer.setInput(inputs);
		//TODO: add sorter

		Composite inputButtonComposite = new Composite(inputsGroup, SWT.NONE);
		gridLayout = new GridLayout(1, false);
		inputButtonComposite.setLayout(gridLayout);
		inputButtonComposite.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false));

		addInputButton = new Button(inputButtonComposite, SWT.PUSH);
		addInputButton.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));
		addInputButton.setText(getString("NewOperationWizardFirstPage.add"));
		addInputButton.addSelectionListener(this);

		removeInputButton = new Button(inputButtonComposite, SWT.PUSH);
		removeInputButton.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));
		removeInputButton.setText(getString("NewOperationWizardFirstPage.remove"));
		removeInputButton.addSelectionListener(this);


		// -------------- Production Time Part ----------------//
		final Label productionTimeLabel = new Label (composite, SWT.NONE);
		productionTimeLabel.setText (getString("NewOperationWizardFirstPage.productionTime"));

		productionTimeText = new Text(composite, SWT.BORDER);
		productionTimeText.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));

		final Label productionSecondLabel = new Label (composite, SWT.NONE);
		productionSecondLabel.setText(getString("NewOperationWizardFirstPage.seconds"));
		productionSecondLabel.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));

		// -------------- Lot Size Part ----------------//
		final Label lotSizeLabel = new Label (composite, SWT.NONE);
		lotSizeLabel.setText (getString("NewOperationWizardFirstPage.lotSize"));

		lotSizeText = new Text(composite, SWT.BORDER);
		lotSizeText.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));

		final Label lotSizeItemsLabel = new Label (composite, SWT.NONE);
		lotSizeItemsLabel.setText(getString("NewOperationWizardFirstPage.piece"));
		lotSizeItemsLabel.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));

		// -------------- Scrap Rate Part ----------------//
		final Label scrapRateLabel = new Label (composite, SWT.NONE);
		scrapRateLabel.setText (getString("NewOperationWizardFirstPage.scrapRate"));

		scrapRateText = new Text(composite, SWT.BORDER);
		scrapRateText.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));

		final Label scrapRatePercent = new Label (composite, SWT.NONE);
		scrapRatePercent.setText(getString("NewOperationWizardFirstPage.percent"));
		scrapRatePercent.setLayoutData(new GridData (SWT.BEGINNING, SWT.CENTER, false, false));
		setControl(composite);
	}


	private WorkplaceState getFirstSelectedWorkplaceState(Workplace workplace) {
		for (WorkplaceState workplaceState : workplace.getStates()) {
			for (Operation operation : workplaceState.getOperations()) {
				for (Product product : operation.getOutputProducts()) {
					if (product.equals((Product) object)) {
						return workplaceState;
					}
				}
			}
		}
		return null;
	}
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub

	}


	private void refreshWorkplaceStates() {
		IStructuredSelection structuredSelection = (IStructuredSelection) workplaceComboViewer.getSelection();
		if (!structuredSelection.isEmpty()) {
			Workplace selectedWorkplace = (Workplace) structuredSelection.getFirstElement();
			workplaceStateComboViewer.setInput(selectedWorkplace.getStates());
			if (getFirstSelectedWorkplaceState(selectedWorkplace) != null)
				workplaceStateComboViewer.setSelection(new StructuredSelection(getFirstSelectedWorkplaceState(selectedWorkplace)));
		}
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
		if (event.getSource().equals(workplaceComboViewer.getCombo())) {
			refreshWorkplaceStates();
		}
		else if (event.getSource().equals(newWPSRadio)) {
			newWPSRadio.setSelection(true);
			workplaceStateComboViewer.getCombo().setEnabled(false);
			newWorkplaceStateComposite.setEnabled(true);
			for(int i = 0; i < newWorkplaceStateComposite.getChildren().length; i++) {
				newWorkplaceStateComposite.getChildren()[i].setEnabled(true);
			}
		}
		else if (event.getSource().equals(existingWPSRadio)) {
			newWPSRadio.setSelection(false);
			workplaceStateComboViewer.getCombo().setEnabled(true);
			newWorkplaceStateComposite.setEnabled(false);
			for(int i = 0; i < newWorkplaceStateComposite.getChildren().length; i++) {
				newWorkplaceStateComposite.getChildren()[i].setEnabled(false);
			}
			refreshWorkplaceStates();
		}
		else if (event.getSource().equals(addOutputButton)) {
			IStructuredSelection structuredSelection = (IStructuredSelection) outputProductComboViewer.getSelection();
			if (!structuredSelection.isEmpty() && !outputQuantityText.equals("")) {
				Product selectedProduct = (Product) structuredSelection.getFirstElement();
				int quantity = Integer.parseInt(outputQuantityText.getText());
				outputs.add(new ProductQuantity(selectedProduct, quantity));
				outputTableViewer.refresh();
			}
		}
		else if (event.getSource().equals(removeOutputButton)){
			IStructuredSelection structuredSelection = (IStructuredSelection) outputTableViewer.getSelection();
			outputs.removeAll(structuredSelection.toList());
			outputTableViewer.refresh();
		}
		else if (event.getSource().equals(addInputButton)) {
			IStructuredSelection structuredSelection = (IStructuredSelection) inputProductComboViewer.getSelection();
			if (!structuredSelection.isEmpty() && !inputQuantityText.equals("")) {
				Product selectedProduct = (Product) structuredSelection.getFirstElement();
				int quantity = Integer.parseInt(inputQuantityText.getText());
				inputs.add(new ProductQuantity(selectedProduct, quantity));
				inputTableViewer.refresh();
			}
		}
		else if (event.getSource().equals(removeInputButton)) {
			IStructuredSelection structuredSelection = (IStructuredSelection) inputTableViewer.getSelection();
			inputs.removeAll(structuredSelection.toList());
			inputTableViewer.refresh();
		}

	}

	public void assignSelectedComponents() {
		IStructuredSelection structuredSelection = (IStructuredSelection) workplaceComboViewer.getSelection();
		if (!structuredSelection.isEmpty()) {
			workplace = (Workplace) structuredSelection.getFirstElement();

		}

		if (existingWPSRadio.getSelection()) {
			structuredSelection = (IStructuredSelection) workplaceStateComboViewer.getSelection();
			if (!structuredSelection.isEmpty()) {
				workplaceState = (WorkplaceState) structuredSelection.getFirstElement();
			}

		}
		else {
			workplaceState = new WorkplaceState();
			workplaceState.setRetoolingCost(Integer.parseInt(retoolingCostText.getText()));
			workplaceState.setRetoolingTime(Integer.parseInt(retoolingTimeText.getText()));
			workplace.getStates().add(workplaceState);
			workplaceState.setWorkplace(workplace);
		}
		productionTime = Double.parseDouble(productionTimeText.getText());
		lotSize = Integer.parseInt(lotSizeText.getText());
		scrapRate = Double.parseDouble(scrapRateText.getText());
	}


	public int getLotSize() {
		return lotSize;
	}

	public WorkplaceState getWorkplaceState() {
		return workplaceState;
	}

	public List<ProductQuantity> getOutputs() {
		return outputs;
	}

	public List<ProductQuantity> getInputs() {
		return inputs;
	}

	public double getProductionTime() {
		return productionTime;
	}

	public void setProductionTime(double productionTime) {
		this.productionTime = productionTime;
	}


	public double getScrapRate() {
		return scrapRate;
	}

	private void createProductQuantityColumns(final Composite parent, final TableViewer viewer) {
		String titles[] = {getString("NewOperationWizardFirstPage.product"), getString("NewOperationWizardFirstPage.quantity")};
		int[] bounds = {300, 100};

		//Product column
		TableViewerColumn col = createTableViewerColumn(viewer, titles[0], bounds[0], 0);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				ProductQuantity pq = (ProductQuantity) element;
				return pq.getProduct().getName();
			}
		});

		//quantity column
		col = createTableViewerColumn(viewer, titles[1], bounds[1], 1);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				ProductQuantity pq = (ProductQuantity) element;
				return Integer.toString(pq.getQuantity());
			}
		});
	}

	private TableViewerColumn createTableViewerColumn(TableViewer viewer, String title, int bound, final int colNumber) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(viewer,
				SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	}


}

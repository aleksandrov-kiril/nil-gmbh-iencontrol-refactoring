package de.nilgmbh.app.ui.depricated;

import java.util.Calendar;
import java.util.Date;
import java.util.Map.Entry;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;

import de.fzi.wenpro.core.model.Workplace;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;

public class ChartCreator {
	private final String title;
	private final String xLabel;
	private final String yLabel;
	private final boolean hasLegend = true;
	private final boolean hasTooltips = true; 
	private final boolean hasUrls = false;

	public ChartCreator(String title, String xLabel, String yLabel) {
		this.title = title;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
	}

	public ChartCreator() {
		this.title = "Energieverbrauch";
		this.xLabel = "";
		this.yLabel = "kW";
	}

	public JFreeChart getChart(ProductionPlan plan) {
		TimeSeries s = new TimeSeries("Energieverbrauch");
		TimeSeriesCollection data = new TimeSeriesCollection();

		Calendar c = (Calendar) plan.getPlanStartDate().clone();
		long startTime = c.getTimeInMillis();

		//FIXME: Assuming that profiles are sorted
		for (Entry<Double, Double> e : plan.getEnergyProfile()) {
			long currTime = startTime + e.getKey().longValue() * 1000;
			Second sec = new Second(new Date(currTime));
			TimeSeriesDataItem i = new TimeSeriesDataItem(sec, e.getValue());
			s.add(i);
		}
		data.addSeries(s);

		JFreeChart chart = ChartFactory.createXYStepChart(title, xLabel, yLabel, data, PlotOrientation.VERTICAL, hasLegend, hasTooltips, hasUrls);
		return chart;
	}

	public JFreeChart getChart(ProductionPlan plan, Workplace wp) {
		TimeSeries s = new TimeSeries("Energieverbrauch");
		TimeSeriesCollection data = new TimeSeriesCollection();

		Calendar c = (Calendar) plan.getPlanStartDate().clone();
		long startTime = c.getTimeInMillis();

		//FIXME: Assuming that profiles are sorted
		if (plan.getEnergyProfiles().get(wp)!=null)
			for (Entry<Double, Double> e : plan.getEnergyProfiles().get(wp)) {
				long currTime = startTime + e.getKey().longValue() * 1000;
				Second sec = new Second(new Date(currTime));
				s.addOrUpdate(sec, e.getValue());
//				TimeSeriesDataItem i = new TimeSeriesDataItem(sec, e.getValue());
//				s.add(i);
			}
		data.addSeries(s);

		JFreeChart chart = ChartFactory.createXYStepChart(title + " " + wp.getName(), xLabel, yLabel, data, PlotOrientation.VERTICAL, hasLegend, hasTooltips, hasUrls);
		return chart;
	}
}

package de.nilgmbh.app.ui.common.dnd;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;

import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.ProductQuantity;


public class TreeDragListener implements DragSourceListener, DropTargetListener {
	private TreeViewer viewer;

	public TreeDragListener (TreeViewer viewer){
		this.viewer = viewer;
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		Product product = null;
		if (selection.getFirstElement() instanceof Product)
			product = (Product) selection.getFirstElement();
		else if (selection.getFirstElement() instanceof ProductQuantity)
			product = ((ProductQuantity) selection.getFirstElement()).getProduct();
		event.data = product.getName();
		event.doit = true;
		DNDContainer.data = product;	
	} 

	@Override
	public void dragStart(DragSourceEvent event) {	
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		Product product = null;
		if (selection.getFirstElement() instanceof Product)
			product = (Product) selection.getFirstElement();
		else if (selection.getFirstElement() instanceof ProductQuantity)
			product = ((ProductQuantity) selection.getFirstElement()).getProduct();
		event.data = product.getName();
		event.doit = true;
		DNDContainer.data = product;		


		//		}
	}

	@Override
	public void dragEnter(DropTargetEvent event) {

	}

	@Override
	public void dragLeave(DropTargetEvent event) {
	

	}

	@Override
	public void dragOperationChanged(DropTargetEvent event) {


	}

	@Override
	public void dragOver(DropTargetEvent event) {

	}

	@Override
	public void drop(DropTargetEvent event) {


	}

	@Override
	public void dropAccept(DropTargetEvent event) {


	}

}

package de.nilgmbh.app.ui.common;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ICheckStateProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;

import de.fzi.wenpro.core.model.Factory;
import de.fzi.wenpro.core.model.Order;
import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.util.CalendarUtil;
import de.fzi.wenpro.util.Numbers;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.ui.dialogs.NewOrderDialog;

public class OrderViewer extends Composite {

	private static final boolean moveable = false;
	private static final boolean resizable = true;

	private final String[] columnNames = new String[] { "Zustand", getString("OrderView.product"),
			getString("OrderView.quantity"), getString("OrderView.startPeriod"),
			getString("OrderView.finishingPeriod") };

	// private TableViewer ordersTableViewer;
	private CheckboxTableViewer ordersCheckBoxTableViewer;

	public CheckboxTableViewer getOrdersCheckBoxTableViewer() {
		return ordersCheckBoxTableViewer;
	}

	public OrderViewer(Composite parent, boolean canEdit) {
		super(parent, SWT.NONE);
		this.setLayout(GridLayoutFactory.swtDefaults().numColumns(5).equalWidth(false).create());
		createOrdersTable(this, canEdit);
		createButtons(this, canEdit);
	}

	public OrderViewer(Composite parent, int h, int w, boolean canEdit) {
		super(parent, SWT.NONE);
		this.setLayout(GridLayoutFactory.swtDefaults().create());
		createOrdersTable(this, canEdit);
		createButtons(this, canEdit);
	}

	private void createButtons(final OrderViewer orderViewer, boolean canEdit) {
		if (canEdit) {

			// create new order button
			GridDataFactory gdfButton = GridDataFactory.swtDefaults().hint(SWT.DEFAULT, SWT.DEFAULT)
			.minSize(150, SWT.DEFAULT);
			Button createNew = new Button(orderViewer, SWT.PUSH);
			createNew.setLayoutData(gdfButton.create());
			createNew.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {

					// if
					// (Project.getCurrentProject().getCurrentAlternativeFactory()
					// == null) {
					// if (Project.getCurrentProject().getOriginalFactory() ==
					// null) {
					//
					// MessageBox warning = new MessageBox(getShell());
					// warning.setMessage(getString("OrderView.noAlternativeFactory"));
					// warning.open();
					// return;
					// }

					NewOrderDialog dialog = new NewOrderDialog(e.display.getActiveShell(),
							getString("OrderView.newOrderDialog"));
					int returnCode = dialog.open();
					if (returnCode == Dialog.OK) {
						Order newOrder = dialog.getOrder();
						Project.getCurrentProject().getOriginalFactory().addOrder(newOrder);
						Activator.getNewOrders().add(newOrder);
						refresh();
					}

				}
			});
			createNew.setText(getString("OrderView.createNewOrder"));
			createNew.setToolTipText(getString("OrderView.createNewOrderTooltip"));

			// edit selected order button
			Button edit = new Button(orderViewer, SWT.PUSH);
			edit.setLayoutData(gdfButton.create());
			edit.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if (ordersCheckBoxTableViewer.getSelection().isEmpty()) {
						MessageBox warning = new MessageBox(getShell());
						warning.setMessage(getString("OrderView.noOrderSelected"));
						warning.open();
						return;
					} else {
						NewOrderDialog dialog = new NewOrderDialog(e.display.getActiveShell(),getString("OrderView.editOrderDialog"));
						dialog.setInput((Order) ((IStructuredSelection) ordersCheckBoxTableViewer.getSelection()).getFirstElement());
						int returnCode = dialog.open();
						if (returnCode == Dialog.OK) {
							update(dialog.getOrder());
						}
					}
				}
			});
			edit.setText(getString("OrderView.editOrder"));
			edit.setToolTipText(getString("OrderView.editOrderTooltip"));

			// delete selected order button
			Button delete = new Button(orderViewer, SWT.PUSH);
			delete.setLayoutData(gdfButton.create());
			delete.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					// if (Project.getCurrentProject()
					// .getCurrentAlternativeFactory() == null) {
					// MessageBox warning = new MessageBox(getShell());
					// warning.setMessage(getString("OrderView.noAlternativeFactory"));
					// warning.open();
					// return;
					// }
					if (ordersCheckBoxTableViewer.getCheckedElements().length==0) {
						MessageBox warning = new MessageBox(getShell());
						warning.setMessage(getString("OrderView.noOrderSelected"));
						warning.open();
						return;
					} else {
						List<Object> selectedOrder = Arrays.asList(ordersCheckBoxTableViewer.getCheckedElements());
						Project.getCurrentProject().getOriginalFactory().getOrders().removeAll(selectedOrder);
						Activator.getNewOrders().remove(selectedOrder);
						refresh();
					}
				}
			});
			delete.setText(getString("OrderView.deleteOrder"));
			delete.setToolTipText(getString("OrderView.deleteOrderTooltip"));

			// select all order button
			Button selectAll = new Button(orderViewer, SWT.PUSH);
			selectAll.setLayoutData(gdfButton.create());
			selectAll.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					ordersCheckBoxTableViewer.setAllChecked(true);
				}
			});
			selectAll.setText(getString("OrderView.selectAll"));
			selectAll.setToolTipText(getString("OrderView.selectAllTooltip"));
			
			// select none order button
			Button selectNone = new Button(orderViewer, SWT.PUSH);
			selectNone.setLayoutData(gdfButton.create());
			selectNone.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					ordersCheckBoxTableViewer.setAllChecked(false);
				}
			});
			selectNone.setText(getString("OrderView.selectNone"));
			selectNone.setToolTipText(getString("OrderView.selectNoneTooltip"));
		}
	}

	public void createOrdersTable(Composite parent, boolean canEdit) {

		this.ordersCheckBoxTableViewer = CheckboxTableViewer.newCheckList(parent,
				SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION );
		this.ordersCheckBoxTableViewer.getTable().setLayoutData(
				GridDataFactory.fillDefaults().span(5, 1)
						.grab(true, true).create());
		this.ordersCheckBoxTableViewer.getTable().setLinesVisible(true);
		this.ordersCheckBoxTableViewer.getTable().setHeaderVisible(true);
	
	    
		TableViewerColumn col1 = new TableViewerColumn(ordersCheckBoxTableViewer, SWT.BEGINNING);
		col1.getColumn().setWidth(30);
		col1.getColumn().setText(columnNames[0]);
		col1.getColumn().setMoveable(moveable);
		col1.getColumn().setResizable(resizable);
		col1.getColumn().setResizable(resizable);

		TableViewerColumn col2 = new TableViewerColumn(ordersCheckBoxTableViewer, SWT.CENTER);
		col2.getColumn().setWidth(150);
		col2.getColumn().setText(columnNames[1]);
		col2.getColumn().setMoveable(moveable);
		col2.getColumn().setResizable(resizable);
		TableViewerColumn col3 = new TableViewerColumn(ordersCheckBoxTableViewer, SWT.CENTER);
		col3.getColumn().setWidth(150);
		col3.getColumn().setText(columnNames[2]);
		col3.getColumn().setMoveable(moveable);
		col3.getColumn().setResizable(resizable);
		TableViewerColumn col4 = new TableViewerColumn(ordersCheckBoxTableViewer, SWT.CENTER);
		col4.getColumn().setWidth(150);
		col4.getColumn().setText(columnNames[3]);
		col4.getColumn().setMoveable(moveable);
		col4.getColumn().setResizable(resizable);
		TableViewerColumn col5 = new TableViewerColumn(ordersCheckBoxTableViewer, SWT.CENTER);
		col5.getColumn().setWidth(150);
		col5.getColumn().setText(columnNames[4]);
		col5.getColumn().setMoveable(moveable);
		col5.getColumn().setResizable(resizable);

		this.ordersCheckBoxTableViewer.setContentProvider(new ArrayContentProvider());
		this.ordersCheckBoxTableViewer.setLabelProvider(new OrdersLabelProvider());
		this.ordersCheckBoxTableViewer.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
			}
		});
		

		this.ordersCheckBoxTableViewer.setCheckStateProvider(new ICheckStateProvider() {

			@Override
			public boolean isGrayed(Object element) {
				Calendar now = Calendar.getInstance();
				Order order = (Order) element;
				return order.getEndDate().after(now);
			}

			@Override
			public boolean isChecked(Object element) {
				Order order = (Order) element;
				return !Activator.getNewOrders().contains(order);

			}
		});
		
	
		if (canEdit) {
			this.ordersCheckBoxTableViewer.addDoubleClickListener(new IDoubleClickListener() {

				@Override
				public void doubleClick(DoubleClickEvent event) {
					NewOrderDialog dialog = new NewOrderDialog(event.getViewer().getControl()
							.getShell(), getString("OrderView.editOrderDialog"));
					dialog.setInput((Order) ((IStructuredSelection) event.getSelection())
							.getFirstElement());
					int returnCode = dialog.open();
					if (returnCode == Dialog.OK) {
						// Project.getCurrentProject().getCurrentAlternativeFactory().addOrder(dialog.getOrder());
						update(dialog.getOrder());
					}
				}
			});
		}

	}

	public void setInput(Factory f) {
		this.ordersCheckBoxTableViewer.setInput(f.getOrders());
	}

	public void refresh() {
		this.ordersCheckBoxTableViewer.refresh();
		// layout();
	}

	public void update(Order element){
		this.ordersCheckBoxTableViewer.update(element, null);
	}
	
	public void cleanInput() {
		this.ordersCheckBoxTableViewer.setInput(new ArrayList<Order>());
		this.ordersCheckBoxTableViewer.refresh();
	}

	private class OrdersLabelProvider extends LabelProvider implements ITableLabelProvider {

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {

			if (!(element instanceof Order)) // possibly exception
				return "";
			Order order = (Order) element;
			switch (columnIndex) {
			case 0:
				return "";
			case 1:
				return order.getProduct().getName() != null ? order.getProduct().getName()
						: "unknown product";
			case 2:
				return Numbers.formatPiecesUnit(order.getQuantity());
			case 3:
				return CalendarUtil.getDateTextNice(order.getStartDate());
			case 4:
				return CalendarUtil.getDateTextNice(order.getEndDate());
			default:
				return "";
			}

		}

	}

}

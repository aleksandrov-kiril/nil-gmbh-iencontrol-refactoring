package de.nilgmbh.app.ui.common;


/**
 * @author Kiril Aleksandrov
 *
 */
public interface InputUpdate {
	
	public enum UpdateType{
		
		ALTERNATIVE_MODIFIED,
		NEW_ALTERNATIVE,
		SWITCH_ALTERNATIVE,
		SWITCH_PERSPECTIVE, 
		PROJECT_LOADED
		
	}
	
	public void updateInput(UpdateType type);
}
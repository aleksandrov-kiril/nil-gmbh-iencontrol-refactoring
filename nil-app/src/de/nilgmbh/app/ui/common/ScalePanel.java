/**
 * 
 */
package de.nilgmbh.app.ui.common;

import static de.nilgmbh.app.i18n.Messagesi18n.getString;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;

import de.jaret.util.ui.timebars.swt.TimeBarViewer;


/**
 * @author Kiril Aleksandrov
 *
 */
public class ScalePanel extends Composite {
	
	private TimeBarViewer _tbv;


	public ScalePanel(Composite parent, int style,TimeBarViewer tbv) {
		super(parent, style);
		_tbv = tbv;
		createControls(this);
		
	}
	
	private void createControls(ScalePanel panel) {
		
		GridLayout gridLayout = new GridLayout(2, false);
		panel.setLayout(gridLayout);		
		Label scaleLabel = new Label(this, SWT.NONE);
		scaleLabel.setText(getString("FeinScheduleView.scale")+" : ");		
		final Scale pixPerSecondsScale = new Scale(this, SWT.HORIZONTAL);
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		pixPerSecondsScale.setLayoutData(gridData);		
		pixPerSecondsScale.setMaximum(1000);
		pixPerSecondsScale.setMinimum(1);
		if (_tbv.getPixelPerSecond() * (24.0 * 60.0 * 60.0) > 700) {
			pixPerSecondsScale.setMaximum((int) (_tbv.getPixelPerSecond() * (24.0 * 60.0 * 60.0)));
		}
		pixPerSecondsScale.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent ev) {
				int val = pixPerSecondsScale.getSelection();
				double pps = ((double) val) / (24.0 * 60.0 * 60.0);
				_tbv.setPixelPerSecond(pps);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
		pixPerSecondsScale.setSelection((int) (_tbv.getPixelPerSecond() * (24.0 * 60.0 * 60.0)));


	}

		
	

}

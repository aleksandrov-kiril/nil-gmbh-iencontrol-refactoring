package de.nilgmbh.app.i18n;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 16.04.2015
 */
public class Messagesi18n {
	private static String BUNDLE_NAME = "de.nilgmbh.app.i18n.messages";
	
	private Messagesi18n(){
	}
	
	// languanges of the plugin
	private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME, Locale.GERMAN);
//	private static ResourceBundle rb = ResourceBundle.getBundle(BUNDLE_NAME, Locale.ENGLISH);


	
	public static String getString(String key){
		try{
			return rb.getString(key);
		}
		catch(MissingResourceException e){
			return e + "!"; 
		}
	}
	
}

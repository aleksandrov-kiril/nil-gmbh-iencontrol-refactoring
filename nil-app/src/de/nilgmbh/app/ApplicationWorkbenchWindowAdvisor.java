package de.nilgmbh.app;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.internal.part.NullEditorInput;

import de.nilgmbh.app.perspectives.Perspective;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {
	
	private static final String PERSPECTIVE_ID = Perspective.ID;

	public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	public ActionBarAdvisor createActionBarAdvisor(
			IActionBarConfigurer configurer) {
		return new ApplicationActionBarAdvisor(configurer);
	}

	public static String getPerspectiveId() {
		return PERSPECTIVE_ID;
	}

	public void preWindowOpen() {
		final IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
		Display display = Display.getCurrent();
		if (display == null){
			configurer.setInitialSize(new Point(1200,600));
		}else{
			configurer.setInitialSize(new Point(display.getBounds().width, display.getBounds().height));
		}
		configurer.setShowCoolBar(false);
//		configurer.setShowPerspectiveBar(true);
		configurer.setShowStatusLine(false);
		configurer.setTitle("Energie-Prozessplaner");
		
		//TODO: do not hard code this
//		String pbar = "de.nilgmbh.app.i18n.app.perspectives.perspective, de.nilgmbh.app.i18n.app.perspectives.energymonitoringperspective, " +
//				"de.nilgmbh.app.i18n.app.perspectives.energyprognoseperspective ";
//		PlatformUI.getWorkbench().getPerspectiveRegistry().getPerspectives();
//		PlatformUI.getPreferenceStore().setDefault(IWorkbenchPreferenceConstants.PERSPECTIVE_BAR_EXTRAS, pbar);
//		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.PERSPECTIVE_BAR_EXTRAS, pbar);

		
		
	}
	
//	public void postWindowOpen() {
//		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
//		try {
//			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(new NullEditorInput(), "de.nilgmbh.app.i18n.layout.mygraphicaleditor");
//		} catch (PartInitException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
////	}
	
}

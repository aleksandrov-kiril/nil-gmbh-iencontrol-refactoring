package de.nilgmbh.app.internalmodel;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public class WarningViewerFilter extends ViewerFilter {
	
	private String searchString;

	public void setSearchText(String s) {
		// Search must be a substring of the existing value
		this.searchString = ".*" + s + ".*";
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if(searchString == null || searchString.length() ==0){
			return true;
		}
		WarningRow wr = (WarningRow) element;
		if (wr.getName().toString().matches(searchString)){
			return true;
		}
		if (wr.getType().toString().matches(searchString)){
			return true;
		}
		if (wr.getWorkplace().toString().matches(searchString)){
			return true;
		}
		if (wr.getMessage().toString().matches(searchString)){
			return true;
		}
		return false;
	}

}

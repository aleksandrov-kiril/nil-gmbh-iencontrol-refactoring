package de.nilgmbh.app.internalmodel;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import de.nilgmbh.app.internalmodel.WarningRow;

public class WarningViewerComparator extends ViewerComparator {
	private int propertyIndex;
	private static final int DESCENDING = 1;
	private int direction = DESCENDING;
	
	public WarningViewerComparator(){
		this.propertyIndex =0;
		direction = DESCENDING;
	}
	
	public void setColumn (int column){
		if (column == this.propertyIndex){
			direction = 1- direction;
		}
		else{
			this.propertyIndex = column;
			direction = DESCENDING;
		}
	}
	
	@Override
	public int compare (Viewer viewer, Object e1, Object e2){
		
		WarningRow wr1 = (WarningRow) e1;
		WarningRow wr2 = (WarningRow) e2;
		int rc = 0;
		switch(propertyIndex){
		case 0 :
			rc = wr1.getName().toString().compareTo(wr2.getName().toString());
			break;
		case 1 :
			rc = wr1.getType().toString().compareTo(wr2.getType().toString());
			break;
		case 2 :
			rc = wr1.getWorkplace().toString().compareTo(wr2.getWorkplace().toString());
			break;
		case 3 :
			rc = wr1.getMessage().toString().compareTo(wr2.getMessage().toString());
			break;
		default :
			rc = 0;			
		}	
		if (direction == DESCENDING) {
			rc = -rc;
		}
		return rc;
	}
}

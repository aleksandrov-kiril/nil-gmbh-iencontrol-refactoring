package de.nilgmbh.app.internalmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.fzi.wenpro.core.model.Project;
import de.fzi.wenpro.core.model.productionplan.ProductionPlan;

public class WertpronetProjectPlan implements Serializable{

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -5819706982108025220L;
	
	private Project p;
	private List<ProductionPlan> pps;
	
	public WertpronetProjectPlan(){
		pps = new ArrayList<ProductionPlan>();
	}
	
	public WertpronetProjectPlan(Project p, List<ProductionPlan> pps){
		this.p = p;
		this.pps = pps;
	}
	
	public Project getWertpronetProject() {
		return p;
	}
	
	public List<ProductionPlan> getProducionPlans() {
		return pps;
	}
}

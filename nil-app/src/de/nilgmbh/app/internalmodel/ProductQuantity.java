package de.nilgmbh.app.internalmodel;

import de.fzi.wenpro.core.model.Product;

public class ProductQuantity {
	private Product product;
	private int quantity;
	
	public ProductQuantity (Product product, int quantity){
		this.product = product;
		this.quantity = quantity;
	}
	public Product getProduct() {
		return product;
	}
	public int getQuantity() {
		return quantity;
	}

}

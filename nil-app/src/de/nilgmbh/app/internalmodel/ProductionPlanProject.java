/**
 * 
 */
package de.nilgmbh.app.internalmodel;

import java.util.ArrayList;
import java.util.List;

import de.jaret.util.date.JaretDate;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.ui.jaretimpl.ProductionPlanTimeBarModel;

/**
 * This class is a singleton that represents a production plan project.
 * 
 * @author Kiril Aleksandrov
 *
 */
public class ProductionPlanProject {
	private  static ProductionPlanProject currentProductionPlanProject;
	private static JaretDate currentDate = new JaretDate(Activator.getCurrentTime().getTime());
	private List<ProductionPlanTimeBarModel> productionPlanTimeBarModels;
	private ProductionPlanTimeBarModel currentProductionPlanTimeBarModel;
	
	public ProductionPlanProject() {
		this.productionPlanTimeBarModels = new ArrayList<ProductionPlanTimeBarModel>();
	}
	
	public ProductionPlanProject(ProductionPlanTimeBarModel input) {
		this();

		this.currentProductionPlanTimeBarModel = input;
	}
	
	public static ProductionPlanProject getCurrentProductionPlanProject() {
		if (currentProductionPlanProject == null)
			currentProductionPlanProject = new ProductionPlanProject(null);
		return currentProductionPlanProject;
	}
	public static void setCurrentProductionPlanProject(
			ProductionPlanProject currentProductionPlanProject) {
		ProductionPlanProject.currentProductionPlanProject = currentProductionPlanProject;
	}

	public List<ProductionPlanTimeBarModel> getProductionPlanTimeBarModels() {
		return productionPlanTimeBarModels;
	}
	public void setProductionPlanTimeBarModels(
			List<ProductionPlanTimeBarModel> productionPlanTimeBarModels) {
		this.productionPlanTimeBarModels = productionPlanTimeBarModels;
	}

	public static JaretDate getCurrentDate() {
		return currentDate;
	}

	public static void setCurrentDate(JaretDate currentDate) {
		ProductionPlanProject.currentDate = currentDate;
	}

	public ProductionPlanTimeBarModel getCurrentProductionPlanTimeBarModel() {
		if(currentProductionPlanTimeBarModel==null)
			if(productionPlanTimeBarModels.size()>0)
				currentProductionPlanTimeBarModel = productionPlanTimeBarModels.get(0);
		return currentProductionPlanTimeBarModel;
	}

	public void setCurrentProductionPlanTimeBarModel(
			ProductionPlanTimeBarModel currentTimeBarModel) {
		this.currentProductionPlanTimeBarModel = currentTimeBarModel;
	}

}

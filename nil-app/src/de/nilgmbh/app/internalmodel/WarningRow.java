package de.nilgmbh.app.internalmodel;

//public class WarningRow {
//	private String name;
//	private String type;
//	private String workplace;
//	private String message;
//	
//	public WarningRow(String name, String type, String workplace, String message) {
//		super();
//		this.name = name;
//		this.type = type;
//		this.workplace = workplace;
//		this.message = message;
//	}
//	
//
//
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getType() {
//		return type;
//	}
//	public void setType(String type) {
//		this.type = type;
//	}
//	public String getWorkplace() {
//		return workplace;
//	}
//	public void setWorkplace(String workplace) {
//		this.workplace = workplace;
//	}
//	public String getMessage() {
//		return message;
//	}
//
//	public void setMessage(String message) {
//		this.message = message;
//	}
//}




public class WarningRow {
	private Object name;
	private Object type;
	private Object workplace;
	private Object message;
	
	public WarningRow(Object name, Object type, Object workplace, Object message) {
		super();
		this.name = name;
		this.type = type;
		this.workplace = workplace;
		this.message = message;
	}
	


	public Object getName() {
		return name;
	}
	public void setName(Object name) {
		this.name = name;
	}

	public Object getType() {
		return type;
	}
	public void setType(Object type) {
		this.type = type;
	}
	public Object getWorkplace() {
		return workplace;
	}
	public void setWorkplace(Object workplace) {
		this.workplace = workplace;
	}
	public Object getMessage() {
		return message;
	}

	public void setMessage(Object message) {
		this.message = message;
	}
}

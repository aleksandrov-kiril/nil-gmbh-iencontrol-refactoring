package de.nilgmbh.app.internalmodel;

import java.util.List;

import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;
import de.fzi.wenpro.core.model.WorkplaceState;

public class WorkplaceStateProduct {
	private Product product;
	private WorkplaceState ws;
	
	public WorkplaceStateProduct (Product product, WorkplaceState ws){
		this.product = product;
		this.ws = ws;
	}
	public Product getProduct() {
		return product;
	}
	public WorkplaceState getWorkplaceState() {
		return ws;
	}
	
	public List<Operation> getOperation(){
		return ws.getOperation(product);
	}

}

/**
 * 
 */
package de.nilgmbh.app.internalmodel;

/**
 * @author wicaksono
 *
 */
public class StrategyOption {
	private int strategyID;
	private String strategyName;
	
	
	public StrategyOption(int strategyID, String strategyName) {
		super();
		this.strategyID = strategyID;
		this.strategyName = strategyName;
	}
	public int getStrategyID() {
		return strategyID;
	}
	public void setStrategyID(int strategyID) {
		this.strategyID = strategyID;
	}
	public String getStrategyName() {
		return strategyName;
	}
	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}
	
	
	
}

package de.nilgmbh.app.internalmodel;

public class EnergyPropertyRow {
	private Object name;
	private double value;
	
	public EnergyPropertyRow(Object name, double value) {
		this.name = name;
		this.value = value;
	}
	public Object getName() {
		return name;
	}
	public void setName(Object name) {
		this.name = name;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
}

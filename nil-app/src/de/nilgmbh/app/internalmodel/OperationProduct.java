package de.nilgmbh.app.internalmodel;

import de.fzi.wenpro.core.model.Operation;
import de.fzi.wenpro.core.model.Product;

public class OperationProduct{
	private Product p;
	private Operation o;
	public OperationProduct(Product p, Operation o) {
		this.p = p;
		this.o = o;
	}
	
	public Product getProduct(){
		return p;
	}
	
	public Operation getOperation() {
		return o;
	}
}
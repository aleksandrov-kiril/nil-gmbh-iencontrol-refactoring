package de.nilgmbh.app.internalmodel;

public class FourColPropertyRow {
	Object firstColumn;
	Object secondColumn;
	Object thirdColumn;
	Object fourthColumn;
	
	public FourColPropertyRow(Object firstColumn, Object secondColumn){
		super();
		this.firstColumn = firstColumn;
		this.secondColumn = secondColumn;

	}
	
	public FourColPropertyRow(Object firstColumn, Object secondColumn, Object thirdColumn){
		super();
		this.firstColumn = firstColumn;
		this.secondColumn = secondColumn;
		this.thirdColumn = thirdColumn;
	}
	

	public FourColPropertyRow(Object firstColumn, Object secondColumn, Object thirdColumn, Object fourthColumn ){
		super();
		this.firstColumn = firstColumn;
		this.secondColumn = secondColumn;
		this.thirdColumn = thirdColumn;
		this.fourthColumn = fourthColumn;
	}


	public Object getFirstColumn() {
		return firstColumn;
	}


	public void setFirstColumn(Object firstColumn) {
		this.firstColumn = firstColumn;
	}


	public Object getSecondColumn() {
		return secondColumn;
	}


	public void setSecondColumn(Object secondColumn) {
		this.secondColumn = secondColumn;
	}


	public Object getThirdColumn() {
		return thirdColumn;
	}


	public void setThirdColumn(Object thirdColumn) {
		this.thirdColumn = thirdColumn;
	}


	public Object getFourthColumn() {
		return fourthColumn;
	}


	public void setFourthColumn(Object fourthColumn) {
		this.fourthColumn = fourthColumn;
	}


}

package de.nilgmbh.app;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.fzi.wenpro.core.model.Order;
import de.nilgmbh.app.Activator;
import de.nilgmbh.app.updatemechanism.UpdateListener;
import de.nilgmbh.app.updatemechanism.UpdateSource;
import de.nilgmbh.app.updatemechanism.UpdateType;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin implements UpdateSource {

	// The plug-in ID
	public static final String PLUGIN_ID = "nil-app";
	
	private List<UpdateListener> updateListeners; 
	
	private List<Order> newOrders = new ArrayList<Order>();
	
	private static final Calendar currentTime = Calendar.getInstance();
	
	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		super.initializeImageRegistry(reg);

        reg.put("productEnd", ImageDescriptor.createFromURL(FileLocator.find(getDefault().getBundle(),
        		new Path("icons/productEnd.png"), null)));
        reg.put("productIntermediate", ImageDescriptor.createFromURL(FileLocator.find(getDefault().getBundle(),
        		new Path("icons/productIntermediate.png"), null)));
        reg.put("metallRing", ImageDescriptor.createFromURL(FileLocator.find(getDefault().getBundle(),
        		new Path("icons/metall_ring.jpg"), null)));
   

	}

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
		updateListeners = new ArrayList<UpdateListener>();
//		Project project = new Project();
//		project.setOriginalFactory(Ecoflex.getExampleFactory());
//		project.setAlternativeFactoryList(Ecoflex.getAlternativeList());		
//		Project.setCurrentProject(project);
		
//		ProductionPlan productionPlan = new ProductionPlan();
//		ProductionPlanTimeBarModel newTimeBarModel = null;
//		newTimeBarModel = new ProductionPlanTimeBarModel(productionPlan);
//		ProductionPlanProject productionPlanProject = new ProductionPlanProject(newTimeBarModel);
//		ProductionPlanProject.setCurrentProductionPlanProject(productionPlanProject);
//		productionPlanProject = ProductionPlanProject.getCurrentProductionPlanProject();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	@Override
	public void registerUpdateListener(UpdateListener listener) {
		this.updateListeners.add(listener);
		
	}

	@Override
	public void unregisterUpdateListener(UpdateListener listener) {
		this.updateListeners.remove(listener);
		
	}

	@Override
	public void notifyAllListener(UpdateType type) {
		for (UpdateListener listener : updateListeners) {
			listener.update(type);
		}
		
	}

	public static List<Order> getNewOrders() {
		return getDefault().newOrders;
	}

	public static void clearNewOrderList() {
		getDefault().newOrders.clear();
	}

	public static Calendar getCurrentTime() {
		return currentTime;
	}
	

}
